# tool-spacemedia

Spacemedia is a [tool](https://spacemedia.toolforge.org/) that continuously harvests media libraries of various space agencies in order to find free media not yet uploaded to Wikimedia Commons.

See [documentation on Wikimedia Commons](https://commons.wikimedia.org/wiki/Commons:Spacemedia).
