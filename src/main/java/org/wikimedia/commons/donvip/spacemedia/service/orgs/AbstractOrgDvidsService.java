package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.wikimedia.commons.donvip.spacemedia.utils.CsvHelper.loadCsvMapping;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToLongFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsLocation;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaType;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiPageInfo;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiSearchResponse;
import org.wikimedia.commons.donvip.spacemedia.exception.ApiException;
import org.wikimedia.commons.donvip.spacemedia.exception.ImageNotFoundException;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService.MediaUpdateResult;
import org.wikimedia.commons.donvip.spacemedia.service.dvids.DvidsMediaProcessorService;
import org.wikimedia.commons.donvip.spacemedia.service.dvids.DvidsService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.utils.UnitedStates;
import org.wikimedia.commons.donvip.spacemedia.utils.UnitedStates.VirinTemplates;
import org.wikimedia.commons.donvip.spacemedia.utils.Utils;

/**
 * Service fetching images from https://api.dvidshub.net/
 */
@Service
public abstract class AbstractOrgDvidsService extends AbstractOrgService<DvidsMedia> {

    private static final String MEDIA_REMOVED_FROM_DVIDS = "Media removed from DVIDS";
    private static final String IMAGES_REMOVED_FROM_DVIDS = "Images removed from DVIDS";
    private static final String VIDEOS_REMOVED_FROM_DVIDS = "Videos removed from DVIDS";

    private static final Pattern US_MEDIA_BY = Pattern
            .compile(".*\\((U\\.S\\. .+ (?:photo|graphic|video) by )[^\\)]+\\)", Pattern.DOTALL);

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOrgDvidsService.class);

    protected static final Map<String, String> KEYWORDS_CATS = loadCsvMapping(
            AbstractOrgDvidsService.class, "dvids.keywords.csv");

    private static final String CN_WD = "People's Republic of China";
    private static final String GB_WD = "United Kingdom";
    private static final String US_WD = "United States";

    @Lazy
    @Autowired
    private DvidsMediaProcessorService dvidsProcessor;

    @Lazy
    @Autowired
    private DvidsService dvids;

    private final DvidsMediaRepository<DvidsMedia> dvidsRepository;

    private final SortedSet<String> countries;
    /** Countries where we can find more than 1000 images per month */
    private final SortedSet<String> topCountries;

    private final int minYear;

    private final boolean blocklist;

    private final boolean realCountries;

    private SortedSet<String> uiRepoIds;

    protected AbstractOrgDvidsService(DvidsMediaRepository<DvidsMedia> repository, String id, Set<String> units,
            Set<String> countries, Set<String> topCountries, int minYear, boolean blocklist) {
        this(repository, id, units, countries, topCountries, minYear, blocklist, true);
    }

    protected AbstractOrgDvidsService(DvidsMediaRepository<DvidsMedia> repository, String id, Set<String> units,
            Set<String> countries, Set<String> topCountries, int minYear, boolean blocklist, boolean realCountries) {
        super(repository, id, units);
        this.dvidsRepository = repository;
        this.countries = new TreeSet<>(countries.stream().map(x -> x.replace(':', ',')).collect(toSet()));
        this.topCountries = new TreeSet<>(topCountries);
        this.minYear = minYear;
        this.blocklist = blocklist;
        this.realCountries = realCountries;
    }

    @Override
    @PostConstruct
    void init() throws IOException {
        super.init();
        uiRepoIds = star(getRepoIds())
                ? new TreeSet<>(countries.stream()
                        .flatMap(x -> US_WD.equals(x) ? dvids.getStates(x).stream() : Stream.of(x)) // NOSONAR
                        .collect(toSet()))
                : getRepoIds();
    }

    @Override
    protected Class<DvidsMedia> getMediaClass() {
        return DvidsMedia.class;
    }

    @Override
    public String getOrgName(DvidsMedia media) {
        return dvids.getUnitName(media.getId().repoId());
    }

    @Override
    protected boolean checkBlocklist(DvidsMedia media) {
        return blocklist;
    }

    @Override
    public final void checkCommonsCategories() {
        checkCommonsCategories(KEYWORDS_CATS);
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "DVIDS files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected List<String> getReviewCategories(DvidsMedia media) {
        if ("United States".equals(media.getLocation().getCountry())) {
            return getMilitaryReviewCategories(media, UnitedStates.getUsStateCategory(media.getLocation().getState()));
        } else {
            return getMilitaryReviewCategories(media);
        }
    }

    @Override
    public SortedSet<String> getUiRepoIds() {
        return uiRepoIds;
    }

    @Override
    public String getUiRepoId(String repoId) {
        if (star(getRepoIds())) {
            try {
                return realCountries ? getRegionalIndicatorSymbol(
                        wikidata.searchCountry(fixCountry(repoId)).orElseThrow(() -> new IllegalStateException(repoId))
                                .qid())
                        : fixFakeCountry(repoId);
            } catch (IOException | RuntimeException e) {
                LOGGER.error("Failed to retrieve country {}", repoId, e);
                GlitchTip.capture(e);
            }
        }
        return super.getUiRepoId(repoId);
    }

    private boolean isUsState(String s) {
        return countries.contains(US_WD) && dvids.getStates(US_WD).contains(s);
    }

    private boolean isUsState(Set<String> set) {
        return set.stream().allMatch(this::isUsState);
    }

    private String fixCountry(String dvidsCountry) {
        return isUsState(dvidsCountry) ? US_WD
                : dvidsCountry.replace(" And ", " and ").replace("Guam", US_WD).replace("Virgin Islands, US", US_WD)
                .replace("Northern Mariana Islands", US_WD).replace("Puerto Rico", US_WD)
                .replace(" Minor Outlying Islands", "")
                .replace("Micronesia, Federated States Of", "Federated States of Micronesia")
                .replace("Cocos (Keeling) Islands", "Australia")
                .replace("Heard Island and McDonald Islands", "Australia").replace("New Caledonia", "France")
                .replace("Wallis and Futuna", "France").replace("Netherlands Antilles", "Netherlands")
                .replace("China", CN_WD).replace("Hong Kong", CN_WD).replace("Macau", CN_WD)
                .replace("Faroe Islands", "Denmark").replaceAll("^Congo$", "Republic of the Congo")
                .replace("Congo, The Democratic Republic of The", "Democratic Republic of the Congo")
                .replace("British Indian Ocean Territory", GB_WD).replace("Cayman Islands", GB_WD)
                .replace("Falkland Islands (Malvinas)", GB_WD)
                .replace("South Georgia and The South Sandwich Islands", GB_WD)
                .replace("Turks and Caicos Islands", GB_WD).replace("Virgin Islands, British", GB_WD)
                .replace("West Bank", "Palestine")
                .replace("Åland Islands", "Finland").replace("Sao Tome and Principe", "São Tomé and Príncipe");
    }

    private static final String fixFakeCountry(String dvidsCountry) {
        return dvidsCountry.replace(" Ocean", "").replace("Sea of ", "").replace("Bay of ", "").replace("Gulf of ", "")
                .replace("Strait of ", "").replace("Straits of ", "").replace(" Sea", "")
                .replace(" area of responsibility", "").replace("U.S. ", "");
    }

    protected SortedSet<String> filteredCountries(Predicate<String> filter) {
        return countries.stream().filter(filter).collect(toCollection(TreeSet::new));
    }

    @Override
    protected boolean shouldPostOnSocialMedia() {
        return false;
    }

    @Override
    public void updateMedia(String[] args) {
        LocalDateTime start = startUpdateMedia();
        Set<String> idsKnownToDvidsApi = new HashSet<>();
        List<DvidsMedia> uploadedMedia = new ArrayList<>();
        int count = 0;
        LocalDate doNotFetchEarlierThan = getRuntimeData().getDoNotFetchEarlierThan();
        LocalDate doNotFetchLaterThan = getRuntimeData().getDoNotFetchLaterThan();
        for (int year = ofNullable(doNotFetchLaterThan).orElseGet(LocalDate::now).getYear(); year >= minYear
                && (doNotFetchEarlierThan == null || year >= doNotFetchEarlierThan.getYear()); year--) {
            for (int month = year == Year.now().getValue() ? YearMonth.now().getMonthValue() : 12; month > 0; month--) {
                for (String country : countries) {
                    for (String state : dvids.getStates(country)) {
                        for (int day : getDays(year, month, country)) {
                            for (String unit : getRepoIdsFromArgs(args)) {
                                Pair<Integer, Collection<DvidsMedia>> update = updateDvidsMedia(unit, country, state,
                                        year, month, day, DvidsMediaType.image, idsKnownToDvidsApi);
                                if (shouldPostOnSocialMedia()) {
                                    uploadedMedia.addAll(update.getRight());
                                }
                                count += update.getLeft();
                                ongoingUpdateMedia(start, count);
                                if (mediaService.isVideosEnabled()) {
                                    update = updateDvidsMedia(unit, country, state, year, month, day,
                                            DvidsMediaType.video, idsKnownToDvidsApi);
                                    if (shouldPostOnSocialMedia()) {
                                        uploadedMedia.addAll(update.getRight());
                                    }
                                    count += update.getLeft();
                                    ongoingUpdateMedia(start, count);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (doNotFetchEarlierThan == null) {
            // Only delete pictures not found in complete updates
            deleteOldDvidsMedia(idsKnownToDvidsApi);
        }
        endUpdateMedia(count, uploadedMedia.size(), uploadedMedia, allMetadata(uploadedMedia),
                LocalDate.now().minusYears(1), null);
    }

    protected int[] getDays(int year, int month, String country) {
        if (topCountries.contains(country)) {
            int lastDayOfMonth = YearMonth.of(year, month).atEndOfMonth().getDayOfMonth();
            return IntStream.range(1, lastDayOfMonth).map(i -> lastDayOfMonth - i).toArray();
        }
        // Iterate at month level by default
        return new int[] { 0 };
    }

    private void deleteOldDvidsMedia(Set<String> idsKnownToDvidsApi) {
        // DVIDS API Terms of Service force us to check for deleted content
        // https://api.dvidshub.net/docs/tos
        for (DvidsMedia media : listMissingMedia()) {
            String id = media.getId().mediaId();
            if ((mediaService.isVideosEnabled() || !media.isVideo()) && !idsKnownToDvidsApi.contains(id)) {
                try {
                    refreshAndSaveById(media.getId().toString());
                } catch (IOException | ImageNotFoundException e) {
                    LOGGER.error(e.getMessage(), e);
                    GlitchTip.capture(e);
                }
            }
        }
    }

    private Pair<Integer, Collection<DvidsMedia>> updateDvidsMedia(String unit, String country, String state, int year,
            int month, int day, DvidsMediaType type, Set<String> idsKnownToDvidsApi) {
        List<DvidsMedia> uploadedMedia = new ArrayList<>();
        int count = 0;
        try {
            boolean loop = true;
            int page = 1;
            LocalDateTime start = LocalDateTime.now();
            count = 0;
            LOGGER.info("Fetching DVIDS {}s from unit '{}', country '{}', state '{}' for {}-{}-{} (page {}/?)...",
                    type, unit, country, state, year, month, day, page);
            while (loop) {
                DvidsUpdateResult ur = doUpdateDvidsMedia(
                        dvids.searchDvidsMediaIds(type, unit, country, state, year, month, day, page++), unit);
                idsKnownToDvidsApi.addAll(ur.idsKnownToDvidsApi);
                if (shouldPostOnSocialMedia()) {
                    uploadedMedia.addAll(ur.uploadedMedia);
                }
                count += ur.count;
                ongoingUpdateMedia(start, unit, count);
                loop = page <= ur.numberOfPages();
                if (loop) {
                    LOGGER.info(
                            "Fetching DVIDS {}s from unit '{}', country '{}', state {} for {}-{}-{} (page {}/{})...",
                            type, unit, country, state, year, month, day, page, ur.numberOfPages());
                }
            }
            LOGGER.info("{}/{} {}s for {}-{}-{} completed: {} {}s in {}", unit, country, type, year, month, day, count,
                    type, Utils.durationInSec(start));
        } catch (ApiException exx) {
            LOGGER.error("Error while fetching DVIDS " + type + "s from unit " + unit + " / country " + country
                    + " / state " + state, exx);
            GlitchTip.capture(exx);
        }
        return Pair.of(count, uploadedMedia);
    }

    private DvidsUpdateResult doUpdateDvidsMedia(ApiSearchResponse response, String unit) {
        int count = 0;
        LocalDateTime start = LocalDateTime.now();
        List<DvidsMedia> uploadedMedia = new ArrayList<>();
        Set<String> idsKnownToDvidsApi = new HashSet<>();
        for (CompositeMediaId id : response.getResults().stream().map(x -> {
            try {
                return x.toCompositeMediaId(unit, dvids);
            } catch (RuntimeException e) {
                LOGGER.error("Failed to retrieve unit abbreviation for {}: {}", x, e.toString());
                LOGGER.trace("Failed to retrieve unit abbreviation for {}", x, e);
                return null;
            }
        }).filter(Objects::nonNull).distinct().sorted().toList()) {
            try {
                idsKnownToDvidsApi.add(id.mediaId());
                Pair<DvidsMedia, Integer> result = dvidsProcessor.processDvidsMedia(
                        () -> repository.findById(id), () -> dvids.getMediaFromApi(id, this::addMetadata),
                        media -> processDvidsMediaUpdate(media, false).result(), this::checkRemoteMedia,
                        this::shouldUploadAuto, this::uploadWrapped);
                if (result.getValue() > 0 && shouldPostOnSocialMedia()) {
                    uploadedMedia.add(result.getKey());
                }
                ongoingUpdateMedia(start, unit, count++);
            } catch (HttpClientErrorException e) {
                LOGGER.error("API error while processing DVIDS {}: {}", id, smartExceptionLog(e));
                GlitchTip.capture(e);
            } catch (DataAccessException e) {
                LOGGER.error("DAO error while processing DVIDS {}: {}", id, smartExceptionLog(e));
                GlitchTip.capture(e);
            } catch (RuntimeException e) {
                LOGGER.error("Error while processing DVIDS {}: {}", id, smartExceptionLog(e));
                GlitchTip.capture(e);
            }
        }
        ApiPageInfo pi = response.getPageInfo();
        return new DvidsUpdateResult(pi.resultsPerPage(), pi.totalResults(), count, uploadedMedia, idsKnownToDvidsApi);
    }

    private static record DvidsUpdateResult(
            int resultsPerPage, int totalResults, int count, Collection<DvidsMedia> uploadedMedia,
            Set<String> idsKnownToDvidsApi) {

        public int numberOfPages() {
            return (int) Math.ceil((double) totalResults / (double) resultsPerPage);
        }
    }

    private MediaUpdateResult<DvidsMedia> processDvidsMediaUpdate(DvidsMedia media, boolean forceUpdate) {
        try (CloseableHttpClient httpClient = httpClientBuilder().build()) {
            MediaUpdateResult<DvidsMedia> commonUpdate = doCommonUpdate(media, httpClient, null, forceUpdate);
            boolean save = commonUpdate.result();
            if (!media.isIgnored()) {
                if (dvids.isIgnoredCategory(media.getCategory())) {
                    save = mediaService.ignoreMedia(media, "Ignored category: " + media.getCategory());
                } else if (findLicenceTemplates(media, media.getUniqueMetadata()).isEmpty()) {
                    // DVIDS media with VIRIN "O". we can assume it implies a courtesy photo
                    // https://www.dvidshub.net/image/3322521/45th-sw-supports-successful-atlas-v-oa-7-launch
                    save = mediaService.ignoreMedia(media, "No template found (VIRIN O): " + media.getVirin());
                }
            }
            return new MediaUpdateResult<>(media, commonUpdate.localPath(), save, commonUpdate.exception());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public final DvidsMedia refreshAndSave(DvidsMedia media) throws IOException {
        media = refresh(media);
        Exception e = processDvidsMediaUpdate(media, true).exception();
        if (e instanceof NotFound) {
            return deleteMedia(media, e);
        } else {
            return saveMedia(media);
        }
    }

    @Override
    protected final DvidsMedia refresh(DvidsMedia media) throws IOException {
        return doRefreshOrSync(media, null);
    }

    private DvidsMedia doRefreshOrSync(DvidsMedia media, Set<String> syncCategories) {
        // DVIDS API Terms of Service force us to check for deleted content
        // https://api.dvidshub.net/docs/tos
        try {
            return media.copyDataFrom(dvids.getMediaFromApi(media.getId(), this::addMetadata));
        } catch (IllegalArgumentException e) {
            String message = e.getMessage();
            if (message != null && message.startsWith("No result from DVIDS API for ")) {
                return deleteMediaIfNotSync(media, e, syncCategories);
            } else {
                LOGGER.error(message, e);
                GlitchTip.capture(e);
            }
        } catch (BadRequest e) {
            String message = e.getMessage();
            if (message != null && message.contains(" was not found")) {
                return deleteMediaIfNotSync(media, e, syncCategories);
            } else {
                LOGGER.error(message, e);
                GlitchTip.capture(e);
            }
        } catch (Forbidden e) {
            String message = e.getMessage();
            if (message != null && message.contains(" was found, but is not published.")) {
                return deleteMediaIfNotSync(media, e, syncCategories);
            } else {
                LOGGER.error(message, e);
                GlitchTip.capture(e);
            }
        }
        return media;
    }

    private DvidsMedia deleteMediaIfNotSync(DvidsMedia media, Exception e, Set<String> syncCategories) {
        String cat = media.isVideo() ? VIDEOS_REMOVED_FROM_DVIDS
                : media.isImage() ? IMAGES_REMOVED_FROM_DVIDS : MEDIA_REMOVED_FROM_DVIDS;
        if (syncCategories != null) {
            syncCategories.add(cat);
            return media;
        }
        for (String filename : media.getAllCommonsFileNames()) {
            try {
                commonsService.addCategories(filename, cat);
            } catch (IOException ex) {
                LOGGER.warn("Cannot update {}: {}", filename, ex.getMessage());
            }
        }
        return deleteMedia(media, e);
    }

    @Override
    protected Set<String> getSyncCategories(DvidsMedia media, FileMetadata fm, String filename) {
        Set<String> result = new HashSet<>(super.getSyncCategories(media, fm, filename));
        doRefreshOrSync(media, result);
        return result;
    }

    @Override
    public final URL getSourceUrl(DvidsMedia media, FileMetadata metadata, String ext) {
        try {
            return dvids.getSourceUrl(media.getId().mediaId().split(":"));
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected final String getSource(DvidsMedia media, FileMetadata metadata) {
        URL sourceUrl = getSourceUrl(media, metadata, metadata.getExtension());
        VirinTemplates t = UnitedStates.getUsVirinTemplates(media.getVirin(), sourceUrl);
        return t != null ? "{{" + t.virinTemplate() + "}}" : sourceUrl.toExternalForm();
    }

    @Override
    protected String getTakenLocation(DvidsMedia media) {
        return fixCountryForCommons(fixCountry(media.getLocation().getCountry()), media.getDescription());
    }

    @Override
    protected final String getAuthor(DvidsMedia media, FileMetadata metadata) {
        StringBuilder result = new StringBuilder();
        Matcher m = US_MEDIA_BY.matcher(media.getDescription().text());
        if (m.matches()) {
            result.append(m.group(1));
        } else if (isNotBlank(media.getBranch()) && !"Joint".equals(media.getBranch())) {
            result.append("U.S. ").append(media.getBranch()).append(' ').append(media.getId().repoId())
                    .append(" by ");
        }
        return result.append(media.getCredits()).toString();
    }

    @Override
    protected final StringBuilder appendInformationTemplate(StringBuilder sb, DvidsMedia media, FileMetadata metadata,
            List<LocalizedText> descriptions) {
        return appendMilim(sb, media, metadata, descriptions, media.getVirin(),
                ofNullable(media.getLocation()).map(DvidsLocation::toString),
                ofNullable(media.getRating()));
    }

    @Override
    public Set<String> findCategories(DvidsMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        result.addAll(media.getKeywordStream().map(KEYWORDS_CATS::get).filter(StringUtils::isNotBlank)
                .flatMap(s -> Arrays.stream(s.split(";"))).collect(toSet()));
        VirinTemplates t = UnitedStates.getUsVirinTemplates(media.getVirin(), media.getUniqueMetadata().getAssetUrl());
        if (t != null) {
            String country = media.getLocation().getCountry();
            if ("PD-USGov-Military-Navy".equals(t.pdTemplate())
                    && BODIES_OF_WATER.stream().anyMatch(country::contains)) {
                result.add("United States Navy images from the " + country);
            }
            if (metadata.isVideo() && isNotBlank(t.videoCategory())) {
                result.add(t.videoCategory());
            }
        }
        if (includeHidden) {
            result.add("Photographs by Defense Video and Imagery Distribution System");
        }
        return result;
    }

    @Override
    public Set<String> findLicenceTemplates(DvidsMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        VirinTemplates t = UnitedStates.getUsVirinTemplates(media.getVirin(), media.getUniqueMetadata().getAssetUrl());
        if (t != null && isNotBlank(t.pdTemplate())) {
            result.add(t.pdTemplate());
        }
        if (media.getDescription() != null) {
            if (media.getDescription().contains("Space Force photo")) {
                result.add("PD-USGov-Military-Space Force");
                result.remove("PD-USGov-Military-Air Force");
            }
            if (media.getDescription().contains("hoto by SpaceX") || media.getDescription().contains("hoto/SpaceX")) {
                result.add("PD-SpaceX");
            }
            if (media.getDescription().contains("hoto by NASA") || media.getDescription().contains("hoto/NASA")) {
                result.add("PD-USGov-NASA");
            }
        }
        return result;
    }

    @Override
    protected SdcStatements getStatements(DvidsMedia media, FileMetadata metadata) {
        SdcStatements result = super.getStatements(media, metadata);
        UnitedStates.getUsMilitaryCreator(media, wikidata).ifPresent(result::creator);
        ofNullable(media.getVirin()).ifPresent(result::virin);
        return result;
    }

    @Override
    protected Set<String> getEmojis(DvidsMedia uploadedMedia) {
        Set<String> result = super.getEmojis(uploadedMedia);
        result.add(UnitedStates.getUsMilitaryEmoji(uploadedMedia));
        return result;
    }

    private static boolean star(Set<String> set) {
        return Set.of("*").equals(set);
    }

    private Set<String> countriesOr(Set<String> set) {
        return star(set) ? countries : set;
    }

    private long count(ToLongFunction<Set<String>> byCountry, ToLongFunction<Set<String>> byState,
            ToLongFunction<Set<String>> byRepo, Set<String> set) {
        return star(getRepoIds())
                ? isUsState(set) ? byState.applyAsLong(set) : byCountry.applyAsLong(countriesOr(set)) // NOSONAR
                : byRepo.applyAsLong(set);
    }

    @Override
    public long countAllMedia() {
        return count(dvidsRepository::countByCountry, dvidsRepository::countByState, repository::count, getRepoIds());
    }

    @Override
    public long countAllMedia(String roc) {
        return isBlank(roc) ? countAllMedia()
                : count(dvidsRepository::countByCountry, dvidsRepository::countByState, repository::count, Set.of(roc));
    }

    @Override
    public long countIgnored() {
        return count(dvidsRepository::countByMetadata_IgnoredTrueByCountry,
                dvidsRepository::countByMetadata_IgnoredTrueByState, repository::countByMetadata_IgnoredTrue,
                getRepoIds());
    }

    @Override
    public long countIgnored(String roc) {
        return isBlank(roc) ? countIgnored()
                : count(dvidsRepository::countByMetadata_IgnoredTrueByCountry,
                        dvidsRepository::countByMetadata_IgnoredTrueByState, repository::countByMetadata_IgnoredTrue,
                        Set.of(roc));
    }

    @Override
    public long countMissingMedia() {
        return count(dvidsRepository::countMissingInCommonsByCountry, dvidsRepository::countMissingInCommonsByState,
                repository::countMissingInCommons, getRepoIds());
    }

    @Override
    public long countMissingMedia(String roc) {
        return isBlank(roc) ? countMissingMedia()
                : count(dvidsRepository::countMissingInCommonsByCountry, dvidsRepository::countMissingInCommonsByState,
                        repository::countMissingInCommons,
                        Set.of(roc));
    }

    @Override
    public long countMissingImages() {
        return count(dvidsRepository::countMissingImagesInCommonsByCountry,
                dvidsRepository::countMissingImagesInCommonsByState, repository::countMissingImagesInCommons,
                getRepoIds());
    }

    @Override
    public long countMissingImages(String roc) {
        return isBlank(roc) ? countMissingImages()
                : count(dvidsRepository::countMissingImagesInCommonsByCountry,
                        dvidsRepository::countMissingImagesInCommonsByState, repository::countMissingImagesInCommons,
                        Set.of(roc));
    }

    @Override
    public long countMissingVideos() {
        return count(dvidsRepository::countMissingVideosInCommonsByCountry,
                dvidsRepository::countMissingVideosInCommonsByState, repository::countMissingVideosInCommons,
                getRepoIds());
    }

    @Override
    public long countMissingVideos(String roc) {
        return isBlank(roc) ? countMissingVideos()
                : count(dvidsRepository::countMissingVideosInCommonsByCountry,
                        dvidsRepository::countMissingVideosInCommonsByState, repository::countMissingVideosInCommons,
                        Set.of(roc));
    }

    @Override
    public long countMissingDocuments() {
        return count(dvidsRepository::countMissingDocumentsInCommonsByCountry,
                dvidsRepository::countMissingDocumentsInCommonsByState,
                repository::countMissingDocumentsInCommons, getRepoIds());
    }

    @Override
    public long countMissingDocuments(String roc) {
        return isBlank(roc) ? countMissingDocuments()
                : count(dvidsRepository::countMissingDocumentsInCommonsByCountry,
                        dvidsRepository::countMissingDocumentsInCommonsByState,
                        repository::countMissingDocumentsInCommons, Set.of(roc));
    }

    @Override
    public long countPerceptualHashes() {
        return count(dvidsRepository::countByMetadata_PhashNotNullByCountry,
                dvidsRepository::countByMetadata_PhashNotNullByState, repository::countByMetadata_PhashNotNull,
                getRepoIds());
    }

    @Override
    public long countPerceptualHashes(String roc) {
        return isBlank(roc) ? countPerceptualHashes()
                : count(dvidsRepository::countByMetadata_PhashNotNullByCountry,
                        dvidsRepository::countByMetadata_PhashNotNullByState,
                        repository::countByMetadata_PhashNotNull, Set.of(roc));
    }

    @Override
    public long countUploadedMedia() {
        return count(dvidsRepository::countUploadedToCommonsByCountry, dvidsRepository::countUploadedToCommonsByState,
                repository::countUploadedToCommons, getRepoIds());
    }

    @Override
    public long countUploadedMedia(String roc) {
        return isBlank(roc) ? countUploadedMedia()
                : count(dvidsRepository::countUploadedToCommonsByCountry,
                        dvidsRepository::countUploadedToCommonsByState, repository::countUploadedToCommons,
                        Set.of(roc));
    }

    private <T> T list(Function<Set<String>, T> byCountry, Function<Set<String>, T> byState,
            Function<Set<String>, T> byRepo, Set<String> set) {
        return star(getRepoIds()) ? isUsState(set) ? byState.apply(set) : byCountry.apply(countriesOr(set)) // NOSONAR
                : byRepo.apply(set);
    }

    private <T, U> T list(BiFunction<Set<String>, U, T> byCountry, BiFunction<Set<String>, U, T> byState,
            BiFunction<Set<String>, U, T> byRepo, Set<String> set, U criteria) {
        return star(getRepoIds())
                ? isUsState(set) ? byState.apply(set, criteria) : byCountry.apply(countriesOr(set), criteria) // NOSONAR
                : byRepo.apply(set, criteria);
    }

    @Override
    public Iterable<DvidsMedia> listAllMedia() {
        return list(dvidsRepository::findAllByCountry, dvidsRepository::findAllByState, repository::findAll,
                getRepoIds());
    }

    @Override
    public Page<DvidsMedia> listAllMedia(Pageable page) {
        return list(dvidsRepository::findAllByCountry, dvidsRepository::findAllByState, repository::findAll,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listAllMedia(String roc, Pageable page) {
        return isBlank(roc) ? listAllMedia(page)
                : list(dvidsRepository::findAllByCountry, dvidsRepository::findAllByState, repository::findAll,
                        Set.of(roc), page);
    }

    @Override
    public List<DvidsMedia> listMissingMedia() {
        return list(dvidsRepository::findMissingInCommonsByCountry, dvidsRepository::findMissingInCommonsByState,
                repository::findMissingInCommons, getRepoIds());
    }

    @Override
    public Page<DvidsMedia> listMissingMedia(Pageable page) {
        return list(dvidsRepository::findMissingInCommonsByCountry, dvidsRepository::findMissingInCommonsByState,
                repository::findMissingInCommons, getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listMissingMedia(String roc, Pageable page) {
        return isBlank(roc) ? listMissingMedia(page)
                : list(dvidsRepository::findMissingInCommonsByCountry, dvidsRepository::findMissingInCommonsByState,
                        repository::findMissingInCommons, Set.of(roc), page);
    }

    @Override
    public Page<DvidsMedia> listMissingImages(Pageable page) {
        return list(dvidsRepository::findMissingImagesInCommonsByCountry,
                dvidsRepository::findMissingImagesInCommonsByState, repository::findMissingImagesInCommons,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listMissingImages(String roc, Pageable page) {
        return isBlank(roc) ? listMissingImages(page)
                : list(dvidsRepository::findMissingImagesInCommonsByCountry,
                        dvidsRepository::findMissingImagesInCommonsByState, repository::findMissingImagesInCommons,
                        Set.of(roc), page);
    }

    @Override
    public Page<DvidsMedia> listMissingVideos(Pageable page) {
        return list(dvidsRepository::findMissingVideosInCommonsByCountry,
                dvidsRepository::findMissingVideosInCommonsByState, repository::findMissingVideosInCommons,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listMissingVideos(String roc, Pageable page) {
        return isBlank(roc) ? listMissingVideos(page)
                : list(dvidsRepository::findMissingVideosInCommonsByCountry,
                        dvidsRepository::findMissingVideosInCommonsByState, repository::findMissingVideosInCommons,
                        Set.of(roc), page);
    }

    @Override
    public Page<DvidsMedia> listMissingDocuments(Pageable page) {
        return list(dvidsRepository::findMissingDocumentsInCommonsByCountry,
                dvidsRepository::findMissingDocumentsInCommonsByState, repository::findMissingDocumentsInCommons,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listMissingDocuments(String roc, Pageable page) {
        return isBlank(roc) ? listMissingDocuments(page)
                : list(dvidsRepository::findMissingDocumentsInCommonsByCountry,
                        dvidsRepository::findMissingDocumentsInCommonsByState,
                        repository::findMissingDocumentsInCommons, Set.of(roc), page);
    }

    @Override
    public List<DvidsMedia> listMissingMediaByDate(LocalDate date, String roc) {
        return list(dvidsRepository::findMissingInCommonsByPublicationDateByCountry,
                dvidsRepository::findMissingInCommonsByPublicationDateByState,
                repository::findMissingInCommonsByPublicationDate, isBlank(roc) ? getRepoIds() : Set.of(roc), date);
    }

    @Override
    public List<DvidsMedia> listMissingMediaByMonth(YearMonth month, String roc) {
        return list(dvidsRepository::findMissingInCommonsByPublicationMonthByCountry,
                dvidsRepository::findMissingInCommonsByPublicationMonthByState,
                repository::findMissingInCommonsByPublicationMonth, isBlank(roc) ? getRepoIds() : Set.of(roc), month);
    }

    @Override
    public List<DvidsMedia> listMissingMediaByYear(Year year, String roc) {
        return list(dvidsRepository::findMissingInCommonsByPublicationYearByCountry,
                dvidsRepository::findMissingInCommonsByPublicationYearByState,
                repository::findMissingInCommonsByPublicationYear, isBlank(roc) ? getRepoIds() : Set.of(roc), year);
    }

    @Override
    public List<DvidsMedia> listMissingMediaByTitle(String title, String roc) {
        return list(dvidsRepository::findMissingInCommonsByTitleByCountry,
                dvidsRepository::findMissingInCommonsByTitleByState, repository::findMissingInCommonsByTitle,
                isBlank(roc) ? getRepoIds() : Set.of(roc), title);
    }

    @Override
    public Page<DvidsMedia> listHashedMedia(Pageable page) {
        return list(dvidsRepository::findByMetadata_PhashNotNullByCountry,
                dvidsRepository::findByMetadata_PhashNotNullByState, repository::findByMetadata_PhashNotNull,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listHashedMedia(String roc, Pageable page) {
        return isBlank(roc) ? listHashedMedia(page)
                : list(dvidsRepository::findByMetadata_PhashNotNullByCountry,
                        dvidsRepository::findByMetadata_PhashNotNullByState, repository::findByMetadata_PhashNotNull,
                        Set.of(roc), page);
    }

    @Override
    public List<DvidsMedia> listUploadedMedia() {
        return list(dvidsRepository::findUploadedToCommonsByCountry, dvidsRepository::findUploadedToCommonsByState,
                repository::findUploadedToCommons, getRepoIds());
    }

    @Override
    public Page<DvidsMedia> listUploadedMedia(Pageable page) {
        return list(dvidsRepository::findUploadedToCommonsByCountry, dvidsRepository::findUploadedToCommonsByState,
                repository::findUploadedToCommons, getRepoIds(), page);
    }

    @Override
    public List<DvidsMedia> listUploadedMediaByDate(LocalDate date) {
        return list(dvidsRepository::findUploadedToCommonsByPublicationDateByCountry,
                dvidsRepository::findUploadedToCommonsByPublicationDateByState,
                repository::findUploadedToCommonsByPublicationDate, getRepoIds(), date);
    }

    @Override
    public Page<DvidsMedia> listUploadedMedia(String roc, Pageable page) {
        return isBlank(roc) ? listUploadedMedia(page)
                : list(dvidsRepository::findUploadedToCommonsByCountry, dvidsRepository::findUploadedToCommonsByState,
                        repository::findUploadedToCommons, Set.of(roc), page);
    }

    @Override
    public List<DvidsMedia> listDuplicateMedia() {
        return list(dvidsRepository::findDuplicateInCommonsByCountry, dvidsRepository::findDuplicateInCommonsByState,
                repository::findDuplicateInCommons, getRepoIds());
    }

    @Override
    public List<DvidsMedia> listIgnoredMedia() {
        return list(dvidsRepository::findByMetadata_IgnoredTrueByCountry,
                dvidsRepository::findByMetadata_IgnoredTrueByState, repository::findByMetadata_IgnoredTrue,
                getRepoIds());
    }

    @Override
    public Page<DvidsMedia> listIgnoredMedia(Pageable page) {
        return list(dvidsRepository::findByMetadata_IgnoredTrueByCountry,
                dvidsRepository::findByMetadata_IgnoredTrueByState, repository::findByMetadata_IgnoredTrue,
                getRepoIds(), page);
    }

    @Override
    public Page<DvidsMedia> listIgnoredMedia(String roc, Pageable page) {
        return isBlank(roc) ? listIgnoredMedia(page)
                : list(dvidsRepository::findByMetadata_IgnoredTrueByCountry,
                        dvidsRepository::findByMetadata_IgnoredTrueByState, repository::findByMetadata_IgnoredTrue,
                        Set.of(roc), page);
    }
}
