package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SharingCreateResponse extends DsmWebapiResponse<SharingCreate> {

    public SharingCreateResponse(@JsonProperty("success") boolean success, @JsonProperty("data") SharingCreate data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
