package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.youtube.YouTubeMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.youtube.YouTubeMediaRepository;

@Service
public class SdasmYouTubeService extends AbstractOrgYouTubeService {

    public SdasmYouTubeService(YouTubeMediaRepository repository,
            @Value("${sdasm.youtube.channels}") Set<String> youtubeChannels) {
        super(repository, "sdasm.youtube", youtubeChannels);
    }

    @Override
    public String getName() {
        return "San Diego Air and Space Museum (YouTube)";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "San Diego Air and Space Museum YouTube files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected String getAuthor(YouTubeMedia media, FileMetadata metadata) {
        return "San Diego Air & Space Museum";
    }

    @Override
    protected List<String> getOrgCategories() {
        return List.of("Videos of NASA");
    }
}
