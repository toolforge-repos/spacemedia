package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilAsiaDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilAsiaDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmilasia.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmilasia.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmilasia.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmilasia.dvids.min.year}") int minYear,
            @Value("${usmilasia.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmilasia.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in Asia (DVIDS)";
    }
}
