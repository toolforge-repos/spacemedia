package org.wikimedia.commons.donvip.spacemedia.data.domain.djangoplicity;

public enum DjangoplicityMediaType {
    Artwork,
    Chart,
    Collage,
    Observation,
    Photographic,
    Planetary,
    Simulation,
    Annotated,
    Annotated_Photo
}
