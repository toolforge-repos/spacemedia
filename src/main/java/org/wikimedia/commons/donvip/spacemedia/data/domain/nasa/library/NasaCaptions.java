package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

import java.net.URL;

public record NasaCaptions(URL location) {
}
