package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerUrl(URL fr, URL en) {

}
