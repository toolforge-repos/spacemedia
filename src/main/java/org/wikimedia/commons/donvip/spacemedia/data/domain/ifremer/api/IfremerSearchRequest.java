package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.time.Year;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerSearchRequest(List<IfremerCriteria> criteriaList,
        IfremerDefaultCriteriaValues defaultCriteriaValues, boolean groupedSearch, String languageEnum,
        IfremerPagination pagination) {

    private static final IfremerCriteria FREE_LICENCE = new IfremerCriteria(null, "licence", null, "Licence",
            List.of("TRANSLATED", "NESTED"), null, null, null, List.of("FACET"),
            List.of(new IfremerCriteriaValue("CC0"), new IfremerCriteriaValue("CC-BY")), 1);

    private static final IfremerCriteria SORT_BY_DATE = new IfremerCriteria(0, "date", java.util.Date.class.getName(),
            "Date", List.of("SORTABLE_FIELD"), "DESC", Boolean.FALSE, 0, List.of("SORT_ONLY"), List.of(), 1);

    public IfremerSearchRequest(int page) {
        this(List.of(FREE_LICENCE, SORT_BY_DATE), page);
    }

    public IfremerSearchRequest(SortedSet<Year> years, int page) {
        this(List.of(dateCriteria(years), FREE_LICENCE, SORT_BY_DATE), page);
    }

    private IfremerSearchRequest(List<IfremerCriteria> criteriaList, int page) {
        this(criteriaList, new IfremerDefaultCriteriaValues(), true, "fr", new IfremerPagination(true, page, 60));
    }

    private static IfremerCriteria dateCriteria(Collection<Year> years) {
        return new IfremerCriteria(null, "datePublicationYear", null, "Année", List.of("SORTED_VALTXT_DESC"), null,
                null, null, List.of("FACET"),
                years.stream().map(Year::toString).map(IfremerCriteriaValue::new).toList(), 1);
    }
}
