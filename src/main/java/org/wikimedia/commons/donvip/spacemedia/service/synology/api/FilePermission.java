package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record FilePermission(@JsonProperty("posix") int posix, @JsonProperty("is_acl_mode") boolean aclMode,
        @JsonProperty("acl") Acl acl) {

}
