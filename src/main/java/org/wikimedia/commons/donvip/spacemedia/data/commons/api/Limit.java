package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

public record Limit (
    int hits,
    int seconds) {
}
