package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryResponse<T> extends ApiResponse {

    private String batchcomplete;

    private T query;

    @JsonProperty("continue")
    private Continue continu;

    public String getBatchcomplete() {
        return batchcomplete;
    }

    public void setBatchcomplete(String batchcomplete) {
        this.batchcomplete = batchcomplete;
    }

    public T getQuery() {
        return query;
    }

    public void setQuery(T query) {
        this.query = query;
    }

    public Continue getContinue() {
        return continu;
    }

    public void setContinue(Continue continu) {
        this.continu = continu;
    }
}
