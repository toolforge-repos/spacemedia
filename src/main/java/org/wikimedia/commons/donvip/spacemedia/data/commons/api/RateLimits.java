package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RateLimits (

    RateLimit move,
    RateLimit edit,
    RateLimit upload,

    @JsonProperty("linkpurge")
    RateLimit linkPurge,

    @JsonProperty("badcaptcha")
    RateLimit badCaptcha,

    @JsonProperty("emailuser")
    RateLimit emailUser,

    @JsonProperty("changeemail")
    RateLimit changeEmail,

    RateLimit rollback,
    RateLimit purge,

    @JsonProperty("renderfile")
    RateLimit renderFile,

    @JsonProperty("renderfile-nonstandard")
    RateLimit renderFileNonStandard,

    @JsonProperty("cxsave")
    RateLimit cxSave,

    @JsonProperty("urlshortcode")
    RateLimit urlShortCode,

    @JsonProperty("thanks-notification")
    RateLimit thanksNotification,

    @JsonProperty("badoath")
    RateLimit badOath) {

}
