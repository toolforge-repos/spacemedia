package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

public record EditResponse (
    String result,
    int pageid,
    String title,
    String contentmodel,
    int oldrevid,
    int newrevid,
    String newtimestamp) {

}
