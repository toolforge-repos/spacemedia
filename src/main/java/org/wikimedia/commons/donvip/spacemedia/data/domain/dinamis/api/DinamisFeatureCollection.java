package org.wikimedia.commons.donvip.spacemedia.data.domain.dinamis.api;

import java.util.List;

public record DinamisFeatureCollection(String type, List<DinamisFeature> features) {

}
