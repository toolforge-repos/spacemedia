package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequestStream;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpGet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURI;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalDocSubType;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalDocType;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api.MedihalDoc;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api.MedihalSearchResponse;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api.MedihalSearchResponsePayload;
import org.wikimedia.commons.donvip.spacemedia.exception.UploadException;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;

@Service
public class MedihalService extends AbstractOrgService<MedihalMedia> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedihalService.class);

    private static final String BASE_API_URL = "https://api.archives-ouvertes.fr/search/medihal/?q=";

    private static final Set<URI> LICENCES = Set.of(
            newURI("http://hal.archives-ouvertes.fr/licences/etalab/"),
            newURI("http://creativecommons.org/publicdomain/zero/1.0/"),
            newURI("http://creativecommons.org/licenses/by-sa/"),
            newURI("http://hal.archives-ouvertes.fr/licences/publicDomain/"));

    @Autowired
    public MedihalService(MedihalMediaRepository repository) {
        super(repository, "medihal", Set.of("medihal"));
    }

    @Override
    public String getName() {
        return "MédiHAL";
    }

    @Override
    protected boolean checkBlocklist(MedihalMedia media) {
        return false;
    }

    @Override
    public void updateMedia(String[] args) throws IOException, UploadException {
        int count = 0;
        LocalDateTime start = startUpdateMedia();
        List<MedihalMedia> uploadedMedia = new ArrayList<>();
        LOGGER.info("Updating Medihal media");
        try (CloseableHttpClient httpclient = httpClientBuilder().build()) {
            String url = BASE_API_URL + "licence_s:"
                    + URLEncoder.encode(
                            "(" + LICENCES.stream().map(x -> '"' + x.toString() + '"').collect(joining("||")) + ")",
                            UTF_8)
                    + "&sort=submittedDate_tdate%20desc&start=";
            boolean done = false;
            int index = 0;
            while (!done) {
                LOGGER.debug("Requesting results from {}", index);
                try (InputStream in = executeRequestStream(newHttpGet(url + index), httpclient, null)) {
                    MedihalSearchResponse results = jackson.readValue(in, MedihalSearchResponsePayload.class)
                            .response();
                    int size = results.docs().size();
                    for (MedihalDoc doc : results.docs()) {
                        try {
                            updateImage(newId(doc.docid()), uploadedMedia);
                        } catch (IOException | UploadException | RuntimeException e) {
                            LOGGER.error("Failed to process {}", doc, e);
                        }
                        ongoingUpdateMedia(start, count++);
                    }
                    index += size;
                    done = size == 0;
                }
            }
        }
        endUpdateMedia(count, uploadedMedia);
    }

    private MedihalMedia updateImage(CompositeMediaId id, List<MedihalMedia> uploadedMedia)
            throws IOException, UploadException {
        boolean save = false;
        MedihalMedia media = null;
        Optional<MedihalMedia> imageInDb = repository.findById(id);
        if (imageInDb.isPresent()) {
            media = imageInDb.get();
        } else {
            media = fetchMedia(id);
            save = true;
        }
        if (doCommonUpdate(media)) {
            save = true;
        }
        if (shouldUploadAuto(media, false)) {
            media = saveMedia(upload(save ? saveMedia(media) : media, false, false).getLeft());
            if (shouldPostOnSocialMedia()) {
                uploadedMedia.add(media);
            }
            save = false;
        }
        return save ? saveMedia(media) : media;
    }

    private MedihalMedia fetchMedia(CompositeMediaId id) throws IOException {
        try (CloseableHttpClient httpclient = httpClientBuilder().build();
                InputStream in = executeRequestStream(newHttpGet(BASE_API_URL + "docid:" + id.mediaId() + "&fl=*"),
                        httpclient, null)) {
            MedihalSearchResponse response = jackson.readValue(in, MedihalSearchResponsePayload.class).response();
            if (response.docs().size() != 1) {
                throw new IOException("Didn't find exactly 1 doc for " + id + ": " + response);
            }
            return mapMedia(response.docs().get(0));
        }
    }

    private static CompositeMediaId newId(String id) {
        return new CompositeMediaId("medihal", id);
    }

    MedihalMedia mapMedia(MedihalDoc doc) {
        MedihalMedia media = new MedihalMedia();
        media.setId(newId(doc.docid()));
        media.setHalId(doc.halId_s());
        if (isNotEmpty(doc.fr_title_s())) {
            media.setTitle(new LocalizedText("fr", doc.fr_title_s().get(0)));
        } else if (isNotEmpty(doc.en_title_s())) {
            media.setTitle(new LocalizedText("en", doc.en_title_s().get(0)));
        } else if (isNotEmpty(doc.title_s())) {
            media.setTitle(new LocalizedText(doc.title_s().get(0)));
        }
        media.setLicenceUrl(doc.licence_s());
        media.setUri(doc.uri_s());
        if (doc.authFullName_s() != null) {
            media.setCredits(doc.authFullName_s().stream().filter(StringUtils::isNotBlank).collect(joining(", ")));
        } else {
            LOGGER.error("Media without authors? {}", doc);
        }
        media.getAuthors().addAll(doc.authors());
        media.setComments(doc.comment_s());
        // Description
        if (isNotEmpty(doc.en_abstract_s())) {
            media.setDescription(new LocalizedText("en", doc.en_abstract_s().get(0)));
        } else if (isNotEmpty(doc.fr_abstract_s())) {
            media.setDescription(new LocalizedText("fr", doc.fr_abstract_s().get(0)));
        } else if (isNotEmpty(doc.abstract_s())) {
            media.setDescription(new LocalizedText(doc.abstract_s().get(0)));
        } else if (isNotBlank(doc.label_s())) {
            media.setDescription(new LocalizedText(doc.label_s()));
        }
        // Keywords
        if (isNotEmpty(doc.en_keyword_s())) {
            media.setKeywords(new TreeSet<>(doc.en_keyword_s()));
        } else if (isNotEmpty(doc.keyword_s())) {
            media.setKeywords(new TreeSet<>(doc.keyword_s()));
        } else if (isNotEmpty(doc.fr_keyword_s())) {
            media.setKeywords(new TreeSet<>(doc.fr_keyword_s()));
        }
        // Types
        if (isNotBlank(doc.docType_s())) {
            media.setDocType(MedihalDocType.valueOf(doc.docType_s()));
        }
        if (isNotBlank(doc.docSubType_s())) {
            media.setDocSubType(MedihalDocSubType.valueOf(doc.docSubType_s()));
        }
        // Dates
        media.setCreationDateTime(doc.producedDate_tdate());
        media.setPublicationDateTime(doc.publicationDate_tdate());
        // Location
        if (isNotBlank(doc.coordinates_s())) {
            String[] ll = doc.coordinates_s().split(",");
            if (ll.length == 2) {
                try {
                    media.setLatitude(Double.parseDouble(ll[0]));
                    media.setLongitude(Double.parseDouble(ll[1]));
                } catch (NumberFormatException e) {
                    GlitchTip.capture(e);
                    LOGGER.error("Invalid coordinates: {}", doc.coordinates_s());
                }
            } else {
                LOGGER.error("Invalid coordinates: {}", doc.coordinates_s());
            }
        }
        // Files
        if (doc.thumbId_i() > 0) {
            media.setThumbnailUrl("https://thumb.ccsd.cnrs.fr/" + doc.thumbId_i() + "/medium");
        }
        if (doc.files_s() != null) {
            for (URL file : doc.files_s()) {
                try {
                    addMetadata(media, file, null);
                } catch (RuntimeException e) {
                    LOGGER.error("Failed to add Medihal file {}: {}", file, e.toString());
                    GlitchTip.capture(e);
                }
            }
        } else {
            LOGGER.error("Medihal doc without file: {}", doc.docid());
        }
        return media;
    }

    @Override
    public URL getSourceUrl(MedihalMedia media, FileMetadata metadata, String ext) {
        return media.getUri();
    }

    @Override
    protected Optional<String> getOtherFields(MedihalMedia media) {
        StringBuilder sb = new StringBuilder();
        addOtherField(sb, "Keyword", media.getKeywords());
        return Optional.of(sb.toString());
    }

    @Override
    protected MedihalMedia refresh(MedihalMedia media) throws IOException {
        return media.copyDataFrom(fetchMedia(media.getId()));
    }

    @Override
    protected SdcStatements getStatements(MedihalMedia media, FileMetadata metadata) {
        SdcStatements result = super.getStatements(media, metadata);
        if (media.getDocSubType() != null) {
            result.instanceOf(media.getDocSubType().item());
        } else if (media.getDocType() != null) {
            result.instanceOf(media.getDocType().item());
        }
        return result;
    }

    @Override
    protected boolean isPhotography(MedihalMedia media, FileMetadata metadata) {
        return media.getDocSubType() == MedihalDocSubType.PHOTOGRAPHY || super.isPhotography(media, metadata);
    }

    @Override
    protected StringBuilder appendInformationTemplate(StringBuilder sb, MedihalMedia media, FileMetadata metadata,
            List<LocalizedText> descriptions) {
        if (isPhotography(media, metadata)) {
            return appendPhotograph(
                    sb, media, metadata, descriptions, null, null, institutions(media, metadata),
                    null, null, media.getComments(), media.getHalId(), null, null);
        } else {
            return super.appendInformationTemplate(sb, media, metadata, descriptions);
        }
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "MédiHAL files uploaded by OptimusPrimeBot";
    }

    @Override
    protected List<String> getReviewCategories(MedihalMedia media) {
        return List.of("MédiHAL files (review needed)");
    }

    @Override
    protected Class<MedihalMedia> getMediaClass() {
        return MedihalMedia.class;
    }

    @Override
    protected boolean shouldPostOnSocialMedia() {
        return false;
    }
}
