package org.wikimedia.commons.donvip.spacemedia.service;

import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequestStream;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpGet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpPut;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.sleep;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.HttpServerErrorException.BadGateway;
import org.springframework.web.client.HttpServerErrorException.ServiceUnavailable;
import org.springframework.web.client.ResourceAccessException;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.hashes.HashAssociation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

@Lazy
@Service
public class RemoteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteService.class);

    @Value("${remote.application.uri}")
    private URI remoteApplication;

    @Value("${remote.application.enabled:true}")
    private boolean enabled;

    @Value("${git.build.version}")
    private String buildVersion;

    @Value("${git.commit.id.full}")
    private String commitId;

    @Lazy
    @Autowired
    protected ObjectMapper jackson;

    private CloseableHttpClient client;

    @PostConstruct
    public void init() {
        client = httpClientBuilder().setDefaultHeaders(List.of(new BasicHeader("User-Agent", getUserAgent(true))))
                .build();
    }

    @PreDestroy
    public void destroy() throws IOException {
        if (client != null) {
            client.close();
            client = null;
        }
    }

    private StringEntity newStringEntity(Object obj) throws JsonProcessingException {
        return new StringEntity(jackson.writeValueAsString(obj), ContentType.APPLICATION_JSON);
    }

    public String getHashLastTimestamp() throws IOException {
        return exchange(newHttpGet(remoteApplication + "/hashLastTimestamp"), String.class);
    }

    public String putHashAssociation(HashAssociation hash) throws IOException {
        do {
            try {
                return exchange(newHttpPut(remoteApplication + "/hashAssociation", newStringEntity(hash)),
                        String.class);
            } catch (IOException e) {
                if (e.getCause() instanceof BadGateway || e.getCause() instanceof ServiceUnavailable
                        || e.getCause() instanceof ResourceAccessException) {
                    // Tool is restarting... loop until it comes back
                    LOGGER.debug("{}", e.getMessage(), e);
                    sleep(500);
                }
                throw e;
            } catch (BadGateway | ServiceUnavailable | ResourceAccessException e) {
                // Tool is restarting... loop until it comes back
                LOGGER.debug("{}", e.getMessage(), e);
                sleep(500);
            }
        } while (true);
    }

    public String saveMedia(String orgId, Media media) throws IOException {
        return exchange(newHttpPut(String.join("/", restOrgEndpoint(orgId), "media", media.getIdUsedInOrg()),
                newStringEntity(media)), String.class);
    }

    public <T extends Media> T getMedia(String orgId, String mediaId, Class<T> mediaClass) throws IOException {
        try {
            return exchange(newHttpGet(String.join("/", restOrgEndpoint(orgId), "media", mediaId)), mediaClass);
        } catch (NotFound e) {
            return null;
        }
    }

    public void evictCaches(String orgId) throws IOException {
        exchange(newHttpGet(restOrgEndpoint(orgId) + "/evictcaches"), Void.class);
    }

    public String getUserAgent(boolean includeCommit) {
        return "Spacemedia " + buildVersion + (includeCommit ? '/' + commitId : "");
    }

    public boolean isEnabled() {
        return enabled;
    }

    private String restOrgEndpoint(String orgId) {
        return String.join("/", remoteApplication.toString(), orgId, "rest");
    }

    private <T> T exchange(HttpUriRequestBase request, Class<T> responseType) throws IOException {
        if (!enabled) {
            throw new UnsupportedOperationException("Remote service is disabled");
        }

        try (InputStream in = executeRequestStream(request, client, null)) {
            return responseType != Void.class ? jackson.readValue(in, responseType) : null;
        }
    }
}
