package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerDocument(int docId, String etat, IfremerText title, ZonedDateTime date, String dateString,
        IfremerTemporalExtend temporalExtend, List<IfremerGeoExtend> geoExtendList, List<IfremerAuthor> authors,
        List<IfremerAffiliation> affiliations, List<IfremerContributor> contributors, List<IfremerSpecies> species,
        IfremerText description, URL licenceUrl, List<IfremerFile> files, IfremerQuote documentQuote,
        String urlSection, URL urlLandingPage, int docType, URL thumbnailUrl, String thumbnailName,
        int thumbnailMediaId, List<IfremerText> keywordsControlled, String landingPageLang) {

}
