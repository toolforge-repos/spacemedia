package org.wikimedia.commons.donvip.spacemedia.service.wikimedia;

public enum WikidataProperty {

    /**
     * that class of which this subject is a particular example and member;
     * different from P279 (subclass of); for example: K2 is an instance of
     * mountain; volcano is a subclass of mountain (and an instance of volcanic
     * landform)
     */
    P31_INSTANCE_OF(31),

    /**
     * the area of the celestial sphere of which the subject is a part (from a
     * scientific standpoint, not an astrological one)
     */
    P59_CONSTELLATION(59),

    /**
     * maker of this creative work or other object (where no more specific property
     * exists)
     */
    P170_CREATOR(170),

    /**
     * entity visually depicted in an image, literarily described in a work, or
     * otherwise incorporated into an audiovisual or other medium; see also P921,
     * 'main subject'
     */
    P180_DEPICTS(180),

    /**
     * International Standard Name Identifier for an identity. Starting with 0000.
     */
    P213_ISNI_ID(213),

    /**
     * identifier for the Virtual International Authority File database [format: up
     * to 22 digits]
     */
    P214_VIAF_ID(214),

    /**
     * identifier for the subject issued by BNF (Bibliothèque nationale de France).
     * Format: 8 digits followed by a check-digit or letter, do not include the
     * initial 'cb'.
     */
    P268_BNF_ID(268),

    /**
     * identifier for authority control in the French collaborative library catalog
     * (see also P1025). Format: 8 digits followed by a digit or "X"
     */
    P269_IDREF_ID(269),

    /**
     * location of the object, structure or event. In the case of an administrative
     * entity as containing item use P131. For statistical entities use P8138. In
     * the case of a geographic entity use P706.
     */
    P276_LOCATION(276),

    /**
     * identifier for a country in two-letter format per ISO 3166-1
     */
    P297_ISO_3166_1_ALPHA_2_CODE(297),

    /**
     * name of the Wikimedia Commons category containing files related to this item
     * (without the prefix "Category:")
     */
    P373_COMMONS_CATEGORY(373),

    /** ORCID identifier for a person */
    P496_ORCID_ID(496),

    /** catalog name of an object, use with qualifier P972 */
    P528_CATALOG_CODE(528),

    /** part of full name of person */
    P734_FAMILY_NAME(734),

    /** identifier in the World Register of Marine Species */
    P850_WORMS_ID(850),

    /** person(s) that participated operating or serving aboard this vehicle */
    P1029_CREW_MEMBERS(1029),

    /**
     * identifier for a researcher in a system for scientific authors, redirects to
     * a Web of Science ID, along with P3829
     */
    P1053_RESEARCHERID_ID(1053),

    /**
     * place where the item was conceived or made; where applicable, location of
     * final assembly
     */
    P1071_LOCATION_OF_CREATION(1071),

    /** identifier of a video on YouTube */
    P1651_YOUTUBE_VIDEO_ID(1651),

    /** official name of the subject in its official language(s) */
    P1448_OFFICIAL_NAME(1448),

    /**
     * label for the items in their official language (P37) or their original
     * language (P364)
     */
    P1705_NATIVE_LABEL(1705),

    /**
     * short name of a place, organisation, person, journal, instrument, etc.
     */
    P1813_SHORT_NAME(1813),

    /**
     * method, process or technique used to grow, cook, weave, build, assemble,
     * manufacture the item
     */
    P2079_FABRICATION_METHOD(2079),

    /** Flickr user ID */
    P3267_FLICKR_ID(3267),

    /**
     * equipment (e.g. model of camera, lens microphone), used to capture this
     * image, video, audio, or data
     */
    P4082_CAPTURED_WITH(4082),

    /** identifier for an author on arXiv */
    P4594_ARXIV_AUTHOR_ID(4594),

    /**
     * Persistent identifier for images on Flickr
     */
    P12120_FLICKR_PHOTO_ID(12120),

    /**
     * unique identifier assigned by the United States Department of Defense to
     * official still photographs, motion picture footage, video recordings, and
     * audio recordings made by USDOD personnel
     */
    P12967_VIRIN(12967),

    /** identifier of military unit on dvids website */
    P13072_DVIDS_UNIT_ID(13072);

    private final int code;

    private WikidataProperty(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "P" + code;
    }
}
