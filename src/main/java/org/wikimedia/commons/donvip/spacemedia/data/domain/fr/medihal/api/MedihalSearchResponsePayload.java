package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api;

public record MedihalSearchResponsePayload(MedihalSearchResponse response) {

}
