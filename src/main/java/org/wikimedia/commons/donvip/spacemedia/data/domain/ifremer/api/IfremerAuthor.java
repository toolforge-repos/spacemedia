package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import static java.util.stream.Collectors.joining;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerAuthor(IfremerPerson person, List<IfremerAffiliation> affiliations) {

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder().append(person);
        if (isNotEmpty(affiliations)) {
            sb.append(" (")
                    .append(affiliations.stream().map(IfremerAffiliation::toString).collect(joining(", ")))
                    .append(')');
        }
        return sb.toString();
    }
}
