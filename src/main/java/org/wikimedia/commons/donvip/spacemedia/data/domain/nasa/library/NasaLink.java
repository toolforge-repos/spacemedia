package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

import java.net.URL;

public record NasaLink(String rel, String prompt, URL href) {
}
