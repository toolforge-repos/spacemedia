package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store;

import okhttp3.HttpUrl;

public class RelayCookie {

    private final String serverID;
    private final String id;
    private final boolean gofile;
    private final HttpUrl resolvedUrl;

    private RelayCookie(Builder builder) {
        serverID = builder.serverID;
        id = builder.id;
        gofile = builder.gofile;
        resolvedUrl = builder.resolvedUrl;
    }

    public Builder newBuilder() {
        return new Builder().serverID(serverID).id(id).gofile(gofile).resolvedUrl(resolvedUrl);
    }

    public String serverID() {
        return serverID;
    }

    public String id() {
        return id;
    }

    public boolean gofile() {
        return gofile;
    }

    public HttpUrl resolvedUrl() {
        return resolvedUrl;
    }

    public static class Builder {
        private String serverID;
        private String id;
        private boolean gofile;
        private HttpUrl resolvedUrl;

        public Builder serverID(String serverID) {
            this.serverID = serverID;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder gofile(boolean gofile) {
            this.gofile = gofile;
            return this;
        }

        public Builder resolvedUrl(HttpUrl resolvedUrl) {
            this.resolvedUrl = resolvedUrl;
            return this;
        }

        public RelayCookie build() {
            return new RelayCookie(this);
        }
    }
}
