package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public record ServerInfoJson(String command, EnvJson env, int errno, ServerJson server, ServiceJson service,
        SmartDnsJson smartdns, int version) {

    public static record ServerJson(String ddns, String ds_state, ExternalJson external, String fqdn, String gateway,
            @SerializedName("interface") List<InterfaceJson> interfaces, boolean is_bsm, String pingpong_path,
            String redirect_prefix, String serverID, int tcp_punch_port, int udp_punch_port) {
    }

    public static record InterfaceJson(String ip, List<Ipv6Json> ipv6, String mask, String name) {

        public static record Ipv6Json(int addr_type, String address, int prefix_length, String scope) {
        }
    }

    public static record ExternalJson(String ip, String ipv6) {
    }

    public static record EnvJson(String relay_region, String control_host) {
    }

    public static record ServiceJson(int port, int ext_port, String pingpong, String relay_ip, String relay_ipv6,
            String relay_dualstack, String relay_dn, int relay_port, String vpn_ip, String https_ip, int https_port) {
    }

    public static record SmartDnsJson(String host, String external, String externalv6, List<String> lan,
            List<String> lanv6, String hole_punch) {
    }
}
