package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record User(@JsonProperty("gid") int gid, @JsonProperty("group") String group, @JsonProperty("uid") int uid,
        @JsonProperty("user") String username) {

}
