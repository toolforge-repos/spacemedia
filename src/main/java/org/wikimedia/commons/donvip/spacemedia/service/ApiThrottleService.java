package org.wikimedia.commons.donvip.spacemedia.service;

import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.sleep;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.threeten.bp.Duration;

@Service
public class ApiThrottleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiThrottleService.class);

    // Minimal delays between successive API requests, in milliseconds.
    private static final Map<String, Integer> DELAYS = Map.of("NASA", 3601, "Flickr", 1);

    private final Map<String, LocalDateTime> lastRequests = new HashMap<>();

    /**
     * Makes sure the service complies rate limit
     */
    public void ensureApiLimit(String service) {
        Integer delay = DELAYS.get(service);
        if (delay == null) {
            throw new IllegalArgumentException("Unknown service: " + service);
        }
        LocalDateTime someTimeAgo = now().minus(delay, ChronoUnit.MILLIS);
        LocalDateTime lastRequest = lastRequests.get(service);
        if (lastRequest != null && lastRequest.isAfter(someTimeAgo)) {
            long millis = MILLIS.between(now(), lastRequest.plus(delay, ChronoUnit.MILLIS));
            LOGGER.info("Sleeping {} to conform to {} API limit policy", Duration.ofMillis(millis), service);
            sleep(millis);
        }
        lastRequests.put(service, now());
    }
}
