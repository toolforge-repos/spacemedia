package org.wikimedia.commons.donvip.spacemedia.service.flickr;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.toDate;

import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrLicense;
import org.wikimedia.commons.donvip.spacemedia.service.ApiThrottleService;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.people.User;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotoSet;
import com.flickr4java.flickr.photos.SearchParameters;

@Lazy
@Service
public class FlickrService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlickrService.class);

    private static final int MAX_PER_PAGE = 500;

    private static final Set<String> EXTRAS = new HashSet<>(Arrays.asList(
            "description", "license", "date_upload", "date_taken",
            "original_format", "last_update", "geo", "tags",
            "media", "path_alias", "url_m", "url_o"));

    private final Flickr flickr;

    @Lazy
    @Autowired
    private ApiThrottleService apiThrottle;

    @Autowired
    public FlickrService(
            @Value("${FLICKR_API_KEY}") String flickrApiKey,
            @Value("${FLICKR_SECRET}") String flickrSecret) {
        flickr = new Flickr(flickrApiKey, flickrSecret, new REST());
    }

    @Cacheable("usersByUrl")
    public User findUser(URL url) throws FlickrException {
        return flickr.getUrlsInterface().lookupUserByURL(url.toExternalForm());
    }

    @Cacheable("userNamesByNsid")
    public String findUserName(String nsid) throws FlickrException {
        return flickr.getPeopleInterface().getInfo(nsid).getUsername();
    }

    @Cacheable("userProfilesByNsid")
    public URL findUserProfileUrl(String nsid) throws FlickrException {
        return newURL(flickr.getUrlsInterface().getUserProfile(nsid));
    }

    @Cacheable("flickrPhotoById")
    public Photo findPhoto(String photoId) throws FlickrException {
        return flickr.getPhotosInterface().getPhoto(photoId);
    }

    public List<Photo> findPhotos(Set<String> photoIds) throws FlickrException {
        List<Photo> result = new ArrayList<>();
        for (String photoId : photoIds) {
            result.add(findPhoto(photoId));
        }
        return result;
    }

    public List<Photo> searchPhotos(String userId, LocalDate maxUploadDate, LocalDate minUploadDate, boolean includeAll)
            throws FlickrException {
        if (maxUploadDate == null) {
            maxUploadDate = LocalDate.now().plusDays(2);
        }
        if (minUploadDate == null) {
            minUploadDate = LocalDate.of(2004, 2, 10); // Flickr birthday
        }

        List<Photo> result = new ArrayList<>();
        SearchParameters params = new SearchParameters();
        params.setUserId(requireNonNull(userId));
        params.setExtras(EXTRAS);
        // Multi-license search does not work
        // https://www.flickr.com/groups/51035612836@N01/discuss/72157665503298714/72157667263924940
        for (int license : Arrays.stream(FlickrLicense.values()).filter(l -> includeAll || l.isFree())
                .map(FlickrLicense::getCode).toList()) {
            params.setLicense(Integer.toString(license));
            result.addAll(searchPhotosRecursive(params, maxUploadDate, minUploadDate));
        }
        return result;
    }

    private PhotoList<Photo> searchPhotosSafely(SearchParameters params, int page) throws FlickrException {
        PhotoList<Photo> photos = new PhotoList<>();
        boolean responseOk = false;
        do {
            try {
                // https://www.flickr.com/services/developer/api/
                // Makes sure the service complies with Flickr API hourly limit of 3,600 requests per hour
                apiThrottle.ensureApiLimit("Flickr");
                photos = flickr.getPhotosInterface().search(params, MAX_PER_PAGE, page);
                // Flickr API sometimes returns empty list of results for no reason ?!
                responseOk = photos.getPages() > 0 || photos.getPage() == 1;
            } catch (FlickrException e) {
                if (e.getMessage() != null
                        && e.getMessage().contains("Received 'Bad Gateway' error from Flickr with status 502")) {
                    LOGGER.warn(e.getMessage());
                } else {
                    throw e;
                }
            }
        } while (!responseOk);
        return photos;
    }

    private PhotoList<Photo> searchPhotosRecursive(SearchParameters params, LocalDate maxUploadDate,
            LocalDate minUploadDate) throws FlickrException {
        LOGGER.trace("Searching from {} to {}", minUploadDate, maxUploadDate);
        params.setMaxUploadDate(toDate(maxUploadDate));
        params.setMinUploadDate(toDate(minUploadDate));
        int page = 1;
        PhotoList<Photo> photos = searchPhotosSafely(params, page);
        if (isNotEmpty(photos)) {
            LOGGER.info("License {} ({}-{}) => Page {}/{} => {}", params.getLicense(), minUploadDate, maxUploadDate,
                    photos.getPage(), photos.getPages(), photos.size());

            // https://www.flickr.com/services/api/flickr.photos.search.html
            // Flickr will return at most the first 4,000 results for any given search query
            if (photos.getTotal() >= 4000 && !maxUploadDate.equals(minUploadDate)) {
                photos = new PhotoList<>();
                LocalDate avgUploadDate = minUploadDate
                        .plusDays(ChronoUnit.DAYS.between(minUploadDate, maxUploadDate) / 2);
                photos.addAll(searchPhotosRecursive(params, maxUploadDate, avgUploadDate));
                photos.addAll(searchPhotosRecursive(params, avgUploadDate, minUploadDate));
            } else {
                if (maxUploadDate.equals(minUploadDate)) {
                    LOGGER.warn("Fetch only 4000 results from Flickr for {}", minUploadDate);
                }
                PhotoList<Photo> photosPage;
                do {
                    photosPage = searchPhotosSafely(params, ++page);
                    photos.addAll(photosPage);
                    if (isNotEmpty(photosPage)) {
                        LOGGER.info("License {} ({}-{}) => Page {}/{} => {}", params.getLicense(), minUploadDate,
                                maxUploadDate, photosPage.getPage(), photosPage.getPages(), photos.size());
                    }
                } while (photosPage.getPage() < photosPage.getPages());
            }
        }
        return photos;
    }

    @Cacheable("flickrPhotoSetsById")
    public List<PhotoSet> findPhotoSets(String photoId) throws FlickrException {
        return flickr.getPhotosInterface().getAllContexts(photoId).getPhotoSetList();
    }
}
