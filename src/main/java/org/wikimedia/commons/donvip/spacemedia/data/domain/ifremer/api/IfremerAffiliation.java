package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerAffiliation(int affiliationId, String affiliationName) {

    public IfremerAffiliation(String text) {
        this(-1, text);
    }

    @Override
    public String toString() {
        return affiliationName;
    }
}
