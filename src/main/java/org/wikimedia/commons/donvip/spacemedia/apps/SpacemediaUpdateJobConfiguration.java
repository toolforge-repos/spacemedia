package org.wikimedia.commons.donvip.spacemedia.apps;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.wikimedia.commons.donvip.spacemedia.service.ApiThrottleService;
import org.wikimedia.commons.donvip.spacemedia.service.CategorizationService;
import org.wikimedia.commons.donvip.spacemedia.service.GeometryService;
import org.wikimedia.commons.donvip.spacemedia.service.GoogleTranslateService;
import org.wikimedia.commons.donvip.spacemedia.service.HashService;
import org.wikimedia.commons.donvip.spacemedia.service.InternetArchiveService;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService;
import org.wikimedia.commons.donvip.spacemedia.service.RemoteService;
import org.wikimedia.commons.donvip.spacemedia.service.SearchService;
import org.wikimedia.commons.donvip.spacemedia.service.mastodon.MastodonService;
import org.wikimedia.commons.donvip.spacemedia.service.synology.SynologyService;

@Configuration
@Import(SpacemediaCommonConfiguration.class)
@ComponentScan(basePackages = { "org.wikimedia.commons.donvip.spacemedia.data",
        "org.wikimedia.commons.donvip.spacemedia.service.osm",
        "org.wikimedia.commons.donvip.spacemedia.service.wikimedia" }, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".+Test.*"))
public class SpacemediaUpdateJobConfiguration {

    @Lazy
    @Bean
    public ApiThrottleService apiThrottleService() {
        return new ApiThrottleService();
    }

    @Lazy
    @Bean
    public HashService hashService() {
        return new HashService();
    }

    @Lazy
    @Bean
    public MediaService mediaService() {
        return new MediaService();
    }

    @Lazy
    @Bean
    public CategorizationService categorizationService() {
        return new CategorizationService();
    }

    @Lazy
    @Bean
    public RemoteService remoteService() {
        return new RemoteService();
    }

    @Lazy
    @Bean
    public SearchService searchService() {
        return new SearchService();
    }

    @Lazy
    @Bean
    public GeometryService geometryService() {
        return new GeometryService();
    }

    @Lazy
    @Bean
    public GoogleTranslateService translateService() {
        return new GoogleTranslateService();
    }

    @Lazy
    @Bean
    public MastodonService mastodonService(
            @Value("${mastodon.instance}") String instance,
            @Value("${MASTODON_API_OAUTH2_CLIENT_ID}") String clientId,
            @Value("${MASTODON_API_OAUTH2_CLIENT_SECRET}") String clientSecret,
            @Value("${MASTODON_API_OAUTH2_ACCESS_TOKEN}") String accessToken) {
        return new MastodonService(instance, clientId, clientSecret, accessToken);
    }

    @Lazy
    @Bean
    public InternetArchiveService internetArchiveService() {
        return new InternetArchiveService();
    }

    @Lazy
    @Bean
    public SynologyService synologyService() {
        return new SynologyService();
    }
}
