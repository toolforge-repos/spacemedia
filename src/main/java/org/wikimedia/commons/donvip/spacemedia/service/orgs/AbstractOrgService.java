package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Arrays.stream;
import static java.util.Locale.ENGLISH;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService.formatWikiCode;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService.normalizeFilename;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.Video2CommonsService.V2C_VIDEO_EXTENSIONS;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q5_HUMAN;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.durationInSec;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequest;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.https;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpGet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.Header;
import org.jsoup.HttpStatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.RestClientException;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;
import org.wikimedia.commons.donvip.spacemedia.data.domain.Statistics;
import org.wikimedia.commons.donvip.spacemedia.data.domain.UploadMode;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadataRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaDimensions;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.RuntimeData;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.RuntimeDataRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.SingleFileMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithAuthors;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithKeywords;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithLatLon;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithLicenceUrl;
import org.wikimedia.commons.donvip.spacemedia.exception.IgnoreException;
import org.wikimedia.commons.donvip.spacemedia.exception.ImageNotFoundException;
import org.wikimedia.commons.donvip.spacemedia.exception.ImageUploadForbiddenException;
import org.wikimedia.commons.donvip.spacemedia.exception.TooManyResultsException;
import org.wikimedia.commons.donvip.spacemedia.exception.UploadException;
import org.wikimedia.commons.donvip.spacemedia.exception.WrappedUploadException;
import org.wikimedia.commons.donvip.spacemedia.service.AbstractSocialMediaService;
import org.wikimedia.commons.donvip.spacemedia.service.CategorizationService;
import org.wikimedia.commons.donvip.spacemedia.service.ExecutionMode;
import org.wikimedia.commons.donvip.spacemedia.service.GeometryService;
import org.wikimedia.commons.donvip.spacemedia.service.GoogleTranslateService;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService.MediaUpdateContext;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService.MediaUpdateResult;
import org.wikimedia.commons.donvip.spacemedia.service.RemoteService;
import org.wikimedia.commons.donvip.spacemedia.service.SearchService;
import org.wikimedia.commons.donvip.spacemedia.service.UrlResolver;
import org.wikimedia.commons.donvip.spacemedia.service.mastodon.MastodonService;
import org.wikimedia.commons.donvip.spacemedia.service.osm.NominatimService;
import org.wikimedia.commons.donvip.spacemedia.service.osm.NominatimService.Address;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService.WikidataInfo;
import org.wikimedia.commons.donvip.spacemedia.utils.Emojis;
import org.wikimedia.commons.donvip.spacemedia.utils.ImageUtils;
import org.wikimedia.commons.donvip.spacemedia.utils.UnitedStates;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Superclass of orgs services.
 *
 * @param <T>  the media type the repository manages
 */
public abstract class AbstractOrgService<T extends Media>
        implements Comparable<AbstractOrgService<T>>, Org<T> {

    private static final String Q20007257_CC_BY_40 = "Q20007257";

    private static final int LOTS_OF_MP = 150_000_000;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOrgService.class);

    private static final Pattern PATTERN_SHORT = Pattern
            .compile(
                    "(?:https?://)?(?:bit\\.?ly(?:\\.com)?/[0-9a-zA-Z]{6,7}|youtu\\.be/[\\w\\-]{11}|flic\\.kr/[ps]/[0-9a-zA-Z]{5,10}|fb\\.me/e/[0-9a-zA-Z]{9}|ow\\.ly/[0-9a-zA-Z]{5}|tinyurl\\.com/[0-9a-zA-Z]{7,8}|goo\\.gl/[0-9a-zA-Z]{5,6}|wp\\.me/[0-9a-zA-Z\\-]{6,10})");

    private static final Pattern PATTERN_TWITTER_SEARCH = Pattern
            .compile("<a href=\"https://twitter\\.com/search?[^\"]+\">([^<]*)</a>");

    private static final Pattern PATTERN_UNINTERESTING_TITLE = Pattern.compile("Picture \\d+");

    private static final Set<String> PD_US = Set.of("PD-US", "PD-NASA", "PD-Hubble", "PD-Webb");

    private static final Map<String, String> LICENCES = Map.ofEntries(e("YouTube CC-BY", "Q14947546"),
            e("Cc-by-2.0", "Q19125117"), e("Cc-by-4.0", Q20007257_CC_BY_40), e("Cc-by-sa-2.0", "Q19068220"),
            e("Cc-by-sa-4.0", "Q18199165"), e("Cc-zero", "Q6938433"),
            e("DLR-License", "Q62619894"), e("ESA|", "Q26259495"), e("ESO", Q20007257_CC_BY_40),
            e("IAU", Q20007257_CC_BY_40), e("KOGL", "Q12584618"), e("NOIRLab", Q20007257_CC_BY_40),
            e("ESA-Hubble", Q20007257_CC_BY_40), e("ESA-Webb", Q20007257_CC_BY_40));

    private static final List<String> COURTESY_SPELLINGS = List.of("courtesy", "courtsey", "contributed photo");

    private static final Set<String> MARS_KEYWORDS = Set.of("mars", "martian", "curiosity", "perseverance");

    private static final Set<String> SATELLITE_KEYWORDS = Set.of("satelliteimagery", "landsat", "modis", "aster",
            "sentinel1", "sentinel2", "sentinel3");

    protected static final List<String> BODIES_OF_WATER = List.of("Sea", "Ocean", "Gulf", "Bay", "Strait", "Channel");

    protected final MediaRepository<T> repository;

    private final String id;

    private final SortedSet<String> repoIds;

    @Autowired
    private FileMetadataRepository metadataRepository;
    @Autowired
    protected RuntimeDataRepository runtimeDataRepository;
    @Autowired
    protected ObjectMapper jackson;
    @Lazy
    @Autowired
    protected MediaService mediaService;
    @Lazy
    @Autowired
    protected CategorizationService categorizationService;
    @Lazy
    @Autowired
    protected CommonsService commonsService;
    @Lazy
    @Autowired
    protected WikidataService wikidata;
    @Lazy
    @Autowired
    private SearchService searchService;
    @Lazy
    @Autowired
    private RemoteService remoteService;
    @Lazy
    @Autowired
    private GoogleTranslateService translateService;
    @Lazy
    @Autowired
    private List<AbstractSocialMediaService<?, ?>> socialMediaServices;
    @Lazy
    @Autowired
    protected GeometryService geometry;
    @Lazy
    @Autowired
    protected NominatimService nominatim;

    @Autowired
    protected Environment env;

    private boolean postSocialMedia;
    private UploadMode uploadMode;
    private boolean forceDuplicateDetection;
    private LocalDateTime lastCacheEviction;

    protected AbstractOrgService(MediaRepository<T> repository, String id, Set<String> repoIds) {
        this.repository = requireNonNull(repository);
        this.id = requireNonNull(id);
        this.repoIds = new TreeSet<>(repoIds);
    }

    @PostConstruct
    void init() throws IOException {
        uploadMode = UploadMode.valueOf(
                env.getProperty(id + ".upload", String.class, UploadMode.DISABLED.name()).toUpperCase(ENGLISH));
        LOGGER.info("{} upload mode: {}", id, uploadMode);
        postSocialMedia = "TRUE".equalsIgnoreCase(env.getProperty(id + ".post.social.media", String.class, "TRUE"));
        LOGGER.info("{} post social media: {}", id, postSocialMedia);
        forceDuplicateDetection = env.getProperty(id + ".force.duplicate.detection", Boolean.class, Boolean.FALSE);
        if (forceDuplicateDetection) {
            LOGGER.info("{} duplicate detection forced: {}", id, forceDuplicateDetection);
        }
    }

    @Override
    public boolean updateOnProfiles(List<String> activeProfiles) {
        return activeProfiles.stream().anyMatch(p -> "web".equals(p) || ("job-" + getId().replace('.', '-')).equals(p));
    }

    @Override
    public void checkCommonsCategories() {
        // To be overriden if applicable
    }

    /**
     * Checks that given Commons categories exist and are not redirected. Otherwise, log a warning.
     *
     * @param categories Commons categories to check
     */
    protected void checkCommonsCategories(Map<String, String> categories) {
        Set<String> problematicCategories = commonsService.findNonUpToDateCategories(categories.values());
        if (!problematicCategories.isEmpty()) {
            LOGGER.warn("problematicCategories : {}", problematicCategories);
        }
    }

    @Override
    public void evictCaches() {
        LOGGER.info("Evicting caches of {}...", getId());
        repository.evictCaches();
    }

    @Override
    public long countAllMedia() {
        return repository.count(getRepoIds());
    }

    @Override
    public long countAllMedia(String repo) {
        return isBlank(repo) ? countAllMedia() : repository.count(Set.of(repo));
    }

    @Override
    public long countIgnored() {
        return repository.countByMetadata_IgnoredTrue(getRepoIds());
    }

    @Override
    public long countIgnored(String repo) {
        return isBlank(repo) ? countIgnored() : repository.countByMetadata_IgnoredTrue(Set.of(repo));
    }

    @Override
    public long countMissingMedia() {
        return repository.countMissingInCommons(getRepoIds());
    }

    @Override
    public long countMissingMedia(String repo) {
        return isBlank(repo) ? countMissingMedia() : repository.countMissingInCommons(Set.of(repo));
    }

    @Override
    public long countMissingImages() {
        return repository.countMissingImagesInCommons(getRepoIds());
    }

    @Override
    public long countMissingImages(String repo) {
        return isBlank(repo) ? countMissingImages() : repository.countMissingImagesInCommons(Set.of(repo));
    }

    @Override
    public long countMissingVideos() {
        return repository.countMissingVideosInCommons(getRepoIds());
    }

    @Override
    public long countMissingVideos(String repo) {
        return isBlank(repo) ? countMissingVideos() : repository.countMissingVideosInCommons(Set.of(repo));
    }

    @Override
    public long countMissingDocuments() {
        return repository.countMissingDocumentsInCommons(getRepoIds());
    }

    @Override
    public long countMissingDocuments(String repo) {
        return isBlank(repo) ? countMissingDocuments() : repository.countMissingDocumentsInCommons(Set.of(repo));
    }

    @Override
    public long countPerceptualHashes() {
        return repository.countByMetadata_PhashNotNull(getRepoIds());
    }

    @Override
    public long countPerceptualHashes(String repo) {
        return isBlank(repo) ? countPerceptualHashes() : repository.countByMetadata_PhashNotNull(Set.of(repo));
    }

    @Override
    public long countUploadedMedia() {
        return repository.countUploadedToCommons(getRepoIds());
    }

    @Override
    public long countUploadedMedia(String repo) {
        return isBlank(repo) ? countUploadedMedia() : repository.countUploadedToCommons(Set.of(repo));
    }

    @Override
    public Iterable<T> listAllMedia() {
        return repository.findAll(getRepoIds());
    }

    @Override
    public Page<T> listAllMedia(Pageable page) {
        return repository.findAll(getRepoIds(), page);
    }

    @Override
    public Page<T> listAllMedia(String repo, Pageable page) {
        return isBlank(repo) ? listAllMedia(page) : repository.findAll(Set.of(repo), page);
    }

    @Override
    public List<T> listMissingMedia() {
        return repository.findMissingInCommons(getRepoIds());
    }

    @Override
    public Page<T> listMissingMedia(Pageable page) {
        return repository.findMissingInCommons(getRepoIds(), page);
    }

    @Override
    public Page<T> listMissingMedia(String repo, Pageable page) {
        return isBlank(repo) ? listMissingMedia(page) : repository.findMissingInCommons(Set.of(repo), page);
    }

    @Override
    public Page<T> listMissingImages(Pageable page) {
        return repository.findMissingImagesInCommons(getRepoIds(), page);
    }

    @Override
    public Page<T> listMissingImages(String repo, Pageable page) {
        return isBlank(repo) ? listMissingImages(page) : repository.findMissingImagesInCommons(Set.of(repo), page);
    }

    @Override
    public Page<T> listMissingVideos(Pageable page) {
        return repository.findMissingVideosInCommons(getRepoIds(), page);
    }

    @Override
    public Page<T> listMissingVideos(String repo, Pageable page) {
        return isBlank(repo) ? listMissingVideos(page) : repository.findMissingVideosInCommons(Set.of(repo), page);
    }

    @Override
    public Page<T> listMissingDocuments(Pageable page) {
        return repository.findMissingDocumentsInCommons(getRepoIds(), page);
    }

    @Override
    public Page<T> listMissingDocuments(String repo, Pageable page) {
        return isBlank(repo) ? listMissingDocuments(page)
                : repository.findMissingDocumentsInCommons(Set.of(repo), page);
    }

    @Override
    public List<T> listMissingMediaByDate(LocalDate date, String repo) {
        return repository.findMissingInCommonsByPublicationDate(isBlank(repo) ? getRepoIds() : Set.of(repo), date);
    }

    @Override
    public List<T> listMissingMediaByMonth(YearMonth month, String repo) {
        return repository.findMissingInCommonsByPublicationMonth(isBlank(repo) ? getRepoIds() : Set.of(repo), month);
    }

    @Override
    public List<T> listMissingMediaByYear(Year year, String repo) {
        return repository.findMissingInCommonsByPublicationYear(isBlank(repo) ? getRepoIds() : Set.of(repo), year);
    }

    @Override
    public List<T> listMissingMediaByTitle(String title, String repo) {
        return repository.findMissingInCommonsByTitle(isBlank(repo) ? getRepoIds() : Set.of(repo), title);
    }

    @Override
    public Page<T> listHashedMedia(Pageable page) {
        return repository.findByMetadata_PhashNotNull(getRepoIds(), page);
    }

    @Override
    public Page<T> listHashedMedia(String repo, Pageable page) {
        return isBlank(repo) ? listHashedMedia(page) : repository.findByMetadata_PhashNotNull(Set.of(repo), page);
    }

    @Override
    public List<T> listUploadedMedia() {
        return repository.findUploadedToCommons(getRepoIds());
    }

    @Override
    public Page<T> listUploadedMedia(Pageable page) {
        return repository.findUploadedToCommons(getRepoIds(), page);
    }

    @Override
    public List<T> listUploadedMediaByDate(LocalDate date) {
        return repository.findUploadedToCommonsByPublicationDate(getRepoIds(), date);
    }

    @Override
    public Page<T> listUploadedMedia(String repo, Pageable page) {
        return isBlank(repo) ? listUploadedMedia(page) : repository.findUploadedToCommons(Set.of(repo), page);
    }

    @Override
    public List<T> listDuplicateMedia() {
        return repository.findDuplicateInCommons(getRepoIds());
    }

    @Override
    public List<T> listIgnoredMedia() {
        return repository.findByMetadata_IgnoredTrue(getRepoIds());
    }

    @Override
    public Page<T> listIgnoredMedia(Pageable page) {
        return repository.findByMetadata_IgnoredTrue(getRepoIds(), page);
    }

    @Override
    public Page<T> listIgnoredMedia(String repo, Pageable page) {
        return isBlank(repo) ? listIgnoredMedia(page) : repository.findByMetadata_IgnoredTrue(Set.of(repo), page);
    }

    @Override
    public List<T> searchMedia(String q) {
        searchService.checkSearchEnabled();
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<T> searchMedia(String q, Pageable page) {
        searchService.checkSearchEnabled();
        throw new UnsupportedOperationException();
    }

    /**
     * Returns an unique identifier used for REST controllers and database entries.
     *
     * @return an unique identifier specified by implementations
     */
    @Override
    public String getId() {
        return id;
    }

    public SortedSet<String> getRepoIds() {
        return repoIds;
    }

    public SortedSet<String> getRepoIdsSortedByUiText() {
        return getUiRepoIds().parallelStream()
                .collect(toCollection(
                        () -> new TreeSet<>((a, b) -> getSortableUiRepoId(a).compareTo(getSortableUiRepoId(b)))));
    }

    private String getSortableUiRepoId(String repoId) {
        // TreeSet expects Comparable behaviour is consistent with equals
        return getUiRepoId(repoId) + repoId;
    }

    protected Set<String> getRepoIdsFromArgs(String[] args) {
        if (args != null && args.length >= 1 && !isBlank(args[args.length - 1])) {
            Set<String> ids = stream(args[args.length - 1].split(",")).filter(repoIds::contains).collect(toSet());
            if (!ids.isEmpty()) {
                return ids;
            }
        }
        return repoIds;
    }

    protected static LocalDate getLocalDateFromArgs(String[] args) {
        if (args != null && args.length >= 1 && !isBlank(args[args.length - 1])) {
            try {
                return LocalDate.parse(args[args.length - 1]);
            } catch (DateTimeParseException e) {
                LOGGER.trace("Can't parse date", e);
            }
        }
        return LocalDate.now();
    }

    public SortedSet<String> getUiRepoIds() {
        return getRepoIds();
    }

    public String getUiRepoId(String repoId) {
        return repoId;
    }

    public String getUiTooltip(String repoId) {
        return repoId;
    }

    protected String getRegionalIndicatorSymbol(String countryQid) throws IOException {
        // https://en.wikipedia.org/wiki/Regional_indicator_symbol
        String countryCode = wikidata.findIso3166Alpha2(countryQid);
        if (countryCode == null) {
            throw new IllegalStateException(countryQid);
        }
        return new String(Character.toChars(Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6))
                + new String(Character.toChars(Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6));
    }

    public boolean isMultiRepo() {
        return getUiRepoIds().size() > 1;
    }

    protected final LocalDateTime startUpdateMedia() {
        Thread.currentThread().setName("media-update-" + getId());
        LOGGER.info("Starting {} medias update...", getName());
        RuntimeData runtimeData = getRuntimeData();
        runtimeData.setLastUpdateStart(LocalDateTime.now());
        return runtimeDataRepository.save(runtimeData).getLastUpdateStart();
    }

    protected final void ongoingUpdateMedia(LocalDateTime start, int count) {
        ongoingUpdateMedia(start, getId(), count);
    }

    protected final void ongoingUpdateMedia(LocalDateTime start, String who, int count) {
        if (LOGGER.isInfoEnabled() && count > 0 && count % 1000 == 0) {
            Duration durationInSec = durationInSec(start);
            LOGGER.info("Processed {} {} media in {} ({} media/s) - ({} ms/media)", count, who, durationInSec,
                    String.format("%.2f", (double) count / durationInSec.getSeconds()),
                    String.format("%d", durationInSec.getSeconds() * 1000 / count));
        }
    }

    protected final List<FileMetadata> allMetadata(Collection<T> uploadedMedia) {
        return uploadedMedia.stream().flatMap(Media::getMetadataStream).toList();
    }

    protected final void endUpdateMedia(int count, Collection<T> uploadedMedia) {
        endUpdateMedia(count, uploadedMedia.size(), uploadedMedia);
    }

    protected final void endUpdateMedia(int count, int uploadCount, Collection<T> uploadedMedia) {
        endUpdateMedia(count, uploadCount, uploadedMedia, allMetadata(uploadedMedia));
    }

    protected final void endUpdateMedia(int count, Collection<T> uploadedMedia,
            Collection<FileMetadata> uploadedMetadata) {
        endUpdateMedia(count, uploadedMetadata.size(), uploadedMedia, uploadedMetadata);
    }

    protected final void endUpdateMedia(int count, int uploadCount, Collection<T> uploadedMedia,
            Collection<FileMetadata> uploadedMetadata) {
        endUpdateMedia(count, uploadCount, uploadedMedia, uploadedMetadata, LocalDate.now().minusDays(7), null);
    }

    protected final void endUpdateMedia(int count, int uploadCount, Collection<T> uploadedMedia,
            Collection<FileMetadata> uploadedMetadata, LocalDate newDoNotFetchEarlierThan) {
        endUpdateMedia(count, uploadCount, uploadedMedia, uploadedMetadata, newDoNotFetchEarlierThan, null);
    }

    protected final void endUpdateMedia(int count, int uploadCount, Collection<T> uploadedMedia,
            Collection<FileMetadata> uploadedMetadata, LocalDate newDoNotFetchEarlierThan,
            LocalDate newDoNotFetchLaterThan) {
        RuntimeData runtimeData = getRuntimeData();
        LocalDateTime start = runtimeData.getLastUpdateStart();
        LocalDateTime end = LocalDateTime.now();
        runtimeData.setLastUpdateEnd(end);
        Duration lastUpdateDuration = Duration.between(start, end);
        runtimeData.setLastUpdateDuration(lastUpdateDuration);
        runtimeData.setLastUpdateDurationMin(lastUpdateDuration.toMinutes());
        if (uploadCount <= 0) {
            runtimeData.setLastUpdateDurationWithoutUploadsMin(lastUpdateDuration.toMinutes());
        } else {
            runtimeData.setLastUpdateDurationWithUploadsMin(lastUpdateDuration.toMinutes());
        }
        runtimeData.setDoNotFetchEarlierThan(newDoNotFetchEarlierThan);
        runtimeData.setDoNotFetchLaterThan(newDoNotFetchLaterThan);
        LOGGER.info("{} medias update completed: {} medias in {}", getName(), count,
                runtimeDataRepository.save(runtimeData).getLastUpdateDuration());
        if (postSocialMediaAtUpdateEnd()) {
            postSocialMedia(uploadedMedia, uploadedMetadata);
        }
    }

    protected Collection<String> getStringsToRemove(T media) {
        // To be overriden if special strings have to be removed from description
        return List.of();
    }

    protected Collection<Pattern> getPatternsToRemove(T media) {
        // To be overriden if special strings have to be removed from description
        return List.of();
    }

    protected final void postSocialMedia(Collection<? extends T> uploadedMedia) {
        postSocialMedia(uploadedMedia, uploadedMedia.stream().flatMap(Media::getMetadataStream).toList());
    }

    protected final void postSocialMedia(Collection<? extends T> uploadedMedia, Collection<FileMetadata> uploadedMetadata) {
        if (!uploadedMedia.isEmpty()) {
            LOGGER.info("Uploaded media: {}", uploadedMedia.size());
            if (shouldPostOnSocialMedia()) {
                socialMediaServices.forEach(socialMedia -> {
                    try {
                        Set<String> accounts = getSocialMediaAccounts(socialMedia, uploadedMedia);
                        if (accounts.isEmpty()) {
                            accounts = Set.of(getName());
                        }
                        socialMedia.postStatus(uploadedMedia, uploadedMetadata, getEmojis(uploadedMedia), accounts);
                    } catch (IOException e) {
                        LOGGER.error("Failed to post status: {}", e.getMessage());
                        LOGGER.debug("Failed to post status: {}", e.getMessage(), e);
                        GlitchTip.capture(e);
                    }
                });
            }
        }
    }

    /**
     * Determines if the social media sharing feature is enabled for this
     * organization.
     *
     * @return {@code true} if the social media sharing feature is enabled for this
     *         organization
     */
    protected boolean shouldPostOnSocialMedia() {
        return postSocialMedia;
    }

    /**
     * Determines if the social media sharing shall be performed at the end of the
     * media update process.
     *
     * @return {@code true} if the social media sharing shall be performed at the
     *         end of the media update process.
     */
    protected boolean postSocialMediaAtUpdateEnd() {
        return shouldPostOnSocialMedia();
    }

    protected final Set<String> getEmojis(Collection<? extends T> uploadedMedia) {
        return uploadedMedia.stream().flatMap(media -> getEmojis(media).stream()).collect(toSet());
    }

    protected final Set<String> getSocialMediaAccounts(AbstractSocialMediaService<?, ?> socialMedia,
            Collection<? extends T> uploadedMedia) {
        if (socialMedia instanceof MastodonService) {
            return uploadedMedia.stream().flatMap(media -> getMastodonAccounts(media).stream()).collect(toSet());
        }
        return Set.of();
    }

    protected Set<String> getEmojis(T uploadedMedia) {
        Set<String> result = new HashSet<>();
        if (uploadedMedia.getDescription() != null) {
            String description = uploadedMedia.getDescription().text();
            if (description != null) {
                if (List.of("astronaut", "cosmonaut", "spationaut").stream().anyMatch(description::contains)) {
                    result.add(Emojis.ASTRONAUT);
                }
                if (List.of("lift off", "lifts off").stream().anyMatch(description::contains)) {
                    result.add(Emojis.ROCKET);
                }
            }
        }
        if (uploadedMedia instanceof WithKeywords kw) {
            result.addAll(AbstractSocialMediaService.getEmojis(kw.getKeywords()));
        }
        return result;
    }

    protected Set<String> getMastodonAccounts(T uploadedMedia) {
        return new HashSet<>();
    }

    @Override
    public Statistics getStatistics(boolean details) {
        Statistics stats = new Statistics(getName(), getId(), countAllMedia(), countUploadedMedia(), countIgnored(),
                countMissingImages(), countMissingVideos(), countMissingDocuments(), countPerceptualHashes(),
                getRuntimeData().getLastUpdateEnd());
        if (details && getRepoIds().size() > 1) {
            stats.setDetails(getRepoIds().stream().map(this::getStatistics).sorted().toList());
        }
        return stats;
    }

    private Statistics getStatistics(String alias) {
        Set<String> singleton = Collections.singleton(alias);
        return new Statistics(alias, alias, repository.count(singleton), repository.countUploadedToCommons(singleton),
                repository.countByMetadata_IgnoredTrue(singleton), repository.countMissingImagesInCommons(singleton),
                repository.countMissingVideosInCommons(singleton), repository.countMissingDocumentsInCommons(singleton),
                repository.countByMetadata_PhashNotNull(singleton),
                null);
    }

    protected final void problem(URL problematicUrl, Throwable t) {
        problem(problematicUrl, t.toString());
    }

    protected final void problem(String problematicUrl, Throwable t) {
        problem(problematicUrl, t.toString());
    }

    protected final void problem(String problematicUrl, String errorMessage) {
        problem(newURL(problematicUrl), errorMessage);
    }

    protected final void problem(URL problematicUrl, String errorMessage) {
        LOGGER.warn("{} => {}", problematicUrl, errorMessage);
    }

    protected final T findBySomeSha1OrThrow(String sha1, Function<String, List<T>> finder, boolean throwIfNotFound)
            throws TooManyResultsException {
        List<T> result = finder.apply(sha1);
        if (isEmpty(result)) {
            if (throwIfNotFound) {
                throw new ImageNotFoundException(sha1);
            } else {
                return null;
            }
        }
        if (result.size() > 1) {
            throw new TooManyResultsException("Several images found for " + sha1);
        }
        return result.get(0);
    }

    protected T findBySha1OrThrow(String sha1, boolean throwIfNotFound) throws TooManyResultsException {
        return findBySomeSha1OrThrow(sha1, repository::findByMetadata_Sha1, throwIfNotFound);
    }

    @Override
    public T getById(CompositeMediaId id) throws ImageNotFoundException {
        LOGGER.info("Looking for media by id: {}", id);
        return repository.findById(id).orElseThrow(() -> new ImageNotFoundException(id.toString()));
    }

    @Override
    public void deleteById(String id) throws ImageNotFoundException {
        repository.deleteById(new CompositeMediaId(id));
    }

    @Override
    public T refreshAndSaveById(String id) throws ImageNotFoundException, IOException {
        return refreshAndSave(getById(id));
    }

    @Override
    public List<T> refreshAndSaveByDate(LocalDate date, String repo, Predicate<Media> predicate)
            throws ImageNotFoundException, IOException {
        return refreshAndSaveMedias(listMissingMediaByDate(date, repo).stream().filter(predicate));
    }

    @Override
    public List<T> refreshAndSaveByMonth(YearMonth month, String repo, Predicate<Media> predicate)
            throws ImageNotFoundException, IOException {
        return refreshAndSaveMedias(listMissingMediaByMonth(month, repo).stream().filter(predicate));
    }

    @Override
    public List<T> refreshAndSaveByYear(Year year, String repo, Predicate<Media> predicate)
            throws ImageNotFoundException, IOException {
        return refreshAndSaveMedias(listMissingMediaByYear(year, repo).stream().filter(predicate));
    }

    private List<T> refreshAndSaveMedias(Stream<T> medias) {
        return medias.map(media -> {
            try {
                return refreshAndSave(media);
            } catch (IOException e) {
                LOGGER.error("Failed to refresh {}", media, e);
                GlitchTip.capture(e);
                return null;
            }
        }).filter(Objects::nonNull).toList();
    }

    @Override
    public T refreshAndSave(T media) throws IOException {
        T refreshedMedia = null;
        try {
            refreshedMedia = refresh(media);
        } catch (NotFound e) {
            LOGGER.warn("Refresh of {} failed: {}", media, e.getMessage());
        } catch (HttpStatusException e) {
            if (e.getStatusCode() != 404) {
                throw e;
            }
            LOGGER.warn("Refresh of {} failed: {}", media, e.getMessage());
        }
        if (refreshedMedia != null) {
            try (CloseableHttpClient httpClient = configureHttpClientBuilder(httpClientBuilder()).build()) {
                doCommonUpdate(refreshedMedia, httpClient, null, true);
            }
            return saveMedia(refreshedMedia);
        } else {
            deleteMedia(media, "refresh did not find media anymore");
            return null;
        }
    }

    protected abstract T refresh(T media) throws IOException;

    @Override
    public T saveMedia(T media) {
        LOGGER.info("Saving {}", media);
        T result = repository.save(media);
        checkRemoteMedia(result);
        return result;
    }

    @Override
    public T syncAndSave(T media) throws IOException {
        media.getMetadataStream().forEach(fm -> {
            for (String filename : fm.getCommonsFileNames()) {
                SortedSet<String> catsToAdd = new TreeSet<>();
                for (String cat : getSyncCategories(media, fm, filename)) {
                    if (!commonsService.isInCategory(filename, c -> cat.replace(' ', '_').equals(c.getId().getTo()))) {
                        LOGGER.warn("{} should belong to Category:{}", filename, cat);
                        catsToAdd.add(cat);
                    }
                }
                if (!catsToAdd.isEmpty()) {
                    try {
                        commonsService.addCategories(filename, catsToAdd.toArray(new String[0]));
                    } catch (IOException e) {
                        LOGGER.warn("Cannot update {}: {}", filename, e.getMessage());
                    }
                }
            }
        });
        return media;
    }

    protected Set<String> getSyncCategories(T media, FileMetadata fm, String filename) { // NOSONAR
        if (commonsService.hasBeenUploadedByMe(filename)) {
            return Set.of(hiddenUploadCategory(media.getId().repoId()));
        }
        return Set.of();
    }

    protected final void checkRemoteMedia(T media) {
        try {
            if (mediaService.getExecutionMode() == ExecutionMode.REMOTE
                    && remoteService.getMedia(getId(), media.getId().toString(), media.getClass()) == null) {
                remoteService.saveMedia(getId(), media);
            } else if (mediaService.getExecutionMode() == ExecutionMode.LOCAL) {
                evictRemoteCaches();
            }
        } catch (IOException | RestClientException e) {
            LOGGER.warn("Remote instance returned: {}", e.getMessage());
        }
    }

    protected final void evictRemoteCaches() {
        try {
            if (remoteService.isEnabled() && canEvictCache()) {
                remoteService.evictCaches(getId());
                lastCacheEviction = LocalDateTime.now();
            }
        } catch (IOException | RestClientException e) {
            LOGGER.warn("Remote instance returned: {}", e.getMessage());
        }
    }

    private boolean canEvictCache() {
        return lastCacheEviction == null || lastCacheEviction.isBefore(LocalDateTime.now().minusMinutes(5));
    }

    protected final T saveMediaOrCheckRemote(boolean save, T media) {
        if (save) {
            T savedMedia = saveMedia(media);
            checkRemoteMedia(savedMedia);
            return savedMedia;
        } else {
            return media;
        }
    }

    protected final T deleteMedia(T media, Exception e) {
        return deleteMedia(media, e.getMessage());
    }

    protected final T deleteMedia(T media, String message) {
        LOGGER.warn("Deleting {} ({})", media, message);
        repository.delete(media);
        return media;
    }

    public final boolean isUploadEnabled() {
        return uploadMode != UploadMode.DISABLED;
    }

    @Override
    public T uploadAndSaveById(String id, boolean isManual) throws UploadException, TooManyResultsException {
        return saveMedia(upload(getById(id), false, isManual).getLeft());
    }

    @Override
    public T uploadAndSaveBySha1(String sha1, boolean isManual) throws UploadException, TooManyResultsException {
        return saveMedia(upload(findBySha1OrThrow(sha1, true), true, isManual).getLeft());
    }

    @Override
    public List<T> uploadAndSaveByDate(LocalDate date, String repo, Predicate<Media> predicate, boolean isManual)
            throws UploadException {
        return uploadAndSaveMedias(listMissingMediaByDate(date, repo).stream().filter(predicate), isManual);
    }

    @Override
    public List<T> uploadAndSaveByMonth(YearMonth month, String repo, Predicate<Media> predicate, boolean isManual)
            throws UploadException {
        return uploadAndSaveMedias(listMissingMediaByMonth(month, repo).stream().filter(predicate), isManual);
    }

    @Override
    public List<T> uploadAndSaveByYear(Year year, String repo, Predicate<Media> predicate, boolean isManual)
            throws UploadException {
        return uploadAndSaveMedias(listMissingMediaByYear(year, repo).stream().filter(predicate), isManual);
    }

    @Override
    public List<T> uploadAndSaveByTitle(String title, String repo, Predicate<Media> predicate, boolean isManual)
            throws UploadException {
        return uploadAndSaveMedias(listMissingMediaByTitle(title, repo).stream().filter(predicate), isManual);
    }

    private List<T> uploadAndSaveMedias(Stream<T> medias, boolean isManual) {
        return medias.map(media -> {
            try {
                return saveMedia(upload(media, false, isManual).getLeft());
            } catch (UploadException e) {
                LOGGER.error("Failed to upload or save {}", media, e);
                GlitchTip.capture(e);
                return null;
            }
        }).filter(Objects::nonNull).toList();
    }

    protected final Triple<T, Collection<FileMetadata>, Integer> uploadWrapped(T media) {
        try {
            return upload(media, false, false);
        } catch (UploadException e) {
            LOGGER.error("Failed to upload {}", media, e);
            GlitchTip.capture(e);
            throw new WrappedUploadException(e);
        }
    }

    @Override
    public Triple<T, Collection<FileMetadata>, Integer> upload(T media, boolean checkUnicity, boolean isManual)
            throws UploadException {
        if (!isUploadEnabled()) {
            throw new ImageUploadForbiddenException("Upload is not enabled for " + getClass().getSimpleName());
        }
        try {
            checkUploadPreconditions(media, checkUnicity, isManual);
            List<FileMetadata> uploaded = new ArrayList<>();
            return Triple.of(media, uploaded, doUpload(media, checkUnicity, uploaded, isManual));
        } catch (RuntimeException e) {
            LOGGER.error("Failed to upload {}", media, e);
            GlitchTip.capture(e);
            throw new UploadException(e);
        }
    }

    protected int doUpload(T media, boolean checkUnicity, Collection<FileMetadata> uploaded, boolean isManual) {
        int count = 0;
        for (FileMetadata metadata : media.getMetadataStream().filter(FileMetadata::shouldUpload).toList()) {
            try {
                count += doUpload(media, metadata, checkUnicity, uploaded, isManual);
            } catch (ImageUploadForbiddenException | UploadException | IOException e) {
                LOGGER.warn("File {} not uploaded: {}", metadata, e.getMessage());
            } catch (IgnoreException e) {
                mediaService.ignoreAndSaveMetadata(metadata, e.getMessage(), e);
            } catch (RuntimeException e) {
                LOGGER.error("Failed to upload {}", metadata, e);
                throw e;
            }
        }
        return count;
    }

    protected final int doUpload(T media, FileMetadata metadata, boolean checkUnicity, Collection<FileMetadata> uploaded,
            boolean isManual) throws IOException, UploadException {
        if (metadata != null && metadata.getAssetUrl() != null && new UploadContext<>(media, metadata, getUploadMode(),
                mediaService.getMinYearUploadAuto(), this::isPermittedFileType, isManual).shouldUpload()) {
            checkUploadPreconditions(media, metadata, checkUnicity);
            List<String> smallerFiles = mediaService.findSmallerCommonsFilesWithSearchTermAndPhash(media, metadata,
                    false);
            URL downloadUrl = getUrlResolver().resolveDownloadUrl(media, metadata);
            LOGGER.debug("Download URL resolved to {}", downloadUrl);
            if (!smallerFiles.isEmpty()) {
                LOGGER.info(
                        "Found existing smaller files with same id and perceptual hash on Commons, replacing them: {}",
                        smallerFiles);
                for (String smallerFile : smallerFiles) {
                    commonsService.uploadExistingFile(smallerFile, downloadUrl, metadata.getSha1(), getId(),
                            media.getId(), metadata.getId());
                }
                metadata.setCommonsFileNames(new HashSet<>(smallerFiles));
            } else {
                Pair<String, List<LocalizedText>> codeAndLegends = getWikiCode(media, metadata);
                LOGGER.debug("Wikicode and legends: {}", codeAndLegends);
                String uploadedFilename = commonsService.uploadNewFile(codeAndLegends.getLeft(),
                        media.getUploadTitle(metadata, () -> getOrgName(media)), metadata.getFileExtension(),
                        downloadUrl, metadata.getSha1(),
                        getId(), media.getId(), metadata.getId(), Boolean.FALSE != metadata.isAudioTrack());
                LOGGER.debug("Uploaded filename: {}", uploadedFilename);
                metadata.setCommonsFileNames(new HashSet<>(Set.of(uploadedFilename)));
                editStructuredDataContent(uploadedFilename, codeAndLegends.getRight(), media, metadata);
                createNavigationalCategory(media);
            }
            uploaded.add(metadataRepository.save(metadata));
            evictRemoteCaches();
            return 1;
        } else {
            if (metadata != null) {
                LOGGER.info("Upload not done for {} / {}. Upload mode: {}. Ignored: {}. Permitted file type: {}",
                        media.getId(), metadata, getUploadMode(), media.isIgnored(), isPermittedFileType(metadata));
            }
            return 0;
        }
    }

    @Override
    public FileMetadata retrieveMetadata(CompositeMediaId mediaId, URL assetUrl) {
        FileMetadata metadata = metadataRepository.findByAssetUrl(assetUrl).orElse(null);
        if (metadata == null && getById(mediaId) instanceof SingleFileMedia sfm) {
            metadata = sfm.getUniqueMetadata();
        }
        if (metadata == null) {
            throw new IllegalStateException("Failed to retrieve metadata for " + mediaId + " / " + assetUrl);
        }
        return metadata;
    }

    @Override
    public void editStructuredDataContent(String uploadedFilename, CompositeMediaId mediaId, URL assetUrl) {
        try {
            T media = getById(mediaId);
            FileMetadata metadata = retrieveMetadata(mediaId, assetUrl);
            editStructuredDataContent(uploadedFilename, getLegends(media, getWikiFileDesc(media, metadata).getValue()),
                    media, metadata);
        } catch (ImageNotFoundException | IOException e) {
            LOGGER.error("Unable to edit SDC data: {} => {} => {}", mediaId, uploadedFilename, e.getMessage());
            GlitchTip.capture(e);
        }
    }

    protected void editStructuredDataContent(String uploadedFilename, List<LocalizedText> legends, T media,
            FileMetadata metadata) {
        SdcStatements statements = getStatements(media, metadata);
        try {
            GlitchTip.setTag("media", media.getIdUsedInOrg());
            GlitchTip.setTag("repo", media.getId().repoId());
            GlitchTip.setTag("metadata", metadata.getId().toString());
            commonsService.editStructuredDataContent(uploadedFilename, legends, statements);
        } catch (MediaWikiApiErrorException | IOException | RuntimeException e) {
            // Silent errors for files uploaded long after by video2commons
            if (V2C_VIDEO_EXTENSIONS.contains(metadata.getFileExtension())
                    || metadata.getAssetUri().toString().startsWith("https://www.youtube.com/")) {
                LOGGER.info("Unable to add SDC data: {} => {}", statements, e.getMessage());
            } else {
                LOGGER.error("Unable to add SDC data: {} => {}", statements, e.getMessage());
                GlitchTip.capture(e);
            }
        }
    }

    protected SdcStatements getStatements(T media, FileMetadata metadata) {
        SdcStatements result = new SdcStatements();
        // Source: file available on the internet
        result.put("P7482", Pair.of("Q74228490",
                new TreeMap<>(Map.of("P973", getSourceUrl(media, metadata, metadata.getExtension()), "P2699",
                        metadata.getAssetUrl()))));
        // Licences
        Set<String> licences = findLicenceTemplates(media, metadata);
        if (PD_US.stream().anyMatch(pd -> licences.stream().anyMatch(l -> l.startsWith(pd)))) {
            result.put("P6216", Pair.of("Q19652", new TreeMap<>(Map.of("P459", "Q60671452", "P1001", "Q30"))));
        } else {
            result.put("P6216", Pair.of("Q50423863", null));
            LICENCES.entrySet().stream().filter(e -> licences.stream().anyMatch(l -> l.startsWith(e.getKey())))
                    .map(Entry::getValue).distinct().forEach(l -> result.put("P275", Pair.of(l, null)));
        }
        // MIME type
        ofNullable(metadata.getMime()).ifPresent(mime -> result.put("P1163", Pair.of(mime, null)));
        // Hashes
        ofNullable(metadata.getSha1()).ifPresent(h -> result.put("P4092", Pair.of(h, Map.of("P459", "Q13414952"))));
        ofNullable(metadata.getPhash()).ifPresent(h -> result.put("P9310", Pair.of(h, Map.of("P459", "Q118189277"))));
        // Dates
        Temporal creationDate = getCreationDate(media).orElse(null);
        Temporal publicationDate = getUploadDate(media).orElse(null);
        if (creationDate != null) {
            result.put("P571", Pair.of(creationDate, null));
            if (publicationDate != null) {
                result.put("P577", Pair.of(publicationDate, null));
            }
        } else if (publicationDate != null) {
            result.put("P571", Pair.of(publicationDate, null));
        }
        // File size
        if (metadata.hasSize()) {
            result.put("P3575", Pair.of(Pair.of(metadata.getSize(), "Q8799"), null));
        }
        // Video
        if (metadata.isVideo()) {
            result.instanceOf(WikidataItem.Q98069877_VIDEO);
        }
        // Dimensions
        MediaDimensions dims = metadata.getMediaDimensions();
        if (dims != null) {
            ofNullable(dims.getWidth())
                    .ifPresent(w -> result.put("P2049", Pair.of(Pair.of(w, "Q355198"), null)));
            ofNullable(dims.getHeight())
                    .ifPresent(h -> result.put("P2048", Pair.of(Pair.of(h, "Q355198"), null)));
            ofNullable(dims.getDuration())
                    .ifPresent(d -> result.put("P2047", Pair.of(Pair.of(d.toMillis(), "Q723733"), null)));
        }
        // Location
        if (media instanceof WithLatLon ll && (ll.getLatitude() != 0d || ll.getLongitude() != 0d)) {
            result.put("P9149", Pair
                    .of(Triple.of(ll.getLatitude(), ll.getLongitude(), ll.getPrecision()), null));
        }
        // Categories
        categorizationService.findCategoriesStatements(result, findCategories(media, metadata, false));
        return result;
    }

    protected final void wikidataStatementMapping(String value, Map<String, Map<String, String>> csvMapping,
            String property, SdcStatements result) {
        ofNullable(value).map(csvMapping::get).map(m -> m.get("Wikidata")).filter(Objects::nonNull)
                .ifPresent(m -> result.put(property, Pair.of(m, null)));
    }

    @Override
    public String getWikiHtmlPreview(String sha1) throws TooManyResultsException {
        return getWikiHtmlPreview(findBySha1OrThrow(sha1, true), metadataRepository.findBySha1(sha1));
    }

    protected final String getWikiHtmlPreview(T media, FileMetadata metadata) {
        try {
            return commonsService.getWikiHtmlPreview(getWikiCode(media, metadata).getKey(), getPageTitle(media),
                    metadata.getAssetUrl().toExternalForm());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected String getPageTitle(T media) {
        return media.getTitle().text();
    }

    @Override
    public String getWikiCode(String sha1) throws TooManyResultsException {
        return getWikiCode(findBySha1OrThrow(sha1, true), metadataRepository.findBySha1(sha1)).getKey();
    }

    @Override
    public Pair<String, List<LocalizedText>> getWikiCode(T media, FileMetadata metadata) {
        try {
            Pair<String, List<LocalizedText>> desc = getWikiFileDesc(media, metadata);
            StringBuilder sb = new StringBuilder("== {{int:filedesc}} ==\n")
                    .append(desc.getKey())
                    .append("\n=={{int:license-header}}==\n");
            findLicenceTemplates(media, metadata).forEach(t -> sb.append("{{").append(t).append("}}\n"));
            commonsService
                    .cleanupCategories(findCategories(media, metadata, true), media.getBestTemporal(), needsReview())
                    .forEach(t -> sb.append("[[Category:").append(t).append("]]\n"));
            return Pair.of(sb.toString(), getLegends(media, desc.getValue()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected List<LocalizedText> getLegends(T media, List<LocalizedText> descriptions) {
        return descriptions;
    }

    protected List<LocalizedText> replaceLegendByCorrectSentence(String text, List<LocalizedText> descriptions) {
        // For services that usually don't describe the picture in the first sentence.
        return descriptions.stream().filter(Objects::nonNull).map(lt -> {
            int idxText = lt.indexOf(text);
            if (idxText > -1) {
                int idxStart = lt.lastIndexOf('.', idxText) + 1;
                int idxEnd = lt.indexOf('.', idxText);
                return (idxEnd > -1 ? lt.substring(idxStart, idxEnd + 1) : lt.substring(idxStart)).trim();
            }
            return lt;
        }).toList();
    }

    protected final Pair<String, List<LocalizedText>> getWikiFileDesc(T media, FileMetadata metadata)
            throws IOException {
        LocalizedText description = getDescription(media, metadata);
        StringBuilder sb = new StringBuilder();
        findBeforeInformationTemplates(media, metadata).forEach(t -> sb.append("{{").append(t).append("}}\n"));
        List<LocalizedText> descriptions = new ArrayList<>(List.of(description));
        if (!"en".equals(description.lang()) && isNotBlank(description)) {
            descriptions.add(
                    new LocalizedText("en", translateService.translate(description.text(), description.lang(), "en")));
        }
        appendInformationTemplate(sb, media, metadata, descriptions);
        if (media instanceof WithLatLon ll && (ll.getLatitude() != 0d || ll.getLongitude() != 0d)) {
            sb.append("{{Object location |1=" + ll.getLatitude() + " |2=" + ll.getLongitude() + "}}\n");
        }
        findAfterInformationTemplates(media, metadata).forEach(t -> sb.append("{{").append(t).append("}}\n"));
        return Pair.of(sb.toString(), descriptions);
    }

    protected StringBuilder appendInformationTemplate(StringBuilder sb, T media, FileMetadata metadata,
            List<LocalizedText> descriptions) {
        sb.append("{{Information");
        appendWikiDescriptions(sb, descriptions, "description");
        appendWikiDate(sb, media);
        appendWikiSource(sb, media, metadata);
        ofNullable(formatWikiCode(getAuthor(media, metadata))).ifPresent(s -> sb.append("\n| author = ").append(s));
        getPermission(media).ifPresent(s -> sb.append("\n| permission = ").append(s));
        return addOthersAndComplete(sb, media, metadata);
    }

    protected final void appendWikiDate(StringBuilder sb, T media) {
        getWikiDate(media).ifPresent(s -> sb.append("\n| date = ").append(s));
    }

    protected final void appendWikiDescriptions(StringBuilder sb, List<LocalizedText> descriptions, String key) {
        sb.append("\n| ").append(key).append(" = ");
        for (LocalizedText description : descriptions) {
            appendWikiDescriptionInLanguage(sb, description);
        }
    }

    protected final void appendWikiSource(StringBuilder sb, T media, FileMetadata metadata) {
        sb.append("\n| source = ").append(getSource(media, metadata));
    }

    protected final void appendWikiOtherVersions(StringBuilder sb, T media, FileMetadata metadata, String key) {
        getOtherVersions(media, metadata).ifPresent(
                s -> sb.append("\n| ").append(key).append(" = <gallery>\n").append(s).append("\n</gallery>"));
    }

    protected final void appendWikiDescriptionInLanguage(StringBuilder sb, LocalizedText description) {
        sb.append("{{").append(description.lang()).append("|1=").append(formatWikiCode(description.text()))
                .append("}}");
    }

    protected final Optional<String> getWikiDate(T media) {
        return getCreationWikiDate(media).or(() -> getUploadWikiDate(media));
    }

    protected final Optional<String> getCreationWikiDate(T media) {
        String location = getTakenLocation(media);
        return getCreationDate(media).map(d -> String.format(
                "{{Taken %s|%s|location=%s}}",
                d instanceof LocalDate || d instanceof LocalDateTime || d instanceof ZonedDateTime
                        || d instanceof Instant ? "on" : "in",
                toIso8601(d), Set.of("At Sea", "Undisclosed Location").contains(location)
                        || location.toLowerCase(ENGLISH).contains("area of responsibility") ? "" : location));
    }

    /**
     * Return the country in which a photograph has been taken, to be used in Taken
     * in/on templates and navigational categories. By extension it can also be a
     * celestial body.
     *
     * In case of overriding implementation, be careful to call
     * {@link #fixCountryForCommons} to take care of special cases like Georgia.
     *
     * @param media media
     * @return English country/celestial body name suitable for Wikimedia Commons
     */
    protected String getTakenLocation(T media) {
        if (media instanceof WithLatLon ll) {
            try {
                Address address = nominatim.reverse(ll.getLatitude(), ll.getLongitude(), 5).address();
                if (address != null && isNotBlank(address.country())) {
                    return fixCountryForCommons(address.country(), media.getDescription());
                }
            } catch (IOException e) {
                LOGGER.error("Failed to retrieve location from nominatim for {}", ll, e);
            }
        }
        return "";
    }

    protected String fixCountryForCommons(String country, LocalizedText description) { // NOSONAR
        return "Georgia".equals(country) ? "Georgia (country)" : country;
    }

    protected final Optional<String> getUploadWikiDate(T media) {
        return getUploadDate(media).map(d -> String.format("{{Upload date|%s}}", toIso8601(d)));
    }

    protected final String toIso8601(Temporal t) {
        Temporal d = t;
        if (d instanceof Instant instant) {
            d = instant.atZone(ZoneOffset.UTC);
        }
        if ((d instanceof LocalDateTime || d instanceof ZonedDateTime)
                && d.get(ChronoField.SECOND_OF_MINUTE) == 0 && d.get(ChronoField.MINUTE_OF_HOUR) == 0) {
            d = LocalDate.of(d.get(ChronoField.YEAR), d.get(ChronoField.MONTH_OF_YEAR), d.get(ChronoField.DAY_OF_MONTH));
        }
        if (d instanceof ZonedDateTime zoned) {
            return zoned.withNano(0).toInstant().toString();
        }
        return d.toString();
    }

    protected LocalizedText getDescription(T media, FileMetadata fileMetadata) {
        LocalizedText description = media.getDescription(fileMetadata);
        String result = ofNullable(description).map(LocalizedText::text).orElse("");
        if (StringUtils.isBlank(result)) {
            result = media.getUploadTitleFirstPart(media.getUploadTitle(), fileMetadata);
        } else {
            // Remove unwanted text
            for (String textToRemove : getStringsToRemove(media)) {
                result = result.replace(textToRemove, "");
            }
            // Resolve url shortener/redirect blocked in spam disallow list
            result = https(PATTERN_SHORT.matcher(result).replaceAll(match -> resolveShortUrl(match.group())));
            // Remove twitter hashtags search links, bloked in spam disallow list
            result = PATTERN_TWITTER_SEARCH.matcher(result).replaceAll(match -> match.group(1));
            for (FileMetadata metadata : media.getMetadata()) {
                result = result.replaceAll(
                        "<a href=\"" + metadata.getAssetUrl() + "\"><img src=\"[^\"]+\" alt=\"[^\"]*\"></a>",
                        "[[File:" + normalizeFilename(media.getUploadTitle(metadata, () -> getOrgName(media))) + '.'
                                + metadata.getFileExtensionOnCommons() + "|120px]]");
                result = result.replaceAll(
                        "<p><a href=\"" + metadata.getAssetUrl() + "\" target=\"new\"><b>[^<]+</b></a></p>", "");
            }
        }
        return new LocalizedText(description != null ? description.lang() : "en", result);
    }

    private String resolveShortUrl(String shortUrl) {
        try {
            String url = shortUrl.startsWith("http") ? shortUrl : "https://" + shortUrl;
            try (CloseableHttpClient httpclient = httpClientBuilder().disableAutomaticRetries()
                    .disableRedirectHandling().build();
                    ClassicHttpResponse response = executeRequest(newHttpGet(https(url)), httpclient, null)) {
                Header location = response.getFirstHeader("Location");
                if (location != null) {
                    return location.getValue().replace("&feature=youtu.be", "");
                }
            } catch (IOException e) {
                if (e.getCause() instanceof NotFound) {
                    return "";
                }
                throw e;
            }
        } catch (IOException e) {
            LOGGER.error(shortUrl + " -> " + e.getMessage(), e);
            GlitchTip.capture(e);
        }
        return shortUrl;
    }

    protected String getSource(T media, FileMetadata metadata) {
        return wikiLink(getSourceUrl(media, metadata, metadata.getExtension()),
                media.getTitle().text().replace('|', '-'));
    }

    protected String getAuthor(T media, FileMetadata metadata) {
        String result = media.getCredits();
        String authors = creators(media, metadata);
        if (!isPhotography(media, metadata)) {
            authors += '\n' + institutions(media, metadata);
        }
        return isNotBlank(authors) ? result + '\n' + authors : result;
    }

    protected String creators(T media, FileMetadata metadata) {
        return authorsStream(media, true).map(x -> "{{Creator |Wikidata=" + x.qid() + "}}").distinct()
                .collect(joining("\n"));
    }

    protected String institutions(T media, FileMetadata metadata) {
        return authorsStream(media, false).map(x -> "{{Institution |Wikidata=" + x.qid() + "}}").distinct()
                .collect(joining("\n"));
    }

    private Stream<WikidataInfo> authorsStream(T media, boolean humans) {
        return media instanceof WithAuthors m
                ? m.getAuthors().stream()
                        .map(a -> wikidata.searchViafId(a.viafId())
                                .or(() -> wikidata.searchIsniId(a.isniId()))
                                .or(() -> wikidata.searchIdrefId(a.idrefId()))
                                .or(() -> wikidata.searchOrcidId(a.orcidId()))
                                .or(() -> wikidata.searchArxivAuthorId(a.arxivId()))
                                .or(() -> wikidata.searchResearcherId(a.researcherId())))
                        .filter(Optional::isPresent).map(Optional::get).filter(
                                x -> Q5_HUMAN.toString().equals(x.nature()) == humans)
                : Stream.empty();
    }

    protected Optional<Temporal> getCreationDate(T media) {
        return Optional.<Temporal>ofNullable(media.getCreationDateTime())
                .or(() -> ofNullable(media.getCreationDate())).map(x ->
                // avoid approximate datetimes (XXXX-01-01T00:00:00Z)
                x instanceof ZonedDateTime dt && dt.getDayOfMonth() == 1 && dt.getMonthValue() == 1
                            && dt.getHour() == 0 && dt.getMinute() == 0 && dt.getSecond() == 0 ? Year.of(dt.getYear())
                                    : x
                );
    }

    protected Optional<Temporal> getCreationDateFromUploadTitle(T media, Pattern pattern, DateTimeFormatter format) {
        String uploadTitle = media.getUploadTitle();
        Matcher m = pattern.matcher(uploadTitle);
        if (m.matches()) {
            try {
                return Optional.of(LocalDate.parse(m.group(1), format));
            } catch (DateTimeParseException e) {
                LOGGER.warn("Cannot parse date in {} with {} => {}", m.group(1), format, e.getMessage());
            }
        }
        return Optional.empty();
    }

    protected final Optional<Temporal> getUploadDate(T media) {
        return Optional.<Temporal>ofNullable(media.getPublicationDateTime())
                .or(() -> ofNullable(media.getPublicationDate()));
    }

    protected Optional<String> getPermission(T media) {
        return Optional.empty();
    }

    protected Optional<String> getOtherVersions(T media, FileMetadata metadata) {
        StringBuilder sb = new StringBuilder();
        media.getMetadataStream()
                .filter(m -> m != metadata && m.getAssetUrl() != null && m.isIgnored() != Boolean.TRUE
                        && !Objects.equals(m.getAssetUrl(), metadata.getAssetUrl()))
                .map(m -> otherVersion(media, m)).distinct().filter(s -> !s.equals(otherVersion(media, metadata)))
                .forEach(sb::append);
        String result = sb.toString();
        return result.isEmpty() ? Optional.empty() : Optional.of(result.trim());
    }

    private static String otherVersion(Media media, FileMetadata m) {
        return media.getFirstCommonsFileNameOrUploadTitle(m) + '|'
                + ofNullable(m.getFileExtensionOnCommons()).orElse("TBD").toUpperCase(ENGLISH) + " version\n";
    }

    protected Optional<String> getOtherFields(T media) {
        if (media instanceof WithKeywords kw) {
            StringBuilder sb = new StringBuilder();
            addOtherField(sb, "Keyword", kw.getKeywords());
            String s = sb.toString();
            return s.isEmpty() ? Optional.empty() : Optional.of(s);
        }
        return Optional.empty();
    }

    protected Optional<String> getOtherFields1(T media) {
        return Optional.empty();
    }

    protected boolean isMarsPicture(T media, FileMetadata metadata) {
        return media instanceof WithKeywords kw && kw.containsKeywords(MARS_KEYWORDS);
    }

    protected boolean isSatellitePicture(T media, FileMetadata metadata) {
        return media instanceof WithKeywords kw && kw.containsKeywords(SATELLITE_KEYWORDS);
    }

    protected boolean isPhotography(T media, FileMetadata metadata) {
        return metadata.isPhotography();
    }

    /**
     * Returns the list of Wikimedia Commons categories to apply to the given media,
     * without hidden ones.
     *
     * @param media    the media for which category names are wanted
     * @param metadata metadata of media being uploaded
     * @return the list of Wikimedia Commons categories to apply to {@code media}
     */
    public final Set<String> findCategories(T media, FileMetadata metadata) {
        return findCategories(media, metadata, false);
    }

    /**
     * Returns the list of Wikimedia Commons categories to apply to the given media.
     *
     * @param media         the media for which category names are wanted
     * @param metadata      metadata of media being uploaded
     * @param includeHidden {@code true} if hidden categories are wanted
     * @return the list of Wikimedia Commons categories to apply to {@code media}
     */
    public Set<String> findCategories(T media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = new TreeSet<>();
        if (media.containsInTitleOrDescriptionOrKeywords("map")) {
            if (media.containsInTitleOrDescriptionOrKeywords("Land Surface Temperature")) {
                result.add("Land surface temperature maps");
            } else if (media.containsInTitleOrDescriptionOrKeywords("Sea Surface Temperature")) {
                result.add("Sea surface temperature maps");
            } else if (media.containsInTitleOrDescriptionOrKeywords("Tropospheric Nitrogen Dioxide")) {
                result.add("Nitrogen dioxide maps");
            }
        }
        if (media.getTitle() != null) {
            categorizationService.findCategoriesFromTitleAndYear(media.getTitle().text(), media.getYear().getValue()).forEach(title -> {
                if (isSatellitePicture(media, metadata)) {
                    categorizationService
                            .findCategoryForEarthObservationTargetOrSubject(x -> "Satellite pictures of " + x, title)
                            .ifPresentOrElse(result::add, () -> result.add(title));
                } else {
                    result.add(title);
                }
            });
        }
        if (isSatellitePicture(media, metadata)) {
            result.add(media.getYear() + " satellite pictures");
            if (media.containsInTitleOrDescriptionOrKeywords("false color")) {
                result.add("False-color satellite images");
            }
            if (metadata.isVideo()) {
                result.add("Animations of satellite pictures");
            }
            if (media.containsInTitleOrDescriptionOrKeywords("timeseries", "time series", "time-series")) {
                result.add("Satellite pictures time series");
            }
        }
        if (media instanceof WithLatLon ll) {
            result.addAll(findGeolocalizedCategories(ll));
        }
        if (metadata.isVideo()) {
            if (isNASA(media) && addNASAVideoCategory()) {
                result.add("NASA videos in " + media.getYear().getValue());
            }
            if (media.containsInTitleOrDescriptionOrKeywords("timelapse")) {
                result.add("Time-lapse videos");
            }
        } else {
            if (isNASA(media) && "stl".equals(metadata.getFileExtension())) {
                result.add("STL files from NASA");
            }
        }
        if (includeHidden) {
            UnitedStates.getUsMilitaryCategory(media).ifPresent(result::add);
            result.add(hiddenUploadCategory(media.getId().repoId()));
            result.addAll(getReviewCategories(media));
            MediaDimensions dims = metadata.getMediaDimensions();
            if ("gif".equals(metadata.getFileExtensionOnCommons())) {
                try {
                    int numImages = ImageUtils.readNumberOfImages(metadata.getAssetUri(), true);
                    LOGGER.info("GIF file with {} image(s): {}", numImages, metadata.getAssetUri());
                    if (numImages > 1) {
                        long megaPixels = numImages * dims.getPixelsNumber();
                        result.add("Animated GIF files"
                                + (megaPixels > 100_000_000 ? " exceeding the 100 MP limit"
                                        : megaPixels > 50_000_000 ? " between 50 MP and 100 MP" : ""));
                        if (isNASA(media)) {
                            result.add("Animations from NASA");
                        }
                        if (isSatellitePicture(media, metadata)) {
                            result.add("Animations of satellite pictures");
                        }
                    } else if (numImages < 1) {
                        LOGGER.error("Failed to read number of images in GIF file: {}", metadata.getAssetUri());
                    }
                } catch (IOException e) {
                    LOGGER.error("Failed to read GIF file: {}", e.getMessage());
                    GlitchTip.capture(e);
                }
            }
            if (metadata.isVideo() && dims != null) {
                result.add(String.format("Video display resolution %d x %d", dims.getWidth(), dims.getHeight()));
            }
        }
        return result;
    }

    protected abstract String hiddenUploadCategory(String repoId);

    protected List<String> getReviewCategories(T media) {
        return List.of(isNASA(media) ? "NASA images (review needed)" : "Spacemedia files (review needed)");
    }

    protected List<String> getArchivesReviewCategories(T media) {
        return getSideProjectReviewCategories(media, "Archimedia");
    }

    protected List<String> getDiplomaticReviewCategories(T media) {
        return getSideProjectReviewCategories(media, "Diplomedia");
    }

    protected List<String> getGovernmentReviewCategories(T media) {
        return getSideProjectReviewCategories(media, "Govmedia");
    }

    protected List<String> getMilitaryReviewCategories(T media) {
        return getSideProjectReviewCategories(media, "Milimedia");
    }

    protected List<String> getMilitaryReviewCategories(T media, String location) {
        return getSideProjectReviewCategories(media, "Milimedia", location);
    }

    protected List<String> getSideProjectReviewCategories(T media, String project) {
        return getSideProjectReviewCategories(media, project, getTakenLocation(media));
    }

    protected List<String> getSideProjectReviewCategories(T media, String project, String location) {
        List<String> result = new ArrayList<>();
        String usReviewCat = UnitedStates.getUsReviewCategory(media);
        if (usReviewCat != null && commonsService.existsCategoryPage(usReviewCat)) {
            result.add(usReviewCat);
        } else {
            String locationCat = project + " files, " + location + " (review needed)";
            if (isNotBlank(location) && commonsService.existsCategoryPage(locationCat)) {
                result.add(locationCat);
            } else {
                result.add(project + " files (review needed)");
            }
        }
        if (media.containsInTitleOrDescriptionOrKeywords("rocket", "satellite", "nasa", "astronaut", "cosmonaut",
                "spacecraft", "space station")
                && !media.containsInTitleOrDescriptionOrKeywords("multiple launch rocket system",
                        "multiple rocket launch system", "rocket launcher", "artillery rocket",
                        "rocket propelled grenade", "missiles, rockets", "nasal")) {
            result.add("Spacemedia files (review needed)");
        }
        return result;
    }

    protected boolean categorizeGeolocalizedByName() {
        return false;
    }

    protected Set<String> findGeolocalizedCategories(WithLatLon media) {
        Set<String> result = new HashSet<>();
        if (categorizeGeolocalizedByName()) {
            boolean preciseCatAdded = false;
            String name = getName();
            try {
                Address address = nominatim.reverse(media.getLatitude(), media.getLongitude(), 5).address();
                if (address != null) {
                    if (isNotBlank(address.state())
                            && addCategoryIfExists(result, "Images of " + address.state() + " by " + name)) {
                        preciseCatAdded = true;
                    } else if (isNotBlank(address.country())) {
                        result.add("Images of " + address.country() + " by " + name);
                        preciseCatAdded = true;
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Nominatim error: {}", e.getMessage());
                GlitchTip.capture(e);
            }
            String continent = geometry.getContinent(media.getLatitude(), media.getLongitude());
            if (!preciseCatAdded) {
                result.add("Images" + (continent != null ? " of " + continent : "") + " by " + name);
            }
        }
        return result;
    }

    boolean addCategoryIfExists(Set<String> result, String cat) {
        if (commonsService.existsCategoryPage(cat)) {
            result.add(cat);
            return true;
        }
        return false;
    }

    protected static Optional<String> findCategoryFromMapping(String value, String type,
            Map<String, Map<String, String>> mappings) {
        if (value != null) {
            Map<String, String> map = mappings.get(value.replace('\n', ' ').trim());
            if (map != null) {
                String cat = map.get("Commons categories");
                if (isBlank(cat)) {
                    LOGGER.warn("No category found for {} {}", type, value);
                } else {
                    return Optional.of(cat);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Returns the list of information templates to apply to the given media, before
     * the main one.
     *
     * @param media    the media for which information template names are wanted
     * @param metadata the file metadata
     * @return the list of information templates to apply to {@code media}
     */
    public Set<String> findBeforeInformationTemplates(T media, FileMetadata metadata) {
        Set<String> result = new LinkedHashSet<>();
        if (metadata.isImage()) {
            Long size = metadata.getSize();
            MediaDimensions dims = metadata.getMediaDimensions();
            if (size != null && size >= LOTS_OF_MP || (dims != null && dims.getPixelsNumber() >= LOTS_OF_MP)) {
                result.add("LargeImage");
            }
        }
        return result;
    }

    /**
     * Returns the list of information templates to apply to the given media, after
     * the main one.
     *
     * @param media    the media for which information template names are wanted
     * @param metadata the file metadata
     * @return the list of information templates to apply to {@code media}
     */
    public Set<String> findAfterInformationTemplates(T media, FileMetadata metadata) {
        Set<String> result = new LinkedHashSet<>();
        if (media.containsInTitleOrDescriptionOrKeywords("360 Panorama")) {
            StringBuilder sb = new StringBuilder("Pano360");
            getPano360Category(media, metadata).ifPresent(s -> sb.append("|cat=[[Category:").append(s).append("]]"));
            result.add(sb.toString());
        }
        return result;
    }

    protected Optional<String> getPano360Category(T media, FileMetadata metadata) {
        return isMarsPicture(media, metadata) ? Optional.of("360° panoramas of Mars") : Optional.empty();
    }

    /**
     * Returns the list of licence templates to apply to the given media.
     *
     * @param media    the media for which licence template names are wanted
     * @param metadata the file metadata for which licence template names are wanted
     * @return the list of licence templates to apply to {@code media}
     */
    public Set<String> findLicenceTemplates(T media, FileMetadata metadata) {
        Set<String> result = new LinkedHashSet<>();
        LocalizedText description = media.getDescription(metadata);
        if (description != null) {
            ofNullable(CategorizationService.extractCopernicusTemplate(description.text())).ifPresent(result::add);
        }
        if (media instanceof WithLicenceUrl m) {
            result.add(switch (m.getLicenceUrl().toExternalForm().replace("http://", "").replace("https://", "")) {
            case "creativecommons.org/licenses/by/4.0/" -> "Cc-by-4.0";
            case "creativecommons.org/licenses/by-sa/" -> "Cc-by-sa-4.0";
            case "creativecommons.org/publicdomain/zero/1.0/" -> "Cc-zero";
            case "hal.archives-ouvertes.fr/licences/etalab/" -> "Licence Ouverte 2";
            case "hal.archives-ouvertes.fr/licences/publicDomain/" -> "PD-France";
            default -> throw new IllegalStateException(media.toString());
            });
        }
        return result;
    }

    /**
     * Determines if the given media has been produced by NASA.
     *
     * @param media media
     * @return {@code true} if media has been produced by NASA
     */
    protected boolean isNASA(T media) {
        return getClass().getSimpleName().toLowerCase(ENGLISH).contains("nasa");
    }

    protected boolean addNASAVideoCategory() {
        return true;
    }

    protected final String wikiLink(URL url, String text) {
        return "[" + requireNonNull(url, "url").toExternalForm().replace(" ", "%20") + ' '
                + requireNonNull(text, "text") + ']';
    }

    protected void checkUploadPreconditions(T media, boolean checkUnicity, boolean isManual) {
        if (UploadContext.isForbiddenUpload(media, isManual)) {
            throw new ImageUploadForbiddenException(media + " is marked as ignored (manual: " + isManual + ")");
        }
    }

    protected void checkUploadPreconditions(T media, FileMetadata metadata, boolean checkUnicity) throws IOException {
        if (!metadata.hasSha1()) {
            throw new ImageUploadForbiddenException(media + " SHA-1 has not been computed.");
        }
        // Forbid upload of duplicate medias for a single repo, they may have different descriptions
        if (checkUnicity && repository.countByMetadata_Sha1(metadata.getSha1()) > 1) {
            throw new ImageUploadForbiddenException(media + " is present several times.");
        }
        // Double-check for duplicates before upload!
        if (isNotEmpty(metadata.getCommonsFileNames())) {
            throw new ImageUploadForbiddenException(media + " is already on Commons: " + metadata.getCommonsFileNames());
        }
        List<FileMetadata> metadataList = List.of(metadata);
        if (mediaService.findCommonsFiles(metadataList, media,
                () -> getSimilarUploadedMediaByDate(media, media.getPublicationDate(), 14),
                includeByPerceptualHash(), forceDuplicateDetection())) {
            metadata = metadataRepository.save(metadata);
            throw new ImageUploadForbiddenException(
                    media + " is already on Commons: " + metadata.getCommonsFileNames());
        }
        if (findLicenceTemplates(media, metadata).isEmpty()) {
            mediaService.ignoreAndSaveMetadata(metadata, "no template found, so may be not free");
            throw new ImageUploadForbiddenException(media + " has no template, so may be not free");
        }
    }

    protected abstract Class<T> getMediaClass();

    protected Path getLocalMediaPath(T media) throws IOException { // NOSONAR
        return null;
    }

    protected final MediaUpdateResult<T> doCommonUpdate(T media, HttpClient httpClient, HttpClientContext context,
            boolean forceUpdate) throws IOException {
        Path path = getLocalMediaPath(media);
        MediaUpdateResult<T> ur = mediaService.updateMedia(
                new MediaUpdateContext<T>(media, path, getUrlResolver(), httpClient, context, forceUpdate,
                        ignoreExifMetadata(), forceDuplicateDetection()),
                getPatternsToRemove(media), getStringsToRemove(media), this::getSimilarUploadedMediaByDate,
                checkAllowlist(media), checkBlocklist(media), includeByPerceptualHash());
        boolean result = ur.result();
        if (!media.isIgnored() && media.hasMetadata()) {
            LOGGER.trace("Start common update checks for {}", media);
            for (FileMetadata fm : media.getMetadata()) {
                if (Boolean.TRUE != fm.isIgnored()) {
                    GlitchTip.setExtra("asset_url", fm.getAssetUri().toString());
                    GlitchTip.setTag("ext", fm.getFileExtension());
                    result |= doCheckNonFree(media, fm);
                    result |= doCheckShortOrMissingTitleAndDescription(media, fm);
                    result |= doCheckImageTooBigOrTooSmall(fm);
                    result |= doCheckUninterestingTitle(media, fm);
                }
            }
            GlitchTip.removeExtra("asset_url", "ext");
            LOGGER.trace("Ended common update checks for {}", media);
        }
        ur = new MediaUpdateResult<>(media, path, result, ur.exception());
        postDoCommonUpdate(media, ur);
        return ur;
    }

    protected void postDoCommonUpdate(T media, MediaUpdateResult<T> ur) throws IOException {
        // Override if custom behaviour has to be performed after a common update
        if (ur.localPath() != null) {
            Files.deleteIfExists(ur.localPath());
        }
    }

    private boolean doCheckNonFree(T media, FileMetadata fm) {
        String description = ofNullable(media.getDescription(fm)).flatMap(x -> ofNullable(x.toLowerCase()))
                .map(LocalizedText::text).orElse("");
        if (description.contains("by-nc") || description.contains("by-nd")) {
            LOGGER.debug("Non-free licence test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Non-free licence");
        } else if (isCourtesy(description) && (findLicenceTemplates(media, fm).isEmpty()
                || mediaService.getCourtesyOk().stream().noneMatch(description::contains))) {
            LOGGER.debug("Courtesy test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Probably non-free file (courtesy)");
        } else if (description.contains("citizen scien") || description.contains("citizenscien")) {
            LOGGER.debug("Citizen science test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Probably non-free file (citizen science)");
        }
        return false;
    }

    private boolean doCheckShortOrMissingTitleAndDescription(T media, FileMetadata fm) {
        if (StringUtils.length(media.getTitle().text()) + StringUtils.length(media.getDescription(fm)) <= 2) {
            // To ignore https://www.dvidshub.net/image/6592675 (title and desc are '.')
            LOGGER.debug("Short title or description test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Very short or missing title and description");
        }
        return false;
    }

    private boolean doCheckImageTooBigOrTooSmall(FileMetadata fm) {
        if (fm.isImage() && fm.hasValidDimensions() && fm.getMediaDimensions().getPixelsNumber() < 20_000) {
            LOGGER.debug("Too small image test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Too small image");
        } else if (fm.getSize() != null && fm.getSize() > CommonsService.MAX_FILE_SIZE) {
            // See https://commons.wikimedia.org/wiki/Commons:Maximum_file_size
            LOGGER.debug("Too big file test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Too big file");
        }
        return false;
    }

    private boolean doCheckUninterestingTitle(T media, FileMetadata fm) {
        if (isBlank(media.getDescription(fm)) && media.getTitle() != null
                && PATTERN_UNINTERESTING_TITLE.matcher(media.getTitle().text()).matches()) {
            LOGGER.debug("No description and uninteresting title test has been trigerred for {}", fm);
            return mediaService.ignoreAndSaveMetadata(fm, "Media without description and with uninteresting title");
        }
        return false;
    }

    protected static boolean isCourtesy(String description) {
        return COURTESY_SPELLINGS.stream().anyMatch(description::contains);
    }

    protected boolean checkAllowlist(T media) {
        return false;
    }

    protected boolean checkBlocklist(T media) {
        return true;
    }

    protected boolean includeByPerceptualHash() {
        return true;
    }

    protected boolean ignoreExifMetadata() {
        return false;
    }

    protected boolean needsReview() {
        return true;
    }

    protected HttpClientBuilder configureHttpClientBuilder(HttpClientBuilder builder) {
        return builder;
    }

    protected final boolean doCommonUpdate(T media) throws IOException {
        try (CloseableHttpClient httpClient = configureHttpClientBuilder(httpClientBuilder()).build()) {
            return doCommonUpdate(media, httpClient, null);
        }
    }

    protected final boolean doCommonUpdate(T media, HttpClient httpClient, HttpClientContext context)
            throws IOException {
        return doCommonUpdate(media, httpClient, context, false).result();
    }

    @Override
    public int compareTo(AbstractOrgService<T> o) {
        return getName().compareTo(o.getName());
    }

    protected final RuntimeData getRuntimeData() {
        return runtimeDataRepository.findById(getId()).orElseGet(() -> new RuntimeData(getId()));
    }

    protected final UploadMode getUploadMode() {
        return uploadMode;
    }

    protected final boolean forceDuplicateDetection() {
        return forceDuplicateDetection;
    }

    protected boolean isPermittedFileType(FileMetadata metadata) {
        return commonsService.isPermittedFileExt(metadata.getExtension())
                || V2C_VIDEO_EXTENSIONS.contains(metadata.getExtension())
                || (metadata.getAssetUrl() != null && (commonsService.isPermittedFileUrl(metadata.getAssetUrl())
                        || V2C_VIDEO_EXTENSIONS.stream()
                                .anyMatch(e -> metadata.getAssetUrl().getFile().endsWith('.' + e))
                        || "www.youtube.com".equals(metadata.getAssetUrl().getHost())));
    }

    protected boolean shouldUploadAuto(T media, boolean isManual) {
        return media.getMetadataStream().anyMatch(
                metadata -> new UploadContext<>(media, metadata, getUploadMode(), mediaService.getMinYearUploadAuto(),
                        this::isPermittedFileType, isManual).shouldUploadAuto());
    }

    protected UrlResolver<T> getUrlResolver() {
        return (_, metadata) -> metadata.getAssetUrl();
    }

    protected List<AbstractOrgService<?>> getSimilarOrgServices(T media) {
        return List.of();
    }

    protected final List<? extends Media> getSimilarUploadedMediaByDate(T media, LocalDate date, int numDaysBefore) {
        return getSimilarOrgServices(media).stream().flatMap(
            x -> IntStream.range(0, numDaysBefore + 1).parallel().mapToObj(
                i -> x.listUploadedMediaByDate(date.minusDays(i)).stream()).flatMap(s -> s)).toList();
    }

    protected FileMetadata addMetadata(T media, String assetUrl, Consumer<FileMetadata> consumer) {
        return addMetadata(media, newURL(assetUrl), consumer);
    }

    protected FileMetadata addMetadata(T media, URL assetUrl, Consumer<FileMetadata> consumer) {
        return addMetadata(media, assetUrl, consumer, false);
    }

    protected FileMetadata addMetadata(T media, URL assetUrl, Consumer<FileMetadata> consumer, boolean forceConsumer) {
        try {
            FileMetadata fm = metadataRepository.findByAssetUrl(assetUrl).map(x -> {
                if (forceConsumer && consumer != null) {
                    consumer.accept(x);
                    x = metadataRepository.save(x);
                }
                return x;
            }).orElseGet(() -> metadataRepository.save(newFileMetadata(assetUrl, consumer)));
            media.addMetadata(requireNonNull(fm, "fm for " + assetUrl));
            return fm;
        } catch (RuntimeException e) {
            LOGGER.error("Error while adding metadata {} for media {}", assetUrl, media);
            GlitchTip.capture(e);
            throw e;
        }
    }

    protected static FileMetadata newFileMetadata(URL assetUrl, Consumer<FileMetadata> consumer) {
        FileMetadata metadata = new FileMetadata(assetUrl);
        if (consumer != null) {
            consumer.accept(metadata);
        }
        return metadata;
    }

    protected static void addOtherField(StringBuilder sb, String name, Collection<?> values, Map<String, String> catMapping) {
        if (isNotEmpty(values)) {
            addOtherField(sb, name + (values.size() > 1 ? "s" : ""),
                    values.stream().filter(Objects::nonNull).map(Object::toString).filter(StringUtils::isNotBlank).map(s -> {
                        if (catMapping != null) {
                            String cat = catMapping.get(s);
                            if (StringUtils.isNotBlank(cat)) {
                                return stream(cat.split(";"))
                                        .map(c -> "[[:Category:" + c + '|' + s + "]]")
                                        .collect(joining("; "));
                            }
                        }
                        return s;
                    }).collect(joining("; ")));
        }
    }

    protected static void addOtherField(StringBuilder sb, String name, Collection<?> values) {
        addOtherField(sb, name, values, null);
    }

    protected static void addOtherField(StringBuilder sb, String name, String value) {
        addOtherField(sb, name, value, null);
    }

    protected static void addOtherField(StringBuilder sb, String name, String value, Map<String, String> catMapping) {
        if (StringUtils.isNotBlank(value)) {
            String s = value;
            if (catMapping != null) {
                String cat = catMapping.get(value);
                if (StringUtils.isNotBlank(cat)) {
                    s = "[[:Category:" + cat + '|' + value + "]]";
                }
            }
            sb.append("{{information field|name=").append(name).append("|value=").append(s).append("}}");
        }
    }

    protected static void doFor(Iterable<String> set, Function<String, Optional<String>> mapper,
            Consumer<String> consumer) {
        if (set != null) {
            for (String s : set) {
                mapper.apply(s).ifPresent(consumer::accept);
            }
        }
    }

    protected static SimpleEntry<String, String> e(String k, String v) {
        return new SimpleEntry<>(k, v);
    }

    protected static Object smartExceptionLog(Throwable e) {
        return e.getCause() instanceof RuntimeException ? e : e.toString();
    }

    protected StringBuilder appendWikiLocalizedText(StringBuilder sb, String field, LocalizedText lt) {
        return sb.append(field).append("{{").append(lt.lang()).append("|1=").append(formatWikiCode(lt.text())).append("}}");
    }

    protected StringBuilder appendMilim(StringBuilder sb, T media, FileMetadata metadata,
            List<LocalizedText> descriptions, String virin, Optional<String> location, Optional<Float> rating) {
        // https://commons.wikimedia.org/wiki/Template:Milim
        sb.append("{{milim");
        appendWikiDescriptions(sb, descriptions, "description");
        appendWikiDate(sb, media);
        appendWikiSource(sb, media, metadata);
        sb.append("\n| author = ").append(getAuthor(media, metadata));
        getPermission(media).ifPresent(s -> sb.append("\n| permission = ").append(s));
        location.ifPresent(l -> sb.append("\n| location = ").append(l));
        sb.append("\n| virin = ").append(virin).append("\n| dateposted = ").append(toIso8601(
                Optional.<Temporal>ofNullable(media.getPublicationDateTime()).orElse(media.getPublicationDate())));
        rating.ifPresent(r -> sb.append("\n| stars = ").append(r.intValue()));
        return addOthersAndComplete(sb, media, metadata);
    }

    protected StringBuilder appendPhotograph(StringBuilder sb, T media, FileMetadata metadata,
            List<LocalizedText> descriptions, String medium, String dimens, String instit, String depart, String inscr,
            String notes, String accNum, String wikidataQid, String cameraCoord) {
        // https://commons.wikimedia.org/wiki/Template:Photograph
        sb.append("{{Photograph");
        sb.append("\n| photographer = ").append(getAuthor(media, metadata));
        appendWikiLocalizedText(sb, "\n| title = ", media.getTitle());
        appendWikiDescriptions(sb, descriptions, "description");
        appendWikiDate(sb, media);
        ofNullable(medium).ifPresent(s -> sb.append("\n| medium = ").append(s));
        ofNullable(dimens).ifPresent(s -> sb.append("\n| dimensions = ").append(s));
        ofNullable(instit).ifPresent(s -> sb.append("\n| institution = ").append(s));
        ofNullable(depart).ifPresent(s -> sb.append("\n| department = ").append(s));
        ofNullable(media.getCredits()).ifPresent(s -> sb.append("\n| credit line = ").append(s));
        ofNullable(inscr).ifPresent(s -> sb.append("\n| inscriptions = ").append(s));
        ofNullable(notes).ifPresent(s -> sb.append("\n| notes = ").append(s));
        ofNullable(accNum).ifPresent(s -> sb.append("\n| accession number = ").append(s));
        appendWikiSource(sb, media, metadata);
        getPermission(media).ifPresent(s -> sb.append("\n| permission = ").append(s));
        ofNullable(wikidataQid).ifPresent(s -> sb.append("\n| wikidata = ").append(s));
        ofNullable(cameraCoord).ifPresent(s -> sb.append("\n| camera coord = ").append(s));
        return addOthersAndComplete(sb, media, metadata);
    }

    protected StringBuilder addOthersAndComplete(StringBuilder sb, T media, FileMetadata metadata) {
        appendWikiOtherVersions(sb, media, metadata, "other versions");
        getOtherFields(media).ifPresent(s -> sb.append("\n| other fields = ").append(s));
        getOtherFields1(media).ifPresent(s -> sb.append("\n| other fields 1 = ").append(s));
        return sb.append("\n}}");
    }

    private void createNavigationalCategory(T media) throws IOException {
        String location = getTakenLocation(media);
        LocalDate date = media.getCreationDate();
        if (date != null && isNotBlank(location)) {
            switch (location) {
            case "ISS":
                categorizationService.createIssNavigationalCategory(date);
                break;
            case "Sun":
                categorizationService.createSunNavigationalCategory(date);
                break;
            case "Moon", "Mercury", "Mars", "Venus", "Jupiter", "Saturn", "Uranus", "Neptune", "Undisclosed Location":
                break;
            default:
                if (!location.toLowerCase(ENGLISH).contains("area of responsibility")
                        && BODIES_OF_WATER.stream().noneMatch(location::contains)) {
                    categorizationService.createCountryNavigationalCategory(location, date);
                }
                break;
            }
        }
    }
}
