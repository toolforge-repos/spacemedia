package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

public record NasaResponse(NasaCollection collection) {
}
