package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerPerson(String lastName, String firstName, String orcid, int registrationNumber,
        IfremerUrl ifremerUrl, URL defaultIfremerUrl, String ldapSite, String ldapTeam, String ldapEmployer) {

    @Override
    public String toString() {
        return new StringBuilder(firstName).append(' ').append(lastName).toString();
    }
}
