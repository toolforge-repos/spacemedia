package org.wikimedia.commons.donvip.spacemedia.service.orgs;
import static java.util.Locale.ENGLISH;

import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;

@Service
public class SdasmFlickrService extends AbstractOrgFlickrService {

    public static final List<String> STRINGS_TO_REMOVE = List.of(
            ":Piction ID",
            "---Please tag these photos so information can be recorded.",
            "---- Digitization of this image made possible by a grant from NEH: <a href=\"https://sandiegoairandspace.org/research/project/nehfunded\">NEH and the San Diego Air and Space Museum </a>",
            "----Note: This material may be protected by Copyright Law (Title 17 U.S.C.)",
            "--Repository: <a href=\"http://www.sandiegoairandspace.org/library/stillimages.html\" rel=\"noreferrer nofollow\">San Diego Air and Space Museum </a>");

    private static final Pattern DATE_US_PATTERN = Pattern.compile(".* (\\d{2}-\\d{2}-\\d{4})");
    private static final DateTimeFormatter DATE_US_FORMAT = DateTimeFormatter.ofPattern("MM-dd-yyyy", ENGLISH);

    @Autowired
    public SdasmFlickrService(FlickrMediaRepository repository,
            @Value("${sdasm.flickr.accounts}") Set<String> flickrAccounts) {
        super(repository, "sdasm.flickr", flickrAccounts);
    }

    @Override
    public String getName() {
        return "San Diego Air & Space Museum (Flickr)";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "SDASM Archives Flickr files uploaded by " + commonsService.getAccount();
    }

    @Override
    public Set<String> findLicenceTemplates(FlickrMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add("PD-SDASM"); // Will categorize in "Files from the SDASM Archives Flickr stream"
        return result;
    }

    @Override
    protected Collection<String> getStringsToRemove(FlickrMedia media) {
        return STRINGS_TO_REMOVE;
    }

    @Override
    protected Optional<Temporal> getCreationDate(FlickrMedia media) {
        // "creation" date cannot be trusted, it is in fact the upload date
        return media.getYear().getValue() < 2010 ? super.getCreationDate(media)
                : getCreationDateFromUploadTitle(media, DATE_US_PATTERN, DATE_US_FORMAT);
    }

    @Override
    protected List<String> getReviewCategories(FlickrMedia media) {
        return List.of("Photographs by SDASM Archives (check needed)");
    }

    @Override
    protected boolean checkBlocklist(FlickrMedia media) {
        return false;
    }
}
