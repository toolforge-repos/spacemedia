package org.wikimedia.commons.donvip.spacemedia.service.synology;

import static java.util.Locale.ENGLISH;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import org.wikimedia.commons.donvip.spacemedia.service.FileProvider;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.ApiInfo;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.ApiInfoWebapiResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.DownloadResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.DsmWebapiRequest;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.DsmWebapiResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.File;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.File.FileList;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.FileListResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.LoginWebapiResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.SharingCreateResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.SharingGetInfoResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.SharingLink;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.SharingListResponse;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.QuickConnectInterceptor;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PostConstruct;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Lazy
@Service
public class SynologyService implements FileProvider<File> {

    // Inspired by https://github.com/gauthierj/dsm-webapi-client

    // https://global.download.synology.com/download/Document/Software/DeveloperGuide/Package/FileStation/All/enu/Synology_File_Station_API_Guide.pdf

    private static final String SYNO_API_INFO = "SYNO.API.Info";
    private static final String SYNO_API_AUTH = "SYNO.API.Auth";
    private static final String SYNO_FILE_STATION_LIST = "SYNO.FileStation.List";
    private static final String SYNO_FILE_STATION_DOWNLOAD = "SYNO.FileStation.Download";
    private static final String SYNO_FILE_STATION_SHARING = "SYNO.FileStation.Sharing";

    private static final Logger LOGGER = LoggerFactory.getLogger(SynologyService.class);

    @Autowired
    private CommonsService commonsService;

    @Autowired
    private ObjectMapper jackson;

    private OkHttpClient client;

    @Value("${synology.zone:global}")
    private String zone;

    @PostConstruct
    void init() {
        LOGGER.info("Initializing Synology QuickConnectInterceptor for zone {}", zone);
        client = new OkHttpClient.Builder().hostnameVerifier((_, _) -> true) // NOSONAR
                .addInterceptor(new QuickConnectInterceptor(zone)).build();
    }

    public List<ApiInfo> retrieveApiInfos(String quickconnectId, List<String> apis) throws IOException {
        return callApi(quickconnectId, new ApiInfo(SYNO_API_INFO, 1, 1, "query.cgi", null), "query",
                Map.of("query", apis.stream().collect(joining(","))), null, ApiInfoWebapiResponse.class).getApiInfos();
    }

    public ApiInfo retrieveApiInfo(String quickconnectId, String api) throws IOException {
        return retrieveApiInfos(quickconnectId, List.of(api)).stream().filter(x -> api.equals(x.api())).findFirst()
                .orElseThrow(() -> new IOException("No " + api + " found"));
    }

    public String login(String quickconnectId, String login, String password, String session) throws IOException {
        return callApi(quickconnectId, SYNO_API_AUTH, "login",
                Map.of("account", login, "passwd", password, "session", session, "format", "cookie"),
                null, LoginWebapiResponse.class).sid();
    }

    @SuppressWarnings("unchecked")
    public void logout(String quickconnectId, String session, String sid) throws IOException {
        callApi(quickconnectId, SYNO_API_AUTH, "logout", Map.of("session", session, "_sid", sid),
                null, DsmWebapiResponse.class);
    }

    public String createSharingLink(String quickconnectId, File file, String sid) {
        return createSharingLink(quickconnectId, file.path(), sid);
    }

    public String createSharingLink(String quickconnectId, String path, String sid) {
        try {
            return callApi(quickconnectId, SYNO_FILE_STATION_SHARING, "create", Map.of("_sid", sid, "path", path),
                    null, SharingCreateResponse.class).getElements().get(0).url().replace("http://", "https://");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public List<SharingLink> listSharingLinks(String quickconnectId, String sid) throws IOException {
        return callApi(quickconnectId, SYNO_FILE_STATION_SHARING, "list", Map.of("_sid", sid),
                null, SharingListResponse.class).getElements();
    }

    public SharingLink getSharingInfo(String quickconnectId, String id, String sid) throws IOException {
        return callApi(quickconnectId, SYNO_FILE_STATION_SHARING, "getinfo", Map.of("id", id, "_sid", sid), null,
                SharingGetInfoResponse.class);
    }

    public ResponseBody download(String quickconnectId, String path, String sid) throws IOException {
        return callApi(quickconnectId, SYNO_FILE_STATION_DOWNLOAD, "download",
                Map.of("_sid", sid, "path", "[\"" + path + "\"]"), null, DownloadResponse.class);
    }

    @Override
    public <T> List<T> getFiles(String quickconnectId, String share, Function<File, T> mapper,
            Comparator<T> comparator, Map<String, String> params) {
        try {
            ApiInfo api = retrieveApiInfo(quickconnectId, SYNO_FILE_STATION_LIST);
            LOGGER.info("Getting files from {}", share);
            List<File> result = listFilesRecursive(quickconnectId, api, share, params);
            LOGGER.info("Found {} files in {}", result.size(), share);
            Stream<T> stream = result.stream().map(mapper);
            if (comparator != null) {
                stream = stream.sorted(comparator);
            }
            return stream.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private List<File> listFilesRecursive(String quickconnectId, ApiInfo api, String folderPath, Map<String, String> params)
            throws IOException {
        Map<String, String> parameters = new HashMap<>(
                Map.of("additional", "[\"real_path\",\"size\",\"time\",\"type\"]"));
        parameters.putAll(params);
        parameters.put("folder_path", folderPath);
        FileList list = callApi(quickconnectId, api, "list", parameters, null, FileListResponse.class);
        LOGGER.debug("Found {} direct files in {}", list.getTotal(), folderPath);
        List<File> result = new ArrayList<>();
        for (File file : list.getElements()) {
            if (!folderPath.equals(file.path())) {
                if (file.directory()) {
                    result.addAll(listFilesRecursive(quickconnectId, api, file.path(), parameters));
                } else if (params.containsKey("filetype") || isWantedFile(file)) {
                    result.add(file);
                }
            }
        }
        LOGGER.debug("Found {} total files in {} and subdirectories", result.size(), folderPath);
        return result;
    }

    @Override
    public long getSize(File file) {
        return file.properties().size();
    }

    @Override
    public String getName(File file) {
        return file.name();
    }

    @Override
    public String getExtension(File file) {
        return file.properties().type().toLowerCase(ENGLISH);
    }

    @Override
    public boolean isPermittedFileExt(String extension) {
        return commonsService.isPermittedFileExt(extension);
    }

    private <T, R extends DsmWebapiResponse<T>> T callApi(String quickconnectId, String apiName, String method,
            Map<String, String> params, String cookie, Class<R> rClass) throws IOException {
        return callApi(quickconnectId, retrieveApiInfo(quickconnectId, apiName), method, params, cookie, rClass);
    }

    private <T, R extends DsmWebapiResponse<T>> T callApi(String quickconnectId, ApiInfo api, String method,
            Map<String, String> params, String cookie, Class<R> rClass) throws IOException {
        LOGGER.debug("{} API: {}", quickconnectId, api);

        DsmWebapiRequest apiRequest = new DsmWebapiRequest(api.api(), api.maxVersion(), api.path(), method, params);
        LOGGER.debug("{} request: {}", quickconnectId, apiRequest);

        ResponseBody body = quickConnectCall(buildUri("https://" + quickconnectId, apiRequest), cookie).body();
        @SuppressWarnings("unchecked")
        R response = DownloadResponse.class.equals(rClass) ? (R) new DownloadResponse(body)
                : jackson.readValue(body.bytes(), rClass);
        LOGGER.debug("{} response: {}", quickconnectId, response);

        if (!response.success()) {
            throw new IOException(response.error().toString());
        }

        return response.data();
    }

    private Response quickConnectCall(String uri, String cookie) throws IOException {
        Builder builder = new Request.Builder().url(HttpUrl.parse(uri));
        if (isNotBlank(cookie)) {
            builder = builder.header("Cookie", cookie);
        }
        LOGGER.debug("{}", uri);
        return client.newCall(builder.build()).execute();
    }

    private static String buildUri(String urlBase, DsmWebapiRequest request) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(urlBase).path("/webapi/")
                .path(request.path()).queryParam("api", request.api()).queryParam("version", request.version())
                .queryParam("method", request.method());
        for (Entry<String, String> entry : request.parameters().entrySet()) {
            uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        return uriComponentsBuilder.toUriString();
    }
}
