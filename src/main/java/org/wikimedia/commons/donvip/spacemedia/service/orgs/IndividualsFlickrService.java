package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Locale.ENGLISH;
import static java.util.regex.Pattern.compile;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.replace;

import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.utils.Emojis;

@Service
public class IndividualsFlickrService extends AbstractOrgFlickrService {

    public static final List<String> STRINGS_TO_REMOVE = List.of(
            "Follow me on Twitter:",
            "<a href=\"https://twitter.com/Pierre_Markuse\" rel=\"nofollow\">twitter.com/Pierre_Markuse</a>",
            "Do you want to support this collection of satellite images? Any donation, no matter how small, would be appreciated. <a href=\"https://www.paypal.com/paypalme/PierreMarkuse\">PayPal me!</a>",
            "Follow me on <a href=\"https://twitter.com/Pierre_Markuse\">Twitter!</a> and <a href=\"https://mastodon.world/@pierre_markuse\">Mastodon!</a>");

    public static final List<Pattern> PATTERNS_TO_REMOVE = List.of(compile(
            "Feel free to share, giving the appropriate credit and providing a link to the original image or tweet(?::)? <a href=\"https://creativecommons.org/licenses/by/3.0/\">creativecommons.org/licenses/by/3.0/</a>"));

    private static final Pattern DATE_PATTERN = Pattern.compile(".* ([A-S][a-y]{2,8} \\d{1,2} [1-2]\\d{3})");
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("MMMM d yyyy", ENGLISH);

    @Autowired
    public IndividualsFlickrService(FlickrMediaRepository repository,
            @Value("${individuals.flickr.accounts}") Set<String> flickrAccounts) {
        super(repository, "individuals.flickr", flickrAccounts);
    }

    @Override
    public String getName() {
        return "Individuals (Flickr)";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Spacemedia Individuals Flickr files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected Optional<Temporal> getCreationDate(FlickrMedia media) {
        return getCreationDateFromUploadTitle(media, DATE_PATTERN, DATE_FORMAT).or(() -> super.getCreationDate(media));
    }

    @Override
    protected Collection<String> getStringsToRemove(FlickrMedia media) {
        return STRINGS_TO_REMOVE;
    }

    @Override
    protected Collection<Pattern> getPatternsToRemove(FlickrMedia media) {
        return PATTERNS_TO_REMOVE;
    }

    @Override
    public Set<String> findLicenceTemplates(FlickrMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        switch (media.getPathAlias()) {
        case "harrystrangerphotography", "194849271@N04":
            if ((media.getDescription() != null && media.getDescription().toLowerCase().contains("usgs"))
                    || media.getPhotosets().stream().anyMatch(s -> s.getTitle().startsWith("KH-"))) {
                result.add("PD-USGov-NRO");
                replace(result, "Flickr-public domain mark", "PD-USGov-USGS");
            }
            break;
        default:
            break;
        }
        return result;
    }

    @Override
    public Set<String> findCategories(FlickrMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        LocalizedText titleLc = media.getTitle().toLowerCase();
        switch (media.getPathAlias()) {
        case "kevinmgill":
            if (result.contains("Photos by the Curiosity rover")
                    && (result.contains("Photos by Martian rover Mastcams") || titleLc.contains("mastcam"))) {
                result.remove("Photos by Martian rover Mastcams");
                result.remove("Photos by the Curiosity rover");
                result.add("Photos by the Curiosity rover Mastcam");
            } else if (result.contains("Photos by the Perseverance rover")) {
                if (titleLc.contains("mastcam")) {
                    replace(result, "Photos by the Perseverance rover", "Photos by the Perseverance rover Mastcams");
                } else if (titleLc.contains("navleft") || titleLc.contains("navright")) {
                    replace(result, "Photos by the Perseverance rover", "Photos by the Perseverance rover Navcams");
                } else if (titleLc.contains("supercam")) {
                    replace(result, "Photos by the Perseverance rover", "Photos by the Perseverance rover SuperCam");
                } else if (titleLc.contains("watson")) {
                    replace(result, "Photos by the Perseverance rover", "Photos by WATSON");
                }
            }
            break;
        case "pierre_markuse":
            if (result.contains("Satellite pictures of tropical cyclones") && result.contains("Photos by VIIRS")) {
                result.remove("Satellite pictures of tropical cyclones");
                result.remove("Photos by Suomi NPP");
                result.remove("Photos by VIIRS");
                result.add("Photos of tropical cyclones by VIIRS");
            } else if (media.getTags().contains("sentinel") && media.getTags().contains("fire")) {
                result.add("Photos of wildfires by Sentinel satellites");
            } else if (media.getTags().contains("sentinel") && media.getTags().contains("flood")) {
                result.add("Photos of floods by Sentinel satellites");
            }
            for (String sentinel : new String[] { "Sentinel-1", "Sentinel-2", "Sentinel-3" }) {
                if (result.contains(sentinel + " images")) {
                    result.addAll(categorizationService.findCategoriesForEarthObservationImage(media,
                            x -> "Photos of " + x + " by " + sentinel, sentinel + " images", true, true, true));
                }
            }
            break;
        default:
            break;
        }
        if (result.contains("Photos of Jupiter system by spacecraft") && result.contains("Photos by JunoCam")) {
            replace(result, "Photos of Jupiter system by spacecraft", "Photos of Jupiter system by JunoCam");
        }
        if (result.contains("Jupiter (planet)") && result.contains("Photos by JunoCam")) {
            replace(result, "Jupiter (planet)", "Photos of Jupiter by JunoCam");
        }
        if (includeHidden) {
            switch (media.getPathAlias()) {
            case "geckzilla":
                result.add("Files from Judy Schmidt Flickr stream");
                break;
            case "kevinmgill":
                result.add("Files from Kevin Gill Flickr stream");
                break;
            case "pierre_markuse":
                result.add("Files from Pierre Markuse Flickr stream");
                break;
            case "192271236@N03":
                result.add("Files from Andrea Luck Flickr stream");
                break;
            case "semeion":
                result.add("Files from Simeon Schmauß Flickr stream");
                break;
            case "194849271@N04":
                result.add("Files from SpaceFromSpace Flickr stream");
                break;
            case "jurvetson":
                result.add("Photographs by Steve Jurvetson");
                break;
            default:
                break;
            }
        }
        return result;
    }

    @Override
    protected boolean isSatellitePicture(FlickrMedia media, FileMetadata metadata) {
        return Set.of("pierre_markuse", "harrystrangerphotography", "194849271@N04").contains(media.getPathAlias())
                || super.isSatellitePicture(media, metadata);
    }

    @Override
    protected Set<String> getEmojis(FlickrMedia uploadedMedia) {
        Set<String> result = super.getEmojis(uploadedMedia);
        switch (uploadedMedia.getPathAlias()) {
        case "geckzilla":
            result.add(Emojis.STARS);
            break;
        case "kevinmgill", "192271236@N03", "semeion":
            result.add(Emojis.PLANET_WITH_RINGS);
            break;
        case "pierre_markuse", "harrystrangerphotography", "194849271@N04":
            result.add(Emojis.EARTH_EUROPE);
            result.add(Emojis.SATELLITE);
            break;
        default:
            result.add("❔");
        }
        return result;
    }

    @Override
    protected Set<String> getMastodonAccounts(FlickrMedia uploadedMedia) {
        switch (uploadedMedia.getPathAlias()) {
        case "geckzilla":
            return Set.of("@spacegeck@astrodon.social");
        case "kevinmgill":
            return Set.of("@kevinmgill@deepspace.social");
        case "pierre_markuse":
            return Set.of("@pierre_markuse@mastodon.world");
        case "harrystrangerphotography", "194849271@N04":
            return Set.of("@spacefromspace@spacey.space");
        case "192271236@N03":
            return Set.of("@andrealuck@fosstodon.org");
        case "semeion":
            return Set.of("@stim3on@fosstodon.org");
        default:
            return Set.of();
        }
    }
}
