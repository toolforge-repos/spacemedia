package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerGeoExtend(double north, double south, double east, double west) {

    public CoordinateSequence toCoordinates() {
        Coordinate closingCoordinate = new Coordinate(west, north);
        return new CoordinateArraySequence(new Coordinate[] { closingCoordinate, new Coordinate(east, north),
                new Coordinate(east, south), new Coordinate(west, south), closingCoordinate });
    }
}
