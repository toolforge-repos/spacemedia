package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.wikidata.wdtk.datamodel.interfaces.GlobeCoordinatesValue;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaIndexes;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithAuthors;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithKeywords;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithLatLon;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithLicenceUrl;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.OrderColumn;

@Entity
@MediaIndexes
public class MedihalMedia extends Media implements WithAuthors, WithKeywords, WithLatLon, WithLicenceUrl {

    // https://doc.hal.science/deposer/metadonnees-document/#liste-des-metadonnees-de-documents

    @Column(nullable = false)
    private URL licenceUrl;

    @Column(nullable = false, unique = true, length = 275)
    private URL uri;

    @Column(nullable = false)
    private String halId;

    private double latitude;
    private double longitude;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MedihalDocType docType;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private MedihalDocSubType docSubType;

    @Column(columnDefinition = "TEXT")
    private String comments;

    @Column
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> keywords = new HashSet<>();

    @OrderColumn
    @ElementCollection(fetch = FetchType.EAGER)
    protected List<Author> authors = new ArrayList<>();

    public URL getUri() {
        return uri;
    }

    public void setUri(URL uri) {
        this.uri = uri;
    }

    public String getHalId() {
        return halId;
    }

    public void setHalId(String halId) {
        this.halId = halId;
    }

    @Override
    public Set<String> getKeywords() {
        return keywords;
    }

    @Override
    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public List<Author> getAuthors() {
        return authors;
    }

    @Override
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public URL getLicenceUrl() {
        return licenceUrl;
    }

    @Override
    public void setLicenceUrl(URL licenceUrl) {
        this.licenceUrl = licenceUrl;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public double getPrecision() {
        return GlobeCoordinatesValue.PREC_MILLI_ARCSECOND;
    }

    public MedihalDocType getDocType() {
        return docType;
    }

    public void setDocType(MedihalDocType docType) {
        this.docType = docType;
    }

    public MedihalDocSubType getDocSubType() {
        return docSubType;
    }

    public void setDocSubType(MedihalDocSubType docSubType) {
        this.docSubType = docSubType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String getUploadId(FileMetadata fileMetadata) {
        return "MédiHAL " + super.getUploadId(fileMetadata);
    }

    @Override
    public List<String> getIdUsedInCommons() {
        return List.of(getIdUsedInOrg(), halId);
    }

    public MedihalMedia copyDataFrom(MedihalMedia media) {
        super.copyDataFrom(media);
        this.licenceUrl = media.licenceUrl;
        this.uri = media.uri;
        this.halId = media.halId;
        this.latitude = media.latitude;
        this.longitude = media.longitude;
        this.docType = media.docType;
        this.docSubType = media.docSubType;
        this.comments = media.comments;
        return this;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(docType, docSubType, keywords, authors, latitude, licenceUrl, uri, halId, longitude,
                        comments);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj) || getClass() != obj.getClass())
            return false;
        MedihalMedia other = (MedihalMedia) obj;
        return docType == other.docType && docSubType == other.docSubType
                && Objects.equals(keywords, other.keywords)
                && Objects.equals(authors, other.authors)
                && Double.doubleToLongBits(latitude) == Double.doubleToLongBits(other.latitude)
                && Objects.equals(licenceUrl, other.licenceUrl) && Objects.equals(uri, other.uri)
                && Objects.equals(halId, other.halId)
                && Double.doubleToLongBits(longitude) == Double.doubleToLongBits(other.longitude)
                && Objects.equals(comments, other.comments);
    }

    @Override
    public String toString() {
        return "MedihalMedia [" + (title != null ? "title=" + title + ", " : "")
                + (publicationDate != null ? "publicationDate=" + publicationDate + ", " : "")
                + (licenceUrl != null ? "licenceUrl=" + licenceUrl + ", " : "")
                + (getId() != null ? "id=" + getId() : "") + "]";
    }
}
