package org.wikimedia.commons.donvip.spacemedia.data.commons;

import org.springframework.data.repository.CrudRepository;

public interface CommonsContentRepository extends CrudRepository<CommonsContent, Integer> {

}
