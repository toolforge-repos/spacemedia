package org.wikimedia.commons.donvip.spacemedia.data.domain.dinamis.api;

public record DinamisFeature(String type, DinamisGeometry geometry, DinamisProperties properties) {

}
