package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.SingleFileMedia;
import org.wikimedia.commons.donvip.spacemedia.exception.UploadException;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;

public abstract class AbstractFileProviderService<T extends SingleFileMedia> extends AbstractOrgService<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileProviderService.class);

    protected AbstractFileProviderService(MediaRepository<T> repository, String id, Set<String> appShares) {
        super(repository, id, appShares);
    }

    protected static final String getApp(CompositeMediaId id) {
        return getApp(id.repoId());
    }

    protected static final String getShare(CompositeMediaId id) {
        return getShare(id.repoId());
    }

    protected static final String getApp(String s) {
        return s.substring(0, s.indexOf('/'));
    }

    protected static final String getShare(String s) {
        return s.substring(s.indexOf('/') + 1);
    }

    @Override
    public void updateMedia(String[] args) {
        LocalDateTime start = startUpdateMedia();
        List<T> uploadedMedia = new ArrayList<>();
        int count = 0;
        for (String appShare : getRepoIdsFromArgs(args)) {
            Pair<Integer, Collection<T>> update = updateFileMedia(getApp(appShare), getShare(appShare));
            if (shouldPostOnSocialMedia()) {
                uploadedMedia.addAll(update.getRight());
            }
            count += update.getLeft();
            ongoingUpdateMedia(start, count);
        }
        endUpdateMedia(count, uploadedMedia);
    }

    private Pair<Integer, Collection<T>> updateFileMedia(String app, String share) {
        List<T> uploadedMedia = new ArrayList<>();
        int count = 0;
        LocalDateTime start = LocalDateTime.now();

        List<T> files = getFiles(app, share, Comparator.comparing(SingleFileMedia::getPublicationDateTime));
        LOGGER.info("Found {} files in {}", files.size(), Duration.between(start, LocalDateTime.now()));

        for (T media : files) {
            try {
                Pair<T, Integer> result = processFileMedia(media);
                if (result.getValue() > 0 && shouldPostOnSocialMedia()) {
                    uploadedMedia.add(result.getKey());
                }
                ongoingUpdateMedia(start, share, count++);
            } catch (UploadException | IOException e) {
                LOGGER.error(e.getMessage(), e);
                GlitchTip.capture(e);
            }
        }

        cleanup(app, share);

        return Pair.of(count, uploadedMedia);
    }

    protected abstract List<T> getFiles(String app, String share, Comparator<T> comparing);

    protected void cleanup(String app, String share) {
        // Override if needed
    }

    private Pair<T, Integer> processFileMedia(T mediaFromApi) throws UploadException, IOException {
        T media;
        boolean save = false;
        Optional<T> mediaInDb = repository.findById(mediaFromApi.getId());
        if (mediaInDb.isPresent()) {
            media = mediaInDb.get();
        } else {
            media = mediaFromApi;
            save = true;
        }
        save |= doCommonUpdate(media);
        int uploadCount = 0;
        if (shouldUploadAuto(media, false)) {
            Triple<T, Collection<FileMetadata>, Integer> upload = upload(media, true, false);
            uploadCount = upload.getRight();
            media = upload.getLeft();
            save = true;
        }
        if (save) {
            saveMedia(media);
        }
        return Pair.of(media, uploadCount);
    }

    @Override
    public final URL getSourceUrl(T media, FileMetadata metadata, String ext) {
        return metadata.getAssetUrl();
    }
}
