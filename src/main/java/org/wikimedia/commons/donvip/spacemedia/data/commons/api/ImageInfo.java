package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.net.URL;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ImageInfo (

    ZonedDateTime timestamp,

    String user,

    @JsonProperty("userid")
    Long userId,

    Long size,

    Integer width,

    Integer height,

    String comment,

    @JsonProperty("parsedcomment")
    String parsedComment,

    String html,

    @JsonProperty("canonicaltitle")
    String canonicalTitle,

    URL url,

    @JsonProperty("descriptionurl")
    URL descriptionUrl,

    @JsonProperty("descriptionshorturl")
    URL descriptionShortUrl,

    String sha1,

    String mime,

    String mediatype,

    @JsonProperty("bitdepth")
    Short bitDepth) {

    @Override
    public String toString() {
        // Override to skip html and comments in logs
        return "ImageInfo [" + (timestamp != null ? "timestamp=" + timestamp + ", " : "")
                + (user != null ? "user=" + user + ", " : "") + (userId != null ? "userId=" + userId + ", " : "")
                + (size != null ? "size=" + size + ", " : "") + (width != null ? "width=" + width + ", " : "")
                + (height != null ? "height=" + height + ", " : "")
                + (canonicalTitle != null ? "canonicalTitle=" + canonicalTitle + ", " : "")
                + (url != null ? "url=" + url + ", " : "")
                + (descriptionUrl != null ? "descriptionUrl=" + descriptionUrl + ", " : "")
                + (descriptionShortUrl != null ? "descriptionShortUrl=" + descriptionShortUrl + ", " : "")
                + (sha1 != null ? "sha1=" + sha1 + ", " : "") + (mime != null ? "mime=" + mime + ", " : "")
                + (mediatype != null ? "mediatype=" + mediatype + ", " : "")
                + (bitDepth != null ? "bitDepth=" + bitDepth : "") + "]";
    }
}
