package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

public record LoginInformation(String username, String session, String sid) {

}
