package org.wikimedia.commons.donvip.spacemedia.data.domain.synology;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Set;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaRepository;

public interface SynologyMediaRepository extends MediaRepository<SynologyMedia> {

    @Retention(RetentionPolicy.RUNTIME)
    @CacheEvict(allEntries = true, cacheNames = { "synologyCount", "synologyCountByNas", "synologyCountIgnored",
            "synologyCountIgnoredByNas", "synologyCountMissing", "synologyCountMissingImagesByNas",
            "synologyCountMissingVideosByNas", "synologyCountMissingDocumentsByNas", "synologyCountMissingByNas",
            "synologyCountUploaded", "synologyCountUploadedByNas", "synologyCountPhashNotNull",
            "synologyCountPhashNotNullByNas" })
    @interface CacheEvictSynologyAll {

    }

    @Override
    @CacheEvictSynologyAll
    default void evictCaches() {

    }

    // COUNT

    @Override
    @Cacheable("synologyCount")
    long count();

    @Override
    @Cacheable("synologyCountByNas")
    long count(Set<String> nas);

    @Override
    @Cacheable("synologyCountIgnored")
    long countByMetadata_IgnoredTrue();

    @Override
    @Cacheable("synologyCountIgnoredByNas")
    long countByMetadata_IgnoredTrue(Set<String> nas);

    @Override
    @Cacheable("synologyCountMissing")
    long countMissingInCommons();

    @Override
    @Cacheable("synologyCountMissingByNas")
    long countMissingInCommons(Set<String> nas);

    @Override
    @Cacheable("synologyCountMissingImagesByNas")
    long countMissingImagesInCommons(Set<String> nas);

    @Override
    @Cacheable("synologyCountMissingVideosByNas")
    long countMissingVideosInCommons(Set<String> nas);

    @Override
    @Cacheable("synologyCountMissingDocumentsByNas")
    long countMissingDocumentsInCommons(Set<String> nas);

    @Override
    @Cacheable("synologyCountUploaded")
    long countUploadedToCommons();

    @Override
    @Cacheable("synologyCountUploadedByNas")
    long countUploadedToCommons(Set<String> nas);

    @Override
    @Cacheable("synologyCountPhashNotNull")
    long countByMetadata_PhashNotNull();

    @Override
    @Cacheable("synologyCountPhashNotNullByNas")
    long countByMetadata_PhashNotNull(Set<String> nas);

    // SAVE

    @Override
    @CacheEvictSynologyAll
    <S extends SynologyMedia> S save(S entity);

    @Override
    @CacheEvictSynologyAll
    <S extends SynologyMedia> List<S> saveAll(Iterable<S> entities);

    // DELETE

    @Override
    @CacheEvictSynologyAll
    void deleteById(CompositeMediaId id);

    @Override
    @CacheEvictSynologyAll
    void delete(SynologyMedia entity);

    @Override
    @CacheEvictSynologyAll
    void deleteAll(Iterable<? extends SynologyMedia> entities);

    @Override
    @CacheEvictSynologyAll
    void deleteAll();
}
