package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json;

public record PingPongJson(boolean success, String ezid) {
}
