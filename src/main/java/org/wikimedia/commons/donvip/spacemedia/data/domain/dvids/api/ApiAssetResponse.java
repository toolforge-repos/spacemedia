package org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api;

import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;

public record ApiAssetResponse(DvidsMedia results) {

}
