package org.wikimedia.commons.donvip.spacemedia.service;

import static java.util.Optional.ofNullable;
import static org.wikimedia.commons.donvip.spacemedia.utils.HashHelper.computePerceptualHash;
import static org.wikimedia.commons.donvip.spacemedia.utils.HashHelper.encode;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.hashes.HashAssociation;
import org.wikimedia.commons.donvip.spacemedia.data.hashes.HashAssociationRepository;
import org.wikimedia.commons.donvip.spacemedia.exception.FileDecodingException;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.utils.MediaUtils;

@Service
public class HashService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HashService.class);

    @Autowired
    private HashAssociationRepository hashRepository;

    @Lazy
    @Autowired
    private RemoteService remote;

    @Value("${execution.mode}")
    private ExecutionMode hashMode;

    @Transactional(transactionManager = "hashesTransactionManager")
    public HashAssociation save(HashAssociation association) {
        return hashRepository.save(association);
    }

    @Transactional(transactionManager = "hashesTransactionManager")
    public HashAssociation saveIfAbsent(String sha1base36, String phash, String mime) {
        if (!hashRepository.existsById(sha1base36)) {
            return hashRepository.save(new HashAssociation(sha1base36, phash, mime));
        }
        return null;
    }

    @Transactional(transactionManager = "hashesTransactionManager")
    public HashAssociation computeAndSaveHash(String sha1base36, Supplier<URL> urlSupplier, String mime) {
        if (!hashRepository.existsById(sha1base36)) {
            return doComputeAndSaveHash(sha1base36, urlSupplier, mime);
        } else {
            // Temporary migration code
            hashRepository.findBySha1AndMimeIsNull(sha1base36).ifPresent(hash -> {
                hash.setMime(mime);
                hashRepository.save(hash);
            });
        }
        return null;
    }

    private HashAssociation doComputeAndSaveHash(String sha1base36, Supplier<URL> urlSupplier, String mime) {
        URL url = urlSupplier.get();
        BufferedImage bi = null;
        try (CloseableHttpClient httpClient = httpClientBuilder().build()) {
            bi = (BufferedImage) MediaUtils.readFile(url, null, null, false, false, httpClient, null).contents();
            if (bi == null) {
                throw new IOException("Failed to read image from " + url);
            }
            HashAssociation hash = hashRepository
                    .save(new HashAssociation(sha1base36, encode(computePerceptualHash(bi)), mime));
            if (hashMode == ExecutionMode.REMOTE) {
                remote.putHashAssociation(hash);
            }
            return hash;
        } catch (IOException | FileDecodingException | RuntimeException e) {
            LOGGER.error("Failed to compute/save hash of {}: {}", url, e.toString());
            GlitchTip.capture(e);
            return null;
        } finally {
            if (bi != null) {
                bi.flush();
            }
        }
    }

    @Transactional(transactionManager = "hashesTransactionManager")
    public Optional<HashAssociation> getOrUpdateHash(String sha1base36, Supplier<URL> urlSupplier, String mime) {
        return hashRepository.findById(sha1base36)
                .or(() -> ofNullable(doComputeAndSaveHash(sha1base36, urlSupplier, mime)));
    }

    @Transactional(transactionManager = "hashesTransactionManager")
    public List<String> getSha1Hashes(FileMetadata metadata) {
        return hashRepository.findSha1ByPhashAndMime(metadata.getPhash(), metadata.getMime());
    }
}
