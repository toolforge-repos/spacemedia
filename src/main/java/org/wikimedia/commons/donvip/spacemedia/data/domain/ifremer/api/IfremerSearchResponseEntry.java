package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerSearchResponseEntry(int docId, IfremerText title, String urlSection, ZonedDateTime date,
        String dateString, List<IfremerAuthor> authors, IfremerText description, URL thumbnailUrl,
        IfremerQuote quote, int docType, int filesCount, int fileId, boolean isDocumentCard, int visibilityHd,
        int visibilityLd, int visibilityMd) {

}
