package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import okhttp3.ResponseBody;

public class DownloadResponse extends DsmWebapiResponse<ResponseBody> {

    public DownloadResponse(ResponseBody data) {
        super(true, data, null);
    }
}
