package org.wikimedia.commons.donvip.spacemedia.utils;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoLocalDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpHead;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpPut;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.CookieStore;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpRequest;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.util.Timeout;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static final String EN = "en";
    public static final String ES = "es";
    public static final String ID = "id";
    public static final String PT_BR = "pt-br";

    // Months + https://www.defense.gov/Resources/Insignia/
    private static final String[] ABBREVIATIONS = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Sept", "Oct", "Nov", "Dec", "Maj", "Jr", "Sr", "Mr", "Mrs", "PV2", "PFC", "CPL", "SPC", "SGT",
            "SSG", "SFC", "MSG", "1SG", "SGM", "CSM", "SMA", "WO1", "CW2", "CW3", "CW4", "CW5", "2LT", "1LT", "CPT",
            "MAJ", "LTC", "COL", "BG", "MG", "LTG", "GEN", "PFC", "LCpl", "Cpl", "Sgt", "SSgt", "GySgt", "MSgt",
            "MGySgt", "SgtMaj", "SgtMajMC", "WO", "CWO2", "CWO3", "CWO4", "CWO5", "2ndLt", "1stLt", "Capt", "Maj",
            "LtCol", "Col", "BGen", "MajGen", "LtGen", "Gen", "SR", "SA", "SN", "PO3", "PO2", "PO1", "CPO", "SCPO",
            "MCPO", "MCPON", "ENS", "LTJG", "LT", "LCDR", "CDR", "CAPT", "RDML", "RADM", "VADM", "ADM", "Amn", "A1C",
            "SrA", "TSgt", "SMSgt", "CMSgt", "CMSAF", "2d Lt", "1st Lt", "Lt Col", "Brig Gen", "Maj Gen", "Lt Gen",
            "Spc1", "Spc2", "Spc3", "Spc4", "CMSSF", "MCPOCG", "St" };

    public static final String UUID_REGEX = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    public static final Pattern UUID_PATTERN = Pattern.compile(UUID_REGEX, Pattern.CASE_INSENSITIVE);

    private Utils() {
        // Hide default constructor
    }

    public static URI newURI(String uri) {
        try {
            return new URI(uri.replace(" ", "%20").replace("[", "%5b").replace("\\", "%5c").replace("]", "%5d"));
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(uri, e);
        }
    }

    public static URL newURL(String url) {
        try {
            return newURI(url).toURL();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(url, e);
        }
    }

    public static URL newURL(String protocol, String host, String file) {
        try {
            return file.contains("://") ? newURL(file.trim()) : new URI(protocol, host, file, null).toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static URL uriToUrLUnchecked(URI uri) {
        try {
            return uri.toURL();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static URI urlToUriUnchecked(URL url) {
        try {
            return urlToUri(url);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static URI urlToUri(URL url) throws URISyntaxException {
        if (url == null) {
            return null;
        }
        URI uri = null;
        try {
            uri = url.toURI();
        } catch (URISyntaxException e) {
            uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), null);
        }
        return new URI(uri.toASCIIString());
    }

    public static HttpGet newHttpGet(String uri) {
        return newHttpGet(URI.create(uri));
    }

    public static HttpGet newHttpGet(URL url) {
        return newHttpGet(urlToUriUnchecked(url));
    }

    public static HttpGet newHttpGet(URI uri) {
        HttpGet request = new HttpGet(uri);
        RequestConfig.Builder requestConfig = RequestConfig.custom();
        requestConfig.setConnectionRequestTimeout(Timeout.ofSeconds(30));
        requestConfig.setResponseTimeout(Timeout.ofSeconds(30));
        request.setConfig(requestConfig.build());
        return request;
    }

    public static HttpHead newHttpHead(URI uri) {
        return new HttpHead(uri);
    }

    public static HttpPost newHttpPost(Element form, BiConsumer<Element, List<NameValuePair>> consumer)
            throws IOException {
        return newHttpPost(form, x -> x, consumer, null);
    }

    public static HttpPost newHttpPost(Element form, UnaryOperator<String> actionFunction,
            BiConsumer<Element, List<NameValuePair>> inputConsumer,
            BiConsumer<Element, List<NameValuePair>> buttonConsumer) throws IOException {
        if (form == null) {
            throw new IOException("Form not found");
        }
        List<NameValuePair> params = new ArrayList<>();
        buildPostParams(form, "input", inputConsumer, params);
        buildPostParams(form, "button", buttonConsumer, params);
        return newHttpPost(actionFunction.apply(form.attr("action")), params);
    }

    public static HttpPost newHttpPost(String uri, Map<String, Object> params) {
        return newHttpPost(uri, params.entrySet().stream()
                .map(e -> new BasicNameValuePair(e.getKey(), Objects.toString(e.getValue()))).toList());
    }

    public static HttpPost newHttpPost(String uri, List<? extends NameValuePair> params) {
        return newHttpPost(uri, new UrlEncodedFormEntity(params, UTF_8));
    }

    public static HttpPost newHttpPost(String uri, final HttpEntity entity) {
        HttpPost post = new HttpPost(uri);
        post.setEntity(entity);
        return post;
    }

    public static HttpPut newHttpPut(String uri, final HttpEntity entity) {
        HttpPut put = new HttpPut(uri);
        put.setEntity(entity);
        return put;
    }

    private static void buildPostParams(Element form, String tag, BiConsumer<Element, List<NameValuePair>> consumer,
            List<NameValuePair> params) {
        if (consumer != null) {
            form.getElementsByTag(tag).forEach(elem -> consumer.accept(elem, params));
        }
    }

    public static InputStream executeRequestStream(HttpUriRequestBase request, HttpClient httpclient,
            HttpClientContext context) throws IOException {
        return requireNonNull(executeRequest(request, httpclient, context).getEntity(), "entity").getContent();
    }

    public static ClassicHttpResponse executeRequest(HttpUriRequestBase request, HttpClient httpclient,
            HttpClientContext context) throws IOException {
        return executeRequest(request, httpclient, context, true);
    }

    public static ClassicHttpResponse executeRequest(HttpUriRequestBase request, HttpClient httpclient,
            HttpClientContext context, boolean checkResponse) throws IOException {
        ClassicHttpResponse response = httpclient.executeOpen(null, request, context);
        try {
            LOGGER.debug("{} - {} : {} ({}) => {}",
                httpclient.getClass().getSimpleName(), context, request, request.getUri(), response);
        } catch (URISyntaxException ignored) {
            LOGGER.trace("", ignored);
        }
        return checkResponse ? checkResponse(request, response) : response;
    }

    public static ClassicHttpResponse checkResponse(HttpRequest request, ClassicHttpResponse response)
            throws IOException {
        int statusCode = response.getCode();
        if (statusCode >= 400) {
            org.springframework.http.HttpStatus status = org.springframework.http.HttpStatus.valueOf(statusCode);
            String message = request + " => " + response.getReasonPhrase();
            HttpHeaders headers = new HttpHeaders();
            Arrays.stream(response.getHeaders()).forEach(h -> headers.add(h.getName(), h.getValue()));
            byte[] body = new byte[0];
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream in = entity.getContent()) {
                    if (in != null) {
                        body = IOUtils.toByteArray(in);
                    }
                } catch (IOException | UnsupportedOperationException e) {
                    LOGGER.error("Failed to read error response: {}", e.toString());
                }
            }
            String detailedMessage = message + " -- " + headers
                    + (entity != null && body != null
                            ? " -- " + new String(body, ofNullable(entity.getContentEncoding()).orElse("UTF-8"))
                            : "");
            if (statusCode >= 500) {
                throw new IOException(detailedMessage,
                        HttpServerErrorException.create(status, message, headers, body, UTF_8));
            } else if (statusCode >= 400) {
                throw new IOException(detailedMessage,
                        HttpClientErrorException.create(status, message, headers, body, UTF_8));
            }
            throw new IOException(detailedMessage);
        }
        return response;
    }

    public static Document getWithJsoup(String pageUrl, int timeout, int nRetries) throws IOException {
        return getWithJsoup(pageUrl, timeout, nRetries, true);
    }

    public static Document getWithJsoup(String pageUrl, int timeout, int nRetries, boolean followRedirects)
            throws IOException {
        return getWithJsoup(pageUrl, timeout, nRetries, followRedirects, c -> c);
    }

    public static Document getWithJsoup(String pageUrl, int timeout, int nRetries, boolean followRedirects,
            UnaryOperator<Connection> setup) throws IOException {
        for (int i = 0; i < nRetries; i++) {
            try {
                LOGGER.debug("Scrapping {}", pageUrl);
                return setup.apply(Jsoup.connect(pageUrl).timeout(timeout).followRedirects(followRedirects)).get();
            } catch (HttpStatusException e) {
                LOGGER.warn("{} when scrapping {} => {}", e.getClass().getSimpleName(), pageUrl, e.getMessage());
                if (e.getStatusCode() >= 400 && e.getStatusCode() < 500) {
                    throw e;
                }
            } catch (UnknownHostException e) {
                throw e;
            } catch (IOException e) {
                LOGGER.warn("{} when scrapping {} => {}", e.getClass().getSimpleName(), pageUrl, e.getMessage());
            }
        }
        throw new IOException("Unable to scrap " + pageUrl);
    }

    public static String findExtension(String path) {
        int idx = path.lastIndexOf('.');
        if (idx < 0) {
            return null;
        }
        String ext = path.substring(idx + 1).toLowerCase(Locale.ENGLISH);
        if ("gne".equals(ext)) {
            return null;
        }
        int len = ext.length();
        if (len < 3 || len > 4) {
            return null;
        }
        return getNormalizedExtension(ext);
    }

    public static String getNormalizedExtension(String ext) {
        return ext != null ? switch (ext) {
        case "apng" -> "png";
        case "djv" -> "djvu";
        case "jpe", "jpeg", "jps" -> "jpg"; // Use the same extension as flickr2commons as it solely relies on filenames
        case "tif" -> "tiff";
        case "mid", "kar" -> "midi";
        case "mpe", "mpg" -> "mpeg";
        default -> ext;
        } : null;
    }

    public static String getFilename(String url) {
        return getFilename(newURL(url));
    }

    public static String getFilename(URL url) {
        String file = url.getFile();
        String result = file.substring(file.lastIndexOf('/') + 1, file.lastIndexOf('.')).trim();
        return result.contains("=") ? result.substring(result.lastIndexOf('=') + 1).trim() : result;
    }

    public static Pair<Path, Long> downloadFile(URI uri, String fileName) throws IOException {
        return downloadFile(uri.toURL(), fileName);
    }

    public static Pair<Path, Long> downloadFile(URL url, String fileName) throws IOException {
        Path output = Files.createDirectories(Path.of("files")).resolve(fileName);
        Long size = 0L;
        LOGGER.info("Downloading file {} as {}", url, fileName);
        try (ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                FileOutputStream fos = new FileOutputStream(output.toString())) {
            size = fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            if (size == 0) {
                throw new IOException("No data transferred from " + url);
            }
        }
        return Pair.of(output, size);
    }

    public static Path extractFile(Path zipFile, String fileName, Path outputFile) throws IOException {
        try (FileSystem fileSystem = FileSystems.newFileSystem(zipFile, (ClassLoader) null)) {
            return Files.copy(fileSystem.getPath(fileName), outputFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static Element newElement(String tag, String text, Map<String, String> attrs) {
        Attributes attributes = new Attributes();
        attrs.entrySet().forEach(e -> attributes.put(e.getKey(), e.getValue()));
        Element elem = new Element(Tag.valueOf(tag), "", attributes);
        if (text != null) {
            elem.text(text);
        }
        return elem;
    }

    public static Element prependChildElement(Element base, String tag, String text, Map<String, String> attrs) {
        Element elem = newElement(tag, text, attrs);
        base.insertChildren(0, elem);
        return elem;
    }

    public static Element appendChildElement(Element base, String tag, String text, Map<String, String> attrs) {
        Element elem = newElement(tag, text, attrs);
        base.appendChild(elem);
        return elem;
    }

    public static boolean isTextFound(String fullText, String textToFind) {
        return fullText != null
                && isTextFoundLowercase(fullText.toLowerCase(Locale.ENGLISH), textToFind.toLowerCase(Locale.ENGLISH));
    }

    public static boolean isTextFoundLowercase(String fullTextLc, String textToFindLc) {
        if (fullTextLc != null) {
            if (textToFindLc.contains("+")) {
                for (String word : textToFindLc.split("\\+")) {
                    if (!isTextFound(fullTextLc, word)) {
                        return false;
                    }
                }
                return true;
            } else {
                int index = fullTextLc.indexOf(textToFindLc);
                if (index > -1) {
                    if (index > 0 && isTokenPart(fullTextLc.charAt(index - 1))) {
                        return false;
                    }
                    if (index + textToFindLc.length() < fullTextLc.length() - 1
                            && isTokenPart(fullTextLc.charAt(index + textToFindLc.length()))) {
                        return false;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isTokenPart(char c) {
        return Character.isAlphabetic(c) || Character.isDigit(c) || c == '-' || c == '_';
    }

    /**
     * Runs an external command and returns the standard output. Waits at most the specified time.
     *
     * @param command the command with arguments
     * @param timeout the maximum time to wait
     * @param unit    the time unit of the {@code timeout} argument. Must not be null
     * @return the output
     * @throws IOException          when there was an error, e.g. command does not exist
     * @throws ExecutionException   when the return code is != 0. The output is can be retrieved in the exception message
     */
    public static String execOutput(List<String> command, long timeout, TimeUnit unit)
            throws IOException, ExecutionException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.join(" ", command));
        }
        Path out = Files.createTempFile("spacemedia_exec_" + command.get(0) + "_", ".txt");
        try {
            Process p = new ProcessBuilder(command).redirectErrorStream(true).redirectOutput(out.toFile()).start();
            boolean error = !p.waitFor(timeout, unit) || p.exitValue() != 0;
            String output = String.join("\n", Files.readAllLines(out)).trim();
            LOGGER.info(output);
            if (error) {
                throw new ExecutionException(command.toString() + " => " + output, null);
            }
            return output;
        } catch (InterruptedException e) {
            GlitchTip.capture(e);
            Thread.currentThread().interrupt();
            throw new IOException(e);
        } finally {
            try {
                Files.delete(out);
            } catch (IOException e) {
                LOGGER.warn("Error while deleting file", e);
            }
        }
    }

    public static Duration durationInSec(Temporal start) {
        return durationInSec(start, LocalDateTime.now());
    }

    public static Duration durationInSec(Temporal start, Temporal end) {
        return Duration.between(start, end).truncatedTo(ChronoUnit.SECONDS);
    }

    public static ZonedDateTime toZonedDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault());
    }

    public static Date toDate(LocalDate date) {
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static RestTemplate restTemplateSupportingAll(Charset charset) {
        return new RestTemplate(List.of(new StringHttpMessageConverter(charset)));
    }

    public static RestTemplate restTemplateSupportingAll(ObjectMapper jackson) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(jackson);
        converter.setSupportedMediaTypes(List.of(MediaType.ALL));
        return new RestTemplate(List.of(converter));
    }

    public static <T> boolean replace(Collection<T> collection, T oldValue, T newValue) {
        return collection.remove(oldValue) && collection.add(newValue);
    }

    public static String getFirstSentence(LocalizedText text) {
        if (text == null) {
            return "";
        }
        String desc = text.text();
        if (isBlank(desc)) {
            return "";
        }
        int idxDotInDesc = desc.indexOf('.');
        if (idxDotInDesc <= 0) {
            return desc;
        }
        int nextDot = checkDotIndex(desc, idxDotInDesc);
        if (nextDot <= 0) {
            return desc;
        }
        while (nextDot > idxDotInDesc) {
            idxDotInDesc = nextDot;
            nextDot = checkDotIndex(desc, idxDotInDesc);
            if (nextDot <= 0) {
                return desc;
            }
        }
        return idxDotInDesc > 0 ? desc.substring(0, idxDotInDesc) : desc;
    }

    private static int checkDotIndex(String desc, int idxDotInDesc) {
        if (desc.charAt(idxDotInDesc - 1) == 'U' && desc.length() > idxDotInDesc + 3
                && desc.charAt(idxDotInDesc + 1) == 'S' && desc.charAt(idxDotInDesc + 2) == '.') {
            // Special case for "U.S."
            return desc.indexOf('.', idxDotInDesc + 3);
        } else if (desc.charAt(idxDotInDesc - 1) == 'D' && desc.length() > idxDotInDesc + 3
                && desc.charAt(idxDotInDesc + 1) == 'C' && desc.charAt(idxDotInDesc + 2) == '.') {
            // Special case for "D.C."
            return desc.indexOf('.', idxDotInDesc + 3);
        } else if ((idxDotInDesc < 2 || desc.charAt(idxDotInDesc - 2) == ' ')
                && Character.isLetter(desc.charAt(idxDotInDesc - 1))) {
            // Special case for initials
            return desc.indexOf('.', idxDotInDesc + 1);
        } else {
            // Ignore known abbrevations
            String subs = desc.substring(0, idxDotInDesc);
            for (String abbr : ABBREVIATIONS) {
                if (subs.endsWith(abbr)) {
                    return desc.indexOf('.', idxDotInDesc + 1);
                }
            }
        }
        return idxDotInDesc;
    }

    public static boolean uriExists(String uri) {
        try (CloseableHttpClient httpClient = httpClientBuilder().disableRedirectHandling().build()) {
            return httpClient.executeOpen(null, new HttpHead(uri), null).getCode() == 200;
        } catch (IOException e) {
            return false;
        }
    }

    public static Optional<ZonedDateTime> extractDate(String desc, Map<DateTimeFormatter, Pattern> patterns) {
        for (Entry<DateTimeFormatter, Pattern> e : patterns.entrySet()) {
            Matcher m = e.getValue().matcher(desc);
            if (m.matches()) {
                String text = m.group(1);
                DateTimeFormatter format = e.getKey();
                return Optional.of(format.toString().contains("Value(MinuteOfHour)")
                        ? LocalDateTime.parse(text, format).atZone(ZoneId.systemDefault())
                        : LocalDate.parse(text, format).atStartOfDay(ZoneId.systemDefault()));
            }
        }
        return Optional.empty();
    }

    public static Optional<Temporal> extractDate(String text, List<DateTimeFormatter> dtfs) {
        if (!text.isEmpty()) {
            String cleanText = text.replace(", ", " ").replace("  ", " ");
            for (DateTimeFormatter dtf : dtfs) {
                try {
                    TemporalAccessor accessor = dtf.parse(cleanText);
                    for (TemporalQuery<Temporal> query : List.<TemporalQuery<Temporal>>of(ZonedDateTime::from,
                            LocalDateTime::from, LocalDate::from, YearMonth::from, Year::from)) {
                        try {
                            Temporal temporal = accessor.query(query);
                            if (temporal != null) {
                                return Optional.of(temporal);
                            }
                        } catch (DateTimeException | ArithmeticException e) {
                            LOGGER.trace(e.getMessage());
                        }
                    }
                } catch (DateTimeParseException e) {
                    LOGGER.trace(e.getMessage());
                }
            }
        }
        return Optional.empty();
    }

    public static boolean isTemporalBefore(Temporal t, LocalDate date) {
        if (t instanceof ChronoLocalDate cld) {
            return cld.isBefore(date);
        } else if (t instanceof ChronoLocalDateTime<?> cldt) {
            return cldt.toLocalDate().isBefore(date);
        } else if (t instanceof ChronoZonedDateTime<?> czdt) {
            return czdt.toLocalDate().isBefore(date);
        } else if (t instanceof YearMonth ym) {
            return ym.isBefore(YearMonth.of(date.getYear(), date.getMonth()));
        } else if (t instanceof Year y) {
            return y.isBefore(Year.of(date.getYear()));
        }
        throw new IllegalArgumentException("Unsupported temporal: " + t);
    }

    public static Optional<Integer> extractFileSize(Pattern pattern, String text) {
        Matcher m = pattern.matcher(text);
        if (m.matches()) {
            double size = Double.parseDouble(m.group(1));
            switch (m.group(2)) {
            case "KB":
                size *= 1024;
                break;
            case "MB":
                size *= 1024 * 1024;
                break;
            case "GB":
                size *= 1024 * 1024 * 1024;
                break;
            default:
                throw new IllegalArgumentException("Unsupported file size unit: '" + m.group(2) + "'");
            }
            return Optional.of((int) size);
        }
        return Optional.empty();
    }

    public static HttpClientBuilder httpClientBuilder() {
        return HttpClientBuilder.create()
                .setConnectionManager(PoolingHttpClientConnectionManagerBuilder.create()
                        .setDnsResolver(new RetryableSystemDnsResolver()).build())
                .setRetryStrategy(new SpacemediaHttpRequestRetryStrategy());
    }

    public static HttpClientContext getHttpClientContext(CookieStore cookieStore) {
        HttpClientContext context = new HttpClientContext();
        context.setCookieStore(cookieStore);
        context.setRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.STRICT).build());
        return context;
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
            GlitchTip.capture(e);
        }
    }

    public static String https(String url) {
        return url != null ? url.replace("http://", "https://") : url;
    }

    public static boolean hasEnoughMemory(Long size) {
        if (size == null) {
            LOGGER.warn("File size undetermined. May be ouf of memory in case of big files");
            return true;
        }
        long freeMemory = Runtime.getRuntime().freeMemory();
        long needs = (long) (3.75 * size);
        if (freeMemory < needs) {
            LOGGER.warn("Less memory available than estimated needs ({} < {}). Aborting", freeMemory, needs);
            return false;
        }
        return true;
    }
}
