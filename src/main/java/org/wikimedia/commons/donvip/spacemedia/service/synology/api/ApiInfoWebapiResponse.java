package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import org.wikimedia.commons.donvip.spacemedia.service.synology.api.ApiInfo.ApiInfoList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiInfoWebapiResponse extends DsmWebapiResponse<ApiInfoList> {

    @JsonCreator
    public ApiInfoWebapiResponse(@JsonProperty("success") boolean success, @JsonProperty("data") ApiInfoList data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
