package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilEuropeDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilEuropeDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmileurope.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmileurope.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmileurope.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmileurope.dvids.min.year}") int minYear,
            @Value("${usmileurope.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmileurope.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in Europe (DVIDS)";
    }
}
