package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerSearchResponse(List<IfremerCriteria> criteriaList,
        List<IfremerSearchResponseEntry> responseEntries, int entriesCount, IfremerPagination pagination) {

}
