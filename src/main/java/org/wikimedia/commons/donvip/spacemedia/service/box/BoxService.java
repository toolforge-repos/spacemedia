package org.wikimedia.commons.donvip.spacemedia.service.box;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequest;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequestStream;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpGet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpPost;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;

import javax.annotation.PostConstruct;

import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.impl.DefaultRedirectStrategy;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpRequest;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.ProtocolException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.service.FileProvider;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFile.Info;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.SharedLinkAPIConnection;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.OkHttpClient;

@Lazy
@Service
public class BoxService implements FileProvider<BoxFile.Info> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BoxService.class);

    private static final String APP_AUTH_URL = "https://app.box.com/api/oauth2/authorize";

    private static final String REDIRECT_URL = "https://spacemedia.toolforge.org";

    private static final Set<String> IGNORED_FOLDERS = Set.of("__MACOSX");

    @Autowired
    private CommonsService commonsService;

    @Autowired
    private ObjectMapper jackson;

    @Value("${BOX_API_OAUTH2_CLIENT_ID}")
    private String clientId;

    @Value("${BOX_API_OAUTH2_CLIENT_SECRET}")
    private String clientSecret;

    @Value("${BOX_API_USER_EMAIL}")
    private String userEmail;

    @Value("${BOX_API_USER_PASSWORD}")
    private String userPassword;

    private BoxAPIConnection api;

    private BoxAPIConnection getApi() {
        if (api == null) {
            initBoxApi();
        }
        return api;
    }

    private void initBoxApi() {
        if (isNotBlank(clientId) && isNotBlank(clientSecret) && isNotBlank(userEmail) && isNotBlank(userPassword)) {
            // Access tokens expire quickly, so request new token at startup
            String authURL = BoxAPIConnection.getAuthorizationURL(clientId, URI.create(REDIRECT_URL), "state", null)
                    .toExternalForm();

            // GET https://account.box.com/api/oauth2/authorize?client_id=xxx&response_type=code&redirect_uri=xxx&state=state
            try (CloseableHttpClient httpclient = httpClientBuilder()
                    .setRedirectStrategy(new BoxRedirectStrategy()).build()) {
                // STEP 1 - Request login page
                HttpUriRequestBase request = newHttpGet(authURL);
                try (InputStream in1 = executeRequestStream(request, httpclient, null)) {
                    Document html1 = Jsoup.parse(in1, "UTF-8", authURL);
                    request = newHttpPost(ofNullable(
                            html1.getElementsByClass("form login_form").first())
                            .orElseThrow(() -> new IOException("Failed to retrieve login form: " + html1)),
                            (input, params) -> params.add(new BasicNameValuePair(input.attr("name"),
                                    "login".equals(input.attr("id")) ? userEmail
                                            : "password".equals(input.attr("id")) ? userPassword
                                                    : input.attr("value"))));

                    // STEP 2 - Login and request authorize page
                    try (InputStream in2 = executeRequestStream(request, httpclient, null)) {
                        Document html2 = Jsoup.parse(in2, "UTF-8", authURL);
                        String js = html2.getElementsByTag("script").first().html();
                        if (isBlank(js)) {
                            throw new IOException("Failed to locate Box JS: " + html2);
                        }
                        int idx = js.indexOf("\"\\/api\\/oauth2\\/authorize\\/data\"");
                        int start = js.indexOf('{', idx);
                        int end = js.indexOf('}', idx);
                        if (idx < 0 || start < 0 || end < 0) {
                            throw new IOException("Failed to locate oauth2 authorize data in JS: " + js);
                        }
                        request = newHttpPost(APP_AUTH_URL,
                                jackson.readValue(js.substring(start, end + 1), BoxOauth2AuthorizeData.class).params());

                        // STEP 3 - authorize and request authorization code
                        try (ClassicHttpResponse response3 = executeRequest(request, httpclient, null)) {
                            // https://app.box.com/developers/console
                            Header location = response3.getFirstHeader("location");
                            if (location == null) {
                                HttpEntity entity = response3.getEntity();
                                throw new IOException("Failed to retrieve authorization code: "
                                    + Arrays.toString(response3.getHeaders())
                                    + "\n" + entity + "\n" + EntityUtils.toString(entity));
                            }
                            api = new BoxAPIConnection(clientId, clientSecret, location.getValue().split("=")[2]);
                        }
                    }
                }
            } catch (IOException | ParseException | RuntimeException e) {
                LOGGER.error("Failed to instantiate Box API: {}", e.getMessage(), e);
                GlitchTip.capture(e);
            }
        } else {
            LOGGER.warn("Incomplete Box credentials configuration => Box API will not be available");
        }
    }

    @PostConstruct
    public void postConstruct() {
        if (LOGGER.isDebugEnabled()) {
            java.util.logging.Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINER);
        } else if (LOGGER.isTraceEnabled()) {
            java.util.logging.Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINEST);
        }
        initBoxApi();
    }

    public String getSharedLink(String app, String share) {
        return "https://" + app + ".app.box.com/s/" + share;
    }

    public String getSharedLink(String app, String share, BoxItem.Info itemInfo) {
        return getSharedLink(app, share, itemInfo.getType(), Long.parseLong(itemInfo.getID()));
    }

    public String getSharedLink(String app, String share, String type, long id) {
        return getSharedLink(app, share) + '/' + type + '/' + id;
    }

    public String getThumbnailUrl(String app, long fileVersion, String share) {
        return "https://" + app + ".app.box.com/representation/file_version_" + fileVersion
                + "/thumb_1024.jpg?shared_name=" + share;
    }

    public BoxItem.Info getSharedItem(URL sharedLink) {
        return getSharedItem(sharedLink.toExternalForm());
    }

    public BoxItem.Info getSharedItem(String sharedLink) {
        return BoxItem.getSharedItem(getApi(), sharedLink);
    }

    public BoxFile getSharedFile(String sharedLink, String fileId) {
        return new BoxFile(new SharedLinkAPIConnection(getApi(), sharedLink), fileId);
    }

    public List<BoxFile.Info> getFiles(String app, String share) {
        return getFiles(app, share, Function.identity(), Comparator.comparing(BoxFile.Info::getCreatedAt));
    }

    @Override
    public <T> List<T> getFiles(String app, String share, Function<BoxFile.Info, T> mapper, Comparator<T> comparator,
            Map<String, String> params) {
        String sharedLink = getSharedLink(app, share);
        LOGGER.info("Fetching files of shared folder {}", sharedLink);
        BoxItem.Info itemInfo = getSharedItem(sharedLink);
        if (itemInfo instanceof BoxFolder.Info folderInfo) {
            return getFiles(folderInfo, folderInfo.getName(), mapper).stream().sorted(comparator.reversed()).toList();
        } else if (itemInfo instanceof BoxFile.Info fileInfo && isWantedFile(fileInfo)) {
            return List.of(mapper.apply(fileInfo));
        }
        return List.of();
    }

    private <T> List<T> getFiles(BoxFolder.Info parentFolderInfo, String path,
            Function<BoxFile.Info, T> mapper) {
        LOGGER.info("Fetching files of folder {} - {}", parentFolderInfo.getID(), path);
        List<T> result = new ArrayList<>();
        for (BoxItem.Info itemInfo : parentFolderInfo.getResource().getChildren(BoxItem.ALL_FIELDS)) {
            if (itemInfo instanceof BoxFolder.Info folderInfo
                    && !IGNORED_FOLDERS.contains(folderInfo.getName())) {
                result.addAll(getFiles(folderInfo, path + " > " + folderInfo.getName(), mapper));
            } else if (itemInfo instanceof BoxFile.Info fileInfo && isWantedFile(fileInfo)) {
                result.add(mapper.apply(fileInfo));
            }
        }
        return result;
    }

    @Override
    public long getSize(Info file) {
        return file.getSize();
    }

    @Override
    public String getName(Info file) {
        return file.getName();
    }

    @Override
    public String getExtension(Info file) {
        return file.getExtension();
    }

    @Override
    public boolean isPermittedFileExt(String extension) {
        return commonsService.isPermittedFileExt(extension);
    }

    private static class BoxRedirectStrategy extends DefaultRedirectStrategy {

        @Override
        public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context)
                throws ProtocolException {
            Header locationHeader = response.getFirstHeader("location");
            return locationHeader != null && !locationHeader.getValue().contains(REDIRECT_URL)
                    && super.isRedirected(request, response, context);
        }
    }
}
