package org.wikimedia.commons.donvip.spacemedia.service;

public enum ExecutionMode {
    LOCAL, REMOTE;
}