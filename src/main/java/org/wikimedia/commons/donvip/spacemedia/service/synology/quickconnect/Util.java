package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect;

import java.util.Collection;

final class Util {

    private Util() {
        // Hide default constructor for utilities class
    }

    static boolean isQuickConnectId(String host) {
        return host != null && host.indexOf('.') < 0 && host.indexOf(':') < 0;
    }

    public static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        } else if (object instanceof String string) {
            return string.isEmpty();
        } else if (object instanceof Collection<?> collection) {
            return collection.isEmpty();
        } else if (object instanceof Object[] objects) {
            return objects.length == 0;
        }
        return true;
    }
}
