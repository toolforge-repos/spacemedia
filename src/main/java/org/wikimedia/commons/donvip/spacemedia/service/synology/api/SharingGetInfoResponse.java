package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SharingGetInfoResponse extends DsmWebapiResponse<SharingLink> {

    public SharingGetInfoResponse(@JsonProperty("success") boolean success, @JsonProperty("data") SharingLink data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
