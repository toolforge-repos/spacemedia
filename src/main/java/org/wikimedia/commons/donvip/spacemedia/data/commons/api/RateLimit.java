package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RateLimit (

    Limit user,

    @JsonProperty("autopatrolled")
    Limit autoPatrolled) {

}
