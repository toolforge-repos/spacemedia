package org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api;

public record ApiUnitResult(String unit_name, String unit_abbrev) {

}
