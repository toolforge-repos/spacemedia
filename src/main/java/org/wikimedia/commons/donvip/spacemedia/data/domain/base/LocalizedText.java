package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import java.util.Locale;

import org.apache.commons.text.StringEscapeUtils;
import org.hibernate.annotations.ColumnDefault;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Lob;

@Embeddable
public record LocalizedText(
        /** ISO 639 (alpha-2) language code */
        @ColumnDefault("'en'") @Column(nullable = true, length = 8) String lang,
        @Lob @Column(nullable = true, columnDefinition = "TEXT") String text) implements CharSequence {

    public LocalizedText(String text) {
        this("en", text);
    }

    @Override
    public char charAt(int index) {
        return text.charAt(index);
    }

    public boolean contains(String s) {
        return text != null && text.contains(s);
    }

    public int indexOf(int ch) {
        return text != null ? text.indexOf(ch) : -1;
    }

    public int indexOf(int ch, int fromIndex) {
        return text != null ? text.indexOf(ch, fromIndex) : -1;
    }

    public int indexOf(String str) {
        return text != null ? text.indexOf(str) : -1;
    }

    public int indexOf(String str, int fromIndex) {
        return text != null ? text.indexOf(str, fromIndex) : -1;
    }

    public boolean isBlank() {
        return text != null && text.isBlank();
    }

    public int lastIndexOf(int ch) {
        return text.lastIndexOf(ch);
    }

    public int lastIndexOf(int ch, int fromIndex) {
        return text.lastIndexOf(ch, fromIndex);
    }

    public int lastIndexOf(String str, int fromIndex) {
        return text.lastIndexOf(str, fromIndex);
    }

    @Override
    public int length() {
        return text != null ? text.length() : 0;
    }

    public boolean matches(String regex) {
        return text != null && text.matches(regex);
    }

    public LocalizedText replace(char oldChar, char newChar) {
        return new LocalizedText(lang, text != null ? text.replace(oldChar, newChar) : null);
    }

    public LocalizedText replace(CharSequence target, CharSequence replacement) {
        return new LocalizedText(lang, text != null ? text.replace(target, replacement) : null);
    }

    public LocalizedText replaceAll(String regex, String replacement) {
        return new LocalizedText(lang, text != null ? text.replaceAll(regex, replacement) : null);
    }

    public boolean startsWith(String prefix) {
        return text != null && text.startsWith(prefix);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return text != null ? text.subSequence(start, end) : null;
    }

    public LocalizedText substring(int beginIndex) {
        return new LocalizedText(lang, text != null ? text.substring(beginIndex) : null);
    }

    public LocalizedText substring(int beginIndex, int endIndex) {
        return new LocalizedText(lang, text != null ? text.substring(beginIndex, endIndex) : null);
    }

    public LocalizedText toLowerCase() {
        return new LocalizedText(lang, text != null ? text.toLowerCase(Locale.of(lang)) : null);
    }

    public LocalizedText toLowerCase(Locale locale) {
        return new LocalizedText(lang, text != null ? text.toLowerCase(locale) : null);
    }

    @Override
    public String toString() {
        return text != null ? text : "";
    }

    public LocalizedText trim() {
        return new LocalizedText(lang, text != null ? text.trim() : null);
    }

    public LocalizedText unescapeHtml4() {
        return new LocalizedText(lang, text != null ? StringEscapeUtils.unescapeHtml4(text) : null);
    }
}
