package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.box.BoxMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.box.BoxMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.UrlResolver;
import org.wikimedia.commons.donvip.spacemedia.service.box.BoxService;

import com.box.sdk.BoxAPIResponseException;
import com.box.sdk.BoxFile;

/**
 * Service fetching images from box.com
 */
public abstract class AbstractOrgBoxService extends AbstractFileProviderService<BoxMedia> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOrgBoxService.class);

    @Lazy
    @Autowired
    private BoxService boxService;

    protected AbstractOrgBoxService(BoxMediaRepository repository, String id, Set<String> appShares) {
        super(repository, id, appShares);
    }

    @Override
    protected Class<BoxMedia> getMediaClass() {
        return BoxMedia.class;
    }

    @Override
    protected HttpClientBuilder configureHttpClientBuilder(HttpClientBuilder builder) {
        // To avoid HTTP 400 "Bad request" errors when reading EXIF metadata
        return builder.disableContentCompression();
    }

    @Override
    protected UrlResolver<BoxMedia> getUrlResolver() {
        return (media, _) -> {
            try {
                return boxService.getSharedFile(getSharedLink(media.getId()), media.getId().mediaId())
                        .getDownloadURL();
            } catch (BoxAPIResponseException e) {
                if (e.getResponseCode() == 404) {
                    LOGGER.warn("Deleting image as no longer found: {}", e.getMessage());
                    deleteMedia(media, e);
                }
                throw e;
            }
        };
    }

    private String getSharedLink(CompositeMediaId id) {
        return boxService.getSharedLink(getApp(id), getShare(id));
    }

    @Override
    protected List<BoxMedia> getFiles(String app, String share, Comparator<BoxMedia> comparing) {
        return boxService.getFiles(app, share, fileInfo -> toMedia(app, share, fileInfo), comparing);
    }

    private BoxMedia toMedia(String app, String share, BoxFile.Info fileInfo) {
        BoxMedia media = new BoxMedia(app, share, Long.parseLong(fileInfo.getID()));
        Optional.ofNullable(fileInfo.getContentCreatedAt()).map(AbstractOrgBoxService::toZonedDateTime)
                .ifPresent(media::setCreationDateTime);
        media.setPublicationDateTime(toZonedDateTime(fileInfo.getCreatedAt()));
        media.setTitle(new LocalizedText("en", fileInfo.getName()));
        media.setDescription(new LocalizedText("en", fileInfo.getDescription()));
        media.setCreator(fileInfo.getCreatedBy().getName());
        media.setThumbnailUrl(
                boxService.getThumbnailUrl(app, Long.parseLong(fileInfo.getVersion().getVersionID()), share));
        addMetadata(media, boxService.getSharedLink(app, share, fileInfo), m -> fillMetadata(m, fileInfo));
        return media;
    }

    private static FileMetadata fillMetadata(FileMetadata m, BoxFile.Info fileInfo) {
        m.setExtension(fileInfo.getExtension());
        m.setSha1(fileInfo.getSha1());
        m.setSize(fileInfo.getSize());
        m.setOriginalFileName(fileInfo.getName());
        return m;
    }

    private static ZonedDateTime toZonedDateTime(Date date) {
        return date.toInstant().atZone(ZoneOffset.ofHours(-4));
    }

    @Override
    protected final BoxMedia refresh(BoxMedia media) throws IOException {
        return media.copyDataFrom(media); // FIXME
    }

    @Override
    protected final String getAuthor(BoxMedia media, FileMetadata metadata) {
        return boxService.getSharedItem(getSharedLink(media.getId())).getCreatedBy().getName();
    }
}
