package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.ES;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.ID;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.PT_BR;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService.WikidataInfo;
import org.wikimedia.commons.donvip.spacemedia.utils.Emojis;

import com.flickr4java.flickr.photos.Photo;

@Service
public class UsEmbassiesFlickrService extends AbstractOrgFlickrService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsEmbassiesFlickrService.class);

    @Autowired
    public UsEmbassiesFlickrService(FlickrMediaRepository repository,
            @Value("${usembassies.flickr.accounts}") Set<String> flickrAccounts) {
        super(repository, "usembassies.flickr", flickrAccounts);
    }

    @Override
    public String getName() {
        return "U.S. Embassies (Flickr)";
    }

    @Override
    protected boolean checkBlocklist(FlickrMedia media) {
        return false;
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "U.S. Diplomatic missions Flickr files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected String getTakenLocation(FlickrMedia media) {
        String result = super.getTakenLocation(media);
        return isBlank(result)
                ? wikidata.searchFlickrUser(media.getId().repoId())
                        .map(x -> fixCountryForCommons(wikidata.findEnglishLabel(x.country()),
                                media.getDescription()))
                        .orElse(result)
                : result;
    }

    @Override
    protected List<String> getReviewCategories(FlickrMedia media) {
        return getDiplomaticReviewCategories(media);
    }

    @Override
    public Set<String> findLicenceTemplates(FlickrMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.remove("Flickr-public domain mark");
        result.add("PD-USGov-DOS");
        return result;
    }

    @Override
    public String getUiRepoId(String repoId) {
        return wikidata.searchFlickrUser(repoId).map(WikidataInfo::country).map(country -> {
            try {
                return getRegionalIndicatorSymbol(country);
            } catch (IOException | RuntimeException e) {
                GlitchTip.capture(e);
                LOGGER.warn("No Iso3166Alpha2 code found for {}", country);
                return defaultUiRepoId(repoId);
            }
        }).orElseGet(() -> defaultUiRepoId(repoId));
    }

    private String defaultUiRepoId(String repoId) {
        // Remove "usembassy" from ids displayed in UI to make the list fit in screen
        return super.getUiRepoId(repoId).replace("-", "").replace("_", "").replace("usembassy", "").replace("usemb", "")
                .replace("embassy", "").replace("usmissionto", "").replace("usmission", "").replace("usin", "")
                .replace("embaixadaeua", "").replace("publicdiplomacy", "").replace("usconsulategeneral", "")
                .replace("usconsulate", "").replace("consulate", "").replace("uscg", "").replace("consulado", "")
                .replace("uscons", "").replace("congen", "");
    }

    @Override
    public String getUiTooltip(String repoId) {
        return wikidata.searchFlickrUser(repoId).map(x -> wikidata.findEnglishLabel(x.qid())).orElse(repoId);
    }

    @Override
    protected boolean shouldPostOnSocialMedia() {
        return false;
    }

    @Override
    protected String getLanguage(Photo p, String flickrAccount) {
        return switch (flickrAccount) {
        case "40236643@N04" -> ES;
        case "usembassyjakarta" -> ID;
        case "embaixadaeua-brasil" -> PT_BR;
        default -> super.getLanguage(p, flickrAccount);
        };
    }

    @Override
    protected Set<String> getEmojis(FlickrMedia uploadedMedia) {
        Set<String> result = super.getEmojis(uploadedMedia);
        result.add(Emojis.FLAG_USA);
        return result;
    }
}
