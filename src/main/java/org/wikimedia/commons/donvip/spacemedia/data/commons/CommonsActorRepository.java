package org.wikimedia.commons.donvip.spacemedia.data.commons;

import org.springframework.data.repository.CrudRepository;

public interface CommonsActorRepository extends CrudRepository<CommonsActor, Integer> {

    CommonsActor findByName(String name);
}
