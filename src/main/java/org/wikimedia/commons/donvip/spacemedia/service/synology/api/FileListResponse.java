package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FileListResponse extends DsmWebapiResponse<File.FileList> {

    @JsonCreator
    public FileListResponse(@JsonProperty("success") boolean success, @JsonProperty("data") File.FileList data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
