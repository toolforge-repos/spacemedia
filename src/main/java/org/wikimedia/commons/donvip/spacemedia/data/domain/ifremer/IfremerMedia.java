package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.locationtech.jts.geom.Geometry;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaIndexes;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithAuthors;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithGeometry;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithKeywords;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithLicenceUrl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OrderColumn;

@Entity
@MediaIndexes
public class IfremerMedia extends Media implements WithAuthors, WithGeometry, WithKeywords, WithLicenceUrl {

    @JsonIgnore
    private Geometry geometry;

    private URL licenceUrl;

    @Column(columnDefinition = "VARBINARY(512)")
    private List<Integer> speciesAphiaIds = new ArrayList<>();

    @Column
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> keywords = new HashSet<>();

    @OrderColumn
    @ElementCollection(fetch = FetchType.EAGER)
    protected List<Author> authors = new ArrayList<>();

    @Override
    @JsonIgnore
    public Geometry getGeometry() {
        return geometry;
    }

    @Override
    @JsonIgnore
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    @Override
    public Set<String> getKeywords() {
        return keywords;
    }

    @Override
    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public List<Author> getAuthors() {
        return authors;
    }

    @Override
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public URL getLicenceUrl() {
        return licenceUrl;
    }

    @Override
    public void setLicenceUrl(URL licenceUrl) {
        this.licenceUrl = licenceUrl;
    }

    public List<Integer> getSpeciesAphiaIds() {
        return speciesAphiaIds;
    }

    public void setSpeciesAphiaIds(List<Integer> speciesAphiaIds) {
        this.speciesAphiaIds = speciesAphiaIds;
    }

    @Override
    public String getUploadId(FileMetadata fileMetadata) {
        return "Ifremer " + super.getUploadId(fileMetadata);
    }

    public IfremerMedia copyDataFrom(IfremerMedia media) {
        super.copyDataFrom(media);
        this.geometry = media.geometry;
        this.licenceUrl = media.licenceUrl;
        this.speciesAphiaIds = media.speciesAphiaIds;
        return this;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + Objects.hash(geometry, licenceUrl, speciesAphiaIds, authors);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj) || getClass() != obj.getClass())
            return false;
        IfremerMedia other = (IfremerMedia) obj;
        return Objects.equals(geometry, other.geometry) && Objects.equals(licenceUrl, other.licenceUrl)
                && Objects.equals(speciesAphiaIds, other.speciesAphiaIds) && Objects.equals(authors, other.authors);
    }

    @Override
    public String toString() {
        return "IfremerMedia [" + (title != null ? "title=" + title + ", " : "")
                + (publicationDate != null ? "publicationDate=" + publicationDate + ", " : "")
                + (licenceUrl != null ? "licenceUrl=" + licenceUrl + ", " : "")
                + (getId() != null ? "id=" + getId() : "") + "]";
    }
}
