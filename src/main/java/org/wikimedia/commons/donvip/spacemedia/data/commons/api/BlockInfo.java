package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.List;

public record BlockInfo(List<BlockInfo> blockcomponents, String blockid, String blockedby, String blockedbyid,
        String blockreason, String blockedtimestamp, String blockexpiry, String blocknocreate, String blockowntalk,
        String blockedtimestampformatted, String blockexpiryformatted, String blockexpiryrelative) {

}
