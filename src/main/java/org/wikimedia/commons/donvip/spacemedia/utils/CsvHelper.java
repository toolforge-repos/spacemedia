package org.wikimedia.commons.donvip.spacemedia.utils;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toSet;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiFunction;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;

public final class CsvHelper {

    private CsvHelper() {
        // Hide default constructor
    }

    public static Set<String> loadSet(URL url) throws IOException {
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        return mapper.readerFor(String[].class).readValues(url).readAll().stream().map(a -> ((String[]) a)[0])
                .collect(toSet());
    }

    public static Map<String, String> loadMap(URL url) throws IOException {
        return url == null ? Map.of() : doLoadMap(url.openStream(), UTF_8, ',', 0, (_, row) -> row[1]);
    }

    public static Map<String, Map<String, String>> loadMapMap(URL url) throws IOException {
        return url == null ? Map.of() : loadMapMap(url.openStream(), UTF_8, ',', 0);
    }

    public static Map<String, Map<String, String>> loadMapMap(InputStream in, Charset cs, char delimiter, int keyIndex)
            throws IOException {
        return in == null ? Map.of() : doLoadMap(in, cs, delimiter, keyIndex, (header, row) -> {
            Map<String, String> map = new TreeMap<>();
            for (int i = 0; i < header.length; i++) {
                if (i != keyIndex && i < row.length) {
                    map.put(header[i], row[i]);
                }
            }
            return map;
        });
    }

    private static <V> Map<String, V> doLoadMap(InputStream in, Charset cs, char delimiter, int keyIndex,
            BiFunction<String[], String[], V> valueMapper) throws IOException {
        Map<String, V> result = new TreeMap<>();
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        try (InputStreamReader reader = new InputStreamReader(in, cs)) {
            MappingIterator<String[]> it = mapper.readerFor(String[].class)
                    .with(mapper.schemaFor(String[].class).withColumnSeparator(delimiter)).readValues(reader);
            String[] header = it.next();
            while (it.hasNext()) {
                String[] row = it.next();
                result.put(row[keyIndex], valueMapper.apply(header, row));
            }
        }
        return result;
    }

    public static Map<String, String> loadCsvMapping(String filename) {
        return loadCsvMapping(CsvHelper.class, filename);
    }

    public static Map<String, String> loadCsvMapping(Class<?> klass, String filename) {
        try {
            return loadMap(klass.getResource("/mapping/" + filename));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Map<String, Map<String, String>> loadCsvMapMapping(String filename) throws IOException {
        return loadMapMap(CsvHelper.class.getResource("/mapping/" + filename));
    }
}
