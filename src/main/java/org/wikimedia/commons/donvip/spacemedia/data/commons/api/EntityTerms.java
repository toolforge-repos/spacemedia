package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.List;

public record EntityTerms(List<String> label) {

}
