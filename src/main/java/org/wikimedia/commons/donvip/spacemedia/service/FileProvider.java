package org.wikimedia.commons.donvip.spacemedia.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface FileProvider<F> {

    default <T> List<T> getFiles(String app, String share, Function<F, T> mapper, Comparator<T> comparator) {
        return getFiles(app, share, mapper, comparator, Map.of());
    }

    <T> List<T> getFiles(String app, String share, Function<F, T> mapper, Comparator<T> comparator,
            Map<String, String> params);

    default boolean isWantedFile(F file) {
        return getSize(file) > 0 && !getName(file).startsWith("._")
                && ("mp4".equals(getExtension(file)) || isPermittedFileExt(getExtension(file)));
    }

    long getSize(F file);

    String getName(F file);

    String getExtension(F file);

    boolean isPermittedFileExt(String extension);
}
