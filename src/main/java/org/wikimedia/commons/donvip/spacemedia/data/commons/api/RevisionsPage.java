package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RevisionsPage (
    @JsonProperty("pageid")
    int pageId,
    short ns,
    String title,
    List<Revision> revisions) {

}
