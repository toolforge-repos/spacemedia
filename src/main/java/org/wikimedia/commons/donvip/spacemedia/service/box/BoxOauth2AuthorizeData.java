package org.wikimedia.commons.donvip.spacemedia.service.box;

import java.util.List;

import org.apache.hc.core5.http.message.BasicNameValuePair;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public record BoxOauth2AuthorizeData(
        String serviceName,
        String serviceLogoUrl,
        String serviceRedirectUrl,
        String clientId,
        String responseType,
        String redirectUri,
        String scope,
        String folderId,
        String fileId,
        String parentToken,
        String parentServiceId,
        String serviceActionId,
        String state,
        String ic,
        String codeChallenge,
        List<String> scopeDescriptions) {

    public List<BasicNameValuePair> params() {
        return List.of(
            new BasicNameValuePair("client_id", clientId),
            new BasicNameValuePair("response_type", responseType),
            new BasicNameValuePair("redirect_uri", redirectUri),
            new BasicNameValuePair("scope", scope),
            new BasicNameValuePair("folder_id", folderId),
            new BasicNameValuePair("file_id", fileId),
            new BasicNameValuePair("parent_token", parentToken),
            new BasicNameValuePair("parent_service_id", parentServiceId),
            new BasicNameValuePair("service_action_id", serviceActionId),
            new BasicNameValuePair("ic", ic),
            new BasicNameValuePair("doconsent", "doconsent"),
            new BasicNameValuePair("state", state),
            new BasicNameValuePair("code_challenge", codeChallenge)
            );
    }
}
