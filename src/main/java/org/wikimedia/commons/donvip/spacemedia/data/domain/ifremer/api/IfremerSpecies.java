package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerSpecies(int aphiaId, String scientificName, List<IfremerText> vernicularName) {

}
