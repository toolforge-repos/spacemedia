package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal;

import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q11461_SOUND;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q4006_MAP;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q478798_IMAGE;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q98069877_VIDEO;

import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem;

public enum MedihalDocType {
    IMG(Q478798_IMAGE), MAP(Q4006_MAP), SON(Q11461_SOUND), VIDEO(Q98069877_VIDEO);

    private final WikidataItem item;

    private MedihalDocType(WikidataItem item) {
        this.item = item;
    }

    public WikidataItem item() {
        return item;
    }
}
