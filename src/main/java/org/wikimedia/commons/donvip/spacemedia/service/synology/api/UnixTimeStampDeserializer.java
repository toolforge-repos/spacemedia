package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;

public class UnixTimeStampDeserializer extends StdScalarDeserializer<LocalDateTime> {

    private static final long serialVersionUID = 1L;

    protected UnixTimeStampDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        JsonToken currentToken = parser.getCurrentToken();
        if (currentToken == JsonToken.VALUE_NUMBER_INT) {
            int valueAsInt = parser.getValueAsInt();
            return LocalDateTime.ofEpochSecond(valueAsInt, 0, ZoneOffset.UTC);
        }
        return null;
    }
}
