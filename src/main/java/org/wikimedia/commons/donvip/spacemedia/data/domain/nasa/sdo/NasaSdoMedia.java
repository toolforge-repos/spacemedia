package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.sdo;

import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService.normalizeFilename;

import java.util.Objects;
import java.util.function.Supplier;

import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaIndexes;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.SingleFileMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaMediaType;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Entity
@MediaIndexes
public class NasaSdoMedia extends SingleFileMedia {

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false, columnDefinition = "TINYINT default 0")
    private NasaMediaType mediaType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 6)
    private NasaSdoDataType dataType;

    @Embedded
    private NasaSdoKeywords keywords = new NasaSdoKeywords();

    public NasaMediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(NasaMediaType mediaType) {
        this.mediaType = mediaType;
    }

    public NasaSdoDataType getDataType() {
        return dataType;
    }

    public void setDataType(NasaSdoDataType dataType) {
        this.dataType = dataType;
    }

    public NasaSdoKeywords getKeywords() {
        return keywords;
    }

    public void setKeywords(NasaSdoKeywords keywords) {
        this.keywords = keywords;
    }

    @Override
    public boolean isImage() {
        return mediaType == NasaMediaType.image;
    }

    @Override
    public boolean isVideo() {
        return mediaType == NasaMediaType.video;
    }

    @Override
    public String getUploadTitle(FileMetadata fileMetadata, Supplier<String> orgName) {
        return String.format("SDO_%s (%s)", normalizeFilename(title.text()), dataType.getInstrument().name());
    }

    public NasaSdoMedia copyDataFrom(NasaSdoMedia other) {
        super.copyDataFrom(other);
        this.mediaType = other.mediaType;
        this.dataType = other.dataType;
        this.keywords = other.keywords;
        return this;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + Objects.hash(dataType, mediaType, keywords);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj) || getClass() != obj.getClass())
            return false;
        NasaSdoMedia other = (NasaSdoMedia) obj;
        return dataType == other.dataType && mediaType == other.mediaType
                && Objects.equals(keywords, other.keywords);
    }

    @Override
    public String toString() {
        return "NasaSdoMedia [id=" + getId() + ", mediaType=" + mediaType + ", dataType=" + dataType + ']';
    }
}
