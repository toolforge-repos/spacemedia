package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.time.LocalDateTime;

public record FileArchive (
    int id,
    String name,
    int ns,
    String title,
    LocalDateTime timestamp) {

}
