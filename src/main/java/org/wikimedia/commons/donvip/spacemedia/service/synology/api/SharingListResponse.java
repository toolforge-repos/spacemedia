package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SharingListResponse extends DsmWebapiResponse<SharingList> {

    public SharingListResponse(@JsonProperty("success") boolean success, @JsonProperty("data") SharingList data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
