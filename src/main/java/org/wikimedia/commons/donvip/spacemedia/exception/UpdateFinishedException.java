package org.wikimedia.commons.donvip.spacemedia.exception;

public final class UpdateFinishedException extends Exception {
    private static final long serialVersionUID = 1L;

    public UpdateFinishedException(String message) {
        super(message);
    }
}