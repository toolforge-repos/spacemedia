package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public record TimeInformation(
        @JsonDeserialize(using = UnixTimeStampDeserializer.class) @JsonProperty("atime") LocalDateTime lastAccessTime,
        @JsonDeserialize(using = UnixTimeStampDeserializer.class) @JsonProperty("mtime") LocalDateTime lastModificationTime,
        @JsonDeserialize(using = UnixTimeStampDeserializer.class) @JsonProperty("ctime") LocalDateTime lastChangeTime,
        @JsonDeserialize(using = UnixTimeStampDeserializer.class) @JsonProperty("crtime") LocalDateTime creationTime) {

}
