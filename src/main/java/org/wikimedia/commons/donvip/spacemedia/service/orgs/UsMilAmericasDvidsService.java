package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilAmericasDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilAmericasDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmilamericas.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmilamericas.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmilamericas.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmilamericas.dvids.min.year}") int minYear,
            @Value("${usmilamericas.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmilamericas.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in the Americas (DVIDS)";
    }
}
