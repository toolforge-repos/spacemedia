package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.List;

public record DsmWebApiResponseError(int code, List<DsmWebApiResponseError> errors) {

}
