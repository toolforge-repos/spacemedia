package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public record UserInfo(
    long id,
    String name,
    List<String> groups,
    List<String> rights,
    @JsonProperty("ratelimits")
    RateLimits rateLimits,
    List<BlockInfo> blockcomponents,
    String blockid,
    String blockedby,
    String blockedbyid,
    String blockreason,
    String blockedtimestamp,
    String blockexpiry,
    String blocknocreate,
    String blockowntalk,
    String blockedtimestampformatted,
    String blockexpiryformatted,
    String blockexpiryrelative) {

}
