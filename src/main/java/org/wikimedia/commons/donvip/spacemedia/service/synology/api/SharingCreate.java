package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SharingCreate extends PaginatedList<SharedLink> {

    @JsonCreator
    public SharingCreate(@JsonProperty("total") int total, @JsonProperty("offset") int offset,
            @JsonProperty("links") List<SharedLink> links) {
        super(total, offset, links);
    }
}
