package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

public record Tokens(String csrftoken) {

}
