package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrLicense;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;

@Service
public class NoaaFlickrService extends AbstractOrgFlickrService {

    @Autowired
    public NoaaFlickrService(FlickrMediaRepository repository,
            @Value("${noaa.flickr.accounts}") Set<String> flickrAccounts) {
        super(repository, "noaa.flickr", flickrAccounts);
    }

    @Override
    public String getName() {
        return "NOAA (Flickr)";
    }

    @Override
    public Set<String> findCategories(FlickrMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        if (includeHidden) {
            String s = switch(media.getPathAlias()) {
                case "noaasatellites" -> "NOAA Satellites";
                case "51647007@N08" -> "the NOAA Photo Library";
                case "43788330@N05" -> "NOAA Great Lakes Environmental Research Laboratory";
                case "44124469278@N01" -> "NOAA Office of National Marine Sanctuaries";
                case "40322276@N04" -> "NOAA National Ocean Service";
                case "27077560@N05" -> "NOAA Ocean Exploration";
                default -> throw new IllegalArgumentException(media.getPathAlias());
            };
            result.add("Files from " + s + " Flickr stream");
        }
        return result;
    }

    @Override
    protected List<String> getReviewCategories(FlickrMedia media) {
        return List.of("noaasatellites".equals(media.getPathAlias()) ? "Spacemedia files (review needed)" : "Scientimedia files (review needed)");
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "NOAA Flickr files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected String getNonFreeLicenceTemplate(FlickrMedia media) {
        return "PD-USGov-NOAA";
    }

    @Override
    public Set<String> findLicenceTemplates(FlickrMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add(media.getDescription() != null
                && media.getDescription().toLowerCase().contains("credit: nasa/") ? "PD-USGov-NASA"
                        : "PD-USGov-NOAA");
        result.remove(FlickrLicense.Public_Domain_Mark.getWikiTemplate());
        return result;
    }
}
