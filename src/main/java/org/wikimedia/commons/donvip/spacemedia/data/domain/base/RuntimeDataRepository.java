package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import org.springframework.data.repository.CrudRepository;

public interface RuntimeDataRepository extends CrudRepository<RuntimeData, String> {

}
