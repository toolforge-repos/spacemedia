package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record FileProperties(@JsonProperty("real_path") String realPath, @JsonProperty("size") long size,
        @JsonProperty("owner") User owner, @JsonProperty("time") TimeInformation timeInformation,
        @JsonProperty("perm") FilePermission filePermission, @JsonProperty("mount_point_type") String mountPointType,
        @JsonProperty("type") String type) {

}
