package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record Acl(@JsonProperty("append") boolean append, @JsonProperty("del") boolean delete,
        @JsonProperty("exec") boolean execute, @JsonProperty("read") boolean read,
        @JsonProperty("write") boolean write) {

}
