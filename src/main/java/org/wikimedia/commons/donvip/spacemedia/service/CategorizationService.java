package org.wikimedia.commons.donvip.spacemedia.service;

import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.stream;
import static org.apache.commons.lang3.StringUtils.strip;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.IntFunction;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithKeywords;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.CommonsService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.utils.CsvHelper;

@Lazy
@Service
public class CategorizationService {

    static final Pattern COPERNICUS_CREDIT = Pattern.compile(
            ".*Copernicus[ -](?:Sentinel[ -])?dat(?:a|en)(?:/ESA)? [\\(\\[](2\\d{3}(?:[-–/]\\d{2,4})?)[\\)\\]].*",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    private static final Pattern SENTINEL_SAT = Pattern.compile(".*Sentinel[ -]?[1-6].*",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    private static final Logger LOGGER = LoggerFactory.getLogger(CategorizationService.class);

    private Set<String> satellitePicturesCategories;

    private Map<String, String> categoriesStatements;

    @Lazy
    @Autowired
    protected CommonsService commonsService;

    @PostConstruct
    void init() throws IOException {
        getSatellitePicturesCategories();
        getCategoriesStatements();
    }

    public static record Affixes(Iterable<String> values, boolean areOptional) {
    }

    public Set<String> findCategoriesFromTitleAndYear(String title, int year) {
        return findCategoriesFromTitleAndAffixes(title, new Affixes(List.of(year + " ", year + " in "), true),
                new Affixes(List.of(" (" + year + ")", " in " + year), true));
    }

    public Set<String> findCategoriesFromTitleAndAffixes(String title, Affixes prefixes, Affixes suffixes) {
        Set<String> result = new TreeSet<>();
        if (title != null) {
            String[] words = title.strip().split(" ");
            if (words.length >= 2) {
                // Try first words
                findCategoriesFromWords(words.length, n -> copyOfRange(words, 0, words.length - n), prefixes, suffixes)
                        .ifPresent(result::add);
                // Try last words
                findCategoriesFromWords(words.length, n -> copyOfRange(words, n, words.length), prefixes, suffixes)
                        .ifPresent(result::add);
            }
        }
        return result;
    }

    private Optional<String> findCategoriesFromWords(int len, IntFunction<String[]> words, Affixes prefixes,
            Affixes suffixes) {
        for (int n = 0; n <= len - 2; n++) {
            String firstWords = strip(String.join(" ", words.apply(n)), ",-.!?’'\"");
            if (prefixes != null && suffixes != null) {
                for (String prefix : prefixes.values()) {
                    for (String suffix : suffixes.values()) {
                        String firstWordsWithPrefixAndSuffix = prefix + firstWords + suffix;
                        if (commonsService.isValidCategory(firstWordsWithPrefixAndSuffix)) {
                            return Optional.of(firstWordsWithPrefixAndSuffix);
                        }
                    }
                }
            }
            if (prefixes != null && (suffixes == null || suffixes.areOptional())) {
                for (String prefix : prefixes.values()) {
                    String firstWordsWithPrefix = prefix + firstWords;
                    if (commonsService.isValidCategory(firstWordsWithPrefix)) {
                        return Optional.of(firstWordsWithPrefix);
                    }
                }
            }
            if (suffixes != null && (prefixes == null || prefixes.areOptional())) {
                for (String suffix : suffixes.values()) {
                    String firstWordsWithSuffix = firstWords + suffix;
                    if (commonsService.isValidCategory(firstWordsWithSuffix)) {
                        return Optional.of(firstWordsWithSuffix);
                    }
                }
            }
            if ((prefixes == null || prefixes.areOptional()) && (suffixes == null || suffixes.areOptional())) {
                if (commonsService.isValidCategory(firstWords)) {
                    return Optional.of(firstWords);
                }
                String firstWordsWithoutComma = firstWords.replace(",", "");
                if (commonsService.isValidCategory(firstWordsWithoutComma)) {
                    return Optional.of(firstWordsWithoutComma);
                }
            }
        }
        return Optional.empty();
    }

    private Set<String> getSatellitePicturesCategories() {
        if (satellitePicturesCategories == null) {
            try {
                satellitePicturesCategories = CsvHelper.loadSet(getClass().getResource("/lists/satellite.pictures.categories.txt"));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
        return satellitePicturesCategories;
    }

    private Map<String, String> getCategoriesStatements() {
        if (categoriesStatements == null) {
            categoriesStatements = CsvHelper.loadCsvMapping("categories.statements.csv");
        }
        return categoriesStatements;
    }

    public static String extractCopernicusTemplate(String text) {
        if (text != null) {
            Matcher m = COPERNICUS_CREDIT.matcher(text);
            if (m.matches()) {
                return getCopernicusTemplate(m.group(1));
            }
        }
        return null;
    }

    public static String getCopernicusTemplate(String year) {
        return "Attribution-Copernicus |year=" + year;
    }

    public static String getCopernicusTemplate(int year) {
        return getCopernicusTemplate(Integer.toString(year));
    }

    public void findCategoriesStatements(SdcStatements result, Set<String> cats) {
        for (Entry<String, String> e : getCategoriesStatements().entrySet()) {
            if (stream(e.getKey().split(";")).map(r -> Pattern.compile(r, Pattern.CASE_INSENSITIVE))
                    .anyMatch(p -> cats.stream().anyMatch(c -> p.matcher(c).matches()))) {
                LOGGER.info("SDC category match: {}", e);
                for (String statement : e.getValue().split(";")) {
                    String[] kv = statement.split("=");
                    Pair<Object, Map<String, Object>> old = result.put(kv[0], Pair.of(kv[1], null));
                    if (old != null) {
                        LOGGER.warn("Replaced old SDC: {}", old);
                    }
                }
            }
        }
    }

    public boolean isFromSentinelSatellite(Media media) {
        return SENTINEL_SAT.matcher(media.getTitle()).matches()
                || (media.getDescription() != null && SENTINEL_SAT.matcher(media.getDescription()).matches())
                || (media instanceof WithKeywords mkw
                        && mkw.getKeywordStream().anyMatch(kw -> SENTINEL_SAT.matcher(kw).matches()));
    }

    public void findCategoriesForSentinels(Media media, Set<String> result) {
        if (isFromSentinelSatellite(media)) {
            result.add(getCopernicusTemplate(media.getYear().getValue()));
            if (media.containsInTitleOrDescriptionOrKeywords("fires", "burn scars", "wildfire", "forest fire")) {
                result.add("Photos of wildfires by Sentinel satellites");
            } else if (media.containsInTitleOrDescriptionOrKeywords("Phytoplankton", "algal bloom")) {
                result.add("Satellite pictures of algal blooms");
            } else if (media.containsInTitleOrDescriptionOrKeywords("hurricane")) {
                result.add("Satellite pictures of hurricanes");
            } else if (media.containsInTitleOrDescriptionOrKeywords("floods", "flooding")) {
                result.add("Photos of floods by Sentinel satellites");
            }
        }
        for (String num : new String[] { "1", "2", "3", "4", "5", "5P", "6" }) {
            findCategoriesForSentinel(media, "Sentinel-" + num, result);
        }
    }

    private void findCategoriesForSentinel(Media media, String sentinel, Set<String> result) {
        if (media.containsInTitleOrDescriptionOrKeywords(sentinel)) {
            result.addAll(findCategoriesForEarthObservationImage(media, x -> "Photos of " + x + " by " + sentinel,
                    sentinel + " images", true, true, true));
        }
    }

    public Set<String> findCategoriesForEarthObservationImage(Media image, UnaryOperator<String> categorizer,
            String defaultCat, boolean lookIntoTitle, boolean lookIntoDescription, boolean lookIntoKeywords) {
        Set<String> result = new TreeSet<>();
        for (String targetOrSubject : getSatellitePicturesCategories()) {
            if (image.containsInTitleOrDescriptionOrKeywords(targetOrSubject, lookIntoTitle, lookIntoDescription,
                    lookIntoKeywords)) {
                findCategoryForEarthObservationTargetOrSubject(categorizer, targetOrSubject).ifPresent(result::add);
            }
        }
        if (result.isEmpty()) {
            result.add(defaultCat);
        }
        return result;
    }

    public Optional<String> findCategoryForEarthObservationTargetOrSubject(UnaryOperator<String> categorizer,
            String targetOrSubject) {
        String cat = categorizer.apply(targetOrSubject);
        if (commonsService.isValidCategory(cat)) {
            return Optional.of(cat);
        } else {
            String theCat = categorizer.apply("the " + targetOrSubject);
            if (commonsService.isValidCategory(theCat)) {
                return Optional.of(theCat);
            } else {
                String cats = categorizer.apply(targetOrSubject + "s");
                if (commonsService.isValidCategory(cats)) {
                    return Optional.of(cats);
                }
            }
        }
        return Optional.empty();
    }

    public void createNavigationalCategory(String location, LocalDate date, String wikiCode) throws IOException {
        String cat = location + " photographs taken on " + DateTimeFormatter.ISO_LOCAL_DATE.format(date);
        if (!commonsService.existsCategoryPage(cat)) {
            commonsService.createCategory(cat, wikiCode);
        }
    }

    public void createCountryNavigationalCategory(String country, LocalDate date) throws IOException {
        createNavigationalCategory(country, date, "{{World photos}}");
    }

    public void createIssNavigationalCategory(LocalDate date) throws IOException {
        createNavigationalCategory("ISS", date, "{{ISS photographs taken on navbox}}");
    }

    public void createSunNavigationalCategory(LocalDate date) throws IOException {
        createNavigationalCategory("Sun", date, "{{Sun photographs taken on navbox}}");
    }
}
