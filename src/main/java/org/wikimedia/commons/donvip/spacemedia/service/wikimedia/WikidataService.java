package org.wikimedia.commons.donvip.spacemedia.service.wikimedia;

import static java.util.stream.Collectors.toMap;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q17444909_ASTRONOMICAL_OBJECT_TYPE;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q3099911_SCIENTIFIC_INSTUMENT;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q4213_TELESCOPE;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q5107_CONTINENT;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q515_CITY;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q6256_COUNTRY;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q8928_CONSTELLATION;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P1053_RESEARCHERID_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P13072_DVIDS_UNIT_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P1448_OFFICIAL_NAME;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P1705_NATIVE_LABEL;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P213_ISNI_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P214_VIAF_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P268_BNF_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P269_IDREF_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P3267_FLICKR_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P373_COMMONS_CATEGORY;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P4594_ARXIV_AUTHOR_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P496_ORCID_ID;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P528_CATALOG_CODE;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P734_FAMILY_NAME;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataProperty.P850_WORMS_ID;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.wikidata.wdtk.datamodel.interfaces.EntityIdValue;
import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.LabeledDocument;
import org.wikidata.wdtk.datamodel.interfaces.MonolingualTextValue;
import org.wikidata.wdtk.datamodel.interfaces.Statement;
import org.wikidata.wdtk.datamodel.interfaces.StatementDocument;
import org.wikidata.wdtk.datamodel.interfaces.StatementGroup;
import org.wikidata.wdtk.datamodel.interfaces.StringValue;
import org.wikidata.wdtk.wikibaseapi.WbGetEntitiesSearchData;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

@Lazy
@Service
public class WikidataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WikidataService.class);

    private static final WikibaseDataFetcher fetcher = WikibaseDataFetcher.getWikidataDataFetcher();

    private static final SPARQLRepository sparqlRepository = new SPARQLRepository("https://query.wikidata.org/sparql");

    public Optional<Statement> findCommonsStatement(String title, String property) throws IOException {
        return findStatement("commonswiki", title, property);
    }

    public Optional<Statement> findWikipediaStatement(String title, String property) throws IOException {
        return findStatement("enwiki", title, property);
    }

    public Optional<StatementGroup> findCommonsStatementGroup(String title, String property) throws IOException {
        return findStatementGroup("commonswiki", title, property);
    }

    public Optional<StatementGroup> findWikipediaStatementGroup(String title, String property) throws IOException {
        return findStatementGroup("enwiki", title, property);
    }

    Optional<Statement> findStatement(String site, String title, String property) throws IOException {
        return find(site, title, property, ItemDocument::findStatement);
    }

    Optional<StatementGroup> findStatementGroup(String site, String title, String property) throws IOException {
        return find(site, title, property, ItemDocument::findStatementGroup);
    }

    <T> Optional<T> find(String site, String title, String property, BiFunction<ItemDocument, String, T> func)
            throws IOException {
        try {
            if (fetcher.getEntityDocumentByTitle(site, title) instanceof ItemDocument doc) {
                return Optional.ofNullable(func.apply(doc, property));
            }
            return Optional.empty();
        } catch (MediaWikiApiErrorException e) {
            throw new IOException(e);
        }
    }

    public Map<String, String> mapCommonsCategoriesByFamilyName(Collection<Statement> statements) {
        return statements.parallelStream().map(s -> {
            try {
                if (s.getValue() instanceof EntityIdValue val
                        && fetcher.getEntityDocument(val.getId()) instanceof ItemDocument doc) {
                    return doc;
                }
            } catch (MediaWikiApiErrorException | IOException e) {
                LOGGER.error(e.getMessage(), e);
                GlitchTip.capture(e);
            }
            return null;
        }).filter(Objects::nonNull).collect(toMap(this::findFamilyName, this::findCommonsCategory));
    }

    public String findCommonsCategory(ItemDocument doc) {
        Statement statement = doc.findStatement(P373_COMMONS_CATEGORY.toString());
        return statement != null && statement.getValue() instanceof StringValue sv ? sv.getString() : null;
    }

    public String findFamilyName(ItemDocument doc) {
        Statement statement = doc.findStatement(P734_FAMILY_NAME.toString());
        if (statement != null) {
            return findEnglishLabel(statement);
        }
        StatementGroup statementGroup = doc.findStatementGroup(P734_FAMILY_NAME.toString());
        if (statementGroup != null) {
            StatementGroup best = statementGroup.getBestStatements();
            return findEnglishLabel((best != null ? best : statementGroup).getStatements().get(0));
        }
        String enLabel = findEnglishLabel(doc);
        if (enLabel != null) {
            String[] words = enLabel.split(" ");
            if (words.length > 1) {
                return words[words.length - 1];
            }
            return enLabel;
        }
        return null;
    }

    public String findEnglishLabel(Statement st) {
        try {
            if (st.getValue() instanceof EntityIdValue eiv
                    && fetcher.getEntityDocument(eiv.getId()) instanceof ItemDocument doc) {
                return findEnglishLabel(doc);
            }
        } catch (MediaWikiApiErrorException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            GlitchTip.capture(e);
        }
        return null;
    }

    public String findEnglishLabel(LabeledDocument doc) {
        MonolingualTextValue enLabel = doc.getLabels().get("en");
        return enLabel != null ? enLabel.getText() : null;
    }

    @Cacheable("englishLabelByQid")
    public String findEnglishLabel(String qid) {
        try {
            return fetcher.getEntityDocument(qid) instanceof LabeledDocument doc ? findEnglishLabel(doc) : qid;
        } catch (MediaWikiApiErrorException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            GlitchTip.capture(e);
            return qid;
        }
    }

    @Cacheable("wikidataSearchedEntities")
    public List<WbSearchEntitiesResult> searchEntities(String search) throws IOException {
        try {
            WbGetEntitiesSearchData properties = new WbGetEntitiesSearchData();
            properties.search = search;
            properties.language = "en";
            properties.type = "item";
            properties.limit = 500L;
            return fetcher.searchEntities(properties);
        } catch (MediaWikiApiErrorException e) {
            throw new IOException(e);
        }
    }

    @Cacheable("wikidataContinents")
    public Optional<WikidataInfo> searchContinent(String name) {
        return searchCommonsCategory(WikidataService::getContinentQuery, name);
    }

    @Cacheable("wikidataCountries")
    public Optional<WikidataInfo> searchCountry(String name) {
        return searchCommonsCategory(WikidataService::getCountryQueryByEnglishLabel, name)
                .or(() -> searchCommonsCategory(WikidataService::getCountryQueryByProperty, name));
    }

    @Cacheable("wikidataCities")
    public Optional<WikidataInfo> searchCity(String name) {
        return searchCommonsCategory(WikidataService::getCityQueryByEnglishLabel, name)
                .or(() -> searchCommonsCategory(WikidataService::getCityQueryByProperty, name));
    }

    @Cacheable("wikidataAstronomicalObjects")
    public Optional<WikidataInfo> searchAstronomicalObject(String catalogName) {
        return searchCommonsCategory(WikidataService::getAstronomicalObjectQuery, catalogName);
    }

    @Cacheable("wikidataConstellations")
    public Optional<WikidataInfo> searchConstellation(String officialName) {
        return searchCommonsCategory(WikidataService::getConstellationQuery, officialName);
    }

    @Cacheable("wikidataTelescopes")
    public Optional<WikidataInfo> searchTelescope(String name) {
        return searchCommonsCategory(WikidataService::getTelescopeQuery, name);
    }

    @Cacheable("wikidataInstruments")
    public Optional<WikidataInfo> searchInstrument(String name) {
        return searchCommonsCategory(WikidataService::getInstrumentQuery, name);
    }

    @Cacheable("wikidataFlickrUsers")
    public Optional<WikidataInfo> searchFlickrUser(String id) {
        return searchCommonsCategory(WikidataService::getFlickrUserQuery, id);
    }

    @Cacheable("wikidataMaritimeSpecies")
    public Optional<WikidataInfo> searchMaritimeSpecies(Integer aphiaId) {
        return searchCommonsCategory(WikidataService::getMaritimeSpeciesQuery, aphiaId.toString());
    }

    @Cacheable("wikidataDvidsUnits")
    public Optional<WikidataInfo> searchDvidsUnit(String dvidsUnitId) {
        return searchCommonsCategory(WikidataService::getDvidsUnitQuery, dvidsUnitId);
    }

    @Cacheable("wikidataBnfIds")
    public Optional<WikidataInfo> searchBnfId(String id) {
        return searchCommonsCategory(WikidataService::getBnfIdQuery, id);
    }

    @Cacheable("wikidataIdrefIds")
    public Optional<WikidataInfo> searchIdrefId(String id) {
        return searchCommonsCategory(WikidataService::getIdrefIdQuery, id);
    }

    @Cacheable("wikidataIsniIds")
    public Optional<WikidataInfo> searchIsniId(String id) {
        return searchCommonsCategory(WikidataService::getIsniIdQuery, id);
    }

    @Cacheable("wikidataOrcidIds")
    public Optional<WikidataInfo> searchOrcidId(String id) {
        return searchCommonsCategory(WikidataService::getOrcidIdQuery, id);
    }

    @Cacheable("wikidataViafIds")
    public Optional<WikidataInfo> searchViafId(String id) {
        return searchCommonsCategory(WikidataService::getViafIdQuery, id);
    }

    @Cacheable("wikidataArxivAuthorIds")
    public Optional<WikidataInfo> searchArxivAuthorId(String id) {
        return searchCommonsCategory(WikidataService::getArxivAuthorIdQuery, id);
    }

    @Cacheable("wikidataResearcherIds")
    public Optional<WikidataInfo> searchResearcherId(String id) {
        return searchCommonsCategory(WikidataService::getResearcherIdQuery, id);
    }

    @Cacheable("wikidataIso3166Alpha2")
    public String findIso3166Alpha2(String countryQid) throws IOException {
        try {
            if (fetcher.getEntityDocument(countryQid) instanceof StatementDocument doc
                    && doc.findStatementGroup(
                            WikidataProperty.P297_ISO_3166_1_ALPHA_2_CODE.toString()) instanceof StatementGroup stg
                    && !stg.isEmpty() && stg.getStatements().get(0).getValue() instanceof StringValue sv) {
                return sv.getString();
            }
            throw new IllegalArgumentException(countryQid);
        } catch (MediaWikiApiErrorException e) {
            throw new IOException(e);
        }
    }

    public record WikidataInfo(String qid, String commonsCat, String country, String nature) {

        public WikidataInfo(String qid, Binding commonsCat, Binding country, Binding nature) {
            this(qid, str(commonsCat), str(country), str(nature));
        }
    }

    private static String str(Binding b) {
        return b != null ? b.getValue().stringValue().replace("http://www.wikidata.org/entity/", "") : null;
    }

    private static Optional<WikidataInfo> searchCommonsCategory(UnaryOperator<String> query, String name) {
        if (name != null) {
            try (RepositoryConnection sparqlConnection = sparqlRepository.getConnection();
                    TupleQueryResult result = sparqlConnection
                            .prepareTupleQuery(QueryLanguage.SPARQL, query.apply(name)).evaluate()) {
                List<BindingSet> results = result.stream().distinct().toList();
                BindingSet goodResult = null;
                if (results.size() != 1) {
                    LOGGER.info("Not exactly 1 item when looking for {} in Wikidata: {}", name, results);
                    List<BindingSet> exactMatches = results.stream()
                            .filter(x -> StringUtils.equalsIgnoreCase(name, str(x.getBinding("commonsCat")))).toList();
                    if (exactMatches.size() == 1
                            || exactMatches.stream().map(x -> str(x.getBinding("item"))).distinct().count() == 1) {
                        goodResult = exactMatches.get(0);
                    } else if (results.stream().map(x -> str(x.getBinding("item"))).distinct().count() == 1) {
                        goodResult = results.get(0);
                    }
                } else {
                    goodResult = results.get(0);
                }
                if (goodResult != null) {
                    String itemId = goodResult.getBinding("item").getValue().stringValue()
                            .replace("http://www.wikidata.org/entity/", "");
                    return Optional.of(new WikidataInfo(itemId, goodResult.getBinding("commonsCat"),
                            goodResult.getBinding("country"), goodResult.getBinding("nature")));
                }
            }
        }
        return Optional.empty();
    }

    private static String getContinentQuery(String name) {
        return getNamedObjectQuery(Q5107_CONTINENT, P1705_NATIVE_LABEL, name, "en");
    }

    private static String getCountryQueryByProperty(String name) {
        return getNamedObjectQuery(Q6256_COUNTRY, P1705_NATIVE_LABEL, name, "en");
    }

    private static String getCityQueryByProperty(String name) {
        return getNamedObjectQuery(Q515_CITY, P1705_NATIVE_LABEL, name, "en");
    }

    private static String getCountryQueryByEnglishLabel(String name) {
        return getLabeledObjectQuery(Q6256_COUNTRY, name, "en");
    }

    private static String getCityQueryByEnglishLabel(String name) {
        return getLabeledObjectQuery(Q515_CITY, name, "en");
    }

    private static String getAstronomicalObjectQuery(String catalogName) {
        return getNamedObjectQuery(Q17444909_ASTRONOMICAL_OBJECT_TYPE, P528_CATALOG_CODE, catalogName);
    }

    private static String getConstellationQuery(String officialName) {
        return getNamedObjectQuery(Q8928_CONSTELLATION, P1448_OFFICIAL_NAME, officialName, "la");
    }

    private static String getTelescopeQuery(String name) {
        return getNamedObjectQuery(Q4213_TELESCOPE, P1705_NATIVE_LABEL, name, "en");
    }

    private static String getInstrumentQuery(String name) {
        return getNamedObjectQuery(Q3099911_SCIENTIFIC_INSTUMENT, P1705_NATIVE_LABEL, name, "en");
    }

    private static String getFlickrUserQuery(String id) {
        return getSimpleQuery(P3267_FLICKR_ID, id);
    }

    private static String getMaritimeSpeciesQuery(String aphiaId) {
        return getSimpleQuery(P850_WORMS_ID, aphiaId);
    }

    private static String getDvidsUnitQuery(String dvidsUnitId) {
        return getSimpleQuery(P13072_DVIDS_UNIT_ID, dvidsUnitId);
    }

    private static String getBnfIdQuery(String bnfId) {
        return getSimpleQuery(P268_BNF_ID, bnfId);
    }

    private static String getIdrefIdQuery(String idrefId) {
        return getSimpleQuery(P269_IDREF_ID, idrefId);
    }

    private static String getOrcidIdQuery(String orcidId) {
        return getSimpleQuery(P496_ORCID_ID, orcidId);
    }

    private static String getIsniIdQuery(String isniId) {
        return getSimpleQuery(P213_ISNI_ID, isniId);
    }

    private static String getViafIdQuery(String viafId) {
        return getSimpleQuery(P214_VIAF_ID, viafId);
    }

    private static String getResearcherIdQuery(String researcherId) {
        return getSimpleQuery(P1053_RESEARCHERID_ID, researcherId);
    }

    private static String getArxivAuthorIdQuery(String arxivId) {
        return getSimpleQuery(P4594_ARXIV_AUTHOR_ID, arxivId);
    }

    private static String getNamedObjectQuery(WikidataItem natureQid, WikidataProperty namingProperty, String name) {
        return """
                SELECT DISTINCT ?item ?itemLabel ?commonsCat ?country ?nature
                WHERE
                {
                  ?item wdt:P31/wdt:P31/wdt:P279* wd:$natureQid;
                        wdt:$namingProperty "$name".
                  OPTIONAL { ?item wdt:P373 ?commonsCat }.
                  OPTIONAL { ?item wdt:P17 ?country }.
                  OPTIONAL { ?item wdt:P31 ?nature }.
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
                }
                LIMIT 4
                """.replace("$name", name.replace("\"", "")).replace("$namingProperty", namingProperty.toString())
                .replace("$natureQid", natureQid.toString());
    }

    private static String getNamedObjectQuery(WikidataItem natureQid, WikidataProperty namingProperty, String name,
            String lang) {
        return """
                SELECT DISTINCT ?item ?itemLabel ?commonsCat ?country ?nature
                WHERE
                {
                  ?item wdt:P31/wdt:P279* wd:$natureQid;
                { ?item wdt:$namingProperty ?name. } UNION { ?item wdt:P1813 ?name. } FILTER(?name = "$name"@$lang).
                  OPTIONAL { ?item wdt:P373 ?commonsCat }.
                  OPTIONAL { ?item wdt:P17 ?country }.
                  OPTIONAL { ?item wdt:P31 ?nature }.
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
                }
                LIMIT 4
                """.replace("$lang", lang).replace("$name", name.replace("\"", ""))
                .replace("$namingProperty", namingProperty.toString()).replace("$natureQid", natureQid.toString());
    }

    private static String getSimpleQuery(WikidataProperty textProperty, String value) {
        return """
                SELECT DISTINCT ?item ?itemLabel ?commonsCat ?country ?nature
                WHERE
                {
                  ?item wdt:$textProperty "$value".
                  OPTIONAL { ?item wdt:P373 ?commonsCat }.
                  OPTIONAL { ?item wdt:P17 ?country }.
                  OPTIONAL { ?item wdt:P31 ?nature }.
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
                }
                LIMIT 4
                """.replace("$value", value.replace("\"", "")).replace("$textProperty", textProperty.toString());
    }

    private static String getLabeledObjectQuery(WikidataItem natureQid, String name, String lang) {
        return """
                SELECT distinct ?item ?itemLabel ?commonsCat ?country ?nature
                WHERE
                {
                  ?item wdt:P31/wdt:P279* wd:$natureQid .
                  ?item ?label "$name"@$lang.
                  OPTIONAL { ?item wdt:P373 ?commonsCat }.
                  OPTIONAL { ?item wdt:P17 ?country }.
                  OPTIONAL { ?item wdt:P31 ?nature }.
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "$lang". }
                }
                LIMIT 10
                """.replace("$lang", lang).replace("$name", name.replace("\"", "")).replace("$natureQid",
                natureQid.toString());
    }
}
