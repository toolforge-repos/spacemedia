package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.photojournal;

import java.util.regex.Pattern;

import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaIndexes;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity
@MediaIndexes
public class NasaPhotojournalMedia extends Media {

    private static final Pattern FIGURE = Pattern.compile("PIA\\d+_fig[^\\.]+\\..+", Pattern.CASE_INSENSITIVE);

    @Column(name = "target", nullable = true, length = 50)
    private String target;

    @Column(name = "mission", nullable = true, length = 128)
    private String mission;

    @Column(name = "spacecraft", nullable = true, length = 128)
    private String spacecraft;

    @Column(name = "instrument", nullable = true, length = 128)
    private String instrument;

    @Column(name = "producer", nullable = true, length = 64)
    private String producer;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getSpacecraft() {
        return spacecraft;
    }

    public void setSpacecraft(String spacecraft) {
        this.spacecraft = spacecraft;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public String getUploadId(FileMetadata fileMetadata) {
        String file = fileMetadata.getAssetUrl().getFile();
        if (file.contains("/")) {
            file = file.substring(file.lastIndexOf('/') + 1);
        }
        return FIGURE.matcher(file).matches()
                ? file.substring(0, file.indexOf('.')).replace("_fullres", "").replace("_FIG", "_fig")
                : super.getUploadId(fileMetadata);
    }

    @Override
    public String toString() {
        return "NasaPhotojournalMedia [id=" + getId() + ", "
                + (target != null ? "target=" + target + ", " : "")
                + (mission != null ? "mission=" + mission + ", " : "")
                + (spacecraft != null ? "spacecraft=" + spacecraft + ", " : "")
                + (instrument != null ? "instrument=" + instrument + ", " : "")
                + (producer != null ? "producer=" + producer : "") + ']';
    }

    public NasaPhotojournalMedia copyDataFrom(NasaPhotojournalMedia mediaFromApi) {
        super.copyDataFrom(mediaFromApi);
        setTarget(mediaFromApi.getTarget());
        setMission(mediaFromApi.getMission());
        setSpacecraft(mediaFromApi.getSpacecraft());
        setInstrument(mediaFromApi.getInstrument());
        setProducer(mediaFromApi.getProducer());
        return this;
    }
}
