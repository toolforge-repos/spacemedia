package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Locale.FRENCH;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.executeRequestStream;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.httpClientBuilder;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpGet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newHttpPost;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.IntStream;

import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaDimensions;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.IfremerMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.IfremerMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerAuthor;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerDocument;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerFile;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerPerson;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerSearchRequest;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerSearchResponse;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerSearchResponseEntry;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerSpecies;
import org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api.IfremerText;
import org.wikimedia.commons.donvip.spacemedia.exception.UploadException;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService.WikidataInfo;

@Service
public class IfremerService extends AbstractOrgService<IfremerMedia> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IfremerService.class);

    private static final String BASE_API_URL = "https://image.ifremer.fr/api/full-search-response";

    @Autowired
    public IfremerService(IfremerMediaRepository repository) {
        super(repository, "ifremer", Set.of("oceanotheque"));
    }

    @Override
    public String getName() {
        return "Ifremer";
    }

    @Override
    protected boolean checkBlocklist(IfremerMedia media) {
        return false;
    }

    @Override
    public void updateMedia(String[] args) throws IOException, UploadException {
        int count = 0;
        LocalDateTime start = startUpdateMedia();
        List<IfremerMedia> uploadedMedia = new ArrayList<>();
        LocalDate doNotFetchEarlierThan = getRuntimeData().getDoNotFetchEarlierThan();
        LocalDate doNotFetchLaterThan = getRuntimeData().getDoNotFetchLaterThan();
        SortedSet<Year> years = (doNotFetchEarlierThan != null || doNotFetchLaterThan != null) ? IntStream
                .range(ofNullable(doNotFetchEarlierThan).orElseGet(() -> LocalDate.of(1885, 1, 1)).getYear(),
                        ofNullable(doNotFetchLaterThan).orElseGet(LocalDate::now).getYear() + 1)
                .mapToObj(Year::of).collect(toCollection(TreeSet::new)) : new TreeSet<>();
        LOGGER.info("Updating Ifremer media for years {}", years);
        try (CloseableHttpClient httpclient = httpClientBuilder().build()) {
            boolean done = false;
            int page = 1;
            while (!done) {
                LOGGER.info("Requesting page {}", page);
                IfremerSearchRequest request = years.isEmpty()
                        ? new IfremerSearchRequest(page++)
                        : new IfremerSearchRequest(years, page++);
                try (InputStream in = executeRequestStream(
                        newHttpPost(BASE_API_URL,
                                new StringEntity(jackson.writeValueAsString(request), ContentType.APPLICATION_JSON)),
                        httpclient, null)) {
                    IfremerSearchResponse results = jackson.readValue(in, IfremerSearchResponse.class);
                    for (IfremerSearchResponseEntry entry : results.responseEntries()) {
                        try {
                            updateImage(newId(entry.urlSection() + "/" + entry.docId()), uploadedMedia);
                        } catch (IOException | UploadException | RuntimeException e) {
                            LOGGER.error("Failed to process {}", entry, e);
                        }
                        ongoingUpdateMedia(start, count++);
                    }
                    done = results.entriesCount() <= results.pagination().page() * results.pagination().size();
                }
            }
        }
        endUpdateMedia(count, uploadedMedia);
    }

    private IfremerMedia updateImage(CompositeMediaId id, List<IfremerMedia> uploadedMedia)
            throws IOException, UploadException {
        boolean save = false;
        IfremerMedia media;
        Optional<IfremerMedia> imageInDb = repository.findById(id);
        if (imageInDb.isPresent()) {
            media = imageInDb.get();
        } else {
            media = fetchMedia(id);
            save = true;
        }
        if (doCommonUpdate(media)) {
            save = true;
        }
        if (shouldUploadAuto(media, false)) {
            media = saveMedia(upload(save ? saveMedia(media) : media, true, false).getLeft());
            if (shouldPostOnSocialMedia()) {
                uploadedMedia.add(media);
            }
            save = false;
        }
        return save ? saveMedia(media) : media;
    }

    private IfremerMedia fetchMedia(CompositeMediaId id) throws IOException {
        try (CloseableHttpClient httpclient = httpClientBuilder().build();
                InputStream in = executeRequestStream(
                        newHttpGet("https://image.ifremer.fr/api/find-by-id/" + id.mediaId().split("/")[1]),
                        httpclient, null)) {
            return mapMedia(jackson.readValue(in, IfremerDocument.class));
        }
    }

    private static CompositeMediaId newId(String id) {
        return new CompositeMediaId("oceanotheque", id);
    }

    IfremerMedia mapMedia(IfremerDocument doc) {
        IfremerMedia media = new IfremerMedia();
        media.setId(newId(doc.urlSection() + "/" + doc.docId()));
        media.setTitle(doc.title().enOtherwiseFr().unescapeHtml4());
        media.setPublicationDateTime(doc.date());
        if (isNotEmpty(doc.geoExtendList())) {
            media.setGeometry(new GeometryFactory().createPolygon(doc.geoExtendList().get(0).toCoordinates()));
        }
        List<IfremerAuthor> authors = ofNullable(doc.authors()).orElseGet(() -> List.of()).stream().filter(Objects::nonNull).toList();
        if (isNotEmpty(authors)) {
            media.setCredits(authors.stream().map(IfremerAuthor::toString).collect(joining(", ")));
            for (IfremerAuthor author : authors) {
                Author.Builder b = new Author.Builder().fullName(author.toString());
                IfremerPerson p = author.person();
                if (p != null) {
                    b.firstName(p.firstName()).lastName(p.lastName()).orcidId(p.orcid());
                }
                media.addAuthor(b.build());
            }
        } else {
            media.setCredits("Ifremer");
        }
        media.setDescription(doc.description().frOtherwiseEn());
        media.setThumbnailUrl(doc.thumbnailUrl());
        media.setLicenceUrl(doc.licenceUrl());
        ofNullable(doc.species()).map(x -> x.stream().map(IfremerSpecies::aphiaId).toList())
                .ifPresent(media::setSpeciesAphiaIds);
        ofNullable(doc.keywordsControlled())
                .map(x -> x.stream().map(IfremerText::enOtherwiseFr).map(LocalizedText::text).collect(toSet()))
                .ifPresent(media::setKeywords);
        for (IfremerFile file : doc.files()) {
            try {
                addMetadata(media, ofNullable(file.urlHd()).orElse(file.urlMd()), fm -> {
                    fm.setSize(file.size());
                    fm.setOriginalFileName(file.fileName());
                    fm.setDescription(file.label().frOtherwiseEn());
                    fm.setMediaDimensions(new MediaDimensions(file.width(), file.height()));
                });
            } catch (RuntimeException e) {
                LOGGER.error("Failed to add Ifremer file {}", file);
                GlitchTip.capture(e);
            }
        }
        return media;
    }

    @Override
    public URL getSourceUrl(IfremerMedia media, FileMetadata metadata, String ext) {
        return newURL("https://image.ifremer.fr/data/" + media.getIdUsedInOrg());
    }

    @Override
    protected Optional<String> getOtherFields(IfremerMedia media) {
        StringBuilder sb = new StringBuilder();
        if (media.getGeometry() != null) {
            Geometry envelope = media.getGeometry().getEnvelope();
            if (envelope instanceof Polygon poly) {
                Coordinate[] bb = poly.getCoordinates();
                addOtherField(sb, "Bounding box",
                        "{{Map/bbox|longitude=" + bb[0].x + "/" + bb[2].x + "|latitude=" + bb[0].y + "/" + bb[2].y
                                + "}}");
            }
        }
        addOtherField(sb, "Keyword", media.getKeywords());
        return Optional.of(sb.toString());
    }

    @Override
    protected IfremerMedia refresh(IfremerMedia media) throws IOException {
        return media.copyDataFrom(fetchMedia(media.getId()));
    }

    @Override
    public Set<String> findCategories(IfremerMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        for (int aphiaId : media.getSpeciesAphiaIds()) {
            wikidata.searchMaritimeSpecies(aphiaId).map(WikidataInfo::commonsCat).ifPresent(result::add);
        }
        if (metadata.isVideo()) {
            result.add("Videos from Océanothèque");
        }
        return result;
    }

    @Override
    protected SdcStatements getStatements(IfremerMedia media, FileMetadata metadata) {
        SdcStatements result = super.getStatements(media, metadata);
        if (media.getCredits() != null && media.getCredits().toLowerCase(FRENCH).contains("ifremer")) {
            result.creator("Q1657069");
        }
        if (isNotEmpty(media.getSpeciesAphiaIds())) {
            wikidata.searchMaritimeSpecies(media.getSpeciesAphiaIds().get(0)).map(WikidataInfo::qid)
                    .ifPresent(result::depicts);
        }
        return result;
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Files from Océanothèque uploaded by OptimusPrimeBot";
    }

    @Override
    protected List<String> getReviewCategories(IfremerMedia media) {
        return List.of("Scientimedia files (review needed)");
    }

    @Override
    protected Class<IfremerMedia> getMediaClass() {
        return IfremerMedia.class;
    }

    @Override
    protected boolean shouldPostOnSocialMedia() {
        return false;
    }
}
