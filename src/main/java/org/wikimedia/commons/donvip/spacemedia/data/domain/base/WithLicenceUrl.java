package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import java.net.URL;

public interface WithLicenceUrl {

    public URL getLicenceUrl();

    public void setLicenceUrl(URL licenceUrl);
}
