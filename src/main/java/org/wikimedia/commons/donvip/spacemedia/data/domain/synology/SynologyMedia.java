package org.wikimedia.commons.donvip.spacemedia.data.domain.synology;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaIndexes;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.SingleFileMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.WithAuthors;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OrderColumn;

@Entity
@MediaIndexes
public class SynologyMedia extends SingleFileMedia implements WithAuthors {

    @OrderColumn
    @ElementCollection(fetch = FetchType.EAGER)
    protected List<Author> authors = new ArrayList<>();

    public SynologyMedia() {

    }

    public SynologyMedia(String app, String share, String id) {
        setId(new CompositeMediaId(app + '/' + share, id));
    }

    @Override
    public List<Author> getAuthors() {
        return authors;
    }

    @Override
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public SynologyMedia copyDataFrom(SynologyMedia other) {
        super.copyDataFrom(other);
        return this;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + Objects.hash(authors);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj) || getClass() != obj.getClass())
            return false;
        SynologyMedia other = (SynologyMedia) obj;
        return Objects.equals(authors, other.authors);
    }

    @Override
    public String toString() {
        return "SynologyMedia [" + (title != null ? "title=" + title + ", " : "")
                + (getId() != null ? "id=" + getId() : "") + "]";
    }
}
