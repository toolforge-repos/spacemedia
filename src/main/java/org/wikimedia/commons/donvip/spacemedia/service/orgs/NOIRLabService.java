package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Locale.ENGLISH;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.replace;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.djangoplicity.DjangoplicityMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.djangoplicity.DjangoplicityMediaRepository;

@Service
public class NOIRLabService extends AbstractOrgDjangoplicityService {

    private static final String BASE_URL = "https://noirlab.edu";

    private static final String PUBLIC_PATH = "/public/";

    private static final String BASE_PUBLIC_URL = BASE_URL + PUBLIC_PATH;

    private static final String IMAGES_PATH = "images/";
    private static final String VIDEOS_PATH = "videos/";

    private static final Pattern PATTERN_LOCALIZED_URL = Pattern
            .compile(BASE_PUBLIC_URL + "([a-z]+/)" + IMAGES_PATH + ".*");

    private static final DateTimeFormatter[] DATE_FORMATTERS = {
            ofPattern("MMMM d, yyyy", ENGLISH),
            ofPattern("MMM. d, yyyy", ENGLISH)};

    private static final DateTimeFormatter[] DATE_TIME_FORMATTERS = {
            ofPattern("MMM d, yyyy, h:m\u202Fa", ENGLISH),
            ofPattern("MMM. d, yyyy, h:m\u202Fa", ENGLISH),
            ofPattern("MMMM d, yyyy, h:m\u202Fa", ENGLISH),
            ofPattern("MMMM d, yyyy, h\u202Fa", ENGLISH),
            ofPattern("MMM d, yyyy, B", ENGLISH),
            ofPattern("MMM. d, yyyy, B", ENGLISH),
            ofPattern("MMMM d, yyyy, B", ENGLISH),
            ofPattern("MMM d, yyyy, h\u202Fa", ENGLISH),
            ofPattern("MMM. d, yyyy, h\u202Fa", ENGLISH) };

    @Autowired
    public NOIRLabService(DjangoplicityMediaRepository repository, @Value("${noirlab.search.link}") String searchLink) {
        super(repository, "noirlab", searchLink);
    }

    @Override
    protected LocalDateTime parseDateTime(String dateTimeText) {
        // https://www.unicode.org/cldr/cldr-aux/charts/33/by_type/date_&_time.gregorian.html#72c7f54616968b69
        String text = dateTimeText.replace("Sept.", "Sep.")
            .replace("a.m.", "AM").replace("p.m.", "PM")   // standard English AM/PM marker
                .replace(" AM", "\u202FAM").replace(" PM", "\u202FPM"); // https://bugs.openjdk.org/browse/JDK-8284840 /
                                                                        // https://unicode-org.atlassian.net/browse/CLDR-14032
        try {
            return super.parseDateTime(text);
        } catch (DateTimeParseException e0) {
            for (DateTimeFormatter dtf : DATE_TIME_FORMATTERS) {
                try {
                    return LocalDateTime.parse(text, dtf);
                } catch (DateTimeParseException e1) {
                    // Ignore
                }
            }
            for (DateTimeFormatter df : DATE_FORMATTERS) {
                try {
                    return LocalDate.parse(text, df).atStartOfDay();
                } catch (DateTimeParseException e2) {
                    // Ignore
                }
            }
            throw new DateTimeParseException("No date/time format matching: " + text, text, 0);
        }
    }

    @Override
    public String getName() {
        return "NOIRLab";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Spacemedia NOIRLab files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected List<String> getReviewCategories(DjangoplicityMedia media) {
        return List.of("NOIRLab images (review needed)");
    }

    @Override
    public Set<String> findLicenceTemplates(DjangoplicityMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add("NOIRLab");
        return result;
    }

    @Override
    public URL getSourceUrl(DjangoplicityMedia media, FileMetadata metadata, String ext) {
        return newURL(BASE_PUBLIC_URL + imageOrVideo(ext, IMAGES_PATH, VIDEOS_PATH) + media.getIdUsedInOrg());
    }

    @Override
    protected Matcher getLocalizedUrlMatcher(String imgUrlLink) {
        return PATTERN_LOCALIZED_URL.matcher(imgUrlLink);
    }

    @Override
    protected String getCopyrightLink() {
        return "/public/copyright/";
    }

    @Override
    protected String mainDivClass() {
        return "col-md-9 left-column";
    }

    @Override
    public Set<String> findCategories(DjangoplicityMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        if (result.contains("Gemini Observatory")) {
            boolean north = media.containsInTitleOrDescriptionOrKeywords("Gemini North");
            boolean south = media.containsInTitleOrDescriptionOrKeywords("Gemini South");
            if (north && !south) {
                replace(result, "Gemini Observatory", "Gemini North Observatory");
            } else if (south && !north) {
                replace(result, "Gemini Observatory", "Gemini South Observatory");
            }
        }
        return result;
    }
}
