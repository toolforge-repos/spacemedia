package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public record File(@JsonProperty("path") String path, @JsonProperty("name") String name,
        @JsonProperty("isdir") boolean directory,
        @JsonProperty("additional") FileProperties properties,
        @JsonProperty("children") List<File> children) {

    public static class FileList extends PaginatedList<File> {

        public FileList(@JsonProperty("total") int total, @JsonProperty("offset") int offset,
                @JsonProperty("files") List<File> files) {
            super(total, offset, files);
        }
    }
}
