package org.wikimedia.commons.donvip.spacemedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.RuntimeDataRepository;
import org.wikimedia.commons.donvip.spacemedia.data.hashes.HashAssociation;
import org.wikimedia.commons.donvip.spacemedia.service.HashService;

@RestController
public class HashController {

    @Lazy
    @Autowired
    private RuntimeDataRepository runtimeRepo;

    @Lazy
    @Autowired
    private HashService hashService;

    @GetMapping("/hashLastTimestamp")
    public String hashLastTimestamp() {
        return runtimeRepo.findById("commons").orElseThrow().getLastTimestamp();
    }

    @PutMapping("/hashAssociation")
    public HashAssociation putHashAssociation(@RequestBody HashAssociation association) {
        return hashService.save(association);
    }
}
