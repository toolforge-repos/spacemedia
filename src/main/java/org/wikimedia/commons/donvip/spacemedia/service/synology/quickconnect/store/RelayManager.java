package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store;

public class RelayManager {

    private static RelayManager sInstance;

    public static RelayManager getDefault() {
        return sInstance;
    }

    public static void setDefault(RelayManager relayHandler) {
        sInstance = relayHandler;
    }

    private final RelayStore relayJar;

    public RelayManager() {
        relayJar = new RelayStore();
    }

    public RelayCookie get(String serverID, int port) {
        return relayJar.get(serverID, port);
    }

    public void put(String serverID, int port, RelayCookie cookie) {
        relayJar.add(serverID, port, cookie);
    }

    public void remove(String serverID, int port) {
        relayJar.remove(serverID, port);
    }

    public void removeAll() {
        relayJar.removeAll();
    }
}
