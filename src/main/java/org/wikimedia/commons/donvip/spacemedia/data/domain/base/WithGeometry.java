package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import org.locationtech.jts.geom.Geometry;

public interface WithGeometry {

    Geometry getGeometry();

    void setGeometry(Geometry geometry);
}
