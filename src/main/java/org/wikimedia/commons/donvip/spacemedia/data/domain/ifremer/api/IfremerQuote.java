package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerQuote(List<IfremerAuthor> authors, int year, String titles, String publisher, List<URL> links) {

}
