package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.Map;

public record RevisionsQuery(Map<Integer, RevisionsPage> pages) {

}
