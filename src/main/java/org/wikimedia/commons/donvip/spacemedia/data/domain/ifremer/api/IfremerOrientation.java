package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

public enum IfremerOrientation {
    LANDSCAPE, PORTRAIT
}
