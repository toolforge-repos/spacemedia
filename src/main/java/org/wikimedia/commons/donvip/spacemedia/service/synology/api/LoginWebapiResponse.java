package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginWebapiResponse extends DsmWebapiResponse<LoginInformation> {

    @JsonCreator
    public LoginWebapiResponse(@JsonProperty("success") boolean success, @JsonProperty("data") LoginInformation data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        super(success, data, error);
    }
}
