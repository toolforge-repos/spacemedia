package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store;

import java.util.HashMap;
import java.util.Map;

public class RelayStore {

    private final Map<String, RelayCookie> allCookies = new HashMap<>();

    public void add(String serverID, int port, RelayCookie relayCookie) {
        allCookies.put(createKey(serverID, port), relayCookie);
    }

    public RelayCookie get(String serverID, int port) {
        return allCookies.get(createKey(serverID, port));
    }

    public void remove(String serverID, int port) {
        allCookies.remove(createKey(serverID, port));
    }

    public void removeAll() {
        allCookies.clear();
    }

    private String createKey(String serverID, int port) {
        return String.format("%s_%s", serverID, port);
    }
}
