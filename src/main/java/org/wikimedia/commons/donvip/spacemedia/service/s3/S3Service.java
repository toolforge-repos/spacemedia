package org.wikimedia.commons.donvip.spacemedia.service.s3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import software.amazon.awssdk.auth.credentials.AnonymousCredentialsProvider;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

@Lazy
@Service
public class S3Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3Service.class);

    public static final Set<String> MEDIA_EXT = Set.of("wav", "mp3", "flac", "midi", "bmp", "jpg", "jpeg", "tif", "tiff",
            "pdf", "png", "webp", "xcf", "gif", "svg", "mp4", "webm", "ogv", "mpeg", "avi", "mov");

    private static S3Client client(Region region) {
        return S3Client.builder().region(region).credentialsProvider(AnonymousCredentialsProvider.create()).build();
    }

    public <T> List<T> getFiles(Region region, String bucket, String prefix, Set<String> allowedExtensions,
            Function<S3Object, T> mapper, Predicate<T> predicate, Comparator<T> comparator) {
        LOGGER.info("Looking for S3 files in {} with prefix {} ...", bucket, prefix);
        S3Client s3 = client(region);
        List<T> result = new ArrayList<>();
        for (ListObjectsV2Response response : StringUtils.isBlank(prefix)
                ? s3.listObjectsV2Paginator(b -> b.bucket(bucket))
                : s3.listObjectsV2Paginator(b -> b.bucket(bucket).prefix(prefix))) {
            result.addAll(response.contents().stream()
                    .filter(x -> allowedExtensions.contains(x.key().substring(x.key().lastIndexOf('.') + 1)))
                    .map(mapper).toList());
        }
        LOGGER.debug("S3 files before filtering: {}", result);
        return result.stream().filter(predicate).sorted(comparator.reversed()).toList();
    }

    public byte[] getObject(Region region, String bucket, String key) {
        return client(region).getObject(b -> b.bucket(bucket).key(key), ResponseTransformer.toBytes()).asByteArray();
    }
}
