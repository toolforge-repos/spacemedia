package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.Map;

public record DsmWebapiRequest(String api, int version, String path, String method, Map<String, String> parameters) {

}
