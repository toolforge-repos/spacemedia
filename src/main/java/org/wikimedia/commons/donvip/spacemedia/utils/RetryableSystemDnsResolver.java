package org.wikimedia.commons.donvip.spacemedia.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.hc.client5.http.SystemDefaultDnsResolver;
import org.springframework.retry.support.RetryTemplate;

/**
 * DNS resolver that uses the default OS implementation for resolving host names
 * that retries resolution every second, up to an hour, in case of temporary
 * name resolution failures that occur frequently on WMCS infrastructure.
 */
public class RetryableSystemDnsResolver extends SystemDefaultDnsResolver {

    @Override
    public InetAddress[] resolve(final String host) throws UnknownHostException {
        return RetryTemplate.builder().maxAttempts(3600).retryOn(UnknownHostException.class).build()
                .execute(_ -> super.resolve(host));
    }
}
