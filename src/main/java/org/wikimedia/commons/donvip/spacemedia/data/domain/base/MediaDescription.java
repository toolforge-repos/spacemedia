package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

public interface MediaDescription {

    LocalizedText getDescription();

    void setDescription(LocalizedText description);
}
