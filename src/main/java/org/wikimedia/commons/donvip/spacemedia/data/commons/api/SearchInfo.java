package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record SearchInfo (
    @JsonProperty("totalhits")
    int totalHits) {
}
