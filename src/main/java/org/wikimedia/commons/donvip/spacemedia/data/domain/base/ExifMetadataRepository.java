package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import org.springframework.data.repository.CrudRepository;

public interface ExifMetadataRepository extends CrudRepository<ExifMetadata, Long> {

}
