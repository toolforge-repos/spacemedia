package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record SharingLink(
        @JsonProperty("date_available") String dateAvailable, @JsonProperty("date_expired") String dateExpired,
        @JsonProperty("has_password") boolean password, @JsonProperty("id") String id,
        @JsonProperty("isFolder") boolean directory, @JsonProperty("link_owner") String owner,
        @JsonProperty("name") String name, @JsonProperty("path") String path, @JsonProperty("status") String status,
        @JsonProperty("url") String url
) {

}
