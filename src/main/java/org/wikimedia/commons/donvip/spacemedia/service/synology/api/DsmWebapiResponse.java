package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DsmWebapiResponse<T> {

    private final boolean success;
    private final T data;
    private final DsmWebApiResponseError error;

    @JsonCreator
    public DsmWebapiResponse(@JsonProperty("success") boolean success, @JsonProperty("data") T data,
            @JsonProperty("error") DsmWebApiResponseError error) {
        this.success = success;
        this.data = data;
        this.error = error;
    }

    public boolean success() {
        return success;
    }

    public T data() {
        return data;
    }

    public DsmWebApiResponseError error() {
        return error;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [success=" + success + ", " + (data != null ? "data=" + data + ", " : "")
                + (error != null ? "error=" + error : "") + "]";
    }
}
