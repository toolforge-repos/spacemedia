package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ApiError (

    String code,

    String info,

    String errorclass,

    String docref,

    AbuseFilter abusefilter,

    @JsonProperty("*")
    String star) {

}
