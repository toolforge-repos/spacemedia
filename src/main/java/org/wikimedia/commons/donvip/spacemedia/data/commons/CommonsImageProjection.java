package org.wikimedia.commons.donvip.spacemedia.data.commons;

/**
 * Projection of {@link CommonsImage} with only needed information, especially
 * NOT the serialized metadata which causes problems to the MariaDB JDBC driver.
 *
 * Must be a DTO class: the interface-style projection requests all fields from
 * the database.
 */
public record CommonsImageProjection (
        String name, String sha1, String timestamp, int width, int height, long actor,
        CommonsMediaType mediaType, CommonsMajorMime majorMime, String minorMime) {

}
