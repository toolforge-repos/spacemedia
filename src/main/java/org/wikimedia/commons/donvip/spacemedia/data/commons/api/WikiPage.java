package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({ "index" })
public record WikiPage (

    @JsonProperty("pageid")
    long pageId,

    int ns,

    String title,

    long size,

    @JsonProperty("wordcount")
    int wordCount,

    String snippet,

    ZonedDateTime timestamp,

    @JsonProperty("contentmodel")
    String contentModel,

    @JsonProperty("pagelanguage")
    String pageLanguage,

    @JsonProperty("pagelanguagehtmlcode")
    String pageLanguageHtmlCode,

    @JsonProperty("pagelanguagedir")
    String pageLanguageDir,

    ZonedDateTime touched,

    @JsonProperty("lastrevid")
    long lastRevId,

    int length,

    @JsonProperty("entityterms")
    EntityTerms entityTerms,

    @JsonProperty("fullurl")
    URL fullUrl,

    @JsonProperty("editurl")
    URL editUrl,

    @JsonProperty("canonicalurl")
    URL canonicalUrl,

    @JsonProperty("imagerepository")
    String imageRepository,

    @JsonProperty("imageinfo")
    List<ImageInfo> imageInfo) {

}
