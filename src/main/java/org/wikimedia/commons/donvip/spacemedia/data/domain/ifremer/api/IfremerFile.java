package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.net.URL;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerFile(int id, IfremerText label, long size, String checksum, String fileName, URL urlHd, URL urlLd,
        URL urlMd, int visibilityHd, int visibilityLd, int visibilityMd, boolean openAccessHd, boolean openAccessLd,
        boolean openAccessMd, int width, int height, IfremerOrientation orientation, String thumbnailName,
        URL thumbnailUrl, boolean authorizedHd, boolean authorizedLd, boolean authorizedMd) {

}
