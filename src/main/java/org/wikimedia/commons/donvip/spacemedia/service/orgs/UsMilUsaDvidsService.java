package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilUsaDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilUsaDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmilusa.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmilusa.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmilusa.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmilusa.dvids.min.year}") int minYear,
            @Value("${usmilusa.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmilusa.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in the USA (DVIDS)";
    }
}
