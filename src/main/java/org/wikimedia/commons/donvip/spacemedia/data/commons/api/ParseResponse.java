package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ParseResponse (

    @JsonProperty("title")
    String title,

    @JsonProperty("pageid")
    long pageId,

    @JsonProperty("headhtml")
    String headHtml,

    @JsonProperty("text")
    String text,

    @JsonProperty("categorieshtml")
    String categoriesHtml,

    @JsonProperty("displaytitle")
    String displayTitle,

    @JsonProperty("subtitle")
    String subtitle) {

}
