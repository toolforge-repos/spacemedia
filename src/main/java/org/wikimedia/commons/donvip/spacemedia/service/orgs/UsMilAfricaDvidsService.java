package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilAfricaDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilAfricaDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmilafrica.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmilafrica.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmilafrica.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmilafrica.dvids.min.year}") int minYear,
            @Value("${usmilafrica.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmilafrica.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in Africa (DVIDS)";
    }
}
