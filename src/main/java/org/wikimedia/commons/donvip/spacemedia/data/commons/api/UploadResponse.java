package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record UploadResponse (

    long offset,

    String result,

    String filename,

    String filekey,

    @JsonProperty("imageinfo")
    ImageInfo imageInfo) {

}
