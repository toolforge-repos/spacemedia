package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.re2j.Pattern;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record MedihalDoc(String docid, String citationRef_s, String citationFull_s, boolean openAccess_bool,
        List<String> inra_publicVise_local_s, List<String> inra_lieu_local_s, URL licence_s, String scale_s,
        String historicalPeriod_s, String baseMap_s, List<String> acm2012_s, List<String> acm_s,
        List<String> domainAllCode_s, List<String> level0_domain_s, List<String> domain_s, List<String> level1_domain_s,
        List<String> level2_domain_s, List<String> fr_domainAllCodeLabel_fs, List<String> en_domainAllCodeLabel_fs,
        String primaryDomain_s, List<String> fr_title_s, List<String> en_title_s, List<String> title_s,
        List<String> fr_keyword_s, List<String> en_keyword_s, List<String> keyword_s, List<String> abstract_s,
        List<String> en_abstract_s, List<String> fr_abstract_s,

        String label_s, String label_bibtex, String label_endnote, String label_coins, String label_xml,

        List<String> authIdFormPerson_s, List<Integer> authIdForm_i, List<Integer> authIdPerson_i,
        List<String> authLastName_s, List<String> authFirstName_s, List<String> authFullName_s,
        List<String> authLastNameFirstName_s, List<String> authIdLastNameFirstName_fs,
        List<String> authFullNameIdFormPerson_fs, List<String> authAlphaLastNameFirstNameId_fs,
        List<String> authIdFullName_fs, List<String> authFullNameId_fs, List<String> authQuality_s,
        List<String> authEmailDomain_s, List<Integer> authIdHal_i, List<String> authIdHal_s,
        List<String> authArxivIdExt_s, List<String> authResearcherIdIdExt_s, List<String> authORCIDIdExt_s,
        List<String> authVIAFIdExt_s, List<String> authISNIIdExt_s,
        @JsonProperty("authGoogle ScholarIdExt_s") List<String> authGoogleScholarIdExt_s,
        List<String> authIDPInraIdExt_s, List<String> authIDHALIdExt_s, List<String> authIdRefIdExt_s,
        List<String> authFullNameFormIDPersonIDIDHal_fs, List<String> authFullNamePersonIDIDHal_fs,
        List<String> authIdHalFullName_fs, List<String> authFullNameIdHal_fs, List<String> authINRAE_EncryptedIdExt_s,
        List<String> authAlphaLastNameFirstNameIdHal_fs, List<String> authLastNameFirstNameIdHalPersonid_fs,
        List<Integer> authOrganismId_i, List<String> authOrganism_s, List<Integer> authStructId_i,

        List<String> structRorIdExt_s, List<URL> structRorIdExtUrl_s, List<String> structIdrefIdExt_s,
        List<URL> structIdrefIdExtUrl_s, List<String> structValid_s, List<String> structType_s,
        List<String> structCountry_s, List<String> structAddress_s, List<String> structName_s,
        List<String> structAcronym_s, List<String> structName_fs, List<String> structIdName_fs,
        List<String> structNameId_fs, List<Integer> structId_i, List<String> structCode_s,
        List<URL> structRnsrIdExtUrl_s, List<String> structRnsrIdExt_s,
        List<URL> structIsniIdExtUrl_s, List<String> structIsniIdExt_s,
        List<String> structIdunivlorraineIdExtUrl_s, List<String> structIdunivlorraineIdExt_s,
        List<URL> structWikidataIdExtUrl_s, List<String> structWikidataIdExt_s,

        int contributorId_i, String contributorFullName_s, String contributorIdFullName_fs,
        String contributorFullNameId_fs, String country_s, String halId_s, URL uri_s, int version_i, int status_i,
        String instance_s, int sid_i, String submitType_s, String docType_s, String oldDocType_s, String docSubType_s,
        int thumbId_i, boolean selfArchiving_bool, List<String> collaboration_s, List<String> funding_s,
        String comment_s, String city_s, String location, String coordinates_s, List<String> seeAlso_s,
        boolean inPress_bool, String imageType_s, int length_i,

        ZonedDateTime modifiedDate_tdate, String modifiedDate_s, int modifiedDateY_i, int modifiedDateM_i,
        int modifiedDateD_i, ZonedDateTime submittedDate_tdate, String submittedDate_s, int submittedDateY_i,
        int submittedDateM_i, int submittedDateD_i, ZonedDateTime releasedDate_tdate, String releasedDate_s,
        int releasedDateY_i, int releasedDateM_i, int releasedDateD_i, ZonedDateTime producedDate_tdate,
        String producedDate_s, int producedDateY_i, int producedDateM_i, int producedDateD_i,
        ZonedDateTime publicationDate_tdate, String publicationDate_s, int publicationDateY_i, int publicationDateM_i,
        int publicationDateD_i, ZonedDateTime writingDate_tdate, String writingDate_s, int writingDateY_i,
        int writingDateM_i, int writingDateD_i, ZonedDateTime dateLastIndexed_tdate,
        ZonedDateTime conferenceStartDate_tdate, String conferenceStartDate_s, int conferenceStartDateY_i,

        List<Integer> owners_i,

        List<Integer> collId_i, List<String> collName_s, List<String> collCode_s, List<String> collCategory_s,
        List<String> collIdName_fs, List<String> collNameId_fs, List<String> collCodeName_fs,
        List<String> collCategoryCodeName_fs, List<String> collNameCode_fs, List<String> collIsParentOfColl_fs,
        List<String> collIsParentOfCategoryColl_fs, List<String> collIsChildOfColl_fs,

        URL fileMain_s, List<URL> files_s, List<URL> fileAnnexes_s, List<String> fileType_s, long _version_,
        List<String> language_s, String credit_s, String source_s, List<URL> related_s, String conferenceTitle_s,
        String proceedings_s, String audience_s, String invitedCommunication_s, String peerReviewing_s,
        String popularLevel_s, List<String> subTitle_s, List<String> fr_subTitle_s, List<String> en_subTitle_s,
        List<String> localReference_s, String doiId_s, List<String> classification_s, List<String> jel_s,
        String pubmedId_s, List<String> tet_title_s, List<String> halIdSameAs_s, List<String> prodinraId_s,
        List<String> publisherLink_s, String page_s, List<String> laas_renatech_s, List<String> okinaId_s,
        List<String> issue_s, List<String> fileAnnexesFigure_s, String description_s, String fileMainAnnex_s) {

    private static final Pattern IGNORED_PROPERTIES = Pattern.compile(
            "(?:anrProject|europeanProject|inra|auth|campusaar|journal|deptStruct|instStruct|labStruct|rgrp|rteam|structHas|structIs[Child|Parent]|structPrimary|[a-z]{2}_).*");

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (!IGNORED_PROPERTIES.matches(name)) {
            IllegalArgumentException iae = new IllegalArgumentException(name + " => " + value);
            GlitchTip.capture(iae);
            throw iae;
        }
    }

    public List<Author> authors() {
        List<Author> result = new ArrayList<>();
        if (authFullName_s() != null) {
            int nAuthors = authFullName_s().size();
            for (int i = 0; i < nAuthors; i++) {
                Author.Builder b = new Author.Builder().firstName(authFirstName_s().get(i))
                        .lastName(authLastName_s().get(i)).fullName(authFullName_s().get(i));
                addValue(authISNIIdExt_s(), i, b::isniId);
                addValue(authVIAFIdExt_s(), i, b::viafId);
                addValue(authIdRefIdExt_s(), i, b::idrefId);
                addValue(authORCIDIdExt_s(), i, b::orcidId);
                addValue(authArxivIdExt_s(), i, b::arxivId);
                addValue(authResearcherIdIdExt_s(), i, b::researcherId);
                result.add(b.build());
            }
        }
        if (structName_s() != null) {
            int nStructs = structName_s().size();
            for (int i = 0; i < nStructs; i++) {
                Author.Builder b = new Author.Builder().fullName(structName_s().get(i));
                addValue(structIsniIdExt_s(), i, b::isniId);
                addValue(structIdrefIdExt_s(), i, b::idrefId);
                addValue(structWikidataIdExt_s(), i, b::wikidataId);
                result.add(b.build());
            }
        }
        return result;
    }

    private static void addValue(List<String> list, int i, Consumer<String> consumer) {
        if (list != null && i < list.size()) {
            consumer.accept(list.get(i));
        }
    }
}
