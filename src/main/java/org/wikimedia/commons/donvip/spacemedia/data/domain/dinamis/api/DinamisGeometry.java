package org.wikimedia.commons.donvip.spacemedia.data.domain.dinamis.api;

import java.util.List;

public record DinamisGeometry(String type, List<List<List<Double>>> coordinates) {

}
