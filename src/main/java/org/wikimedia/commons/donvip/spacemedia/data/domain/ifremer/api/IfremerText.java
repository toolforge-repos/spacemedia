package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import static java.util.Locale.FRENCH;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.regex.Pattern;

import org.apache.commons.text.StringEscapeUtils;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerText(String en, String fr) {

    private static final Pattern FRENCH_CHARS = Pattern.compile("(?iu)[çèéêîôœû]");

    public LocalizedText enOtherwiseFr() {
        return isBlank(en) ? new LocalizedText("fr", fr) : new LocalizedText(lang(en, "en"), en);
    }

    public LocalizedText frOtherwiseEn() {
        return isBlank(fr) ? new LocalizedText(lang(en, "en"), en) : new LocalizedText("fr", fr);
    }

    private static String lang(String text, String def) {
        return text != null && FRENCH_CHARS.matcher(StringEscapeUtils.unescapeHtml4(text).toLowerCase(FRENCH)).find()
                ? "fr"
                : def;
    }
}
