package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

import java.net.URL;
import java.util.List;

public record NasaCollection(NasaMetadata metadata, URL href, List<NasaLink> links, String version,
        List<NasaItem> items) {
}
