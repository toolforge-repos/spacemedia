package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Locale.ENGLISH;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.synology.SynologyMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.synology.SynologyMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.synology.SynologyService;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.File;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.SharingLink;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.utils.MediaUtils;
import org.wikimedia.commons.donvip.spacemedia.utils.Utils;

import okhttp3.ResponseBody;

public abstract class AbstractOrgSynologyService extends AbstractFileProviderService<SynologyMedia> {

    private static final Path FILES = Path.of("files");

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOrgSynologyService.class);

    @Lazy
    @Autowired
    protected SynologyService synology;

    protected AbstractOrgSynologyService(SynologyMediaRepository repository, String id, Set<String> repoIds) {
        super(repository, id, repoIds);
    }

    @Override
    protected final Class<SynologyMedia> getMediaClass() {
        return SynologyMedia.class;
    }

    protected final String login(String app) throws IOException {
        String idup = getId().toUpperCase(ENGLISH);
        String sid = synology.login(app, env.getProperty(idup + "_LOGIN", String.class),
                env.getProperty(idup + "_PASSWORD", String.class), "FileStation");
        LOGGER.info("Logged in to {}: {}", app, sid);
        return sid;
    }

    private Optional<SharingLink> getSharingLink(String app, String share, String sid) throws IOException {
        return synology.listSharingLinks(app, sid).stream().filter(s -> share.equals(s.path())).findFirst();
    }

    private String getSharingLinkUrl(String app, String share, String sid) throws IOException {
        return getSharingLink(app, share, sid).map(SharingLink::url)
                .orElseGet(() -> synology.createSharingLink(app, share, sid)).replace("http://", "https://");
    }

    protected final List<SynologyMedia> getFiles(String app, String share, Comparator<SynologyMedia> comparing,
            Map<String, String> params) throws IOException {
        String sharingLink = getSharingLinkUrl(app, share, params.get("_sid"));
        LOGGER.info("Getting files from {} using sharing link {}", share, sharingLink);
        return synology.getFiles(app, share, x -> toMedia(app, share, x, sharingLink), comparing, params);
    }

    protected final void logout(String app, String sid) throws IOException {
        synology.logout(app, "FileStation", sid);
        LOGGER.info("Logged out from {} ({})", app, sid);
    }

    private SynologyMedia toMedia(String app, String share, File file, String sharingLink) {
        SynologyMedia media = new SynologyMedia(app, share, file.name());
        media.setPublication(file.properties().timeInformation().creationTime());
        media.setTitle(new LocalizedText(getLanguageCode(), file.name()));
        Optional<SynologyMedia> opt = repository.findById(media.getId());
        if (opt.isEmpty() || !opt.get().hasMetadata()) {
            FileMetadata fm = addMetadata(media, sharingLink + "&filepath=" + file.path(), m -> fillMetadata(m, file));
            LOGGER.debug("File metadata added: {}", fm);
        } else {
            media.addMetadata(opt.get().getUniqueMetadata());
        }
        return media;
    }

    @Override
    protected Path getLocalMediaPath(SynologyMedia media) throws IOException {
        URL url = media.getUniqueMetadata().getAssetUrl();
        String uri = url.toExternalForm();
        String filepath = uri.substring(uri.lastIndexOf('=') + 1).replace("%20", " ");
        filepath = filepath.substring(filepath.indexOf('/', 1) + 1);
        return Utils.extractFile(getZipPath(getApp(media.getId()), url), filepath, FILES
                .resolve(filepath.substring(filepath.lastIndexOf('/') + 1)));
    }

    private Path getZipPath(String app, URL url) throws IOException {
        String uri = url.toExternalForm().split("&")[0];
        String id = uri.substring(uri.lastIndexOf('/') + 1);
        String zipFileName = id + ".zip";
        Path path = Files.createDirectories(FILES).resolve(zipFileName);
        if (!path.toFile().exists()) {
            downloadSharedDirectory(app, id, path);
        }
        return path;
    }

    private void downloadSharedDirectory(String app, String id, Path zipOutputPath) throws IOException {
        LOGGER.info("Downloading {} shared folder {} to {}", app, id, zipOutputPath);
        String sid = login(app);
        try {
            ResponseBody body = synology.download(app, synology.getSharingInfo(app, id, sid).path(), sid);
            if (MediaUtils.hasEnoughDiskSpace(id, body::contentLength)) {
                try (InputStream in = body.byteStream()) {
                    long nBytes = Files.copy(in, zipOutputPath);
                    LOGGER.info("Wrote {} bytes to {}", nBytes, zipOutputPath);
                }
            }
        } finally {
            logout(app, sid);
        }
    }

    @Override
    protected final void cleanup(String app, String share) {
        try {
            String sid = login(app);
            try {
                getSharingLink(app, share, sid).map(SharingLink::id).ifPresent(id -> {
                    try {
                        Path path = FILES.resolve(id + ".zip");
                        if (path.toFile().exists()) {
                            LOGGER.info("Deleting {}", path);
                            Files.delete(path);
                        }
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
            } finally {
                logout(app, sid);
            }
        } catch (IOException | UncheckedIOException e) {
            LOGGER.error("Cleanup error: {}", e.getMessage());
            GlitchTip.capture(e);
        }
    }

    private static FileMetadata fillMetadata(FileMetadata m, File file) {
        m.setExtension(file.properties().type().toLowerCase(ENGLISH));
        m.setSize(file.properties().size());
        m.setOriginalFileName(file.name());
        return m;
    }

    protected String getLanguageCode() {
        return "en";
    }

    @Override
    protected SynologyMedia refresh(SynologyMedia media) throws IOException {
        return media.copyDataFrom(media); // FIXME
    }
}
