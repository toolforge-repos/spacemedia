package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

public record SharedLink(String path, String url, String id, String qrcode, int error) {

}
