package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Set;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaRepository;

public interface IfremerMediaRepository extends MediaRepository<IfremerMedia> {

    @Retention(RetentionPolicy.RUNTIME)
    @CacheEvict(allEntries = true, cacheNames = { "ifremerCount", "ifremerCountRepo", "ifremerCountIgnored",
            "ifremerCountIgnoredRepo", "ifremerCountMissing", "ifremerCountMissingRepo",
            "ifremerCountMissingImagesRepo", "ifremerCountMissingVideosRepo", "ifremerCountMissingDocumentsRepo",
            "ifremerCountUploaded", "ifremerCountUploadedRepo", "ifremerCountPhashNotNull",
            "ifremerCountPhashNotNullRepo" })
    @interface CacheEvictIfremerAll {

    }

    @Override
    @CacheEvictIfremerAll
    default void evictCaches() {

    }

    // COUNT

    @Override
    @Cacheable("ifremerCount")
    long count();

    @Override
    @Cacheable("ifremerCountRepo")
    long count(Set<String> repos);

    @Override
    @Cacheable("ifremerCountIgnored")
    long countByMetadata_IgnoredTrue();

    @Override
    @Cacheable("ifremerCountIgnoredRepo")
    long countByMetadata_IgnoredTrue(Set<String> repos);

    @Override
    @Cacheable("ifremerCountMissing")
    long countMissingInCommons();

    @Override
    @Cacheable("ifremerCountMissingRepo")
    long countMissingInCommons(Set<String> repos);

    @Override
    @Cacheable("ifremerCountMissingImagesRepo")
    long countMissingImagesInCommons(Set<String> repos);

    @Override
    @Cacheable("ifremerCountMissingVideosRepo")
    long countMissingVideosInCommons(Set<String> repos);

    @Override
    @Cacheable("ifremerCountMissingDocumentsRepo")
    long countMissingDocumentsInCommons(Set<String> repos);

    @Override
    @Cacheable("ifremerCountUploaded")
    long countUploadedToCommons();

    @Override
    @Cacheable("ifremerCountUploadedRepo")
    long countUploadedToCommons(Set<String> repos);

    @Override
    @Cacheable("ifremerCountPhashNotNull")
    long countByMetadata_PhashNotNull();

    @Override
    @Cacheable("ifremerCountPhashNotNullRepo")
    long countByMetadata_PhashNotNull(Set<String> repos);

    // SAVE

    @Override
    @CacheEvictIfremerAll
    <S extends IfremerMedia> S save(S entity);

    @Override
    @CacheEvictIfremerAll
    <S extends IfremerMedia> List<S> saveAll(Iterable<S> entities);

    // DELETE

    @Override
    @CacheEvictIfremerAll
    void deleteById(CompositeMediaId id);

    @Override
    @CacheEvictIfremerAll
    void delete(IfremerMedia entity);

    @Override
    @CacheEvictIfremerAll
    void deleteAll(Iterable<? extends IfremerMedia> entities);

    @Override
    @CacheEvictIfremerAll
    void deleteAll();
}
