package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.PingPongJson;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.ServerInfoJson;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.ServerInfoJson.InterfaceJson;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.ServerInfoJson.InterfaceJson.Ipv6Json;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.ServerInfoJson.ServerJson;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.json.ServerInfoJson.ServiceJson;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store.RelayCookie;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store.RelayManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

class QuickConnectResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuickConnectResolver.class);

    private final OkHttpClient defaultClient;
    private final HttpUrl requestUrl;
    private final Gson gson;
    private final String zone;

    private final List<HttpUrl> availableServers = new ArrayList<>();
    private final List<HttpUrl> checkedServers = new ArrayList<>();

    public QuickConnectResolver(HttpUrl requestUrl, String zone) {
        this.defaultClient = new OkHttpClient.Builder().hostnameVerifier((_, _) -> true).build(); // NOSONAR
        this.requestUrl = requestUrl;
        this.gson = new Gson();
        this.zone = zone;
    }

    public RelayCookie resolve(String serverID, int port, String id, boolean gofile) throws IOException {
        LOGGER.info("Resolving {}:{} - {}", serverID, port, id);

        if (!Util.isQuickConnectId(serverID)) {
            throw new IllegalArgumentException("serverID isn't a Quick Connect ID");
        }

        RelayManager relayManager = RelayManager.getDefault();
        RelayCookie cookie = relayManager.get(serverID, port);
        if (cookie == null) {
            cookie = new RelayCookie.Builder().serverID(serverID).id(id).gofile(gofile).build();
            relayManager.put(serverID, port, cookie);
        }

        HttpUrl serverUrl = HttpUrl.parse("https://" + zone + ".quickconnect.to/Serv.php");
        ServerInfoJson infoJson = getServerInfo(serverUrl, serverID, id, gofile);

        // ping DSM directly
        HttpUrl resolvedUrl = pingDSM(infoJson);
        if (resolvedUrl != null) {
            cookie = cookie.newBuilder().resolvedUrl(resolvedUrl).build();
            return cookie;
        }

        // ping DSM through tunnel
        resolvedUrl = pingTunnel(infoJson.service());
        if (resolvedUrl != null) {
            cookie = cookie.newBuilder().resolvedUrl(resolvedUrl).build();
            return cookie;
        }

        // request tunnel
        infoJson = requestTunnel(infoJson, serverID, id, gofile);
        if (infoJson != null && infoJson.service() != null) {
            resolvedUrl = requestUrl.newBuilder().host(infoJson.service().relay_ip())
                    .port(infoJson.service().relay_port()).build();
            cookie = cookie.newBuilder().resolvedUrl(resolvedUrl).build();
            return cookie;
        }

        throw new IOException("No valid url resolved");
    }

    public ServerInfoJson getServerInfo(HttpUrl serverUrl, String serverID, String id, boolean gofile)
            throws IOException {
        availableServers.remove(serverUrl);
        checkedServers.add(serverUrl);
        OkHttpClient client = defaultClient.newBuilder().connectTimeout(30, SECONDS).readTimeout(30, SECONDS).build();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("version", 1);
        jsonObject.addProperty("command", "get_server_info");
        jsonObject.addProperty("stop_when_error", "false");
        jsonObject.addProperty("stop_when_success", "false");
        jsonObject.addProperty("id", id);
        jsonObject.addProperty("serverID", serverID);
        jsonObject.addProperty("is_gofile", gofile);
        RequestBody requestBody = RequestBody.create(gson.toJson(jsonObject), MediaType.parse("text/plain"));
        String response = client.newCall(new Request.Builder().url(serverUrl).post(requestBody).build()).execute()
                .body().string();
        LOGGER.debug("POST {} {} => {}", serverUrl, requestBody, response);
        ServerInfoJson serverInfoJson = gson.fromJson(response, ServerInfoJson.class);

        if (serverInfoJson != null) {
            if (serverInfoJson.server() != null) {
                return serverInfoJson;
            }

            if (!availableServers.isEmpty()) {
                return getServerInfo(availableServers.remove(0), serverID, id, gofile);
            }
        }

        throw new IOException("No server info found at " + serverUrl + " / " + serverID);
    }

    public HttpUrl pingDSM(ServerInfoJson infoJson) {
        final OkHttpClient client = defaultClient.newBuilder().connectTimeout(5, SECONDS).readTimeout(5, SECONDS)
                .build();

        final ServerJson serverJson = requireNonNull(infoJson.server());
        final ServiceJson serviceJson = requireNonNull(infoJson.service());

        int port = serviceJson.port();
        int externalPort = serviceJson.ext_port();

        // internal address(192.168.x.x/10.x.x.x)
        ExecutorService executor = Executors.newFixedThreadPool(10);
        CompletionService<String> internalService = new ExecutorCompletionService<>(executor);
        List<InterfaceJson> ifaces = serverJson.interfaces();
        AtomicInteger internalCount = new AtomicInteger(0);
        if (ifaces != null) {
            for (final InterfaceJson iface : ifaces) {
                internalService.submit(createPingTask(client, iface.ip(), port));
                internalCount.incrementAndGet();

                if (iface.ipv6() != null) {
                    for (Ipv6Json ipv6 : iface.ipv6()) {
                        String ipv6Address = "[" + ipv6.address() + "]";
                        internalService.submit(createPingTask(client, ipv6Address, port));
                        internalCount.incrementAndGet();
                    }
                }
            }
        }

        // host address(ddns/fqdn)
        ExecutorCompletionService<String> hostService = new ExecutorCompletionService<>(executor);
        AtomicInteger hostCount = new AtomicInteger(0);
        // ddns
        if (!Util.isEmpty(serverJson.ddns()) && !serverJson.ddns().equals("NULL")) {
            hostService.submit(createPingTask(client, serverJson.ddns(), port));
            hostCount.incrementAndGet();
        }
        // fqdn
        if (!Util.isEmpty(serverJson.fqdn()) && !serverJson.fqdn().equals("NULL")) {
            hostService.submit(createPingTask(client, serverJson.fqdn(), port));
            hostCount.incrementAndGet();
        }

        // external address(public ip address)
        ExecutorCompletionService<String> externalService = new ExecutorCompletionService<>(executor);
        AtomicInteger externalCount = new AtomicInteger(0);
        if (serverJson.external() != null) {
            String ip = serverJson.external().ip();
            if (!Util.isEmpty(ip)) {
                externalService.submit(createPingTask(client, ip, (externalPort != 0) ? externalPort : port));
                externalCount.incrementAndGet();
            }
            String ipv6 = serverJson.external().ipv6();
            if (!Util.isEmpty(ipv6) && !ipv6.equals("::")) {
                externalService
                        .submit(createPingTask(client, "[" + ipv6 + "]", (externalPort != 0) ? externalPort : port));
                externalCount.incrementAndGet();
            }
        }

        while (internalCount.getAndDecrement() > 0) {
            try {
                Future<String> future = internalService.take();
                if (future != null) {
                    String host = future.get();
                    if (!Util.isEmpty(host)) {
                        executor.shutdownNow();
                        return requestUrl.newBuilder().host(host).port(port).build();
                    }
                }
            } catch (InterruptedException | ExecutionException ignored) {
                LOGGER.trace("", ignored);
            }
        }

        while (hostCount.getAndDecrement() > 0) {
            try {
                Future<String> future = hostService.take();
                if (future != null) {
                    String host = future.get();
                    if (!Util.isEmpty(host)) {
                        executor.shutdownNow();
                        return requestUrl.newBuilder().host(host).port(port).build();
                    }
                }
            } catch (InterruptedException | ExecutionException ignored) {
                LOGGER.trace("", ignored);
            }
        }

        while (externalCount.getAndDecrement() > 0) {
            try {
                Future<String> future = externalService.take();
                if (future != null) {
                    String host = future.get();
                    if (!Util.isEmpty(host)) {
                        executor.shutdownNow();
                        return requestUrl.newBuilder().host(host).port(port).build();
                    }
                }
            } catch (InterruptedException | ExecutionException ignored) {
                LOGGER.trace("", ignored);
            }
        }

        executor.shutdownNow();

        return null;
    }

    private HttpUrl pingTunnel(ServiceJson serviceJson) {
        if (serviceJson == null || Util.isEmpty(serviceJson.relay_ip()) || serviceJson.relay_port() == 0) {
            return null;
        }

        // set timeout to 10 seconds
        OkHttpClient client = defaultClient.newBuilder().connectTimeout(5, SECONDS).readTimeout(5, SECONDS).build();

        String relayIp = serviceJson.relay_ip();
        int relayPort = serviceJson.relay_port();

        // tunnel address
        ExecutorService executor = Executors.newFixedThreadPool(10);
        CompletionService<String> service = new ExecutorCompletionService<>(executor);
        service.submit(createPingTask(client, relayIp, relayPort));

        try {
            Future<String> future = service.take();
            if (future != null) {
                String host = future.get();
                if (!Util.isEmpty(host)) {
                    return requestUrl.newBuilder().host(host).port(relayPort).build();
                }
            }
        } catch (InterruptedException | ExecutionException ignored) {
            LOGGER.trace("", ignored);
        } finally {
            executor.shutdownNow();
        }

        return null;
    }

    private Callable<String> createPingTask(final OkHttpClient client, final String host, int port) {
        final HttpUrl pingPongUrl = new HttpUrl.Builder().scheme(requestUrl.scheme()).host(host).port(port)
                .addPathSegment("webman").addPathSegment("pingpong.cgi").addQueryParameter("action", "cors").build();
        LOGGER.debug("pingpong from {}", pingPongUrl);
        return () -> {
            try (JsonReader reader = new JsonReader(new InputStreamReader(
                    client.newCall(new Request.Builder().url(pingPongUrl).build()).execute().body().byteStream(),
                    StandardCharsets.UTF_8))) {
                PingPongJson pingPongJson = gson.fromJson(reader, PingPongJson.class);
                LOGGER.info("{} => {}", pingPongUrl, pingPongJson);
                if (pingPongJson != null && pingPongJson.success()) {
                    return host;
                }
            } catch (IOException e) {
                LOGGER.debug("{} => {}", pingPongUrl, e.toString());
            }
            return null;
        };
    }

    public ServerInfoJson requestTunnel(ServerInfoJson infoJson, String serverID, String id, boolean gofile)
            throws IOException {
        if (infoJson == null || infoJson.env() == null || Util.isEmpty(infoJson.env().control_host())) {
            return null;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("command", "request_tunnel");
        jsonObject.addProperty("version", 1);
        jsonObject.addProperty("serverID", serverID);
        jsonObject.addProperty("id", id);
        jsonObject.addProperty("is_gofile", gofile);

        RequestBody requestBody = RequestBody.create(gson.toJson(jsonObject), MediaType.parse("text/plain"));
        try (JsonReader reader = new JsonReader(
                new InputStreamReader(
                        defaultClient.newBuilder().connectTimeout(30, SECONDS).readTimeout(30, SECONDS).build()
                                .newCall(new Request.Builder()
                                        .url(HttpUrl.parse("https://" + infoJson.env().control_host() + "/Serv.php"))
                                        .post(requestBody).build())
                                .execute().body().byteStream(),
                        StandardCharsets.UTF_8))) {
            return gson.fromJson(reader, ServerInfoJson.class);
        }
    }
}
