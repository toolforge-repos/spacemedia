package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilOceaniaDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilOceaniaDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmiloceania.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmiloceania.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmiloceania.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmiloceania.dvids.min.year}") int minYear,
            @Value("${usmiloceania.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmiloceania.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist);
    }

    @Override
    public String getName() {
        return "U.S. Military in Oceania (DVIDS)";
    }
}
