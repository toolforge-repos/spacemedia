package org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store.RelayCookie;
import org.wikimedia.commons.donvip.spacemedia.service.synology.quickconnect.store.RelayManager;

import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class QuickConnectInterceptor implements Interceptor {

    // Adapted from https://github.com/wujingwe/quickconnect-interceptor

    // Doc: https://github.com/nullvoidptr/qcon/blob/master/protocol.md

    private static final Logger LOGGER = LoggerFactory.getLogger(QuickConnectInterceptor.class);

    private final RelayManager relayManager = new RelayManager();
    private final String zone;

    public QuickConnectInterceptor(String zone) {
        RelayManager.setDefault(relayManager);
        this.zone = zone != null ? zone : "global";
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl requestUrl = request.url();
        String host = requestUrl.host();

        boolean isQuickConnectId = Util.isQuickConnectId(host);
        boolean isGofile = "gofile.me".equals(host);

        if (isQuickConnectId || isGofile) {
            final String serverID = isGofile ? requestUrl.pathSegments().get(0) : host;
            final String id = isGofile ? fileSharingId(requestUrl) : mainAppId(requestUrl);
            final int port = requestUrl.port();
            RelayCookie cookie = relayManager.get(serverID, port);
            if (cookie == null) {
                cookie = new RelayCookie.Builder().serverID(serverID).id(id).gofile(isGofile).build();
            }
            if (cookie.resolvedUrl() == null) {
                cookie = new QuickConnectResolver(requestUrl, zone).resolve(serverID, port, id, isGofile);
                relayManager.put(serverID, port, cookie);
            }
            HttpUrl resolvedUrl = cookie.resolvedUrl();
            if (resolvedUrl == null) {
                throw new IOException("resolvedUrl == null");
            }
            host = resolvedUrl.host();
            if (host.indexOf(':') != -1) {
                host = "[" + host + "]"; // add brackets for IPv6
            }
            Builder builder = requestUrl.newBuilder().host(host).port(resolvedUrl.port());
            if (isGofile) {
                builder.setPathSegment(0, "sharing");
            }
            HttpUrl url = builder.build();
            request = request.newBuilder().url(url).build();
            LOGGER.debug("Resolved url: {}", url);
        }
        return chain.proceed(request);
    }

    private static String fileSharingId(HttpUrl requestUrl) {
        return requestUrl.isHttps() ? "file_sharing_https" : "file_sharing";
    }

    private static String mainAppId(HttpUrl requestUrl) {
        return requestUrl.isHttps() ? "mainapp_https" : "mainapp_http";
    }
}
