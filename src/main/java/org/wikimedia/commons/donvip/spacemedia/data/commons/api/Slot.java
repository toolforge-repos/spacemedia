package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record Slot (

    @JsonProperty("contentmodel")
    String contentModel,

    @JsonProperty("contentformat")
    String contentFormat,

    @JsonProperty("*")
    String content) {

}
