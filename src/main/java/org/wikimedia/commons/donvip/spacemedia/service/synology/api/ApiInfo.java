package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public record ApiInfo(String api, @JsonProperty("maxVersion") int maxVersion,
        @JsonProperty("minVersion") int minVersion, @JsonProperty("path") String path,
        @JsonProperty("requestFormat") String requestFormat) {

    public static class ApiInfoList {

        private final List<ApiInfo> apiInfos = new ArrayList<>();

        @JsonAnySetter
        public void add(String key, ApiInfo value) {
            apiInfos.add(new ApiInfo(key, value.maxVersion, value.minVersion, value.path, value.requestFormat));
        }

        public List<ApiInfo> getApiInfos() {
            return apiInfos;
        }

        @Override
        public String toString() {
            return apiInfos.toString();
        }
    }
}
