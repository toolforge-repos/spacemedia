package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public record Continue(@JsonProperty("continue") String continu, String accontinue, String rvcontinue, int gsroffset) {

}
