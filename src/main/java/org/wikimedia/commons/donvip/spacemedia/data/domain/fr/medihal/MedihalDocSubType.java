package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal;

import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q1027879_GRAPHICS;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q11835431_ENGRAVING;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q125191_PHOTOGRAPH;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q178659_ILLUSTRATION;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q93184_DRAWING;

import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem;

public enum MedihalDocSubType {
    DRAWING(Q93184_DRAWING), GRAPHICS(Q1027879_GRAPHICS), ILLUSTRATION(Q178659_ILLUSTRATION),
    PHOTOGRAPHY(Q125191_PHOTOGRAPH), GRAVURE(Q11835431_ENGRAVING);

    private final WikidataItem item;

    private MedihalDocSubType(WikidataItem item) {
        this.item = item;
    }

    public WikidataItem item() {
        return item;
    }
}
