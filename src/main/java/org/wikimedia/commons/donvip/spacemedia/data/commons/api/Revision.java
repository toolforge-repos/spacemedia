package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.Map;

public record Revision(
    Map<String, Slot> slots) {

}
