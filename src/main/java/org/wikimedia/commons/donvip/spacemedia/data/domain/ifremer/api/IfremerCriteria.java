package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
public record IfremerCriteria(Integer facetOrder, String field, String javaClass, String name, List<String> options,
        String order, Boolean shouldBehaviourCriteria, Integer sortPriority, List<String> types,
        List<IfremerCriteriaValue> values, int weight) {

}
