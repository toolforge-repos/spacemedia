package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Locale.ENGLISH;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q125191_PHOTOGRAPH;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q725252_SATELLITE_IMAGERY;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.lroc.NasaLrocMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.lroc.NasaLrocMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.exception.IgnoreException;
import org.wikimedia.commons.donvip.spacemedia.service.nasa.NasaMappingService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.utils.CsvHelper;

@Service
public class NasaLrocShadowCamService extends AbstractOrgHtmlGalleryService<NasaLrocMedia> {

    private static final String LROC_BASE_URL = "https://www.lroc.asu.edu";
    private static final String SHAD_BASE_URL = "https://www.shadowcam.asu.edu";

    private static final DateTimeFormatter DATE_PATTERN = ofPattern("d MMMM yyyy", ENGLISH);

    private static final Pattern CREDIT_PATTERN = Pattern.compile(".* \\[(.+)\\].?");

    @Lazy
    @Autowired
    private NasaMappingService mappings;

    private Map<String, String> moonKeywords;

    @Autowired
    public NasaLrocShadowCamService(NasaLrocMediaRepository repository) {
        super(repository, "nasa.lroc.shadowcam", Set.of("lroc", "shadowcam"));
    }

    @Override
    @PostConstruct
    void init() throws IOException {
        super.init();
        moonKeywords = CsvHelper.loadCsvMapping("moon.keywords.csv");
    }

    @Override
    public String getName() {
        return "NASA (LROC / ShadowCam)";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Spacemedia " + lrocOrShadowcam(repoId, "LROC", "ShadowCam") + " files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected boolean checkBlocklist(NasaLrocMedia media) {
        return false;
    }

    private static <T> T lrocOrShadowcam(String repoId, T lroc, T shadowcam) {
        return "shadowcam".equals(repoId) ? shadowcam : lroc;
    }

    @Override
    protected List<String> fetchGalleryUrls(String repoId) {
        return List.of(getBaseUrl(repoId));
    }

    @Override
    protected String getGalleryPageUrl(String galleryUrl, int page) {
        return galleryUrl + "?page=" + page;
    }

    @Override
    protected Elements getGalleryItems(String repoId, String url, Element html) {
        return html.getElementsByClass(lrocOrShadowcam(repoId, "block-link", "img-link"));
    }

    @Override
    protected String getAuthor(NasaLrocMedia media, FileMetadata metadata) {
        if (metadata.getDescription() != null) {
            Matcher m = CREDIT_PATTERN.matcher(metadata.getDescription());
            if (m.matches()) {
                String credit = m.group(1);
                if (!credit.contains("NASA")) {
                    throw new IgnoreException("Non-NASA picture: " + credit);
                }
                return credit;
            }
        }
        return "NASA/" + lrocOrShadowcam(media.getId().repoId(), "GSFC", "KARI") + "/Arizona State University";
    }

    private String getBaseUrl(String repoId) {
        return lrocOrShadowcam(repoId, LROC_BASE_URL, SHAD_BASE_URL) + "/images";
    }

    @Override
    protected String getSourceUrl(CompositeMediaId id) {
        return getBaseUrl(id.repoId()) + "/" + id.mediaId();
    }

    @Override
    protected NasaLrocMedia refresh(NasaLrocMedia media) throws IOException {
        return media.copyDataFrom(fetchMedia(media.getId(), ofNullable(media.getBestTemporal())));
    }

    @Override
    protected Class<NasaLrocMedia> getMediaClass() {
        return NasaLrocMedia.class;
    }

    @Override
    public Set<String> findCategories(NasaLrocMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        result.addAll(media.getKeywordStream().map(moonKeywords::get).filter(Objects::nonNull).toList());
        result.addAll(media.getKeywordStream().map(mappings.getNasaKeywords()::get).filter(Objects::nonNull).toList());
        if (media.getPublicationDate().isAfter(LocalDate.of(2009, 7, 1))) {
            result.add("Photos of the Moon by "
                    + lrocOrShadowcam(media.getId().repoId(), "Lunar Reconnaissance Orbiter", "ShadowCam")
                    + (metadata.getMediaDimensions() != null && metadata.getMediaDimensions().getAspectRatio() < 0.25
                            ? " (raw frames)"
                            : ""));
        }
        return result;
    }

    @Override
    public Set<String> findAfterInformationTemplates(NasaLrocMedia media, FileMetadata metadata) {
        Set<String> result = super.findAfterInformationTemplates(media, metadata);
        if ("lroc".equals(media.getId().repoId())) {
            result.add(
                    "Template:NASA Photojournal/attribution|class=LRO|mission=LRO|name=Lunar Reconnaissance Orbiter Camera|credit=LROC");
            result.add("NASA-image|id=" + media.getId() + "|center=GSFC");
        }
        return result;
    }

    @Override
    public Set<String> findLicenceTemplates(NasaLrocMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add("PD-USGov-NASA");
        return result;
    }

    @Override
    protected SdcStatements getStatements(NasaLrocMedia media, FileMetadata metadata) {
        SdcStatements result = super.getStatements(media, metadata).instanceOf(Q125191_PHOTOGRAPH);
        if (media.getPublicationDate().isAfter(LocalDate.of(2009, 7, 1))) {
            String repoId = media.getId().repoId();
            result.creator(lrocOrShadowcam(repoId, "Q331778", "Q30749609")) // Created by LRO / Danuri
                    .depicts("Q405") // Depicts the Moon
                    .locationOfCreation("Q210448") // Created in lunar orbit
                    .fabricationMethod(Q725252_SATELLITE_IMAGERY)
                    .capturedWith(lrocOrShadowcam(repoId, "Q124653753", "Q106473449")); // Taken with LROC / ShadowCam
        }
        return result;
    }

    @Override
    protected String extractIdFromGalleryItem(String url, Element result) {
        String[] items = (result.hasAttr("href") ? result.attr("href")
                : result.getElementsByTag("a").first().attr("href")).split("/");
        return items[items.length - 1];
    }

    @Override
    List<NasaLrocMedia> fillMediaWithHtml(String url, Document html, Element galleryItem, NasaLrocMedia media)
            throws IOException {
        String repoId = media.getId().repoId();
        Element article = ofNullable(html.getElementsByTag("article").first()).orElse(html);
        media.setTitle(
                new LocalizedText("en", html.getElementsByTag("header").first().getElementsByTag("h1").first().text()));
        String[] byline = article.getElementsByClass(lrocOrShadowcam(repoId, "mx-5", "mt-2")).first().text()
                .split(" on ");
        media.setPublicationDate(LocalDate.parse(byline[byline.length - 1], DATE_PATTERN));
        if (article.getElementsByTag("figcaption").isEmpty()
                && article.getElementsByClass("serendipity_imageComment_txt").isEmpty()) {
            media.setDescription(new LocalizedText("en", article.getElementById("post-body").text()));
        }
        Element tags = article.getElementsByClass("tags").first();
        if (tags != null) {
            for (Element tag : tags.getElementsByTag("a")) {
                media.getKeywords().add(tag.text());
            }
        }
        String baseUrl = lrocOrShadowcam(media.getId().repoId(), LROC_BASE_URL, SHAD_BASE_URL);
        for (Element e : article.getElementsByClass("img-polaroid")) {
            String src = e.getElementsByTag("img").attr("src");
            addMetadata(media, src.startsWith("http") ? src : baseUrl + src,
                    fm -> {
                        String figcaption = e.getElementsByTag("figcaption").text();
                        String imgComment = e.getElementsByClass("serendipity_imageComment_txt").text();
                        fm.setDescription(new LocalizedText("en", isNotBlank(figcaption) ? figcaption
                                : isNotBlank(imgComment) ? imgComment : e.getElementsByTag("font").text()));
                    });
        }
        for (Element e : article.getElementsByClass("olZoomify")) {
            addZoomifyFileMetadata(media, e, baseUrl);
        }
        return List.of(media);
    }
}
