package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Set;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaRepository;

public interface MedihalMediaRepository extends MediaRepository<MedihalMedia> {

    @Retention(RetentionPolicy.RUNTIME)
    @CacheEvict(allEntries = true, cacheNames = { "medihalCount", "medihalCountRepo", "medihalCountIgnored",
            "medihalCountIgnoredRepo", "medihalCountMissing", "medihalCountMissingRepo",
            "medihalCountMissingImagesRepo", "medihalCountMissingVideosRepo", "medihalCountMissingDocumentsRepo",
            "medihalCountUploaded", "medihalCountUploadedRepo", "medihalCountPhashNotNull",
            "medihalCountPhashNotNullRepo" })
    @interface CacheEvictMedihalAll {

    }

    @Override
    @CacheEvictMedihalAll
    default void evictCaches() {

    }

    // COUNT

    @Override
    @Cacheable("medihalCount")
    long count();

    @Override
    @Cacheable("medihalCountRepo")
    long count(Set<String> repos);

    @Override
    @Cacheable("medihalCountIgnored")
    long countByMetadata_IgnoredTrue();

    @Override
    @Cacheable("medihalCountIgnoredRepo")
    long countByMetadata_IgnoredTrue(Set<String> repos);

    @Override
    @Cacheable("medihalCountMissing")
    long countMissingInCommons();

    @Override
    @Cacheable("medihalCountMissingRepo")
    long countMissingInCommons(Set<String> repos);

    @Override
    @Cacheable("medihalCountMissingImagesRepo")
    long countMissingImagesInCommons(Set<String> repos);

    @Override
    @Cacheable("medihalCountMissingVideosRepo")
    long countMissingVideosInCommons(Set<String> repos);

    @Override
    @Cacheable("medihalCountMissingDocumentsRepo")
    long countMissingDocumentsInCommons(Set<String> repos);

    @Override
    @Cacheable("medihalCountUploaded")
    long countUploadedToCommons();

    @Override
    @Cacheable("medihalCountUploadedRepo")
    long countUploadedToCommons(Set<String> repos);

    @Override
    @Cacheable("medihalCountPhashNotNull")
    long countByMetadata_PhashNotNull();

    @Override
    @Cacheable("medihalCountPhashNotNullRepo")
    long countByMetadata_PhashNotNull(Set<String> repos);

    // SAVE

    @Override
    @CacheEvictMedihalAll
    <S extends MedihalMedia> S save(S entity);

    @Override
    @CacheEvictMedihalAll
    <S extends MedihalMedia> List<S> saveAll(Iterable<S> entities);

    // DELETE

    @Override
    @CacheEvictMedihalAll
    void deleteById(CompositeMediaId id);

    @Override
    @CacheEvictMedihalAll
    void delete(MedihalMedia entity);

    @Override
    @CacheEvictMedihalAll
    void deleteAll(Iterable<? extends MedihalMedia> entities);

    @Override
    @CacheEvictMedihalAll
    void deleteAll();
}
