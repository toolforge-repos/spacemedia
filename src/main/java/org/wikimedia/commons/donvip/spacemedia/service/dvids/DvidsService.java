package org.wikimedia.commons.donvip.spacemedia.service.dvids;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Consumer;

import org.apache.commons.lang3.function.TriFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaType;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiAssetResponse;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiPageInfo;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiSearchResponse;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiUnitResponse;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.api.ApiUnitResult;
import org.wikimedia.commons.donvip.spacemedia.exception.ApiException;

/**
 * Service fetching images from https://api.dvidshub.net/
 */
@Lazy
@Service
public class DvidsService {

    private static final String API_KEY = "api_key";

    private static final Logger LOGGER = LoggerFactory.getLogger(DvidsService.class);

    private static final int MAX_RESULTS = 1000;

    @Value("${DVIDS_API_KEY}")
    private String apiKey;

    @Value("${dvids.api.search.url}")
    private String searchApiEndpoint;

    @Value("${dvids.api.search.year.url}")
    private String searchYearApiEndpoint;

    @Value("${dvids.api.asset.url}")
    private UriTemplate assetApiEndpoint;

    @Value("${dvids.api.unit.url}")
    private UriTemplate unitApiEndpoint;

    @Value("${dvids.media.url}")
    private UriTemplate mediaUrl;

    @Value("${dvids.ignored.categories}")
    private Set<String> ignoredCategories;

    @Value("${dvids.us.states}")
    private Set<String> usStates;

    private final RestTemplate rest = new RestTemplate();

    public DvidsMedia getMediaFromApi(CompositeMediaId id,
            TriFunction<DvidsMedia, URL, Consumer<FileMetadata>, FileMetadata> addMetadata) {
        DvidsMedia media = ofNullable(
                rest.getForObject(assetApiEndpoint.expand(Map.of(API_KEY, apiKey, "id", id.mediaId())),
                        ApiAssetResponse.class))
                .orElseThrow(() -> new IllegalArgumentException("No result from DVIDS API for " + id)).results();
        media.setId(id);
        addMetadata.apply(media, media.getAssetUrl(), fm -> fm.setMediaDimensions(media.getMediaDimensions()));
        return media;
    }

    public ApiSearchResponse searchDvidsMediaIds(DvidsMediaType type,
            String unit, String country, String state, int year, int month, int day, int page) throws ApiException {
        Map<String, Object> variables = new TreeMap<>(Map.of(API_KEY, apiKey, "type", type, "page", page));
        String template = year <= 0 ? searchApiEndpoint : searchYearApiEndpoint;
        if ("*".equals(unit)) {
            template = template.replace("&unit={unit}", "");
        } else {
            variables.put("unit", unit);
        }
        if ("*".equals(country)) {
            template = template.replace("&country={country}", "");
        } else {
            variables.put("country", country);
        }
        if ("*".equals(state)) {
            template = template.replace("&state={state}", "");
        } else {
            variables.put("state", state);
        }
        if (year > 0) {
            variables.put("from_date", String.format("%04d-%02d-%02dT00:00:00Z", year, month, day > 0 ? day : 1));
            variables.put("to_date", String.format("%04d-%02d-%02dT23:59:59Z", year,
                day > 0 ? month : month == 12 ? 12 : month + 1,
                day > 0 ? day : month == 12 ? 31 : 1));
        }
        URI uri = new UriTemplate(template).expand(variables);
        LOGGER.debug("{}", uri);
        ApiSearchResponse response = rest.getForObject(uri, ApiSearchResponse.class);
        if (response == null || response.getErrors() != null) {
            throw new ApiException(
                    String.format("API error while fetching DVIDS %ss from unit '%s': %s", type, unit, response));
        }
        ApiPageInfo pageInfo = response.getPageInfo();
        if (pageInfo.totalResults() == MAX_RESULTS) {
            String msg = String.format(
                    "Incomplete search! More criteria must be defined for %ss of '%s'/'%s' (%04d-%02d-%02d)!", type, unit,
                    country, year, month, day);
            LOGGER.warn(msg);
        } else if (pageInfo.totalResults() == 0) {
            LOGGER.info("No {} for {}/{} in year {}-{}-{}", type, unit, country, year, month, day);
        } else if (page == 1) {
            LOGGER.debug("{} {}s to process for {}/{}", pageInfo.totalResults(), type, unit, country);
        }
        return response;
    }

    @Cacheable("unitAbbrByFullName")
    public String getUnitAbbreviation(String unitFullName) {
        try {
            return unitSearch("unit_name", unitFullName).unit_abbrev();
        } catch (IllegalStateException | NoSuchElementException e) {
            if (unitFullName != null && unitFullName.endsWith(" Public Affairs")) {
                return getUnitAbbreviation(unitFullName.replace(" Public Affairs", ""));
            }
            throw e;
        }
    }

    @Cacheable("unitNameById")
    public String getUnitName(String unitId) {
        return unitSearch("unit_id", unitId).unit_name();
    }

    private ApiUnitResult unitSearch(String param, String value) {
        return ofNullable(rest.getForObject(
                unitApiEndpoint.expand(Map.of(API_KEY, apiKey, requireNonNull(param), requireNonNull(value))),
                ApiUnitResponse.class)).orElseThrow(() -> new IllegalStateException("No unit found for " + value))
                .results().iterator().next();
    }

    public boolean isIgnoredCategory(String category) {
        return ignoredCategories.contains(category);
    }

    public URL getSourceUrl(String[] typedId) throws MalformedURLException {
        return mediaUrl.expand(Map.of("type", typedId[0], "id", typedId[1])).toURL();
    }

    @Cacheable("statesByCountry")
    public SortedSet<String> getStates(String country) {
        return new TreeSet<>("United States".equals(country) ? usStates : Set.of("*"));
    }
}
