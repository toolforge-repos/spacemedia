package org.wikimedia.commons.donvip.spacemedia.service.nasa;

import static java.util.Collections.singleton;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.https;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.restTemplateSupportingAll;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.urlToUri;

import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.function.TriFunction;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.util.TriConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Caption;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.ExifMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.ExifMetadataRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadataRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaAssets;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaCaptions;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaCollection;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaImage;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaItem;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaLink;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library.NasaResponse;
import org.wikimedia.commons.donvip.spacemedia.service.ApiThrottleService;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.utils.Geo;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.transaction.Transactional;

@Lazy
@Service
public class NasaMediaProcessorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NasaMediaProcessorService.class);

    private static final Pattern PATTERN_NUMBER = Pattern.compile(".*\\d+,\\d+.*");
    private static final Pattern PATTERN_DATE = Pattern.compile(".*\\p{Alpha}+\\.? \\d{1,2}, [12]\\d{3}.*");
    private static final Pattern PATTERN_A_AND_B = Pattern.compile(".*[\\p{Alpha}\\.]+, (and|&) \\p{Alpha}+.*");
    private static final Pattern PATTERN_A_B_AND_C = Pattern.compile(".*\\p{Alpha}+, \\p{Alpha}+ (and|&) \\p{Alpha}+.*");
    private static final Pattern PATTERN_ER = Pattern.compile("[^,]*\\p{Alpha}+er, \\p{Alpha}+er[^,]*");

    @Value("${nasa.max.tries}")
    private int maxTries;

    @Value("${nasa.captions.link}")
    private String captionsLink;

    @Value("${nasa.metadata.link}")
    private String metadataLink;

    @Autowired
    private ExifMetadataRepository exifRepository;

    @Autowired
    private FileMetadataRepository metadataRepository;

    @Autowired
    private NasaMediaRepository<NasaMedia> repository;

    @Autowired
    protected MediaService mediaService;

    @Autowired
    private ObjectMapper jackson;

    @Lazy
    @Autowired
    private ApiThrottleService apiThrottle;

    @Transactional
    public <T extends NasaMedia> String processSearchResults(RestTemplate rest, String searchUrl,
            Collection<T> uploadedMedia, Counter counter, String who, Set<CompositeMediaId> foundIds,
            TriConsumer<LocalDateTime, String, Integer> ongoingUpdateMedia,
            Predicate<NasaMedia> doCommonUpdate, BiPredicate<NasaMedia, Boolean> shouldUploadAuto,
            BiConsumer<URL, Throwable> problem, UnaryOperator<NasaMedia> saveMedia,
            BiFunction<Boolean, NasaMedia, NasaMedia> saveMediaOrCheckRemote,
            TriFunction<NasaMedia, Boolean, Boolean, Triple<NasaMedia, Collection<FileMetadata>, Integer>> uploader) {
        LocalDateTime start = LocalDateTime.now();
        // NASA website can return application/octet-stream instead of application/json
        RestTemplate restExif = restTemplateSupportingAll(jackson);
        boolean ok = false;
        int count = 0;
        for (int i = 0; i < maxTries && !ok; i++) {
            try {
                // Makes sure the service complies with api.nasa.gov hourly limit of 1,000 requests per hour
                apiThrottle.ensureApiLimit("NASA");
                LOGGER.debug("Fetching {}", searchUrl);
                NasaCollection collection = rest.getForObject(searchUrl, NasaResponse.class).collection();
                List<NasaItem> items = collection.items();
                ok = true;
                for (NasaItem item : items) {
                    try {
                        Pair<NasaMedia, Integer> update = processMedia(rest, restExif, item.data().get(0), item.href(),
                                doCommonUpdate, shouldUploadAuto, problem, saveMedia, saveMediaOrCheckRemote, uploader);
                        @SuppressWarnings("unchecked")
                        T media = (T) update.getKey();
                        if (update.getValue() > 0) {
                            uploadedMedia.add(media);
                        }
                        if (foundIds != null) {
                            foundIds.add(media.getId());
                        }
                        ongoingUpdateMedia.accept(start, who, count++);
                        counter.count++;
                    } catch (Forbidden e) {
                        problem.accept(item.href(), e);
                        GlitchTip.capture(e);
                    } catch (RestClientException e) {
                        if (e.getCause() instanceof HttpMessageNotReadableException) {
                            problem.accept(item.href(), e.getCause());
                        } else {
                            LOGGER.error("Cannot process item {}", item, e);
                        }
                        GlitchTip.capture(e);
                    } catch (URISyntaxException | RuntimeException e) {
                        LOGGER.error("Cannot process item {}", item, e);
                        GlitchTip.capture(e);
                    }
                }
                if (!CollectionUtils.isEmpty(collection.links())) {
                    Optional<NasaLink> next = collection.links().stream().filter(l -> "next".equals(l.rel()))
                            .findFirst();
                    if (next.isPresent()) {
                        // API returns http links with 301 redirect in text/html
                        // not correctly handled by RestTemplate, so switch to https
                        return https(next.get().href().toExternalForm());
                    }
                }
            } catch (RestClientException e) {
                LOGGER.error("Unable to process search results for {}", searchUrl, e);
                GlitchTip.capture(e);
            }
        }
        return null;
    }

    public void setId(NasaMedia media) {
        String repoId = media.getCenter();
        if (repoId == null && media instanceof NasaImage img && "NASA/JPL".equals(img.getSecondaryCreator())) {
            repoId = "JPL";
        }
        if (repoId == null) {
            throw new IllegalArgumentException("NASA media without center: " + media);
        }
        media.setId(new CompositeMediaId(repoId, media.getNasaId()));
    }

    private Pair<NasaMedia, Integer> processMedia(RestTemplate rest, RestTemplate restExif, NasaMedia media, URL href,
            Predicate<NasaMedia> doCommonUpdate, BiPredicate<NasaMedia, Boolean> shouldUploadAuto,
            BiConsumer<URL, Throwable> problem, UnaryOperator<NasaMedia> saveMedia,
            BiFunction<Boolean, NasaMedia, NasaMedia> saveMediaOrCheckRemote,
            TriFunction<NasaMedia, Boolean, Boolean, Triple<NasaMedia, Collection<FileMetadata>, Integer>> uploader)
            throws URISyntaxException {
        setId(media);
        Optional<NasaMedia> mediaInRepo = repository.findById(media.getId());
        boolean save = false;
        if (mediaInRepo.isPresent()) {
            media = mediaInRepo.get();
        } else {
            save = processMediaFromApi(rest, media, href, problem, true);
        }
        if (checkIgnoreRules(media)) {
            save = true;
        }
        if (doCommonUpdate.test(media)) {
            save = true;
        }
        if (media.hasMetadata()) {
            FileMetadata metadata = media.getUniqueMetadata();
            if (metadata.getExif() == null) {
                try {
                    ExifMetadata exifMetadata = readExifMetadata(restExif, media.getId().mediaId());
                    if (exifMetadata != null) {
                        metadata.setExif(exifRepository.save(exifMetadata));
                        save = true;
                    }
                } catch (HttpClientErrorException.Forbidden e) {
                    // NHQ202211160205-2 always return http 403
                    LOGGER.error("Unable to retrieve EXIF metadata for {}: {}", media.getId(), e.getMessage());
                    GlitchTip.capture(e);
                } catch (Exception e) {
                    LOGGER.error("Unable to retrieve EXIF metadata for {}", media.getId(), e);
                    GlitchTip.capture(e);
                }
            }
        } else {
            LOGGER.warn("Media without metadata: {}", media);
        }
        int uploadCount = 0;
        if (shouldUploadAuto.test(media, false)) {
            Triple<NasaMedia, Collection<FileMetadata>, Integer> upload = uploader
                    .apply(save ? saveMedia.apply(media) : media, true, false);
            uploadCount += upload.getRight();
            media = saveMedia.apply(upload.getLeft());
            save = false;
        }
        return Pair.of(saveMediaOrCheckRemote.apply(save, media), uploadCount);
    }

    public boolean checkIgnoreRules(NasaMedia media) {
        if (!media.isIgnored()) {
            if (media instanceof NasaImage img && isPhotographerBLocklisted(img)) {
                return mediaService.ignoreMedia(media,
                        "Non-NASA image, photographer blocklisted: " + img.getPhotographer() + " / "
                        + img.getSecondaryCreator());
            } else if (media.getDescription() != null) {
                if (media.getDescription().contains("/photojournal")) {
                    return mediaService.ignoreMedia(media, "Photojournal");
                } else {
                    String desc = ofNullable(media.getDescription().toLowerCase().text()).orElse("");
                    if ((desc.contains("courtesy") && !desc.contains("courtesy of nasa")) || desc.contains("©")) {
                        return mediaService.ignoreMedia(media, "Probably non-free image (courtesy)");
                    }
                }
            }
        }
        return false;
    }

    public boolean processMediaFromApi(RestTemplate rest, NasaMedia media, URL href,
            BiConsumer<URL, Throwable> problem, boolean saveMetadata) throws URISyntaxException {
        findOriginalMedia(rest, href).map(FileMetadata::new).map(x -> saveMetadata ? metadataRepository.save(x) : x)
                .ifPresent(media::addMetadata);
        // API is supposed to send us keywords in a proper JSON array, but not always
        Set<String> normalizedKeywords = normalizeKeywords(media.getKeywords());
        if (!Objects.equals(normalizedKeywords, media.getKeywords())) {
            media.setKeywords(normalizedKeywords);
        }
        if (media.getThumbnailUrl() == null) {
            findThumbnailMedia(rest, href).ifPresent(media::setThumbnailUrl);
        }
        if (media.hasMetadata() && media.getUniqueMetadata() instanceof FileMetadata fm && fm.isVideo()
                && fm.getCaptions().isEmpty()) {
            findCaptionFile(rest, media.getId().mediaId()).ifPresent(url -> fm.getCaptions().add(new Caption("", url)));
        }
        if (media.getTitle() != null && media.getTitle().startsWith("Title: ")) {
            media.setTitle(media.getTitle().replace("Title: ", "").trim());
        }
        if (media.getId().mediaId().length() < 3) {
            problem.accept(media.getAssetUrl(), new Exception("Strange id: '" + media.getId() + "'"));
        }
        return true;
    }

    private boolean isPhotographerBLocklisted(NasaImage img) {
        return (img.getPhotographer() != null && mediaService.isPhotographerBlocklisted(img.getPhotographer()))
                || (img.getSecondaryCreator() != null
                        && mediaService.isPhotographerBlocklisted(img.getSecondaryCreator()))
                || (img.getDescription() != null && img.getDescription().contains("Credit: SpaceX"))
                || (img.hasMetadata() && img.getUniqueMetadata().getExif() != null
                        && img.getUniqueMetadata().getExif().getExifImageDescription() != null
                        && img.getUniqueMetadata().getExif().getExifImageDescription().contains("Credit: SpaceX"));
    }

    ExifMetadata readExifMetadata(RestTemplate restExif, String id)
            throws RestClientException, URISyntaxException {
        return restExif.getForObject(urlToUri(newURL(metadataLink.replace("<id>", id))), ExifMetadata.class);
    }

    Optional<URL> findCaptionFile(RestTemplate rest, String id) throws URISyntaxException {
        NasaCaptions captions = rest.getForObject(urlToUri(newURL(captionsLink.replace("<id>", id))),
                NasaCaptions.class);
        return captions != null ? Optional.of(captions.location()) : Optional.empty();
    }

    private static Optional<URL> findSpecificMedia(RestTemplate rest, URL href, String text) throws URISyntaxException {
        NasaAssets assets = rest.getForObject(urlToUri(href), NasaAssets.class);
        return assets != null ? assets.stream().filter(u -> u.toExternalForm().contains(text)).findFirst()
                : Optional.empty();
    }

    static Optional<URL> findOriginalMedia(RestTemplate rest, URL href) throws URISyntaxException {
        return findSpecificMedia(rest, href, "~orig.");
    }

    static Optional<URL> findThumbnailMedia(RestTemplate rest, URL href) throws URISyntaxException {
        return findSpecificMedia(rest, href, "~thumb.");
    }

    public static Set<String> normalizeKeywords(Set<String> keywords) {
        if (keywords != null && keywords.size() == 1) {
            return doNormalizeKeywords(keywords);
        } else if (keywords != null) {
            // Look for bad situations like https://images.nasa.gov/details/GRC-2017-CM-0155
            // Keyword 1 : GRC-CM => Good :)
            // Keyword 2 : Solar Eclipse, Jefferson City Missouri, ... Reggie Williams,
            // Astronaut Mike Hopkins ==> WTF !?
            Set<String> normalized = new HashSet<>();
            for (Iterator<String> it = keywords.iterator(); it.hasNext();) {
                String kw = it.next();
                if (kw.length() > 300 && StringUtils.countMatches(kw, ",") > 10) {
                    normalized.addAll(doNormalizeKeywords(singleton(kw)));
                    it.remove();
                }
            }
            keywords.addAll(normalized);
        }
        return keywords;
    }

    private static Set<String> doNormalizeKeywords(Set<String> keywords) {
        String kw = keywords.iterator().next();
        for (String sep : Arrays.asList(",", ";")) {
            if (kw.contains(sep) && looksLikeMultipleValues(kw, sep)) {
                return Arrays.stream(kw.split(sep)).map(String::trim).filter(s -> !s.isEmpty()).collect(toSet());
            }
        }
        return keywords;
    }

    private static boolean looksLikeMultipleValues(String kw, String sep) {
        if (",".equals(sep)) {
            if (kw.startsWith("Hi, ") || kw.contains(", by ")) {
                return false;
            }
            if (kw.endsWith(sep)) {
                kw = kw.substring(0, kw.length() - sep.length());
            }
            if (kw.contains(sep)) {
                String after = kw.substring(kw.lastIndexOf(sep) + sep.length() + " ".length());
                for (Collection<String> entities : Arrays.asList(Geo.CONTINENTS, Geo.COUNTRIES, Geo.STATES,
                        Geo.STATE_CODES, Geo.NORTH_SOUTH_STATES)) {
                    if (entities.contains(after)) {
                        return false;
                    }
                }
                for (Pattern pattern : Arrays.asList(PATTERN_NUMBER, PATTERN_DATE, PATTERN_A_AND_B, PATTERN_A_B_AND_C,
                        PATTERN_ER)) {
                    if (pattern.matcher(kw).matches()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static class Counter {
        public int count = 0;
    }
}
