package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Author;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.synology.SynologyMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.synology.SynologyMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.synology.api.File;
import org.wikimedia.commons.donvip.spacemedia.utils.CsvHelper;

@Service
public class WikimediaFranceSynologyService extends AbstractOrgSynologyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WikimediaFranceSynologyService.class);

    private static final Charset ANSI = Charset.forName("Cp1252");

    public WikimediaFranceSynologyService(SynologyMediaRepository repository,
            @Value("${wmfr.fonds}") Set<String> repoIds) {
        super(repository, "wmfr", repoIds);
    }

    @Override
    public String getName() {
        return "Wikimédia France";
    }

    @Override
    protected boolean checkBlocklist(SynologyMedia media) {
        return false;
    }

    @Override
    protected String getLanguageCode() {
        return "fr";
    }

    private static boolean isFoliot(SynologyMedia media) {
        return isFoliot(media.getId().repoId());
    }

    private static boolean isFoliot(String repoId) {
        return "nas-wmfr//FondsPhotos/Fond FOLIO".equals(repoId);
    }

    @Override
    protected List<SynologyMedia> getFiles(String app, String share, Comparator<SynologyMedia> comparing) {
        try {
            String sid = login(app);
            try {
                Map<String, CsvFileMetadata> csvMetadata = loadCsvMetadata(app, share, sid);
                LOGGER.info("Found metadata for {} images", csvMetadata.size());
                return getFiles(app, share, comparing, Map.of("_sid", sid)).stream().map(m -> {
                    FileMetadata fm = m.getUniqueMetadata();
                    if (isFoliot(m)) {
                        m.addAuthor(new Author.Builder().wikidataId("Q60558828").firstName("Françoise")
                                .lastName("Foliot").build());
                    }
                    String filename = fm.getOriginalFileName();
                    ofNullable(csvMetadata.get(filename)).ifPresentOrElse(cfm -> {
                        ofNullable(cfm.year()).filter(StringUtils::isNotBlank).ifPresent(y -> {
                            try {
                                m.setCreationDate(LocalDate.of(Year.parse(y).getValue(), 1, 1));
                            } catch (DateTimeParseException ignored) {
                                LOGGER.trace("", ignored);
                            }
                        });
                        ofNullable(cfm.libelle()).filter(StringUtils::isNotBlank).map(l -> new LocalizedText("fr", l))
                                .ifPresent(m::setTitle);
                        m.setDescription(cfm.description());
                    }, () -> LOGGER.warn("No CSV metadata found for image {}", m));
                    return m;
                }).toList();
            } finally {
                logout(app, sid);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Map<String, CsvFileMetadata> loadCsvMetadata(String app, String share, String sid) throws IOException {
        Map<String, CsvFileMetadata> csvMetadata = new TreeMap<>();
        for (File csv : synology.getFiles(app, share, x -> x, null,
                Map.of("_sid", sid, "filetype", "file", "pattern", "*.csv"))) {
            for (Map.Entry<String, Map<String, String>> e : CsvHelper
                    .loadMapMap(synology.download(app, csv.path(), sid).byteStream(), ANSI, ';', 2).entrySet()) {
                csvMetadata.put(e.getKey(), new CsvFileMetadata(e.getValue()));
            }
        }
        return csvMetadata;
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return (isFoliot(repoId) ? "Fonds Françoise Foliot" : "Archimedia") + " files uploaded by OptimusPrimeBot";
    }

    @Override
    protected List<String> getReviewCategories(SynologyMedia media) {
        return getArchivesReviewCategories(media);
    }

    @Override
    public Set<String> findLicenceTemplates(SynologyMedia media, FileMetadata metadata) {
        return isFoliot(media) ? Set.of("Cc-by-sa-4.0") : super.findLicenceTemplates(media, metadata);
    }

    @Override
    protected String getSource(SynologyMedia media, FileMetadata metadata) {
        return "{{Private collection|owner=Wikimédia France|location=Paris}}";
    }

    @Override
    protected Optional<String> getOtherFields(SynologyMedia media) {
        return isFoliot(media) ? Optional.of("{{Fonds Françoise Foliot - Wikimédia France}}")
                : super.getOtherFields(media);
    }

    @Override
    protected String getTakenLocation(SynologyMedia media) {
        if (isFoliot(media)) {
            LocalizedText descLc = media.getDescription().toLowerCase(Locale.FRENCH).replace('_', ' ');
            if (descLc.contains("-- allemagne")) {
                return "Germany";
            } else if (descLc.contains("-- angleterre")) {
                return "United Kingdom";
            } else if (descLc.contains("-- belgique")) {
                return "Belgium";
            } else if (descLc.contains("-- histoire de france")) {
                return "France";
            } else if (descLc.contains("-- hongrie")) {
                return "Hungary";
            } else if (descLc.contains("-- italie")) {
                return "Italy";
            }
        }
        return super.getTakenLocation(media);
    }

    private static record CsvFileMetadata(String chapter, String subChapter, String libelle, String year,
            String annotation, String details) {

        private static final String LF = "\n\n";

        CsvFileMetadata(Map<String, String> v) {
            this(v.get("Chapitre"), v.get("SousChapitre"), v.get("Libelle"), v.get("Annee"), v.get("Annotation"),
                    v.get("Details"));
        }

        LocalizedText description() {
            StringBuilder sb = new StringBuilder();
            for (String s : List.of(chapter, subChapter, year, annotation, details)) {
                if (isNotBlank(s)) {
                    sb.append(LF).append("-- ").append(s);
                }
            }
            return new LocalizedText("fr", sb.toString().trim());
        }
    }
}
