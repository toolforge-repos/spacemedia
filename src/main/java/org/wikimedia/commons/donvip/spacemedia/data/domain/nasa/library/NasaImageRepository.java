package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

public interface NasaImageRepository extends NasaMediaRepository<NasaImage> {

}
