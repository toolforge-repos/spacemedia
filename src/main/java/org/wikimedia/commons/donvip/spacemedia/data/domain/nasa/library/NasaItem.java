package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

import java.net.URL;
import java.util.List;

public record NasaItem(URL href, List<NasaMedia> data) {
}
