package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.List;

public record AbuseFilter (
    int id,
    String description,
    List<String> actions) {

}
