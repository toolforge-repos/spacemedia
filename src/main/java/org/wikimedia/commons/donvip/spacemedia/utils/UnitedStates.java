package org.wikimedia.commons.donvip.spacemedia.utils;

import static java.util.Locale.ENGLISH;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.net.URL;
import java.time.LocalDate;
import java.time.Year;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataService.WikidataInfo;

public final class UnitedStates {

    /**
     * Exact pattern for US Military VIRIN identifiers. See
     * https://commons.wikimedia.org/wiki/Commons:VIRIN
     * https://www.dimoc.mil/Submit-DoD-VI/Digital-VI-Toolkit-read-first/Create-a-VIRIN/
     */
    private static final Pattern US_VIRIN = Pattern.compile(
            "([\\d]{6})-([A-Z])-([0-9A-Z]{5})-([\\d]{3,4})(?:-([A-Z]{2}))?");

    /**
     * Fake pattern for US Military VIRIN identifiers as found on Flickr.
     */
    private static final Pattern FAKE_US_VIRIN = Pattern.compile(
            "([\\d]{8})-([A-Z])-([0-9A-Z]{5})-([\\d]{3,4})");

    private static final Pattern WHITE_HOUSE = Pattern.compile(
            "[FPSV]\\d{8}[A-Z]{2}-\\d{4}");

    private UnitedStates() {

    }

    public static boolean isClearPublicDomain(String description) {
        return description != null
                && (description.contains("U.S. Army photo")
                    || description.contains("U.S. Navy photo")
                    || description.contains("U.S. Air Force photo")
                    || description.contains("U.S. Marine Corps photo")
                    || description.contains("U.S. Coast Guard photo")
                    || description.contains("U.S. Space Force photo")
                    || description.contains("dds.cr.usgs.gov")
                    || description.contains("eol.jsc.nasa.gov"));
    }

    public static boolean isFakeVirin(String identifier) {
        return FAKE_US_VIRIN.matcher(identifier).matches();
    }

    public static boolean isVirin(String identifier) {
        return US_VIRIN.matcher(identifier).matches();
    }

    public static boolean isWhiteHouse(String identifier) {
        return WHITE_HOUSE.matcher(identifier).matches();
    }

    public static record VirinTemplates(String virinTemplate, String pdTemplate, String videoCategory) {
    }

    public static VirinTemplates getUsVirinTemplates(String virin, URL url) {
        return getUsVirinTemplates(virin, url.toExternalForm());
    }

    public static VirinTemplates getUsVirinTemplates(String virin, String url) {
        if (isBlank(virin)) {
            return null;
        }
        Matcher m = US_VIRIN.matcher(virin);
        return m.matches() && !"ZZ999".equals(m.group(3)) ? switch (m.group(2)) {
            case "A" -> new VirinTemplates(vt(virin, "Army", url), "PD-USGov-Military-Army", vd("Army"));
            case "D" -> new VirinTemplates(vt(virin, "Department of Defense", url), "PD-USGov-Military", "Videos from Defense.Gov");
            case "F" -> new VirinTemplates(vt(virin, "Air Force", url), "PD-USGov-Military-Air Force", vd("Air Force"));
            case "G" -> new VirinTemplates(vt(virin, "Coast Guard", url), "PD-USCG", vd("Coast Guard"));
            case "H" -> new VirinTemplates(vt(virin, "Department of Homeland Security", url), "PD-USGov-DHS", vd("Department of Homeland Security"));
            case "M" -> new VirinTemplates(vt(virin, "Marine Corps", url), "PD-USGov-Military-Marines", vd("Marine Corps"));
            case "N" -> new VirinTemplates(vt(virin, "Navy", url), "PD-USGov-Military-Navy", vd("Navy"));
            case "O" -> new VirinTemplates(vt(virin, "Other", url), null, null);
            case "P" -> new VirinTemplates(vt(virin, "Executive Office of the President", url), "PD-USGov-POTUS", "White House videos");
            case "S" -> new VirinTemplates(vt(virin, "Department of State", url), "PD-USGov-DOS", vd("State Department"));
            case "X" -> new VirinTemplates(vt(virin, "Space Force", url), "PD-USGov-Military-Space Force", vd("Space Force"));
            case "Z" -> new VirinTemplates(vt(virin, "National Guard", url), "PD-USGov-Military-National Guard", vd("National Guard"));
            default -> new VirinTemplates(vt(virin, "Armed Forces", url), null, vd("Armed Forces"));
        } : null;
    }

    private static String vt(String virin, String organization, String url) {
        return "ID-USMil |1=" + virin + " |2= " + organization + "|url= " + url;
    }

    private static String vd(String organization) {
        return "Videos of the United States " + organization;
    }

    public static String getUsStateCategory(String state) {
        return switch (state) {
        case "Georgia" -> "Georgia (U.S. state)";
        case "District of Columbia" -> "Washington, D.C.";
        default -> state;
        };
    }

    public static Optional<String> getUsGovernmentCategory(Media media) {
        return ofNullable(switch (media.getId().repoId()) {
        // BLM
        case "mypubliclands", "91981596@N06", "blm_wo_recreation", "153850842@N02" ->
            "Photographs by Bureau of Land Management";
        case "blmalaska", "76641642@N08" -> "Files from the Bureau of Land Management Alaska Flickr stream";
        case "blmaz", "141771506@N07" -> "Files from the Bureau of Land Management Arizona Flickr stream";
        case "blmcalifornia", "50976304@N07" -> "Files from the Bureau of Land Management California Flickr stream";
        case "blmidaho", "90525319@N07" -> "Files from the Bureau of Land Management Idaho Flickr stream";
        case "blm_mtdks", "137405667@N06" ->
            "Files from the Bureau of Land Management Montana and Dakotas Flickr stream";
        case "blmnevada", "55893585@N08" -> "Photographs by the Nevada Bureau of Land Management";
        case "blmoregon", "50169152@N06" ->
            "Files from the Bureau of Land Management Oregon and Washington Flickr stream";
        case "blmutah", "76340031@N02" -> "Files from the Bureau of Land Management Utah Flickr stream";
        // BPA
        case "bonnevillepower", "45690087@N05" -> "Files from Bonneville Power Flickr stream";
        // Congress
        case "uscapitol", "65191584@N07" -> "Files from US Capitol Flickr stream";
        // DOC-NTIS
        case "ntis", "25226575@N05" -> "Files of the National Technical Information Service Flickr stream";
        // DOE
        case "departmentofenergy", "37916456@N02" -> "Photographs by the U.S. Department of Energy";
        case "doe-oakridge", "78004229@N05" -> "Files from Department of Energy Oak Ridge Flickr stream";
        case "51009184@N06" -> "Images from the Savannah River Site Flickr Stream";
        case "inl", "30369883@N03" -> "Files from Idaho National Laboratory Flickr stream";
        // DOS
        case "statephotos" -> "Photographs by the U.S. Department of State";
        // DVA
        case "veteranshealth", "69288602@N06" -> "Files from Veterans Health Flickr stream";
        // EPA
        case "usepagov", "24400159@N05" -> "Files from US Environmental-Protection-Agency Flickr stream";
        // FDA
        case "fdaphotos", "39736050@N02" -> "Images from the United States Food and Drug Administration";
        // FWS
        case "usfwshq", "50838842@N06" ->
            "Files from the United States Fish and Wildlife Service Headquarters Flickr stream";
        case "usfws_alaska", "54775250@N07" ->
            "Files from the U.S. Fish and Wildlife Service Alaska Region Flickr stream";
        case "usfwsnortheast", "43322816@N08" ->
            "Files from the United States Fish and Wildlife Service Northeast Region Flickr stream";
        case "usfwsmidwest", "49208525@N08" ->
            "Files from the United States Fish and Wildlife Service Midwest Region Flickr stream";
        case "usfwsmtnprairie", "51986662@N05" ->
            "Files from the United States Fish and Wildlife Service Mountain-Prairie Region Flickr stream";
        case "usfwspacific", "52133016@N08" ->
            "Files from the United States Fish and Wildlife Service Pacific Region Flickr stream";
        case "fws-recovery-act-projects", "46536834@N08" ->
            "Files from the US Fish and Wildlife Service - Recovery Act Team Flickr stream";
        case "usfwssoutheast", "41464593@N02" ->
            "Files from the U.S. Fish and Wildlife Service Southeast Region Flickr stream";
        case "usfws_pacificsw", "54430347@N04" ->
            "Files from the United States Fish and Wildlife Service Pacific Southwest Region Flickr stream";
        // GAO
        case "usgao", "58220939@N03" -> "Files from the GAO Flickr Stream";
        // LoC
        case "library_of_congress", "8623220@N02" -> "Flickr images from Library of Congress";
        // NARA
        case "usnationalarchives", "35740357@N03", "archivesnews", "55218980@N03" ->
            "Images from the National Archives and Records Administration flickr stream";
        // NPS
        case "75182224@N04", "alaskanps" -> "Files from the National Park Service Alaska Region Flickr stream";
        case "nationalparkservice", "42600860@N02" -> "Files from the U.S. National Parks Service Flickr stream";
        case "nationalregister", "36222865@N07" -> "Files from the National Register of Historic Places Flickr stream";
        case "archesnps", "72578886@N08" -> "Files from Arches NPS Flickr stream";
        case "canyonlandsnps", "78206310@N03" -> "Files from Canyonlands NPS Flickr stream";
        case "congareenps", "77660923@N08" -> "Files from Congaree NPS Flickr stream";
        case "cratersofthemoonnps", "93062192@N08" ->
            "Files from Craters of the Moon National Monument and Preserve Flickr stream";
        case "denalinps", "57557144@N06" -> "Files from DenaliNPS Flickr stream";
        case "evergladesnps", "56295925@N07" -> "Files from Everglades NPS Flickr stream";
        case "glaciernps", "43288043@N04" -> "Files from Glacier National Park Service Flickr stream";
        case "grand_canyon_nps", "50693818@N08" -> "Files from Grand Canyon NPS Flickr stream";
        case "greatsanddunesnpp", "94707653@N06" -> "Files from Great Sand Dunes NPS Flickr stream";
        case "drytortugasnps", "56355403@N08" -> "Images from Dry Tortugas NPS Flickr account";
        case "greatsmokymountainsnationalpark", "53081745@N05" ->
            "Images from Great Smoky Mountains National Park Flickr stream";
        case "joshuatreenp", "115357548@N08" -> "Files from Joshua Tree National Park Flickr stream";
        case "katmainps", "99350217@N03" -> "Files from Katmai NPS Flickr stream";
        case "lassennps", "61860846@N05" -> "Files from Lassen Volcanic NPS Flickr stream";
        case "mountrainiernps", "78037339@N03" -> "Files from Mount Rainier NPS Flickr stream";
        case "naturenps", "130826934@N07" -> "Files from NPS Natural Resources Flickr stream";
        case "olympicnps", "131856925@N02" -> "Images from Olympic NPS Flickr stream";
        case "rockynps", "127033241@N04" -> "Files from Rocky Mountain National Park Flickr stream";
        case "saguaronps", "64710613@N03" -> "Files from Saguaro NPS Flickr stream";
        case "santamonicamtns", "38729417@N06" -> "Files from the Santa Monica Mountains NPS Flickr stream";
        case "snpphotos", "67015038@N06" -> "Files from Shenandoah NPS Flickr stream";
        case "valleyforgenationalpark", "47634910@N02" -> "Files from Valley Forge NPS Flickr stream";
        case "yellowstonenps", "80223459@N05" -> "Files from Yellowstone NPS Flickr stream";
        case "zionnps", "54168808@N03" -> "Files from Zion NPS Flickr stream";
        // NRC
        case "nrcgov", "69383258@N08" -> "Files from the Nuclear Regulatory Commission Flickr stream";
        // NTSB
        case "ntsb", "68618467@N03" -> "Files from the NTSB Flickr stream";
        // PC
        case "peacecorps", "27814015@N03" -> "Images from the Peace Corps Flickr stream";
        // POTUS
        case "whitehouse", "whitehouse46", "whitehouse45", "obamawhitehouse" ->
            getUsWhiteHouseCategory(media.getCreationDate());
        // SAMHSA
        case "samhsa", "49984523@N02" -> "Substance Abuse and Mental Health Services Administration images";
        // Treasury
        case "ustreasury", "47923257@N08" -> "Files from the U.S. Department of the Treasury Flickr stream";
        // USAID
        case "usaid_images", "usaidoha", "127415024@N03" -> "USAID IMAGES";
        // USDA-FS
        case "usforestservice", "140082569@N07" -> "Files from Forest Service, USDA Flickr stream";
        case "fsnorthernregion", "40882383@N03" -> "Files from the U.S. Forest Service Northern Region Flickr stream";
        case "usfsregion5", "39108150@N05" ->
            "Files from the U.S. Forest Service Pacific Southwest Region Flickr stream";
        // USGS
        case "usgeologicalsurvey", "27784370@N05" -> "Images from the United States Geologic Survey Flickr stream";
        // USTDA
        case "ustdaphotos", "68133850@N06" -> "Files from USTDA Flickr stream";
        // VOA
        case "voiceofamerica", "68764527@N02" -> "Files from Voice of America Flickr stream";
        // Misc.
        case "usmissiontotheunrome", "64731242@N08" ->
            "Files from the US Mission to the United Nations Agencies in Rome Flickr stream";
        case "smithsonian", "25053835@N03" -> "Files from Smithsonian Institution Flickr stream";
        default -> null;
        });
    }

    public static String getUsWhiteHouseCategory(LocalDate creationDate) {
        return creationDate.isAfter(LocalDate.of(2025, 1, 20))
                ? "Photographs from the White House during the second Trump administration"
                : creationDate.isAfter(LocalDate.of(2021, 1, 20))
                        ? "Photographs from the White House during the Biden administration"
                        : creationDate.isAfter(LocalDate.of(2017, 1, 20))
                                ? "Photographs from the White House during the first Trump administration"
                                : creationDate.isAfter(LocalDate.of(2009, 1, 20))
                                        ? "Photographs from the White House during the Obama administration"
                                        : "Photographs from the White House";
    }

    public static Optional<String> getUsGovernmentLicence(Media media) {
        return ofNullable(UsOrg.forId(media.getId().repoId()).map(UsOrg::getLicense).orElse(null));
    }

    public static String getUsGovernmentHiddenUploadCategory(String repoId) {
        return UsOrg.forId(repoId).map(UsOrg::getHiddenUploadCategory).filter(StringUtils::isNotBlank)
                .orElse("U.S. Government Flickr files uploaded by ");
    }

    public static String getUsReviewCategory(Media media) {
        return UsOrg.forId(media.getId().repoId()).map(UsOrg::getReviewCategory).filter(StringUtils::isNotBlank)
                .orElse(null);
    }

    public static Optional<String> getUsMilitaryCategory(Media media) {
        return ofNullable(switch (media.getId().repoId().toLowerCase(ENGLISH)) {
        case "afspc", "afsc", "airforcespacecommand" -> "Photographs by the United States Air Force Space Command";
        case "ssc", "129133022@n07" -> "Photographs by the Space Systems Command";
        case "jtfsd", "spacecom", "usspacecom" -> "Photographs by the United States Space Command";
        case "spoc" ->
            media.getYear().isBefore(Year.of(2020)) ? "Photographs by the United States Air Force Space Command" : null;
        default -> null;
        });
    }

    public static Optional<String> getUsMilitaryCreator(Media media, WikidataService wikidata) {
        String repoId = media.getId().repoId();
        return Optional.ofNullable(switch (repoId.toLowerCase(ENGLISH)) {
            case "ssc" -> "Q2306400";
            case "jtfsd", "spacecom", "usspacecom" -> "Q7892209";
            case "spoc" -> media.getYear().isBefore(Year.of(2020)) ? "Q407203" : "Q80815922";
            case "starcom" -> "Q108226200";
            case "21sw" -> "Q4631162";
            case "310sw" -> "Q4634679";
            case "45sw" -> "Q4638258";
            case "460sw-pa" -> "Q16207108";
            case "50sw" -> "Q609146";
            case "b-gar" -> "Q97671318";
            case "sbd1" -> "Q104869543";
            case "sld30" -> "Q4634644";
            case "ussf-pa" -> "Q55088961";
            default -> null;
        }).or(() -> wikidata.searchDvidsUnit(repoId).or(() -> wikidata.searchFlickrUser(repoId))
                .map(WikidataInfo::qid));
    }

    public static String getUsMilitaryEmoji(Media media) {
        return switch (media.getId().repoId().toLowerCase(ENGLISH)) {
        case "sld30", "45sw", "patrick", "vandenberg" -> Emojis.ROCKET;
        default -> Emojis.FLAG_USA;
        };
    }

    public enum UsOrg {

        BLM("PD-USGov-BLM",
                Set.of("mypubliclands", "91981596@N06", "blm_wo_recreation", "153850842@N02", "blmalaska",
                        "76641642@N08", "blmaz", "141771506@N07", "blmcalifornia", "50976304@N07", "blmidaho",
                        "90525319@N07", "blm_mtdks", "137405667@N06", "blmnevada", "55893585@N08", "blmoregon",
                        "50169152@N06", "blmutah", "76340031@N02"),
                "United States Bureau of Land Management images (review needed)",
                "U.S. Bureau of Land Management Flickr files uploaded by "),

        BPA("PD-USGov-BPA", Set.of("bonnevillepower", "45690087@N05"), "", ""),

        AOC("PD-USGov-Congress-AOC", Set.of("uscapitol", "65191584@N07"), "", ""),

        DOC_NTIS("PD-USGov-DOC-NTIS", Set.of("ntis", "25226575@N05"), "", ""),

        DOE("PD-USGov-DOE",
                Set.of("departmentofenergy", "37916456@N02", "doe-oakridge", "78004229@N05", "51009184@N06", "inl",
                        "30369883@N03"),
                "", ""),

        DOS("PD-USGov-DOS", Set.of("statephotos"), "", ""),

        DVA("PD-USGov-DVA", Set.of("veteranshealth", "69288602@N06"), "", ""),

        EPA("PD-USGov-EPA", Set.of("usepagov", "24400159@N05"), "", ""),

        FDA("PD-USGov-FDA", Set.of("fdaphotos", "39736050@N02"), "", ""),

        FWS("PD-USGov-FWS",
                Set.of("usfwshq", "50838842@N06", "usfws_alaska", "54775250@N07", "usfwsnortheast", "43322816@N08",
                        "usfwsmidwest", "49208525@N08", "usfwsmtnprairie", "51986662@N05", "usfwspacific",
                        "52133016@N08", "fws-recovery-act-projects", "46536834@N08", "usfwssoutheast", "41464593@N02",
                        "usfws_pacificsw", "54430347@N04"),
                "United States Fish and Wildlife Service images (review needed)",
                "U.S. Fish and Wildlife Service Flickr files uploaded by "),

        GAO("PD-USGov-GAO", Set.of("usgao", "58220939@N03"), "", ""),

        LOC(null, Set.of("library_of_congress", "8623220@N02"), "", ""),

        NARA(null, Set.of("usnationalarchives", "35740357@N03", "archivesnews", "55218980@N03"), "", ""),

        NPS("PD-USGov-NPS",
                Set.of("75182224@N04", "alaskanps", "nationalparkservice", "42600860@N02", "archesnps", "72578886@N08",
                        "canyonlandsnps", "78206310@N03", "congareenps", "77660923@N08", "cratersofthemoonnps",
                        "93062192@N08", "denalinps", "57557144@N06", "evergladesnps", "56295925@N07", "glaciernps",
                        "43288043@N04", "grand_canyon_nps", "50693818@N08", "greatsanddunesnpp", "94707653@N06",
                        "drytortugasnps", "56355403@N08", "greatsmokymountainsnationalpark", "53081745@N05",
                        "joshuatreenp", "115357548@N08", "katmainps", "99350217@N03", "lassennps", "61860846@N05",
                        "mountrainiernps", "78037339@N03", "naturenps", "130826934@N07", "olympicnps", "131856925@N02",
                        "rockynps", "127033241@N04", "saguaronps", "64710613@N03", "santamonicamtns", "38729417@N06",
                        "snpphotos", "67015038@N06", "valleyforgenationalpark", "47634910@N02", "yellowstonenps",
                        "80223459@N05", "zionnps", "54168808@N03", "nationalregister", "36222865@N07"),
                "United States National Park Service images (review needed)",
                "U.S. National Park Service Flickr files uploaded by "),

        NRC("PD-USGov-NRC", Set.of("nrcgov", "69383258@N08"), "", ""),

        NTSB("PD-USGov-NTSB", Set.of("ntsb", "68618467@N03"),
                "National Transportation Safety Board images (review needed)",
                "U.S. National Transportation Safety Board Flickr files uploaded by "),

        PC("PD-USGov-PC", Set.of("peacecorps", "27814015@N03"), "", ""),

        POTUS("PD-USGov-POTUS", Set.of("whitehouse", "whitehouse46", "whitehouse45", "obamawhitehouse"), "", ""),

        HHS_SAMHSA("PD-USGov-HHS-SAMHSA", Set.of("samhsa", "49984523@N02"), "", ""),

        TREASURY("PD-USGov-Treasury", Set.of("ustreasury", "47923257@N08"), "", ""),

        USAID("PD-USGov-USAID", Set.of("usaid_images", "usaidoha", "127415024@N03"), "", ""),

        USDA_FS("PD-USGov-USDA-FS",
                Set.of("usforestservice", "140082569@N07", "fsnorthernregion", "40882383@N03", "usfsregion5",
                        "39108150@N05"),
                "United States Geologic Survey images (review needed)", ""),

        USGS("PD-USGov-USGS", Set.of("usgeologicalsurvey", "27784370@N05"), "", ""),

        USTDA("PD-USGov-USTDA", Set.of("ustdaphotos", "68133850@N06"), "", ""),

        VOA("PD-USGov-VOA", Set.of("voiceofamerica", "68764527@N02"), "", ""),

        UN_ROME("PD-USGov", Set.of("usmissiontotheunrome", "64731242@N08"), "", ""),

        SMITHSONIAN(null, Set.of("smithsonian", "25053835@N03"), "", "");

        private final String license;
        private final Set<String> ids;
        private final String reviewCategory;
        private final String hiddenUploadCategory;

        private UsOrg(String license, Set<String> ids, String reviewCategory, String hiddenUploadCategory) {
            this.license = license;
            this.ids = ids;
            this.reviewCategory = reviewCategory;
            this.hiddenUploadCategory = hiddenUploadCategory;
        }

        public String getLicense() {
            return license;
        }

        public String getReviewCategory() {
            return reviewCategory;
        }

        public String getHiddenUploadCategory() {
            return hiddenUploadCategory;
        }

        public static Optional<UsOrg> forId(String id) {
            for (UsOrg org : values()) {
                if (org.ids.contains(id)) {
                    return Optional.of(org);
                }
            }
            return Optional.empty();
        }
    }
}
