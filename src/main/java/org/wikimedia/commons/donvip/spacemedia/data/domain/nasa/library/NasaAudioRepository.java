package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

public interface NasaAudioRepository extends NasaMediaRepository<NasaAudio> {

}
