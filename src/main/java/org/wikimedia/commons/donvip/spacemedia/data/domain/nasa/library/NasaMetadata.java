package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public record NasaMetadata(@JsonProperty("total_hits") int totalHits) {
}
