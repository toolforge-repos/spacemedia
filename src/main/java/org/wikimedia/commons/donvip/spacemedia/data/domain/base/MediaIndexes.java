package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.persistence.Index;
import jakarta.persistence.Table;

@Target(TYPE)
@Retention(RUNTIME)
@Table(indexes = {
        @Index(name = "repo_id", columnList = "repo_id"),
        @Index(name = "media_id", columnList = "media_id"),
        @Index(name = "publication_date", columnList = "publication_date"),
        @Index(name = "publication_month", columnList = "publication_month"),
        @Index(name = "publication_year", columnList = "publication_year") })
public @interface MediaIndexes {

}
