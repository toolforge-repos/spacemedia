package org.wikimedia.commons.donvip.spacemedia.data.domain.base;

import java.util.Set;

public interface WithInstruments {

    Set<String> getInstruments();

    void setInstruments(Set<String> instruments);
}
