package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Locale.ENGLISH;

import java.util.Set;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;

@Service
public class UsMilOtherDvidsService extends AbstractOrgDvidsService {

    @Autowired
    public UsMilOtherDvidsService(DvidsMediaRepository<DvidsMedia> repository,
            @Value("${usmilother.dvids.units:*}") Set<String> dvidsUnits,
            @Value("${usmilother.dvids.countries}") Set<String> dvidsCountries,
            @Value("${usmilother.dvids.top.countries}") Set<String> dvidsTopCountries,
            @Value("${usmilother.dvids.min.year}") int minYear,
            @Value("${usmilother.dvids.blocklist}") boolean blocklist) {
        super(repository, "usmilother.dvids", dvidsUnits, dvidsCountries, dvidsTopCountries, minYear, blocklist, false);
    }

    @Override
    public String getName() {
        return "U.S. Military at sea and undisclosed locations (DVIDS)";
    }

    @Override
    protected String fixCountryForCommons(String country, LocalizedText description) {
        if ("At Sea".equals(country)) {
            // Find a more specific sea, ocean straits, etc.
            LocalizedText descLc = description.toLowerCase(ENGLISH);
            SortedSet<String> otherSeas = filteredCountries(x -> !country.equals(x)
                    && BODIES_OF_WATER.stream().anyMatch(x::contains) && descLc.contains(x.toLowerCase(ENGLISH)));
            if (otherSeas.size() == 1) {
                return otherSeas.iterator().next();
            }
        }
        return super.fixCountryForCommons(country, description);
    }
}
