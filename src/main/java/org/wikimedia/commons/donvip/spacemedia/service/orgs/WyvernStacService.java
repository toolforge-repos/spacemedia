package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.lang.Integer.parseInt;
import static org.wikimedia.commons.donvip.spacemedia.service.wikimedia.WikidataItem.Q725252_SATELLITE_IMAGERY;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.replace;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.stac.StacMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.stac.StacMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.CategorizationService.Affixes;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.SdcStatements;
import org.wikimedia.commons.donvip.spacemedia.utils.Emojis;

@Service
public class WyvernStacService extends AbstractOrgStacService {

    private static final Map<String, String> SATS = Map.ofEntries(e("001", "Q132129504"), e("002", "Q132129513"),
            e("003", "Q132129520"));

    private static final Pattern SAT_PATTERN = Pattern.compile("wyvern_dragonette-(\\d{3})_.*");

    @Autowired
    public WyvernStacService(StacMediaRepository repository,
            @Value("${wyvern.stac.catalogs}") Set<String> catalogs) {
        super(repository, "wyvern", catalogs);
    }

    @Override
    public String getName() {
        return "Wyvern";
    }

    @Override
    protected String getStacLicenceUrl() {
        return "https://wyvern.space/wp-content/uploads/2023/08/Wyvern-EULA-Public-Release-License-1.pdf";
    }

    @Override
    protected String getStacLicenceName() {
        return "Wyvern End User License Agreement";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Files from Wyvern Space uploaded by " + commonsService.getAccount();
    }

    @Override
    protected List<String> getReviewCategories(StacMedia media) {
        return List.of("Wyvern images (review needed)");
    }

    @Override
    protected LocalDate getDate(String itemHref) {
        String s = itemHref.substring(22);
        return LocalDate.of(
                parseInt(s.substring(0, 4)),
                parseInt(s.substring(4, 6)),
                parseInt(s.substring(6, 8)));
    }

    @Override
    protected boolean isStacItemIgnored(String itemHref) {
        return false;
    }

    @Override
    protected boolean categorizeGeolocalizedByName() {
        return true;
    }

    @Override
    public Set<String> findLicenceTemplates(StacMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add("Cc-by-4.0");
        return result;
    }

    @Override
    public Set<String> findCategories(StacMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        result.addAll(categorizationService.findCategoriesFromTitleAndAffixes(media.getTitle().text(),
                new Affixes(List.of("Images of "), false),
                new Affixes(List.of(" by Wyvern"), false)));
        if (replace(result, media.getYear() + " satellite pictures", media.getYear() + " Wyvern images")) {
            result.remove("Images by Wyvern");
        }
        return result;
    }

    @Override
    protected String getAuthor(StacMedia media, FileMetadata metadata) {
        // https://wyvern.space/wp-content/uploads/2023/08/Wyvern-EULA-Public-Release-License-1.pdf
        return "Wyvern Space";
    }

    @Override
    protected SdcStatements getStatements(StacMedia media, FileMetadata metadata) {
        return super.getStatements(media, metadata)
                .creator(media.getIdUsedInOrg(), SAT_PATTERN, SATS) // Created by Wyvern-XX
                .locationOfCreation("Q663611") // Created in low earth orbit
                .fabricationMethod(Q725252_SATELLITE_IMAGERY);
    }

    @Override
    protected Set<String> getEmojis(StacMedia uploadedMedia) {
        Set<String> result = super.getEmojis(uploadedMedia);
        result.add(Emojis.EARTH_AMERICA);
        return result;
    }
}
