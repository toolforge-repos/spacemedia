package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api;

import java.util.List;

public record MedihalSearchResponse(int numFound, int start, double maxScore, boolean numFoundExact,
        List<MedihalDoc> docs) {

}
