package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.library;

public interface NasaVideoRepository extends NasaMediaRepository<NasaVideo> {

}
