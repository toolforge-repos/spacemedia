package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public record SearchQuery (

    @JsonProperty("searchinfo")
    SearchInfo searchInfo,

    Map<Long, WikiPage> pages) {

}
