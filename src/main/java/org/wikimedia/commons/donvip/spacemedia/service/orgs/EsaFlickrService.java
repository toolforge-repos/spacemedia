package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.wikimedia.commons.donvip.spacemedia.utils.CsvHelper.loadCsvMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.utils.Utils;

@Service
public class EsaFlickrService extends AbstractOrgFlickrService {

    private Map<String, String> esaMissions;
    private Map<String, String> esaPeople;

    @Autowired
    public EsaFlickrService(FlickrMediaRepository repository,
            @Value("${esa.flickr.accounts}") Set<String> flickrAccounts) {
        super(repository, "esa.flickr", flickrAccounts);
    }

    @Override
    @PostConstruct
    void init() throws IOException {
        super.init();
        esaMissions = loadCsvMapping("esa.missions.csv");
        esaPeople = loadCsvMapping("esa.people.csv");
    }

    @Override
    public String getName() {
        return "ESA (Flickr)";
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "ESA Flickr files uploaded by " + commonsService.getAccount();
    }

    @Override
    protected List<String> getReviewCategories(FlickrMedia media) {
        List<String> result = new ArrayList<>(super.getReviewCategories(media));
        result.add("ESA images (review needed)");
        return result;
    }

    @Override
    public Set<String> findCategories(FlickrMedia media, FileMetadata metadata, boolean includeHidden) {
        Set<String> result = super.findCategories(media, metadata, includeHidden);
        // Try to find any ESA mission or people in the description and title.
        // Filters in the description search to minimize false positives such as Herschel, Galileo...
        String titleLc = media.getTitle().text().toLowerCase(Locale.ENGLISH);
        String descriptionLc = Optional.ofNullable(media.getDescription().text()).orElse("")
                .toLowerCase(Locale.ENGLISH);
        for (Map<String, String> mapping : Arrays.asList(esaMissions, esaPeople)) {
            for (Entry<String, String> entry : mapping.entrySet()) {
                String key = entry.getKey();
                String keyLc = entry.getKey().toLowerCase(Locale.ENGLISH);
                if (Utils.isTextFound(titleLc, keyLc)
                        || ((key.contains("-") || key.contains(" ")) && Utils.isTextFound(descriptionLc, keyLc))) {
                    result.add(entry.getValue());
                }
            }
        }
        EsaService.enrichEsaCategories(result, media, "");
        return result;
    }
}
