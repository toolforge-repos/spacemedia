package org.wikimedia.commons.donvip.spacemedia.data.commons;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * <a href="https://www.mediawiki.org/wiki/Manual:Actor_table">Mediawiki Actor
 * table</a>
 *
 * <pre>
 * +------------+---------------------+------+-----+---------+----------------+
 * | Field      | Type                | Null | Key | Default | Extra          |
 * +------------+---------------------+------+-----+---------+----------------+
 * | actor_id   | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
 * | actor_user | int(10) unsigned    | YES  | UNI | NULL    |                |
 * | actor_name | varbinary(255)      | NO   | UNI | NULL    |                |
 * +------------+---------------------+------+-----+---------+----------------+
 * </pre>
 */
@Entity
@Table(name = "actor")
public class CommonsActor {

    @Id
    @GeneratedValue
    @Column(name = "actor_id", nullable = false, length = 20)
    private long id;

    @Column(name = "actor_user", nullable = true, length = 10, unique = true)
    private int user;

    @Column(name = "actor_name", nullable = false, length = 255, unique = true)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, user);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        CommonsActor other = (CommonsActor) obj;
        return id == other.id && Objects.equals(name, other.name) && user == other.user;
    }

    @Override
    public String toString() {
        return "CommonsActor [id=" + id + ", user=" + user + ", " + (name != null ? "name=" + name : "") + "]";
    }
}
