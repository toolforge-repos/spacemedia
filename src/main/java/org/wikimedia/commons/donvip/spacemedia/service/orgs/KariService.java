package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static java.util.Collections.emptyList;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.getWithJsoup;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionException;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.kari.KariMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.kari.KariMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.MediaService.MediaUpdateResult;
import org.wikimedia.commons.donvip.spacemedia.service.wikimedia.GlitchTip;
import org.wikimedia.commons.donvip.spacemedia.utils.Emojis;

@Service
public class KariService extends AbstractOrgService<KariMedia> {

    private static final Logger LOGGER = LoggerFactory.getLogger(KariService.class);

    @Autowired
    public KariService(KariMediaRepository repository) {
        super(repository, "kari", Set.of("kari"));
    }

    @Value("${kari.view.link}")
    private String viewLink;

    @Value("${kari.kogl.type1}")
    private String koglType1Icon;

    @Value("${kari.max.failures:50}")
    private int maxFailures;

    @Override
    public final String getName() {
        return "KARI";
    }

    @Override
    protected Class<KariMedia> getMediaClass() {
        return KariMedia.class;
    }

    @Override
    public final URL getSourceUrl(KariMedia media, FileMetadata metadata, String ext) {
        return newURL(getViewUrl(media.getId().mediaId()));
    }

    @Override
    public final String getSource(KariMedia media, FileMetadata metadata) {
        return "{{KARI-source|" + media.getId().mediaId() + "|" + media.getKariId() + "}}";
    }

    @Override
    protected final String getAuthor(KariMedia media, FileMetadata metadata) {
        return "{{Creator:KARI}}";
    }

    private String getViewUrl(String id) {
        return viewLink.replace("<id>", id);
    }

    @Override
    protected boolean checkBlocklist(KariMedia media) {
        return false;
    }

    @Override
    protected String hiddenUploadCategory(String repoId) {
        return "Spacemedia KARI files uploaded by " + commonsService.getAccount();
    }

    @Override
    public Set<String> findLicenceTemplates(KariMedia media, FileMetadata metadata) {
        Set<String> result = super.findLicenceTemplates(media, metadata);
        result.add("KOGL");
        return result;
    }

    @Override
    public void updateMedia(String[] args) throws IOException {
        startUpdateMedia();
        int consecutiveFailures = 0;
        int count = 0;
        String id = repository.findFirst(getRepoIds()).map(Media::getIdUsedInOrg).orElse("1");
        while (consecutiveFailures < maxFailures) {
            boolean save = false;
            String viewUrl = getViewUrl(id);
            URL view = newURL(viewUrl);
            Optional<KariMedia> mediaInRepo = repository.findById(new CompositeMediaId("kari", id));
            KariMedia media = mediaInRepo.orElse(null);
            if (media == null) {
                MediaUpdateResult<KariMedia> result = fetchMedia(id);
                media = result.media();
                save = result.result();
                if (result.resetConsecutiveFailures()) {
                    consecutiveFailures = 0;
                } else if (result.incrementConsecutiveFailures()) {
                    consecutiveFailures++;
                }
            }
            if (media != null) {
                try {
                    processMedia(save, media, view, mediaInRepo.isPresent());
                    count++;
                } catch (TransactionException e) {
                    LOGGER.error("Transaction error when saving {}", media, e);
                    GlitchTip.capture(e);
                }
            }
            id = Integer.toString(Integer.valueOf(id) + 1);
        }
        endUpdateMedia(count, emptyList(), emptyList());
    }

    private MediaUpdateResult<KariMedia> fetchMedia(String id) {
        String viewUrl = getViewUrl(id);
        URL view = newURL(viewUrl);
        KariMedia media = null;
        boolean resetConsecutiveFailures = false;
        boolean incrementConsecutiveFailures = false;
        Exception ex = null;
        try {
            Element div = getWithJsoup(viewUrl, 60_000, 3).getElementsByClass("board_view").get(0);
            String title = div.getElementsByTag("h4").get(0).text();
            if (!title.isEmpty()) {
                resetConsecutiveFailures = true;
                Element infos = div.getElementsByClass("photo_infor").get(0);
                Elements lis = infos.getElementsByTag("li");
                if (lis.get(lis.size() - 1).getElementsByTag("img").attr("src").endsWith(koglType1Icon)) {
                    media = buildMedia(id, view, div, title, infos, lis);
                } else {
                    LOGGER.info("Media {} exists but appears to be non-free (Not KOGL type 1)", id);
                }
            } else {
                incrementConsecutiveFailures = true;
            }
        } catch (DateTimeParseException e) {
            LOGGER.error("Cannot parse HTML", e);
            GlitchTip.capture(e);
            ex = e;
        } catch (IOException e) {
            problem(view, e);
            incrementConsecutiveFailures = true;
            GlitchTip.capture(e);
            ex = e;
        }
        return new MediaUpdateResult<>(media, null, media != null, resetConsecutiveFailures,
                incrementConsecutiveFailures, ex);
    }

    protected KariMedia processMedia(boolean save, KariMedia media, URL view, boolean mediaInRepo) throws IOException {
        if (StringUtils.isBlank(media.getDescription())) {
            problem(view, "Empty description");
        }
        FileMetadata metadata = media.getMetadata().iterator().next();
        if (metadata.getAssetUrl() != null) {
            String mediaUrl = metadata.getAssetUrl().toExternalForm();
            if (StringUtils.isBlank(mediaUrl) || "https://www.kari.re.kr".equals(mediaUrl)) {
                metadata.setAssetUrl(null);
            }
        }
        if (metadata.getAssetUrl() == null) {
            problem(view, "No download link");
            save = false;
            if (mediaInRepo) {
                deleteMedia(media, "No download link");
            }
        }
        if (doCommonUpdate(media)) {
            save = true;
        }
        return saveMediaOrCheckRemote(save, media);
    }

    private KariMedia buildMedia(String id, URL view, Element div, String title, Element infos, Elements lis) {
        KariMedia media = new KariMedia();
        media.setId(new CompositeMediaId("kari", id));
        media.setKariId(lis.get(0).getElementsByClass("txt").get(0).text());
        media.setTitle(new LocalizedText("ko", title));
        media.setPublicationDate(LocalDate.parse(div.getElementsByClass("infor").get(0).getElementsByTag("li")
                .get(0).getElementsByClass("txt").get(0).text()));
        media.setDescription(new LocalizedText("ko", div.getElementsByClass("photo_txt").get(0).text()));
        String href = infos.getElementsByTag("a").attr("href");
        if (StringUtils.isNotBlank(href)) {
            addMetadata(media, newURL(view.getProtocol(), view.getHost(), href), null);
        }
        String src = div.getElementsByClass("board_txt").get(0).getElementsByTag("img").attr("src").replace("/view/",
                "/lst/");
        if (StringUtils.isNotBlank(src)) {
            media.setThumbnailUrl(newURL(view.getProtocol(), view.getHost(), src));
        }
        return media;
    }

    @Override
    protected boolean isPermittedFileType(FileMetadata metadata) {
        return metadata.getAssetUrl() != null && metadata.getAssetUrl().toExternalForm()
                .startsWith("https://www.kari.re.kr/image/kari_image_down.do?");
    }

    @Override
    protected KariMedia refresh(KariMedia media) throws IOException {
        KariMedia mediaFromApi = fetchMedia(media.getId().mediaId()).media();
        return mediaFromApi != null ? media.copyDataFrom(mediaFromApi) : null;
    }

    @Override
    protected Set<String> getEmojis(KariMedia uploadedMedia) {
        return Set.of(Emojis.FLAG_KOR);
    }
}
