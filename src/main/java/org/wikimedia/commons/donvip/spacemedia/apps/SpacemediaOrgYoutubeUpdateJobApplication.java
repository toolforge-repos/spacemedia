package org.wikimedia.commons.donvip.spacemedia.apps;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.Media;
import org.wikimedia.commons.donvip.spacemedia.data.domain.youtube.YouTubeMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.youtube.YouTubeMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.orgs.Org;

@ComponentScan(basePackages = "org.wikimedia.commons.donvip.spacemedia.service.youtube", excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".+Test.*"))
@EnableJpa(basePackageClasses = { Media.class, YouTubeMedia.class })
public class SpacemediaOrgYoutubeUpdateJobApplication extends AbstractSpacemediaOrgUpdateJobApplication {

    public static void main(String[] args) {
        run(app(SpacemediaOrgYoutubeUpdateJobApplication.class), args);
    }

    @Bean
    @SuppressWarnings("unchecked")
    public Org<YouTubeMedia> org(@Value("${org}") String org,
            @Value("${youtube.channels}") Set<String> youtubeChannels,
            @Autowired YouTubeMediaRepository repository,
            ApplicationContext context) throws ReflectiveOperationException {
        return (Org<YouTubeMedia>) Class.forName(org).getConstructor(YouTubeMediaRepository.class, Set.class)
                .newInstance(repository, youtubeChannels);
    }
}
