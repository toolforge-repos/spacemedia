package org.wikimedia.commons.donvip.spacemedia.service.synology.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SharingList extends PaginatedList<SharingLink> {

    @JsonCreator
    public SharingList(@JsonProperty("total") int total, @JsonProperty("offset") int offset,
            @JsonProperty("links") List<SharingLink> links) {
        super(total, offset, links);
    }
}
