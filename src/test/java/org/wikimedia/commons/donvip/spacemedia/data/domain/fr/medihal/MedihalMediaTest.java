package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaDimensions;

class MedihalMediaTest {

    @Test
    void testGetSearchTermsInCommons() {
        FileMetadata fm = new FileMetadata("https://hal.science/hal-02525318/file/ifpo_20037bis.jpg");
        fm.setMediaDimensions(new MediaDimensions(1842, 1503));
        fm.setPhash("2rdgfkcco20jd1kfxj3fa2mhd6ee75un9prgkii3n3r0100wtb");
        fm.setReadable(Boolean.TRUE);
        fm.setSha1("10566c1d33806ec18cd43a9e55d39c3f4853195c");
        fm.setSize(484702L);

        ZonedDateTime date = ZonedDateTime.parse("1935-04-01T00:00:00Z");
        MedihalMedia media = new MedihalMedia();
        media.setId(new CompositeMediaId("medihal", "2525318"));
        media.setCreationDateTime(date);
        media.setPublicationDateTime(date);
        media.setCredits("Institut Français Du Proche-Orient (ifpo), Armée Du Levant");
        media.setDescription(new LocalizedText("fr", "13x18 cm, gélatine plan-film"));
        media.setThumbnailUrl("https://thumb.ccsd.cnrs.fr/8595603/medium");
        media.setTitle(new LocalizedText("fr",
                "Syrie, gouvernorat de Homs, district de Tadmor, ruines de Palmyre, vue aérienne oblique"));
        media.setDocType(MedihalDocType.IMG);
        media.setDocSubType(MedihalDocSubType.PHOTOGRAPHY);
        media.setHalId("hal-02525318");
        media.setLicenceUrl(newURL("http://hal.archives-ouvertes.fr/licences/etalab/"));
        media.setUri(newURL("https://hal.science/hal-02525318v1"));
        media.setLatitude(34.559162942435);
        media.setLongitude(38.30680602999);

        assertEquals(
                List.of("2525318", "hal-02525318", "13x18 cm, gélatine plan-film",
                        "https://hal.science/hal-02525318/file/ifpo_20037bis.jpg"),
                media.getSearchTermsInCommons(List.of(fm)));
    }
}
