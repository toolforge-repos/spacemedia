package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.flickr.FlickrMediaProcessorService;
import org.wikimedia.commons.donvip.spacemedia.service.flickr.FlickrService;

@SpringJUnitConfig(UsEmbassiesFlickrServiceTest.TestConfig.class)
class UsEmbassiesFlickrServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private FlickrMediaRepository repository;

    @MockBean
    private FlickrMediaProcessorService processor;

    @Autowired
    private UsEmbassiesFlickrService service;

    @Test
    void testUploadTitleWithoutAnything() {
        FlickrMedia media = new FlickrMedia();
        media.setTitle(new LocalizedText("en", ""));
        media.setId(new CompositeMediaId("101399499@N08", "9685869252"));
        assertEquals("UNITED STATES EMBASSY JUBA (9685869252)",
                media.getUploadTitle(new FileMetadata(), () -> service.getOrgName(media)));
    }

    @Test
    void testGetUiRepoId() throws IOException {
        when(wikidata.searchFlickrUser(any())).thenCallRealMethod();
        when(wikidata.findIso3166Alpha2(any())).thenCallRealMethod();
        assertEquals("🇸🇸", service.getUiRepoId("101399499@N08"));
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public FlickrService flickr(@Value("${FLICKR_API_KEY}") String flickrApiKey,
                @Value("${FLICKR_SECRET:xxx}") String flickrSecret) {
            return new FlickrService(flickrApiKey, flickrSecret);
        }

        @Bean
        public UsEmbassiesFlickrService service(FlickrMediaRepository repository,
                @Value("${usembassies.flickr.accounts}") Set<String> flickrAccounts) {
            return new UsEmbassiesFlickrService(repository, flickrAccounts);
        }
    }
}
