package org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

class MedihalSearchResponsePayloadTest {

    @Test
    void testJsonDeserialization() throws Exception {
        assertNotNull(new ObjectMapper().registerModules(new Jdk8Module(), new JavaTimeModule()).readValue(
                new File("src/test/resources/medihal/api.search.01.json"),
                MedihalSearchResponsePayload.class));
    }
}
