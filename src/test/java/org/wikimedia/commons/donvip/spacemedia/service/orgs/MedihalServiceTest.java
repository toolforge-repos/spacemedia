package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.MedihalMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api.MedihalDoc;
import org.wikimedia.commons.donvip.spacemedia.data.domain.fr.medihal.api.MedihalSearchResponsePayload;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
@SpringJUnitConfig(MedihalServiceTest.TestConfig.class)
class MedihalServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private MedihalMediaRepository repository;

    @Autowired
    private MedihalService service;

    @Test
    void testGetName() {
        assertEquals("MédiHAL", service.getName());
    }

    @Test
    void testMapMedia() throws Exception {
        when(metadataRepository.save(any(FileMetadata.class))).thenAnswer(a -> a.getArgument(0, FileMetadata.class));
        for (MedihalDoc doc : new ObjectMapper().registerModules(new Jdk8Module(), new JavaTimeModule())
                .readValue(new File("src/test/resources/medihal/api.search.01.json"),
                        MedihalSearchResponsePayload.class)
                .response().docs()) {
            MedihalMedia media = service.mapMedia(doc);
            assertNotNull(media);
            assertTrue(isNotBlank(media.getIdUsedInOrg()));
            assertTrue(isNotBlank(media.getTitle()));
            assertTrue(isNotBlank(media.getDescription()));
            assertTrue(isNotBlank(media.getCredits()));
            assertNotNull(media.getLicenceUrl());
            assertNotNull(media.getUri());
            assertNotNull(media.getDocType());
            assertNotNull(media.getCreationDateTime());
            assertNotNull(media.getPublicationDateTime());
        }
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public MedihalService service(MedihalMediaRepository repository) {
            return new MedihalService(repository);
        }
    }
}
