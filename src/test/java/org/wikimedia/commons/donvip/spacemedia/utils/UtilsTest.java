package org.wikimedia.commons.donvip.spacemedia.utils;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URI;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;

class UtilsTest {

    @Test
    void testUriExists() {
        assertFalse(Utils.uriExists(
                "https://www.nasa.gov/image-detail/gemini_6_7_borman_schirra_goodwill_tour_manilla_presser_march_1966"));
        assertTrue(Utils.uriExists("https://www.nasa.gov"));
    }

    @ParameterizedTest
    @CsvSource(delimiter = '|', value = { "|",
            "U.S. Space Force Sgt. Sergkei Triantafyllidis, a technical training instructor with the United States Air Force Honor Guard, looks on as Honor Guard trainees of Class 24D work on their drill movements on Joint Base Anacostia-Bolling, Washington, D.C., July 22, 2024. The class made history on Aug. 16, 2024, as the first joint class of trainees from two military branches to graduate U.S. Air Honor Guard technical training. (U.S. Air Force photo by Robert W. Mitchell)|U.S. Space Force Sgt. Sergkei Triantafyllidis, a technical training instructor with the United States Air Force Honor Guard, looks on as Honor Guard trainees of Class 24D work on their drill movements on Joint Base Anacostia-Bolling, Washington, D.C., July 22, 2024",
            "Charles F. Hall, Pioneer Project Manager in his office|Charles F. Hall, Pioneer Project Manager in his office",
            "H. Julian 'Harvey' Allen at his retirement party in the hangar|H. Julian 'Harvey' Allen at his retirement party in the hangar",
            "iss071e650313 (Sept. 14, 2024) --- Expedition 71 Flight Engineer (from left) Don Pettit from NASA demonstrates filling a specially-made coffee cup from a drink bag to Expedition 71 Flight Engineer Alexey Ovchinin from Roscosmos. The coffee stays inside the cup due to surface tension, a property that enables liquids to resist an external force, such as microgravity, and adhere, or stick to a surface.|iss071e650313 (Sept. 14, 2024) --- Expedition 71 Flight Engineer (from left) Don Pettit from NASA demonstrates filling a specially-made coffee cup from a drink bag to Expedition 71 Flight Engineer Alexey Ovchinin from Roscosmos" })
    void testGetFirstSentence(String desc, String expected) {
        if (isBlank(desc)) {
            assertEquals("", Utils.getFirstSentence(null));
            assertEquals("", Utils.getFirstSentence(new LocalizedText("en", null)));
            assertEquals("", Utils.getFirstSentence(new LocalizedText("en", "")));
            assertEquals("", Utils.getFirstSentence(new LocalizedText("en", " ")));
        } else {
            assertEquals(expected, Utils.getFirstSentence(new LocalizedText("en", desc)));
        }
    }

    @Test
    void testNewURL() {
        assertEquals("https://foo/bar", Utils.newURL("https", "foo", "/bar").toExternalForm());
        assertEquals(
                "https://umbra-open-data-catalog.s3.amazonaws.com//sar-data/tasks/ad%5c%20hoc/test,%20space/4ad85765-bdab-4fa3-b513-05f760a824b2/2023-02-26/4ad85765-bdab-4fa3-b513-05f760a824b2/2023-02-26-02-14-07_UMBRA-05.tif",
                Utils.newURL(
                        "https://umbra-open-data-catalog.s3.amazonaws.com//sar-data/tasks/ad\\ hoc/test, space/4ad85765-bdab-4fa3-b513-05f760a824b2/2023-02-26/4ad85765-bdab-4fa3-b513-05f760a824b2/2023-02-26-02-14-07_UMBRA-05.tif")
                .toExternalForm());
        assertEquals(
                "https://umbra-open-data-catalog.s3.amazonaws.com/sar-data/tasks/Volcanoes/Etorofu-Atosanupuri%20%5bAtosanupuri%5d,%20Japan%20-%20administered%20by%20Russia/b5587b63-2fa7-4d0d-9639-951b385c8964/2023-11-16-10-56-03_UMBRA-04/2023-11-16-10-56-03_UMBRA-04_GEC.tif",
                Utils.newURL(
                        "https://umbra-open-data-catalog.s3.amazonaws.com/sar-data/tasks/Volcanoes/Etorofu-Atosanupuri [Atosanupuri], Japan - administered by Russia/b5587b63-2fa7-4d0d-9639-951b385c8964/2023-11-16-10-56-03_UMBRA-04/2023-11-16-10-56-03_UMBRA-04_GEC.tif")
                        .toExternalForm());
    }

    @ParameterizedTest
    @CsvSource({
            "https://public.boxcloud.com/d/1/b1!t13nV2jTAqeqKmeN2kIA8Xg8w8A2UlnZhNdHpKh90VcBJh_vh5VR69Oiiin3HRu2urkJKhCG2_fJ6eXIF4UHZoNj2vr224GZBmkyBUlOSLiOdLBppME43Vj2LR4p1sQ8cH3YlhHrRiGOqd0-quuPqzNUPxX-x4XPbfJFzBWOj8cBgMZqrfXA_WTQ7WEfbDqnD4bTHrK9D0rOio3r9KLSOWOax0C_OXzw5NVv9H9iPsfxb4kW93gX_RNPUkzNoVIJKvHyiJLmFRQbAIostXnK59n338lKc-WHXG_HYsrAX3kumHXkUu9I5iL0hXO95KGp1R-5H1pO_WX0gXI4Isl9bdGUmU2GQllKnAhfa3zKZ9WLQqvn2vaIkuk8fRBiDjqw31nhckdHu37ZZe_LYydVQX94FPOgOg_0buqTmPYu6HX1VlxbXqSe-T2q_B_pwdUQmFd6PnJYAlAcwPAFUjvdcnm1pPJPVForRmcL63kv5Zfk3wDDf_VMQCWMyikQitkBzYETki0rz4_62eFT5U1pvjWQte9nPv5eBVvgziAxi09Qsld9CuFaHFOXiIus6zS2jwJmdh2_HC_eiX0aJG0vVn7SnGicXUz9iI53-H-_4oMGAIic6LpxTbtzrJw7PHqg3eUk1ESN4HrlyfDohJRpzPIcC7vDLMvZSCEvgpZjvMpJy2TvjBjqCW-YlWD7TV-E_6w_hF3Cna_OcAI_RsC1J8R7TRzZXc6ZKk8oDmZ4wtoC-hYETjEsLdcmQwwQB6IYD9bvJHawqrYJ_cyJW3W58_UKqvDE-qja17Uc2tAmu5i6W74kstY2zxkLEgPdcyVr2B7Awzl1Y1mnrrzwKWV2af53sl4x1Ysqweh-3BBZwvgM8bCtiXwa0L4c05wTqAb9avZYdI0QkLVppu06uV2yswlh3A1HVYTpx04YJDLgNv3BWqkkFL_bBlZIKw2q01zqEnBrojpt4S4-I4ZUC5VAjWOcVsdRLR3_URsgk3MZzv8aMjNCnUsPE_uXrAXz3mwQ65KxvIIW5IlPEjFwJ70wjXsERiIynVv-L59Wzte26l8CW9qjD9guLazGNHM9i53fsWdNvUOOLtJIZ_xsE-Fd5gjZj1psH4dfA8BJDI8hcDlixo3tJWVs92IaFPuviy5d2txFkROrWTmIr5OeXee379pA45HA15S4ElzeU7iYswQBiwzYRYXxOnKQYqNmNZjdkboXO-51PwXyAD_AcNCWnzvJnvX8lrEf6g0J8i0HRn4cfwGniR41vVsva90YLPDEVFPl-sbjMu1pUKK0k41sEOW8pMCYvKPx3ADFIBbC5KmMS5RD96J0lHbdpM_mc859G2pbOPCccLBAOcrc3rbUBOagtxlRYF2U3Gt_yx6ap4rDHJThCpX6jDsMCQilf4lHE0ZBtZ2yXifmKwGSwUU1msZUGBa7hxDLdZuvy3rCfOthLKnH4wk6FIsQvocJdxri-HOgPzzIgYHtuAZicu5N/download",
            "https://public.boxcloud.com/d/1/b1!zIfRCUQ-0ssFX7MyTg7GtazT6FXYs7wDCYu0WzwcxVKmlVhfcynwuGELg6-2HU5Pd7TWSFBo3-zPZAZQoTXoAcVShSmnJcPvm7Hf7XkrIenNJs6cwL3sD9IpmBh_Ix_G5q2FTbDlWgPOA0hGH__QrXwtjAjQ7LJA7bSEwiHimCKkvcFrfR6Yt1xcJHNUGaj6hmlZ9hfs9VyPNhR4ft2tMQx57PIKrLVicXUCTqgnSHMLPtdxt-1p3Asb7FhZwVWt6_Buma5-L4Dv-Rxh-9WUqcuWNp30vH2fNa8MqEf-muWvIvU_Ku9bG73xzX2rKKI955hNiKgyh9Sg1t_nqx7CnD-WcqH3Rjk7WclkjPgA_1z_AweFabuGCrCLLT1SSwZaXyW_96-o0nkBpRlysVDJ_CbBnhH8dzhfG1LUnYXQt030TilvX6sdVMxMgww2GUNBy8uY-KoAV2x54S8pAJv1Gd9vG201owk9DuRBktXT69SiIs09xa0PsfrsfAJaBKg2fMiMDkHARRyHTgHRK5WaloqAQ66Hoo4wOkzwgyGRN0B3Vz-1XsJGBI9OOIRh97I0sWJhwNMCGQNvHp32yMaYrKerulnAhBww5EHWXAxeNaYAPNgihgMI8Szth-lxFdwbm8cabxf6u4ZgAbFY_a5V5eCYPm-VgE23U_mHNwLU19qy8nBv7LO-XMgxP7fPqTynOE9k8es4Rsp69OPCHA30clLbMcWcjXMO4NjOyk9kzy6FhWH9OvyZheDUgydbywc2NZ0dAw8d1V9hQHm7ME7_mUGyHYFqHbxPoktD_4XqV67y446bOSrC_Y6tI3BRErmnwhOKO0l7uxD7_sUdD5ct09wcxrIxr_N6XJZvufyqF-oZEC0OysP5-QUYOFe_89NvRwZi9SfMyGBSaroepl_MveucZcAyx7fh80DGNF2HovysGbbdvNvqd0npXA5LSl9wpZ0OaERQQaLupTf60esFXoykWZOL673eIu3ot2umGSgJqpHyoeEgkMtE4At1uK3LFlAjoSOn8jCHN4JLZ25OZalwK37DrmfU6FboLhwPRckKQKg--Ze29rIFcEeHgbqHtDO2cUkHXAg0AuAzJyeYCn9rFRpyLCyp2J0aMBVb5UoLU0bH5WI-3xrM1FuhEdow0ioIKSyHWJXJth7pfALNN4EmM6gaZYGUg_GUz9v3g0l3ChtCOf7h-OhcuV7sSrHHCwB-b9U8AZ0_ah9hwhBcbPcR4YwnGTADk1hCO4vvzFUZRnnA58D9A1MVgXgxDVDsO7bMUmYN4yEbO2AfCB79xxrThBXhWs2mYb5fLT1gjxEirr05eBq6U97p33kxqEQfGgTtizzxm5Xrt8n5WX7E4QOUD38OVkw2f78-rwChsl_MhnVkr1UPnJ9OOe7iHebT5pSA694-FZeT_JKFQvxinKcq8Vhg7C5ztI-mqKys0TAwIY2Qu-fQznGtrmLKZYdM-jnYev45KYuK-lNQi2X5CA../download" })
    void testUrlToUri(URI uri) throws MalformedURLException {
        assertEquals(uri, Utils.urlToUriUnchecked(uri.toURL()));
    }
}
