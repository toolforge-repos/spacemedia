package org.wikimedia.commons.donvip.spacemedia.data.domain.nasa.aster;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;

class NasaAsterMediaTest {

    @Test
    void testGetUploadTitleOneImage() {
        NasaAsterMedia media = new NasaAsterMedia();
        FileMetadata m1 = new FileMetadata("https://asterweb.jpl.nasa.gov/gallery/images/2017sarknat.jpg");
        media.addMetadata(m1);
        media.setTitle(new LocalizedText("en", "Sark, English Channel Islands"));

        assertEquals("Sark, English Channel Islands (ASTER)", media.getUploadTitle(m1, () -> ""));
    }

    @Test
    void testGetUploadTitleTwoImages() {
        NasaAsterMedia media = new NasaAsterMedia();
        FileMetadata m1 = new FileMetadata("https://asterweb.jpl.nasa.gov/gallery/images/istanbul.jpg");
        FileMetadata m2 = new FileMetadata("https://asterweb.jpl.nasa.gov/gallery/images/istanbul-city.jpg");
        media.addMetadata(m1);
        media.addMetadata(m2);
        media.setTitle(new LocalizedText("en", "Istanbul"));

        assertEquals("istanbul (ASTER)", media.getUploadTitle(m1, () -> ""));
        assertEquals("istanbul-city (ASTER)", media.getUploadTitle(m2, () -> ""));
    }
}
