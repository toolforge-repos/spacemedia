package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.MediaDimensions;
import org.wikimedia.commons.donvip.spacemedia.data.domain.djangoplicity.DjangoplicityMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.data.domain.djangoplicity.DjangoplicityMediaType;

@SpringJUnitConfig(WebbEsaServiceTest.TestConfig.class)
class WebbEsaServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private DjangoplicityMediaRepository repository;

    @Autowired
    private WebbEsaService service;

    @ParameterizedTest
    @CsvSource(delimiter = '|', value = {
            "WLMb|images|Observation|2022-11-09T17:00|4134|4134|[PGU2007] cep35||Galaxies,NIRCam|NASA, ESA, CSA, STScI, and K. McQuinn (Rutgers University), A. Pagan (STScI).|https://esawebb.org/media/archives/images/original/WLMb.tif,https://cdn.esawebb.org/archives/images/large/WLMb.jpg|Dwarf Galaxy WLM|This image shows a portion of the dwarf galaxy Wolf–Lundmark–Melotte (WLM) captured by the NASA/ESA/CSA James Webb Space Telescope’s <a href=\"https://esawebb.org/about/instruments/nircam-niriss/\">Near-Infrared Camera</a>. The image demonstrates Webb’s remarkable ability to resolve faint stars outside the Milky Way.&nbsp;This observation was taken as part of Webb’s Early Release Science (ERS) program&nbsp;<a href=\"https://www.stsci.edu/jwst/science-execution/program-information.html?id=1334\">1334</a>, focused on resolved stellar populations. The dwarf galaxy WLM was selected for this program as its gas is similar to that which made up galaxies in the early Universe and it is relatively nearby, meaning that Webb can differentiate between its individual stars.&nbsp;Learn more about Webb’s research of the dwarf galaxy WLM <a href=\"https://blogs.nasa.gov/webb/2022/11/09/beneath-the-night-sky-in-a-galaxy-not-too-far-away\">here</a>.The galaxy lies roughly 3 million light-years away.This image includes 0.9-micron light shown in blue, 1.5-micron in cyan, 2.5-micron in yellow, and 4.3-micron in red (filters F090W, F150W, F250M, and F430M).&nbsp;Note: This image highlights Webb’s science in progress, which has not yet been through the peer-review process.[Image Description: This image shows a wide field view of countless stars and dozens of galaxies in clear detail.]|James Webb Space Telescope|NIRCam|Cetus|https://cdn.esawebb.org/archives/images/screen/WLMb.jpg",
            "weic2423a|images|Observation|2024-09-18T16:00|10427|4161|Arp 107||Galaxies,MIRI,NIRCam|NASA, ESA, CSA, STScI|https://esawebb.org/media/archives/images/original/weic2423a.tif,https://cdn.esawebb.org/archives/images/large/weic2423a.jpg|Arp 107 composite image (NIRCam + MIRI)|This composite image of Arp 107, created with data from the James Webb Space Telescope’s NIRCam (Near-InfraRed Camera) and MIRI (Mid-InfraRed Instrument), reveals a wealth of information about the star formation taking place in these two galaxies and how they collided hundreds of million years ago.The near-infrared data, shown in white, show older stars, which shine brightly in both galaxies, as well as the tenuous gas bridge that runs between them. The vibrant background galaxies are also brightly illuminated at these wavelengths.On the other hand, MIRI data show the young stars and star-forming regions in vibrant orange and red. Our view in the mid-infrared provides the best view of the collision point, given the noticeable gap at the top of the spiral galaxy. This collision not only began a new bout of star formation in the region, but also produced an endearing smile.[Image description: A pair of interacting galaxies. The larger of the two galaxies is slightly right of centre, and is composed of a hazy, bright, white centre and a ring of gaseous filaments, which are different shades of red and orange. Toward the bottom left and bottom right of the ring are filaments of gas spiralling inward toward the core. At the top left of the ring is a noticeable gap, bordered by two large, orange pockets of dust and gas. The smaller galaxy is made of hazy and white gas and dust, which become more diffuse further away from its centre. To this galaxy’s bottom left, there is a smaller, more diffuse gas cloud that wafts outward toward the edges of the image. Many red, orange, and white galaxies are spread throughout, with some being hazier in appearance and others having more defined spiral patterns.]|James Webb Space Telescope|MIRI,NIRCam|Leo Minor|https://cdn.esawebb.org/archives/images/screen/weic2423a.jpg" })
    void testReadHtml(String id, String type, DjangoplicityMediaType imageType, String date, int w, int h, String name,
            @ConvertWith(SetArgumentConverter.class) Set<String> types,
            @ConvertWith(SetArgumentConverter.class) Set<String> categories, String credit,
            @ConvertWith(ListArgumentConverter.class) List<String> assetUrls, String title, String description,
            @ConvertWith(SetArgumentConverter.class) Set<String> telescopes,
            @ConvertWith(SetArgumentConverter.class) Set<String> instruments, String constellation, String thumbnailURL)
            throws Exception {
        when(metadataRepository.save(any(FileMetadata.class))).thenAnswer(a -> a.getArgument(0, FileMetadata.class));
        doDjangoplicityMediaTest(
                service.newMediaFromHtml(html("esawebb/" + type + '_' + id + ".html"),
                        newURL("https://esawebb.org/" + type + "/" + id + "/"), id, null),
                id, imageType, date, w > 0 && h > 0 ? new MediaDimensions(w, h) : null, name, types, categories, credit,
                assetUrls, title, description, telescopes, instruments, constellation, thumbnailURL);
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public WebbEsaService service(DjangoplicityMediaRepository repository,
                @Value("${webb.esa.search.link}") String searchLink) {
            return new WebbEsaService(repository, searchLink);
        }
    }
}
