package org.wikimedia.commons.donvip.spacemedia.data.domain.ifremer.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

class IfremerSearchRequestTest {

    @Test
    void testJsonSerialization() throws Exception {
        assertEquals("""
                {
                  "criteriaList" : [ {
                    "field" : "licence",
                    "name" : "Licence",
                    "options" : [ "TRANSLATED", "NESTED" ],
                    "types" : [ "FACET" ],
                    "values" : [ {
                      "code" : "CC0",
                      "n" : 0,
                      "name" : "CC0"
                    }, {
                      "code" : "CC-BY",
                      "n" : 0,
                      "name" : "CC-BY"
                    } ],
                    "weight" : 1
                  }, {
                    "facetOrder" : 0,
                    "field" : "date",
                    "javaClass" : "java.util.Date",
                    "name" : "Date",
                    "options" : [ "SORTABLE_FIELD" ],
                    "order" : "DESC",
                    "shouldBehaviourCriteria" : false,
                    "sortPriority" : 0,
                    "types" : [ "SORT_ONLY" ],
                    "values" : [ ],
                    "weight" : 1
                  } ],
                  "defaultCriteriaValues" : { },
                  "groupedSearch" : true,
                  "languageEnum" : "fr",
                  "pagination" : {
                    "isPaginated" : true,
                    "page" : 1,
                    "size" : 60
                  }
                }""".replace("\r", ""), new ObjectMapper().registerModules(new Jdk8Module(), new JavaTimeModule())
                .writer().withDefaultPrettyPrinter().writeValueAsString(new IfremerSearchRequest(1)).replace("\r", ""));
    }
}
