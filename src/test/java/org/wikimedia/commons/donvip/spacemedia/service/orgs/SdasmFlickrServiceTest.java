package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.flickr.FlickrMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.flickr.FlickrMediaProcessorService;
import org.wikimedia.commons.donvip.spacemedia.service.flickr.FlickrService;

@SpringJUnitConfig(SdasmFlickrServiceTest.TestConfig.class)
class SdasmFlickrServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private FlickrMediaRepository repository;

    @MockBean
    private FlickrMediaProcessorService processor;

    @MockBean
    private FlickrService flickr;

    @Autowired
    private SdasmFlickrService service;

    @Test
    void testGetCreationDate() {
        FlickrMedia media = new FlickrMedia();
        media.setId(new CompositeMediaId("sdasmarchives", "54235038712"));
        media.setTitle(new LocalizedText("en", "23_0071214 Convair Negative Image"));
        media.setCreationDateTime(ZonedDateTime.parse("2024-12-30T07:49:02Z"));
        media.setPublicationDateTime(ZonedDateTime.parse("2024-12-30T15:57:16Z"));

        media.setDescription(
                new LocalizedText("en", "88714165 :Piction ID--General Dynamics F-111 drop test 04/26/1968"));
        assertEquals(LocalDate.parse("1968-04-26"), service.getCreationDate(media).get());

        media.setDescription(new LocalizedText("en", "foo"));
        assertTrue(service.getCreationDate(media).isEmpty());

        media.setDescription(null);
        assertTrue(service.getCreationDate(media).isEmpty());
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public SdasmFlickrService service(FlickrMediaRepository repository,
                @Value("${sdasm.flickr.accounts}") Set<String> flickrAccounts) {
            return new SdasmFlickrService(repository, flickrAccounts);
        }
    }
}
