package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.stac.StacMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.orgs.AbstractOrgStacService.StacCatalog;
import org.wikimedia.commons.donvip.spacemedia.service.orgs.AbstractOrgStacService.StacLink;

@SpringJUnitConfig(AbstractOrgStacServiceTest.TestConfig.class)
class AbstractOrgStacServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private StacMediaRepository repository;

    @Autowired
    private AbstractOrgStacService service;

    @ParameterizedTest
    @CsvSource({ "CAPELLA_C09_SM_GEC_VV_20231118132210_20231118132214",
            "CAPELLA_C14_SP_SLC_VV_20240414014317_20240414014346" })
    void testFetchCapellaMedia(String id) throws MalformedURLException {
        when(metadataRepository.save(any(FileMetadata.class))).thenAnswer(a -> a.getArgument(0, FileMetadata.class));
        assertNotNull(service.fetchStacMedia("capella",
                Path.of("src/test/resources/capella/" + id + ".json").toUri().toURL()));
    }

    @ParameterizedTest
    @CsvSource({ "wyvern_dragonette-001_20250101T072826_f3aa9cc0" })
    void testFetchWyvernMedia(String id) throws MalformedURLException {
        when(metadataRepository.save(any(FileMetadata.class))).thenAnswer(a -> a.getArgument(0, FileMetadata.class));
        assertNotNull(
                service.fetchStacMedia("wyvern", Path.of("src/test/resources/wyvern/" + id + ".json").toUri().toURL()));
    }

    @Test
    void testStacCatalogAndLinks() throws Exception {
        StacCatalog catalog = jackson.readValue(new File("src/test/resources/capella/catalog.json"), StacCatalog.class);
        assertNotNull(catalog);
        assertEquals(5, catalog.links().size());

        URL rootUrl = newURL("https://capella-open-data.s3.us-west-2.amazonaws.com/stac/catalog.json");

        StacLink rootLink = catalog.links().get(0);
        assertNotNull(rootLink);
        assertEquals(rootUrl, rootLink.absoluteHref(rootUrl));

        StacLink childLink = catalog.links().get(1);
        assertNotNull(childLink);
        assertEquals(newURL(
                "https://capella-open-data.s3.us-west-2.amazonaws.com/stac/capella-open-data-by-product-type/catalog.json"),
                childLink.absoluteHref(rootUrl));
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public AbstractOrgStacService service(StacMediaRepository repository) {
            return new AbstractOrgStacService(repository, "test", Set.of()) {

                @Override
                public String getName() {
                    return "test";
                }

                @Override
                protected String getStacLicenceUrl() {
                    return null;
                }

                @Override
                protected String getStacLicenceName() {
                    return null;
                }

                @Override
                protected LocalDate getDate(String itemHref) {
                    return null;
                }

                @Override
                protected boolean isStacItemIgnored(String itemHref) {
                    return false;
                }

                @Override
                protected String hiddenUploadCategory(String repoId) {
                    return "";
                }
            };
        }
    }
}
