package org.wikimedia.commons.donvip.spacemedia.service.s3;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Comparator;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.model.S3Object;

class S3ServiceTest {

    @Test
    void testGetFiles() {
        assertFalse(
                new S3Service().getFiles(Region.US_WEST_2, "umbra-open-data-catalog", null, S3Service.MEDIA_EXT,
                        Function.identity(), _ -> true, Comparator.comparing(S3Object::key)).isEmpty());
    }
}
