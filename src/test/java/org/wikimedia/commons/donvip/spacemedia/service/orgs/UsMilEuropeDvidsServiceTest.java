package org.wikimedia.commons.donvip.spacemedia.service.orgs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.wikimedia.commons.donvip.spacemedia.utils.Utils.newURL;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.FileMetadata;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsCredit;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsImage;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsLocation;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMedia;
import org.wikimedia.commons.donvip.spacemedia.data.domain.dvids.DvidsMediaRepository;
import org.wikimedia.commons.donvip.spacemedia.service.dvids.DvidsService;

@SpringJUnitConfig(UsMilEuropeDvidsServiceTest.TestConfig.class)
class UsMilEuropeDvidsServiceTest extends AbstractOrgServiceTest {

    @MockBean
    private DvidsMediaRepository<DvidsMedia> repository;

    @Autowired
    private UsMilEuropeDvidsService service;

    @Test
    void testGetWikiCode() {
        DvidsMedia media = new DvidsImage();
        media.setId(new CompositeMediaId("AAFES-HQ", "image:8584815"));
        media.setVirin("240812-D-DO482-1111");
        media.setCredit(List.of(new DvidsCredit(1040746, "Caleb Barrieau", "Master Sgt.",
                newURL("https://www.dvidshub.net/portfolio/1040746"))));
        media.setCreationDate(LocalDate.of(2024, 8, 12));
        media.setPublicationDate(LocalDate.of(2024, 8, 12));
        DvidsLocation location = new DvidsLocation();
        location.setCity("Sembach");
        location.setCountry("Germany");
        media.setLocation(location);
        media.setTitle(new LocalizedText(
                "Army & Air Force Exchange Service Welcomes New Commander for Europe, Southwest Asia, Africa"));
        media.setDescription(new LocalizedText(
                "The Army & Air Force Exchange Service has a new commander in the Europe/Southwest Asia/Africa region! Army Col. Everett “Bud” Lacroix, who has served 32 years in the military, joined the DoD’s largest retailer this summer and will support 4,000+ Exchange associates in more than 25 countries across three continents and 1,400 facilities. Welcome, Col. Lacroix! #familyservingfamily Read more: https://wp.me/p9Q7PG-2Kn."));
        FileMetadata fm = new FileMetadata("https://d34w7g4gy10iej.cloudfront.net/photos/2408/8584815.jpg");
        media.addMetadata(fm);

        Pair<String, List<LocalizedText>> pair = service.getWikiCode(media, fm);

        assertEquals(
                """
                        == {{int:filedesc}} ==
                        {{milim
                        | description = {{en|1=The Army & Air Force Exchange Service has a new commander in the Europe/Southwest Asia/Africa region! Army Col. Everett “Bud” Lacroix, who has served 32 years in the military, joined the DoD’s largest retailer this summer and will support 4,000+ Exchange associates in more than 25 countries across three continents and 1,400 facilities. Welcome, Col. Lacroix! #familyservingfamily Read more: https://publicaffairs-sme.com/ExchangePost/?p=10563.}}
                        | date = {{Taken on|2024-08-12|location=Germany}}
                        | source = {{ID-USMil |1=240812-D-DO482-1111 |2= Department of Defense|url= https://www.dvidshub.net/image/8584815}}
                        | author = Master Sgt. [https://www.dvidshub.net/portfolio/1040746 Caleb Barrieau]
                        | location = Sembach, Germany
                        | virin = 240812-D-DO482-1111
                        | dateposted = 2024-08-12
                        }}
                        =={{int:license-header}}==
                        {{PD-USGov-Military}}
                        """,
                pair.getLeft());
    }

    @Test
    void testUploadTitle() {
        DvidsMedia media = new DvidsImage();
        media.setId(new CompositeMediaId("PHCE", "image:7952363"));
        media.setVirin("240804-M-S1234-1001");
        media.setCreationDate(LocalDate.of(2023, 8, 4));
        media.setPublicationDate(LocalDate.of(2023, 8, 4));
        DvidsLocation location = new DvidsLocation();
        location.setCountry("Germany");
        media.setLocation(location);
        media.setTitle(new LocalizedText(
                "Staff Sgt. Lenee Williams, veterinary food inspector and noncommissioned officer in charge of the Stuttgart Veterinary Treatment Facility, earns Bachelor of Science with a focus on health care management from the Excelsior University in New York."));
        media.setDescription(new LocalizedText(
                "Staff Sgt. Lenee Williams, veterinary food inspector and noncommissioned officer in charge of the Stuttgart Veterinary Treatment Facility, earns Bachelor of Science with a focus on health care management from the Excelsior University in New York."));
        FileMetadata fm = new FileMetadata("https://d2z7p5gaic84ah.cloudfront.net/photos/2308/7952363.jpg");
        media.addMetadata(fm);

        String uploadTitle = media.getUploadTitle(fm, () -> service.getOrgName(media));
        assertTrue(uploadTitle.length() < 240, uploadTitle);
        assertEquals(
                "Staff Sgt_ Lenee Williams, veterinary food inspector and noncommissioned officer in charge of the Stuttgart Veterinary Treatment Facility, earns Bachelor of Science with a focus on health care management from the Excelsior U (7952363)",
                uploadTitle);
    }

    @Configuration
    @Import(DefaultOrgTestConfig.class)
    static class TestConfig {

        @Bean
        public DvidsService dvidsService() {
            return new DvidsService();
        }

        @Bean
        public UsMilEuropeDvidsService service(DvidsMediaRepository<DvidsMedia> repository,
                @Value("${usmileurope.dvids.units:*}") Set<String> dvidsUnits,
                @Value("${usmileurope.dvids.countries}") Set<String> dvidsCountries,
                @Value("${usmileurope.dvids.top.countries}") Set<String> dvidsTopCountries,
                @Value("${usmileurope.dvids.min.year}") int minYear,
                @Value("${usmileurope.dvids.blocklist}") boolean blocklist) {
            return new UsMilEuropeDvidsService(repository, dvidsUnits, dvidsCountries, dvidsTopCountries, minYear,
                    blocklist);
        }
    }
}
