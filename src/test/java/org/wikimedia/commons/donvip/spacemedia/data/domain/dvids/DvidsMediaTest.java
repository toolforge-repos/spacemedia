package org.wikimedia.commons.donvip.spacemedia.data.domain.dvids;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.CompositeMediaId;
import org.wikimedia.commons.donvip.spacemedia.data.domain.base.LocalizedText;

class DvidsMediaTest {

    @Test
    void testUploadTitle() {
        DvidsMedia media = new DvidsImage();
        media.setUnitName("Space Systems Command");

        media.setTitle(new LocalizedText("en", "9/11 2017"));
        media.setId(new CompositeMediaId("SSC", "image:3796505"));
        assertEquals("Space Systems Command 3796505", media.getUploadTitle(null, () -> ""));

        media.setTitle(new LocalizedText("en", "AF 70th Birthday at Angel game"));
        media.setId(new CompositeMediaId("SSC", "image:3793475"));
        assertEquals("AF 70th Birthday at Angel game (3793475)", media.getUploadTitle(null, () -> ""));

        media.setTitle(new LocalizedText("en",
                "http://www.patrick.af.mil/News/Article-Display/Article/1319994/air-force-senior-leaders-thank-team-patrick-cape-for-irma-recovery-efforts"));
        media.setId(new CompositeMediaId("SSC", "image:3809731"));
        assertEquals(
                "www_patrick_af_mil-News-Article-Display-Article-1319994-air-force-senior-leaders-thank-team-patrick-cape-for-irma-recovery-efforts (3809731)",
                media.getUploadTitle(null, () -> ""));
    }
}
