#!/bin/sh

DIR='/data/project/spacemedia'
if [ ! -d $DIR ] ; then
  DIR='.'
else
  export JAVA_HOME="$DIR/jdk-23"
fi

SRCDIR="$DIR/spacemedia"
if [ ! -d $SRCDIR ] ; then
  SRCDIR='.'
fi

# Load secrets and configuration
if [ -f $DIR/env.sh ] ; then
  . $DIR/env.sh
fi

updateFullresImages=true
searchEnabled=false

version=`cat $SRCDIR/target/version.txt`

apptype=$1
profile=$2

export PATH="$DIR:$DIR/libwebp-1.2.4-linux-x86-64/bin:/usr/local/bin:$PATH"

#if [ "$apptype" = "youtube" ] ; then
#  youtube-dl -U
#fi

if [ "$apptype" = "web" ] ; then
#  nthreads=2
  JAR="$SRCDIR/target/${apptype}/spacemedia-${version}.jar"
  LOG="$DIR/conf/logback-spring-toolforge.xml"
  if [ ! -f $LOG ] ; then
    LOG="$SRCDIR/target/classes/logback-spring-toolforge.xml"
  fi
  case $(hostname -s) in
    worker-[0-9])
      SPRING_PROFILES="cloudvps-starthashes"
      ;;
    *)
      SPRING_PROFILES="toolforge,web"
      ;;
  esac
  MODE="LOCAL"
else
#  nthreads=1
  JAR="$SRCDIR/target/job-${apptype}/spacemedia-${version}.jar"
  LOG=$DIR/conf/logback-spring-${profile}.xml
  if [ ! -f $LOG ] ; then
    cp $SRCDIR/target/classes/logback-spring-cloudvps.xml $LOG
    sed -i "s/spacemedia\\.log/${profile}.log/g" $LOG
  fi
  SPRING_PROFILES="job-${profile}"
  MODE="LOCAL"
  JAVA_OPTS="$JAVA_OPTS -XX:+UnlockDiagnosticVMOptions -XX:NativeMemoryTracking=summary -XX:+PrintNMTStatistics"
  rm -Rf $DIR/logs/*.gz
fi

dupes=190
dpla=50

killall -q java >> /dev/null 2>&1

rm -f ~/files/*
rm -f /tmp/spacemedia*
rm -f /tmp/sm*
rm -f /tmp*.mp4
rm -Rf /tmp/tomcat*
rm -f /tmp/MediaDataBox*
rm -f nohup.out

if [ -d /srv/tmp ] ; then
  rm -Rf /srv/tmp/*
  TMPDIR=/srv/tmp
else
  TMPDIR=/tmp
fi

$JAVA_HOME/bin/java -Xms$MEMORY -Xmx$MEMORY \
--add-opens java.base/java.lang=ALL-UNNAMED \
-Dserver.port=$PORT $JAVA_OPTS \
-Dexecution.mode=$MODE \
-Djava.io.tmpdir=$TMPDIR \
-Dsentry.stacktrace.app.packages=org.wikimedia.commons.donvip.spacemedia \
-Dsentry.stacktrace.hidecommon=False \
-Dspring.security.oauth2.client.provider.wikimedia.authorization-uri=https://meta.wikimedia.org/w/rest.php/oauth2/authorize \
-Dspring.security.oauth2.client.provider.wikimedia.token-uri=https://meta.wikimedia.org/w/rest.php/oauth2/access_token \
-Dspring.security.oauth2.client.provider.wikimedia.user-info-uri=https://meta.wikimedia.org/w/rest.php/oauth2/resource/profile \
-Dspring.security.oauth2.client.provider.wikimedia.user-info-authentication-method=header \
-Dspring.security.oauth2.client.provider.wikimedia.userNameAttribute=username \
-Dspring.security.oauth2.client.registration.wikimedia.authorizationGrantType=authorization_code \
-Dspring.security.oauth2.client.registration.wikimedia.redirectUri={baseUrl}/{action}/oauth2/code/{registrationId} \
-Dspring.security.oauth2.client.registration.wikimedia.client-id=$OAUTH2_WIKIMEDIA_CLIENT_ID \
-Dspring.security.oauth2.client.registration.wikimedia.client-secret=$OAUTH2_WIKIMEDIA_CLIENT_SECRET \
-Dspring.security.oauth2.client.registration.wikimedia.client-authentication-method=client_secret_post \
-Ddomain.datasource.url=jdbc:mariadb://tools.db.svc.wikimedia.cloud:3306/${TOOL_TOOLSDB_USER}__spacemedia \
-Ddomain.datasource.username=$TOOL_TOOLSDB_USER \
-Ddomain.datasource.password=$TOOL_TOOLSDB_PASSWORD \
-Dcommons.datasource.url=jdbc:mariadb://commonswiki.analytics.db.svc.wikimedia.cloud:3306/commonswiki_p \
-Dhashes.datasource.url=jdbc:mariadb://$TROVE_INSTANCE.svc.trove.eqiad1.wikimedia.cloud:3306/hash_associations \
-Dcommons.datasource.username=$TOOL_REPLICA_USER \
-Dcommons.datasource.password=$TOOL_REPLICA_PASSWORD \
-Dhashes.datasource.username=$HASHES_USER \
-Dhashes.datasource.password=$HASHES_PASSWORD \
-Dsearch.enabled=$searchEnabled \
-Dupdate.fullres.images=$updateFullresImages \
-Dcommons.duplicates.max.files=$dupes \
-Dcommons.dpla.max.duplicates=$dpla \
-Dlogging.config=$LOG \
-Dspring.jpa.show-sql=false \
-Dspring.jpa.properties.hibernate.format_sql=false \
-jar $JAR --spring.profiles.active=$SPRING_PROFILES \
$3

