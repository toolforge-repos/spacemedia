#!/bin/bash

./list-jobs.sh | grep run.sh | cut -d'*' -f4 | while read job
do
  echo $job
  $job
done
