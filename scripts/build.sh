#!/bin/bash

host=$(hostname -s)
case $host in

  worker-[0-9])
    cd /data/project/spacemedia
    crontab -r
    cd spacemedia && git pull
    export JAVA_HOME=/data/project/spacemedia/jdk-23
    ./mvnw -ntp -Pweb -Pjobs -Pversion clean package -Dspring.profiles.active=toolforge -DskipTests
    cronfile="src/main/resources/crontab-$host.txt"
    if [ -f $cronfile ] ; then
        crontab $cronfile
    fi
    cd -
    ;;

  tools-bastion-[0-9][0-9])
    toolforge build clean -y && \
    toolforge build start https://gitlab.wikimedia.org/toolforge-repos/spacemedia --ref main \
                    -e MAVEN_CUSTOM_OPTS='-Pweb -Pversion -ntp -DskipTests -Dspring.profiles.active=toolforge'
    ;;

  *)
    pack build --builder tools-harbor.wmcloud.org/toolforge/heroku-builder:22 myimage \
               --env MAVEN_CUSTOM_OPTS='-Pweb -Pversion -ntp -DskipTests -Dspring.profiles.active=toolforge'
    ;;

esac
